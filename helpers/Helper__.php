<?php

namespace app\helpers;

use app\models\Activities;
use app\models\PropertyCategory;
use app\models\PropertyLocationType;
use app\models\PropertyPurpose;
use app\models\Settings;
use app\models\User;
use app\modules\rbac\models\AuthAssignment;
use DateTime;
use Yii;
use yii\base\Exception;

/**
 * Created by PhpStorm.
 * User: Waqar
 * Date: 10/28/2017
 * Time: 2:35 PM
 */


class Helper
{


    public static function getPropertyPurpose() {

        $options = [];
        $model = PropertyPurpose::find()->where(['active'=>1])->orderBy('sort_order')->all();
        foreach ($model as $mod)
        {
            $options[$mod->name] = $mod->name;
        }

        return $options;
    }

    public  static function getPropertyUnit()
    {
        $options = [];

        $model = Settings::findByName('PROPERTY_LAND_AREA_UNIT');

        $json_decode  = json_decode($model->config_item_value, true);
        $array = $json_decode['property_land_area_unit'];
        $res = array();
        foreach ($array as $each) {

            $options[$each['unit']] = $each['name'];

        }

        return $options;

    }

    public  static function getPropertyType()
    {


        $options = [];
        $model = PropertyCategory::find()->where(['active'=>1])->orderBy('sort_order')->all();
        foreach ($model as $mod)
        {
            $options[$mod->name] = $mod->name;

        }

        return $options;


    }

    public  static function getPropertyLocationType()
    {


        $options = [];
        $model = PropertyLocationType::find()->where(['active'=>1])->orderBy('sort_order')->all();
        foreach ($model as $mod)
        {
            $options[$mod->name] = $mod->name;

        }

        return $options;


    }


    


    public static function sendSMS($phone,$message)
    {
        $developer = 'live';
		$url ='';
        if($developer=='live'){


            $url = 'http://pk.eocean.us/APIManagement/API/RequestAPI?user=winwin&pwd=AAbbqFE8NlLCpE7a8ttT2eBZjXE2N6fPzKWimgLqyHzQerIiNpeVjGhTTLotHheP6g%3d%3d&sender=WIN%20WIN&reciever='.urlencode($phone).'&msg-data='.urlencode($message) .'&response=string';
			$url = 'http://pk.eocean.us/APIManagement/API/RequestAPI?user=winwin&pwd=ADrEZPtJ1W3cI9U2s1cEZ2V2JspXHUa4MiuAZ5nF9pMqhHpikaaD1%2fuSWFeAvrbPqQ%3d%3d&sender='.urlencode("WIN WIN").'&reciever='.urlencode($phone).'&msg-data='.urlencode($message) .'&response=string';
			

        }else
        {
            $url = 'http://pk.eocean.us/APIManagement/API/RequestAPI?user=maaliksoft&pwd=AIsWz8wKQIN7tw5%2fI7H2l%2bb7%2fPsFtZ848AOXCa3ICssl33XSK%2bw%2f5f%2f05i4e2H55Eg%3d%3d&sender=MaalikSoft&reciever='.urlencode($phone).'&msg-data='.urlencode($message).'&response=string';
		'http://pk.eocean.us/APIManagement/API/RequestAPI?user=maaliksoft&pwd=AJaWHOtvb5ufEfw815iWfNP5UaQSyGVc6Gyi16HLCJMcSVSs%2bujjEHtZfHlhQ3THOQ%3d%3d&sender=MaalikSoft&reciever='.urlencode($phone).'&msg-data='.urlencode($message) .'&response=string';
        }

        $cSession = curl_init();
        curl_setopt($cSession, CURLOPT_URL, $url);
        curl_setopt($cSession, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cSession, CURLOPT_HEADER, false);
        $result = curl_exec($cSession);
        curl_close($cSession);
		
        if (strpos($result, 'Message accepted for delivery') !== false) {
            $status = 'sent';
            return true;
			

        }
        else if(strpos($result, 'API Execute Successfully') !== false)
        {
            $status = 'sent';
            return true;
			
        }
        else{
            $status = 'failure';
            return true;
			
        }

    }


    public static function sendEmail($email,$emailmessage,$emailsubject)
    {
		echo $emailmessage;
        open connection
        $ch = curl_init();
		//set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL,'https://winwinassociate.com/email-send/send.php');
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, "email=".$email."&message=".$emailmessage."&subject=".$emailsubject."");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		execute post
        $result = curl_exec($ch);
        
        //close connection
        curl_close($ch);
        // Further processing ...
        if ($result == "OK")
        {
            return true;
        }
        else
        {
        }
    }


    public  static function getSize($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }



    public static function DateTime($dateTime)
    {
        $phpdate = strtotime( $dateTime );
        $date = date( 'd/m/Y', $phpdate );
        $time = date('h:i A',$phpdate);

        return $date.'<br>'.$time;
    }

    public static function getCreated($id)
    {
        $user = User::findOne($id);

        return  $user->username;
    }




    public static function getBaseUrl()
    {
        $url = Yii::$app->homeUrl;
        $str  = str_replace("/web","",$url);

        return $str;
    }


    public static function DatTim($dateTime)
    {
        $phpdate = strtotime($dateTime);
        $date = date('d/m/Y h:i A', $phpdate);


        return $date;
    }

    public static function getMySql($dateTime)
    {
        $phpdate = strtotime($dateTime);
        $date = date('d/m/Y h:i A', $phpdate);


        return $date;
    }

    public static function getUser($id)
    {
        $user = User::findOne($id);
        return $user->username;
    }



    //Activity Saved 


    public static function setCommit($commit='',$task='',$lead='')
    {
        $activity = new Activities();
        $activity->created_on = date("Y-m-d H:i:s");
        $activity->created_by = Yii::$app->user->id;
        $activity->activity = $commit;
        $activity->task_id = $task;
        $activity->lead_id = $lead;
        $activity->save();

        return true;


    }


    public static function assignLead()
    {
        $user = User::find()->all();
        $list = array();
        foreach ($user as $key=>$data)
        {
            if (\Yii::$app->authManager->checkAccess($data->id, 'WorkingOnLeads')) {

                $list[$data->id] = $data->first_name . ' '. $data->last_name;
			}
		}
        return $list;
    }
	
    public static function assignLead1()
    {
        $roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
         if($roles['Admin']->name=='Admin' || $roles['SuperAdmin']->name=='SuperAdmin')
        {
        $user = User::find()->all();
        }
        else
        {
            $user = User::find()->where(['id'=>Yii::$app->user->id])->all();
        }
        $list = array();
        foreach ($user as $key=>$data)
        {
            if (\Yii::$app->authManager->checkAccess($data->id, 'WorkingOnLeads')) {

                $list[$data->id] = $data->first_name . ' '. $data->last_name;


            }


        }


        return $list;
    }


    public static function assignLead2()
    {
        $roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
         if($roles['Admin']->name=='Admin' || $roles['SuperAdmin']->name=='SuperAdmin')
        {
        $user = User::find()->all();
        }
        else
        {
            $user = User::find()->all();//User::find()->where(['id'=>Yii::$app->user->id])->all();
        }
        $list = array();
        foreach ($user as $key=>$data)
        {
            if (\Yii::$app->authManager->checkAccess($data->id, 'WorkingOnLeads')) {

                $list[$data->id] = $data->first_name . ' '. $data->last_name;


            }


        }


        return $list;
    }

    public static function getAssignLeadUser($set)
    {
        $myString = $set;
        $myArray = explode(',', $myString);
        $name = '';
        if(!empty($myArray))
        {
            foreach ($myArray as $key)
            {

                $user = User::findOne($key);
                $name .= $user->first_name.' '.$user->last_name.'<br>';

            }
        }


        return $name;


    }




    public static function showTo()
    {
        $user = User::find()->all();
        $list = array();
        foreach ($user as $key=>$data)
        {
            if (\Yii::$app->authManager->checkAccess($data->id, 'ContactsShow')) {

                $list[$data->id] = $data->first_name . ' '. $data->last_name;


            }


        }


        return $list;
    }

}
?>