<?php

namespace app\models;

use app\modules\rbac\models\AuthAssignment;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;
/**
 * UserSearch represents the model behind the search form of `app\models\user`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['username','first_name','last_name', 'auth_key', 'password_hash', 'password_reset_token', 'email','created_on', 'updated_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        if(Yii::$app->user->can('SuperAdmin'))
        {
            $query = User::find();

        }else {

            $auth_item = AuthAssignment::find()->where([
                'or',
                ['item_name'=>'SuperAdmin'],
                ['item_name'=>'Admin'],
             ])->all();
            $user_array  = [];
            foreach ($auth_item as $key=>$data)
            {

                $user_array[] = $data->user_id;
            }
            $query = User::find()->where(['NOT IN','id',$user_array]);

        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
