<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tasks".
 *
 * @property int $id
 * @property string $task_id
 * @property string $task_name
 * @property string $task_description
 * @property string $task_status
 * @property string $task_priority
 * @property int $user_assigned_id
 * @property string $task_progress
 * @property int $expected_start_datetime
 * @property int $expected_end_datetime
 * @property int $actual_start_datetime
 * @property int $actual_end_datetime
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 */
class Tasks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tasks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['task_name', 'task_status', 'task_priority', 'user_assigned_id', 'created_on', 'created_by'], 'required'],
            [['task_description'], 'string'],
            [['user_assigned_id', 'created_by', 'updated_by'], 'integer'],
            [['expected_start_datetime', 'expected_end_datetime', 'actual_start_datetime', 'actual_end_datetime'], 'safe'],

            [['created_on', 'updated_on'], 'safe'],
            [['task_id', 'task_status', 'task_priority'], 'string', 'max' => 50],
            [['task_name', 'task_progress'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task_id' => 'Task ID',
            'task_name' => 'Task Name',
            'task_description' => 'Task Description',
            'task_status' => 'Task Status',
            'task_priority' => 'Task Priority',
            'user_assigned_id' => 'Assigned User',
            'task_progress' => 'Task Progress',
            'expected_start_datetime' => 'Expected Start Datetime',
            'expected_end_datetime' => 'Expected End Datetime',
            'actual_start_datetime' => 'Actual Start Datetime',
            'actual_end_datetime' => 'Actual End Datetime',
            'created_on' => 'Added On',
            'created_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }
}
