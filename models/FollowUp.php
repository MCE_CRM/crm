<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "follow_up".
 *
 * @property int $id
 * @property int $lead_id
 * @property string $follow_date_time
 * @property string $note
 * @property string $status
 * @property  int status_updated_by
 * @property  string status_updated_on
 * @property string $created_on 
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 */
class FollowUp extends \yii\db\ActiveRecord
{
	public $new_note;
	public $new_status;
	public $date_range;
	public $from;
    public $to;
    public $new_follow_date_time;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'follow_up';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lead_id', 'follow_date_time', 'note', 'created_on', 'created_by'], 'required'],
            [['lead_id', 'created_by','status_updated_by','updated_by'], 'integer'],
            [['follow_date_time', 'created_on', 'updated_on','status_updated_on'], 'safe'],
            [['note'], 'string', 'max' => 1000],
            [['status'], 'string', 'max' => 100],
        ];
    }
 
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lead_id' => 'Lead ID',
            'follow_date_time' => 'Date Time',
            'note' => 'Note',
            'status'=>'Status',
            'status_updated_by'=>'Status Update By',
            'status_updated_on' => 'Status Updated At',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }

    public function getLead()
    {
        return $this->hasOne(Leads::className(), ['id' => 'lead_id']);
    }
}
