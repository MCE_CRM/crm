<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Property;

/**
 * PropertySearch represents the model behind the search form of `app\models\Property`.
 */
class PropertySearch extends Property
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'project_id', 'created_by', 'updated_by','blocksector'], 'integer'],
            [['purpose','status','total_price','extra_charges','member_ship_no','property_type','property_location_type', 'property_title', 'description', 'price', 'price_per', 'land_area_unit', 'land_area_unit_name', 'created_on', 'updated_on','dimension','demand_price','property_assign','statusdesc'], 'safe'],
            [['land_area'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function searchByProject($params,$id)
    {
        $query = Property::find()->where(['project_id'=>$id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'project_id' => $this->project_id,
            'land_area' => $this->land_area,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'purpose', $this->purpose])
            ->andFilterWhere(['like', 'property_type', $this->property_type])
			 ->andFilterWhere(['like', 'property_location_type', $this->property_location_type])
            ->andFilterWhere(['like', 'property_title', $this->property_title])
			 ->andFilterWhere(['like', 'member_ship_no', $this->member_ship_no])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'price_per', $this->price_per])
            ->andFilterWhere(['like', 'land_area_unit', $this->land_area_unit])
            ->andFilterWhere(['like', 'land_area_unit_name', $this->land_area_unit_name]);

        return $dataProvider;
    }
    public function search($params)
    {
        
        $query = Property::find()->orderBy('id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		//$model2 = Property::find()->Where(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,property_assign)'))->addParams([':cat_to_find' => Yii::$app->user->id])->all();
		$userroles = $this->getUserRoles(Yii::$app->user->id);
		if(!empty($this->property_assign))
		{
			$property_assign = User::find()->select('id')->where(['like','first_name', $this->property_assign])->one();
			$property_assign = $property_assign['id'];
			
		}
		if($userroles!='Admin' && $userroles!='SuperAdmin')
		{
			$query->andWhere(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,property_assign)'))->addParams([':cat_to_find' => Yii::$app->user->id])->orWhere(['is','property_assign', NULL])->all();
		}
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'project_id' => $this->project_id,
            'blocksector' => $this->blocksector,
            'land_area' => $this->land_area,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
            'status'=>$this->status,

        ]);

        $query->andFilterWhere(['like', 'purpose', $this->purpose])
            ->andFilterWhere(['like', 'property_type', $this->property_type])
			 ->andFilterWhere(['like', 'property_location_type', $this->property_location_type])
            ->andFilterWhere(['like', 'property_title', $this->property_title])
			 ->andFilterWhere(['like', 'member_ship_no', $this->member_ship_no])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'price', $this->price])
			->andFilterWhere(['like', 'demand_price', $this->demand_price])
			->andFilterWhere(['like', 'property_assign', $property_assign])
			->andFilterWhere(['like', 'price_per', $this->price_per])
            ->andFilterWhere(['like', 'land_area_unit', $this->land_area_unit])
            ->andFilterWhere(['like', 'land_area_unit_name', $this->land_area_unit_name])
            ->andFilterWhere(['like', 'dimension', $this->dimension])
            ->andFilterWhere(['like', 'statusdesc', $this->statusdesc]);

        return $dataProvider;
    }
	function getUserRoles($user_id){
		$connection = \Yii::$app->db;
		$sql="select auth_item.* from auth_item,auth_assignment where auth_item.type=1 and auth_assignment.user_id=$user_id and auth_assignment.item_name=auth_item.name";
		$command=$connection->createCommand($sql);
		$dataReader=$command->queryAll();
		$roles ='';
		if(count($dataReader) > 0){
			foreach($dataReader as $role){
				
				if($role['name']=='Admin' || $role['name']=='SuperAdmin')
				{
					return $role['name'];
					break;
				}
				
			}
		}
	}
}
