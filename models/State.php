<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "state".
 *
 * @property int $id
 * @property string $state
 * @property string $state_code
 * @property int $active
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 * @property int $country_id
 */
class State extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'state';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state', 'active', 'created_on', 'created_by', 'country_id'], 'required'],
            [['active', 'created_by', 'updated_by', 'country_id'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['state', 'state_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc} 
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'state' => 'State',
            'state_code' => 'State Code',
            'active' => 'Active',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            'country_id' => 'Country',
        ];
    }

    public function getCounty()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

   
}
