<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "property".
 *
 * @property int $id
 * @property int $project_id
 * @property int $blocksector
 * @property string $purpose
 * @property string $property_type
  * @property string $property_location_type
 * @property string $property_title
 * @property string $member_ship_no
 * @property string $short_description
 * @property string $dimension
 * @property string $description
 * @property string $price
 * @property double $price_per
 * @property int $extra_charges
 * @property int $total_price
 * @property double $land_area
 * @property string $land_area_unit
 * @property string $land_area_unit_name
 * @property string $additional_form
 * @property string $status
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 */
class Property extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'extra_charges', 'total_price','created_by', 'updated_by','blocksector'], 'integer'],
            [['purpose', 'property_type', 'property_title','land_area','price','description', 'price', 'total_price', 'land_area', 'land_area_unit', 'created_on', 'created_by'], 'required'],
            [['price_per', 'land_area'], 'number'],
            [['created_on', 'updated_on'], 'safe'],
            [['purpose'], 'string', 'max' => 15],
            [['address'], 'string', 'max' => 1000],
            [['status'], 'string', 'max' => 100],
            [['property_type', 'property_title','property_location_type','dimension','length','width'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 10000],
            [['price','demand_price', 'land_area_unit'], 'string', 'max' => 20],
            [['land_area_unit_name'], 'string', 'max' => 30],
            [['additional_form'], 'string', 'max' => 255],
			 [['member_ship_no'], 'string', 'max' => 100],
			  [['short_description'], 'string', 'max' => 5000],
            /*[['country','address'], 'required', 'when' => function($model){
               return $model->project_id =='';
            }],*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project',
            'blocksector'=>'Block/Sector',
            'country'=>'Country',
            'city'=>'City',
            'state'=>'State',
            'address'=>'Address',
            'purpose' => 'Purpose',
            'property_type' => 'Type',
			'property_location_type' => 'Property Location',
            'property_title' => 'Title',
            'width'=>'W',
            'length'=>'H',
			'member_ship_no'=> 'Plot/Member/House#',
			'short_description' => 'Short Description',
            'description' => 'Description',
            'price' => 'Base Price',
            'price_per' => 'Price Per',
            'extra_charges' => 'Extra Charges',
            'total_price' => 'Total Price',
            'land_area' => 'Land Area',
            'land_area_unit' => 'Land Area Unit',
            'land_area_unit_name' => 'Land Area Unit Name',
            'status'=>'Status',
            'property_assign'=>'Property Show',
            'additional_form'=>'additional_form',
            'created_on' => 'Added On',
            'created_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }



    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);

    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);

    }

     public function getBlock()
    {
        return $this->hasOne(Blocksector::className(), ['id' => 'blocksector']);

    }
}
