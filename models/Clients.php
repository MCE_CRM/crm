<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $mobile
 * @property int $country
 * @property int $state
 * @property int $city
 * @property int $zipcode
 * @property int $project_id
 * @property int $property_id
 * @property int $blocksector
 * @property string show_to
 * @property string $address
 * @property string $LeadType
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'mobile', 'country', 'created_on', 'created_by'], 'required'],
            [['country', 'state', 'city', 'created_by', 'updated_by','project_id','property_id','blocksector'], 'integer'],
            [['created_on', 'updated_on','LeadType'], 'safe'],
            [['name', 'address'], 'string', 'max' => 255],
            [['email', 'phone', 'mobile'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 3000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'mobile' => 'Mobile',
            'country' => 'Country',
            'state' => 'State',
            'city' => 'City',
            'address' => 'Address',
            'show_to'=>'Show',
            'project_id'=>'Project',
            'property_id'=>'Property',
            'blocksector'=>'Blocksector',
            'description'=>'Description',
            'created_on' => 'Added On',
            'created_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);

    }

    public function getStat()
    {
        return $this->hasOne(State::className(), ['id' => 'state']);
    }

    public function getCit()
    {
        return $this->hasOne(City::className(), ['id' => 'city']);
    }

    public function getCountr()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }

    public function getpropert()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }

}
