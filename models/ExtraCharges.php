<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "extra_charges".
 *
 * @property int $id
 * @property string $name
 * @property int $charges
 * @property int $property_id
 * @property int feature_form_id
 * @property int project_id
 * @property int $type
 * @property string $value
 * @property string $feature_type
 */
class ExtraCharges extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'extra_charges';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','type'], 'required'],
            [['charges', 'property_id','project_id','feature_form_id','type'], 'integer'],
            [['name', 'value'], 'string', 'max' => 255],
            [['feature_type'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'charges' => 'Charges',
            'property_id' => 'Property ID',
            'type' => 'Type',
            'value' => 'Value',
            'feature_type' => 'Feature Type',
        ];
    }
}
