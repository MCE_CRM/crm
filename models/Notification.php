<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property int $id
 * @property string $message
 * @property string $created_on
 * @property int $created_by
 * @property int $status
 * @property int $user_id
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message', 'created_on'], 'required'],
            [['message'], 'string'],
            [['created_on'], 'safe'],
            [['created_by', 'status', 'user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => 'Message',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'status' => 'Status',
            'user_id' => 'User ID',
        ];
    }
}
