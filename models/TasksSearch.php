<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tasks;

/**
 * TasksSearch represents the model behind the search form of `app\models\Tasks`.
 */
class TasksSearch extends Tasks
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_assigned_id', 'expected_start_datetime', 'expected_end_datetime', 'actual_start_datetime', 'actual_end_datetime', 'created_by', 'updated_by'], 'integer'],
            [['task_id', 'task_name', 'task_description', 'task_status', 'task_priority', 'task_progress', 'created_on', 'updated_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tasks::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_assigned_id' => $this->user_assigned_id,
            'expected_start_datetime' => $this->expected_start_datetime,
            'expected_end_datetime' => $this->expected_end_datetime,
            'actual_start_datetime' => $this->actual_start_datetime,
            'actual_end_datetime' => $this->actual_end_datetime,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'task_id', $this->task_id])
            ->andFilterWhere(['like', 'task_name', $this->task_name])
            ->andFilterWhere(['like', 'task_description', $this->task_description])
            ->andFilterWhere(['like', 'task_status', $this->task_status])
            ->andFilterWhere(['like', 'task_priority', $this->task_priority])
            ->andFilterWhere(['like', 'task_progress', $this->task_progress]);

        return $dataProvider;
    }
}
