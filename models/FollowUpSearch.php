<?php

namespace app\models;

use DateTime;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FollowUp;

/**
 * FollowUpSearch represents the model behind the search form of `app\models\FollowUp`.
 */
class FollowUpSearch extends FollowUp
{
    public $lead_assign;
    /**
     * {@inheritdoc} 
     */
    public function rules()
    {
        return [
            [['id', 'lead_id', 'task_id', 'created_by', 'updated_by','lead_assign'], 'integer'],
            [['follow_date_time', 'note','status', 'created_on', 'updated_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FollowUp::find()->orderBy([
            'lead_id' => SORT_DESC,
            'created_on'=>SORT_DESC,
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
            // this $params['pagesize'] is an id of dropdown list that we set in view file
            'pagesize' => (isset($params['pagesize']) ? $params['pagesize'] :  '20')]
        ]);

        $this->load($params);

        $query->joinWith('lead');
         $roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
       /*  echo"<pre>";
         echo print_r($roles);
         echo"</pre>";exit;*/
         if($roles['Admin']->name=='Admin' || $roles['SuperAdmin']->name=='SuperAdmin')
        {

        }
        else
        {
            if(Yii::$app->user->can('follow_up_user_search')){
            }else
            {
                $query->where(['lead_assign'=>Yii::$app->user->id]);
            }
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
        if ( ! is_null($this->follow_date_time) && strpos($this->follow_date_time, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->follow_date_time);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'follow_date_time', $start_date, $end_date]);



        }


        //this work for created on search filter
         if ( ! is_null($this->created_on) && strpos($this->created_on, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->created_on);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'follow_up.created_on', $start_date, $end_date]);



        }


        $query->andFilterWhere(['leads.lead_assign' => $this->lead_assign]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lead_id' => $this->lead_id,
            'task_id' => $this->task_id,
            /*'created_on' => $this->created_on,*/
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'note', $this->note]);
        $query->andFilterWhere(['like', 'status', $this->status]);
        return $dataProvider;
    }
	
	public function searchuserwise($params)
    {
        $query = FollowUp::find()->orderBy([
            'lead_id' => SORT_DESC,
            'created_on'=>SORT_DESC,
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
            // this $params['pagesize'] is an id of dropdown list that we set in view file
            'pagesize' => (isset($params['pagesize']) ? $params['pagesize'] :  '20')]
        ]);

        $this->load($params);

        $query->joinWith('lead');
         $roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
       /*  echo"<pre>";
         echo print_r($roles);
         echo"</pre>";exit;*/
        if($roles['Admin']->name=='Admin' || $roles['SuperAdmin']->name=='SuperAdmin')
        {

        }
        else
        {
            if(Yii::$app->user->can('user_search_on_follow_up_report')){
                
            }else
            {
                $query->where(['lead_assign'=>Yii::$app->user->id]);
            }
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
        if (!empty($_GET['FollowUpSearch']['from'])) {
			$fromdate = $_GET['FollowUpSearch']['from'];
			$from = DateTime::createFromFormat('d/m/Y',$fromdate)->format('Y-m-d').' 00:00:00';
			$query->andFilterWhere(['>=', 'follow_up.created_on', $from]);
		}else
		{
			//$from = date('Y-m-d',strtotime('-15 days'));
			//$query->andFilterWhere(['>', 'follow_up.created_on', $from]);
		}
		
		if (!empty($_GET['FollowUpSearch']['to'])) {
			$todate = $_GET['FollowUpSearch']['to'];
			$to = DateTime::createFromFormat('d/m/Y',$todate)->format('Y-m-d').' 23:59:59';
			$query->andFilterWhere(['<=', 'follow_up.created_on', $to]);
		} else
		{
			//$to = date('Y-m-d');
			//$query->andFilterWhere(['like', 'follow_up.created_on', $to]);
		}
		
		/*if (empty($_GET['FollowUpSearch']['from']) && empty($_GET['FollowUpSearch']['to'])) {
			$from = date('Y-m-d',strtotime('-15 days')).' 00:00:00';
			$to = date('Y-m-d').' 23:59:59';
			 $query->andFilterWhere(['between', 'follow_up.created_on', $from, $to]);
		}*/
		if ( ! is_null($this->follow_date_time) && strpos($this->follow_date_time, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->follow_date_time);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'follow_date_time', $start_date, $end_date]);



        }
		if(!empty($_GET['FollowUpSearch']['created_by']) && isset($_GET['FollowUpSearch']['created_by']))
		{
            
			 $query->andFilterWhere(['=', 'leads.lead_assign', $_GET['FollowUpSearch']['created_by']]);
			
		}
       // echo $query->createCommand()->getRawSql();exit;
        //$query->andFilterWhere(['leads.lead_assign' => $this->lead_assign]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lead_id' => $this->lead_id,
            'task_id' => $this->task_id,
            /*'created_on' => $this->created_on,*/
            //'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'note', $this->note]);
        $query->andFilterWhere(['like', 'status', $this->status]);
		//echo $query->createCommand()->getRawSql();exit;
        return $dataProvider;
    }

    public function searchByLead($params,$id='')
    {
        $query = FollowUp::find()->where(['lead_id'=>$id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lead_id' => $this->lead_id,
            'task_id' => $this->task_id,
            'follow_date_time' => $this->follow_date_time,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'note', $this->note]);
        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
