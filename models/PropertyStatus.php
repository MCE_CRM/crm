<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "property_status".
 *
 * @property int $id
 * @property string $status
 * @property string $label
 * @property int $active
 * @property int $sort_order
 * @property int $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by

 */
class PropertyStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'active'], 'required'],
            [['active', 'sort_order', 'created_by', 'updated_by'], 'integer'],
            [['updated_on'], 'safe'],
            [['status'], 'unique'],
            [['status'], 'string', 'max' => 255],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'active' => 'Active',
            'sort_order' => 'Sort Order',
            'created_on' => 'Added On',
            'created_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);

    }
}