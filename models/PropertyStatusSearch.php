<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PropertyStatus;

/**
 * LeadStatusSearch represents the model behind the search form of `app\models\leadstatus`.
 */
class PropertyStatusSearch extends \app\models\PropertyStatus
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'active', 'sort_order', 'created_by', 'updated_by'], 'integer'],
            [['status','created_on','updated_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {


        if(empty($_GET['sort'])){
            $query = \app\models\PropertyStatus::find()->orderBy('sort_order');
        }else{
            $query = \app\models\PropertyStatus::find();
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->created_on)
        {
            $mysql_date = $this->created_on;
            // date in Y-m-d format as MySQL stores it
            $date_obj = date_create_from_format('d/m/Y',$mysql_date);
            $created_on = date_format($date_obj, 'Y-m-d');

           


        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'active' => $this->active,
            'sort_order' => $this->sort_order,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);
        $query->andFilterWhere(['like', 'created_on', $created_on]);

        $query->andFilterWhere(['like', 'status', $this->status]);


        
        

        return $dataProvider;
    }
}
