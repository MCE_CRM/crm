<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "leads".
 *
 * @property int $id
 * @property string $lead_title
 * @property string $lead_description
 * @property string $lead_type
 * @property string $lead_source
 * @property string $lead_status
 * @property string $lead_assign
 * @property string $lead_response
 * @property string $follow_up
 * @property int $project_id
 * @property int $property_id
 * @property int $block_sector
 * @property string $contact_name
 * @property string $contact_phone
 * @property string $contact_mobile
 * @property string $contact_email
 * @property string $contact_country
 * @property string $contact_state
 * @property string $contact_city
 * @property string $contact_address
 * @property string $contact_address2
 * @property string $created_on
 * @property string $sms_status
 * @property int $created_by
 * @property int $assign_by
 * @property string $updated_on
 * @property int $updated_by
 */
class Leads extends \yii\db\ActiveRecord
{

    public $follow_date_time;
    public $note;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'leads';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lead_type', 'lead_source', 'lead_status','lead_response','contact_name', 'contact_mobile', 'contact_country', 'contact_state', 'contact_city'], 'required'],
            [['id', 'project_id', 'property_id','active','created_by','assign_by','updated_by','block_sector'], 'integer'],
            [['follow_up','lead_assign', 'created_on', 'updated_on'], 'safe'],
            [['lead_title','sms_status'], 'string', 'max' => 255],
            [['lead_description'], 'string', 'max' => 5000],
            [['lead_type', 'lead_source', 'lead_status', 'contact_name', 'contact_mobile', 'contact_email'], 'string', 'max' => 100],
            [['contact_phone'], 'string', 'max' => 50],
            [['contact_country', 'contact_state'], 'string', 'max' => 15],
            [['contact_city'], 'string', 'max' => 30],
            [['contact_address', 'contact_address2'], 'string', 'max' => 1000],
            [['id'], 'unique'],

        ];
    }

    /**
     * {@inheritdoc}
     */ 
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lead_title' => 'Name',
            'lead_description' => 'Lead Description',
            'lead_type' => 'Purpose',
            'lead_source' => 'Lead Source',
            'lead_status' => 'Lead Status',
            'lead_assign' => 'Assigned',
            'follow_up' => 'Follow Up',
            'project_id' => 'Project',
            'property_id' => 'Property',
            'contact_name' => 'Name',
            'contact_phone' => 'Phone',
            'contact_mobile' => 'Mobile',
            'contact_email' => 'Email',
            'contact_country' => 'Country',
            'contact_state' => 'State',
            'contact_city' => 'City',
            'block_sector' => 'Block/Sector',
            'contact_address' => 'Address',
            'contact_address2' => 'Address 2',
            'created_on' => 'Added On',
            'created_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }


    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);

    }
     public function getCit()
    {
        return $this->hasOne(City::className(), ['id' => 'contact_city']);
    }
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'contact_country']);
    }
}
