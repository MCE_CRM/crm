<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "property_location_type".
 *
 * @property int $id
 * @property string $name
 * @property int $sort_order
 * @property int $active
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 */
class PropertyLocationType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_location_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'sort_order', 'active', 'created_on', 'created_by'], 'required'],
            [['sort_order', 'active', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'unique'],
            [['created_on', 'updated_on'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'sort_order' => 'Sort Order',
            'active' => 'Active',
            'created_on' => 'Added On',
            'created_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);

    }
}
