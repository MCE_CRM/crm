<?php

use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<style>

    .control-label{
        font-size: 0.9rem !important;
        color: black;
    }
</style>


<div class="user-form">

    <?php

    $form = ActiveForm::begin([
        'id' => 'form',
        'enableAjaxValidation' => true,
        'validationUrl' => ($model->isNewRecord) ?Yii::$app->homeUrl.'user/validate':Yii::$app->homeUrl.'user/validate?id='.$model->id.'',
        'errorCssClass' => 'has-danger',
    ]);
    ?>


    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
        </div>
        <?php
        if ($model->isNewRecord) { ?>
        <div class="col-md-6">
            <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>
        </div>

        <?php } else { ?>
        <div class="col-md-6">

            <?= $form->field($model, 'phone_no')->textInput(['maxlength' => true]) ?>
        </div>

        <?php } ?>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList(['1'=>'Active','0'=>'InActive', ]) ?>
        </div>
    </div>
    <?php if ($model->isNewRecord) {?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'phone_no')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">

<?php
             if(Yii::$app->user->can('user/agent-create') && Yii::$app->user->can('user/csr-create') && Yii::$app->user->can('user/teamlead-create'))
              {
                echo $form->field($model, 'role')->widget(Select2::classname(), [
                    'data' =>  ArrayHelper::map(\app\modules\rbac\models\AuthItem::find()->where(['type'=>1])->andWhere(['name'=>'CSR'])->orWhere(['name'=>'Agent'])->orWhere(['name'=>'TeamLeader'])->all(), 'name', 'name'),
                    'options' => ['placeholder' => 'Select a Role ...'],
                    'size' => Select2::SMALL,
                    'hideSearch' => true,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                       // 'multiple' => true,
                    ],
                ]);
              }
              elseif(Yii::$app->user->can('user/agent-create'))
               {
              
                echo $form->field($model, 'role')->widget(Select2::classname(), [
                    'data' =>  ArrayHelper::map(\app\modules\rbac\models\AuthItem::find()->where(['type'=>1])->andWhere(['name'=>'Agent'])->all(), 'name', 'name'),
                    'options' => ['placeholder' => 'Select a Role ...'],
                    'size' => Select2::SMALL,
                    'hideSearch' => true,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                       // 'multiple' => true,
                    ],
                ]);
              }

              elseif(Yii::$app->user->can('user/teamlead-create'))
               {
              
                echo $form->field($model, 'role')->widget(Select2::classname(), [
                    'data' =>  ArrayHelper::map(\app\modules\rbac\models\AuthItem::find()->where(['type'=>1])->andWhere(['name'=>'TeamLeader'])->all(), 'name', 'name'),
                    'options' => ['placeholder' => 'Select a Role ...'],
                    'size' => Select2::SMALL,
                    'hideSearch' => true,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                       // 'multiple' => true,
                    ],
                ]);
              }
              elseif(Yii::$app->user->can('user/csr-create'))
               {
              
                echo $form->field($model, 'role')->widget(Select2::classname(), [
                    'data' =>  ArrayHelper::map(\app\modules\rbac\models\AuthItem::find()->where(['type'=>1])->andWhere(['name'=>'CSR'])->all(), 'name', 'name'),
                    'options' => ['placeholder' => 'Select a Role ...'],
                    'size' => Select2::SMALL,
                    'hideSearch' => true,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                       // 'multiple' => true,
                    ],
                ]);
              }
              
             elseif(Yii::$app->user->can('user/create') && Yii::$app->user->can('user/superadmin')){
               echo $form->field($model, 'role')->widget(Select2::classname(), [
                    'data' =>  ArrayHelper::map(\app\modules\rbac\models\AuthItem::find()->where(['type'=>1])->andWhere(['NOT IN','name',['Software Developer']])->all(), 'name', 'name'),
                    'options' => ['placeholder' => 'Select a Role ...'],
                    'size' => Select2::SMALL,
                    'hideSearch' => true,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                       // 'multiple' => true,
                    ],
                ]);
                 }



            ?>
        </div>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'about')->textarea(['maxlength' => true]) ?>
        </div>

    </div>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
