<?php
/**
 * Created by PhpStorm.
 * User: Multiline
 * Date: 9/23/2018
 * Time: 6:03 PM
 */

use kartik\file\FileInput;
use kartik\password\PasswordInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = "Profile";


$role = \Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id);




?>


<div class="row">
    <div class="col-lg-4 col-xl-3 mb-4 mb-xl-0">

        <section class="card">
            <div class="card-body">
                <div class="thumb-info mb-3">
                    <img src="<?= \app\helpers\Helper::getBaseUrl()?>files/profile_image/<?=Yii::$app->user->identity->image?>" style="width:255px;height: 255px;" class="rounded img-fluid" id="showImage" alt="John Doe">
                    <div class="thumb-info-title">
                        <span class="thumb-info-inner"><?= Yii::$app->user->identity->first_name." ".Yii::$app->user->identity->last_name?></span>
                        <span class="thumb-info-type"><?php
                            $i=0;
                            foreach ($role as $key=>$r)
                            {

                                if($i>0)
                                {
                                    echo $key.",";
                                }
                                $i++;
                            }


                            ?></span>
                    </div>
                </div>


                <hr class="dotted short">

                <h5 class="mb-2 mt-3">About</h5>
                <p class="text-2"><?= Yii::$app->user->identity->about?></p>

            </div>
        </section>

    </div>
    <div class="col-lg-12 col-xl-9">

        <div class="tabs">
            <ul class="nav nav-tabs tabs-primary">
                <li class="nav-item active">
                    <a class="nav-link" href="#edit" data-toggle="tab">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#password" data-toggle="tab">Change Password</a>
                </li>
            </ul>
            <div class="tab-content">

                <div id="edit" class="tab-pane active">

                        <h4 class="mb-3">Personal Information</h4>

                        <?php

                        $form = ActiveForm::begin([
                            'id' => 'form',
                            'errorCssClass' => 'has-danger',
                            'options'=>array('enctype' => 'multipart/form-data')
                        ]);
                        ?>

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'phone_no')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label" for="user-image">Image</label>
                                <?php


                                echo $form->field($model, 'file')->widget(FileInput::classname(), [
                                    'options' => ['multiple' => false, 'accept' => 'image/*'],
                                    'pluginEvents' => [
                                        'change' => 'function() { showImage(); }',
                                    ],
                                    'pluginOptions' => [
                                        'allowedFileExtensions'=>['jpg', 'gif', 'png', 'bmp'],
                                        'showPreview' => false,
                                        'showCaption' => true,
                                        'showRemove' => false,
                                        'showCancel' => false,
                                        'showUpload' => false,
                                        'initialCaption'=> $model->image,


                                    ]
                                ])->label(false);


                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                            <?= $form->field($model, 'about')->textarea(['maxlength' => true]) ?>
                            </div>

                        </div>

                        <div class="form-group">
                            <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
                        </div>



                        <?php ActiveForm::end(); ?>

                </div>
                <div id="password" class="tab-pane">

                        <h4 class="mb-3">Change Password</h4>

                    <?php if (Yii::$app->session->hasFlash('success')): ?>

                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?= Yii::$app->session->getFlash('success') ?>
                        </div>

                    <?php endif; ?>



                    <?php



                        $form2 = ActiveForm::begin([
                            'id' => 'form2',
                            'errorCssClass' => 'has-danger',
                        ]);


                        ?>

                        <div class="row">
                            <div class="col-md-12">
                                <?= $form2->field($changepassword, 'old_password')->passwordInput(['maxlength' => true]) ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                echo $form2->field($changepassword, 'password')->widget(
                                    PasswordInput::classname()
                                )->label("New Password");

                                ?>                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                <?php

                                echo $form2->field($changepassword, 'repeat_password')->widget(
                                    PasswordInput::classname()
                                );

                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <?= Html::submitButton('Change Password', ['class' => 'btn btn-info']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>

</div>

<script>

    $(document).ready(function(e) {


        $('#myTable').on('click', '.checkbox-click', function(){



            var $this = $(this);

            var child = $(this).parent().parent().siblings(":first").text();
            console.log(child);
            var parent = $(this).val();
            if ($(this).is(':checked')) {



                // alert("checked");


                var url = '?child='+child+'&parent='+parent+'';
                $.ajax({
                    url: url,
                    type: "GET",
                    success: function (data) {
                        //alert(data);
                        if(!data=="Revoke")
                        {
                            $this.removeAttr("checked");
                        }


                    },
                    error: function(xhr, ajaxOptions, thrownError){

                    },
                    //timeout : 15000//timeout of the ajax call
                });




            }
            else
            {
                var url = '?child='+child+'&parent='+parent+'&remove_child=true';

                $.ajax({
                    url: url,
                    type: "GET",
                    success: function (data) {

                        //alert(data);

                        if(!data=="ASSIGN")
                        {
                            $this.prop('checked');
                        }




                    },
                    error: function(xhr, ajaxOptions, thrownError){

                    },
                    // timeout : 15000//timeout of the ajax call
                });

            }
        });



        $('a[data-toggle="tab"]').on('shown.bs.tab', function () {

            //save the latest tab; use cookies if you like 'em better:

            localStorage.setItem('lastTab_leadview', $(this).attr('href'));

        });
        $('[data-toggle="hover"]').popover({ trigger: "hover" });


        //go to the latest tab, if it exists:

        var lastTab_leadview = localStorage.getItem('lastTab_leadview');

        if ($('a[href="' + lastTab_leadview + '"]').length > 0) {

            $('a[href="' + lastTab_leadview + '"]').tab('show');
        }
        else
        {
            // Set the first tab if cookie do not exist

            $('a[data-toggle="tab"]:first').tab('show');
        }

    });



    function showImage() {

        readURL(this);

    }

    function readURL(input) {


        console.log(input);

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#showImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


</script>
