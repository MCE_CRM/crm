<?php
/**
 * Created by PhpStorm.
 * User: Multiline
 * Date: 10/20/2018
 * Time: 12:00 PM
 */
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */

?>

<?php $form = ActiveForm::begin([
    'id' => 'form',
]); ?>

<div class="row">

    <div class="col-md-12">

        <?php

        echo $form->field($model, 'property_assign')->widget(Select2::classname(), [
            'data' =>  app\helpers\Helper::showTo(),
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => ['multiple' => true, 'placeholder' => 'Select Users'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label(false);

        ?>

    </div>

</div>
<br>
<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-sm']) ?>
</div>

<?php ActiveForm::end(); ?>

