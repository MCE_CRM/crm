<?php
use app\helpers\Helper;

foreach ($comments as $comment)
{?>

<li>
    <div class="tm-box">
        <div id="userbox" class="userbox">
            <a href="#" data-toggle="dropdown">
                <figure class="profile-picture">
                    <img src="<?=

                    Yii::$app->homeUrl?>porto/img/!logged-user.jpg" alt="admin" class="rounded-circle" data-lock-picture="/crm/web/porto/img/!logged-user.jpg">
                </figure>
                <div class="profile-info">
                    <span class="name"><?= Helper::getCreated($comment->created_by)?></span>
                    <span class="role"><?= Helper::DatTim($comment->created_on) ?></span>
                </div>

            </a>

        </div>
        <br>
        <p id="comment<?=$comment->id?>">
            <?= $comment->comment?>
        </p>

        <?php if(Yii::$app->user->id==$comment->created_by){?>

            <a href="javascript:void(0)" onclick="updateComment(<?= $comment->id?>,event)" class="on-default edit-note"><i class="fas fa-pencil-alt"></i></a>
            &nbsp;

            <a href="javascript:void(0)" onclick="deleteComment(<?= $comment->id?>,event)" class="on-default remove-note"><i class="far fa-trash-alt"></i></a>

        <?php } ?>
    </div>
</li>



<?php }

?>