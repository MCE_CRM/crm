<?php
/**
 * Created by PhpStorm.
 * User: Multiline
 * Date: 10/20/2018
 * Time: 12:00 PM
 */
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
//use yii\web\JsExpression;

/* @var $this yii\web\View */ 
/* @var $model app\models\Leads */
/* @var $form yii\widgets\ActiveForm */ 
 
?>

    <?php $form = ActiveForm::begin([
        'id' => 'form',
    ]); ?>
<style type="text/css">
   }
</style>
<div class="row">
 
    <div class="col-md-12">

        <?php

        echo $form->field($model, 'lead_assign')->widget(Select2::classname(), [
            'data' =>  app\helpers\Helper::assignUser(),
           
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => ['multiple' => true, 'placeholder' => 'Select user'],
            'pluginOptions' => [
                'allowClear' => true,
                
                
            ], 
           
        ])->label(false);


        


        ?>



    </div>
    <input type="hidden" name="myarray" value="<?php echo $_GET['array'];?>">

</div>
<br>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-sm']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <script type="text/javascript">
        $('.bootbox.modal.fade.show').removeAttr('tabindex');
    </script>

