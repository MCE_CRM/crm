<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LeadSource */

$this->title = 'Create Lead Source';
$this->params['breadcrumbs'][] = ['label' => 'Lead Sources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-source-create">

  <!--  <h1><?/*= Html::encode($this->title) */?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
