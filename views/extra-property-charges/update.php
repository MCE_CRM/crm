<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ExtraPropertyCharges */

$this->title = 'Update Extra Property Charges: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Extra Property Charges', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="extra-property-charges-update">
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
