<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;


\app\assets\DashboardAsset::register($this);
$baseUrl = Yii::$app->homeUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

     <script src="<?= Yii::$app->homeUrl?>src/assets/global/plugins/jquery.min.js"></script> 

</head>
<body class="page-container-bg-solid ">
<?php $this->beginBody() ?>
<section class="body">

    <!-- start: header -->
    <?=$this->render('header_menu')?>
    <!-- end: header -->
    <div class="inner-wrapper">
        <section role="main" class="content-body">
            <header class="page-header">
                <h2><?= Html::encode($this->title) ?></h2>

                <!--<div class="right-wrapper text-right">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="index.html">
                                <i class="fas fa-home"></i>
                            </a>
                        </li>
                        <li><span>Layouts</span></li>
                        <li><span>Default</span></li>
                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                </div>-->
            </header>
            <?= $content?>
        </section>

    </div>


</section>


<?php
if (!Yii::$app->user->isGuest) {
    $this->registerJs("
     
        var autoLockTimer;
        window.onload = resetTimer;
        window.onmousemove = resetTimer;
        window.onmousedown = resetTimer; // catches touchscreen presses
        window.onclick = resetTimer;     // catches touchpad clicks
        window.onscroll = resetTimer;    // catches scrolling with arrow keys
        window.onkeypress = resetTimer;
 
        function lockScreen() {
            window.location.href = '".\yii\helpers\Url::to(['site/lock-screen'])."';
        }
 
        function resetTimer() {
            clearTimeout(autoLockTimer);
            autoLockTimer = setTimeout(lockScreen, 100000000000);  // time is in milliseconds
        }
    ");
}
?>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
