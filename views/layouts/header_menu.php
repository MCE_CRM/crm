<?php

$baseUrl = Yii::$app->homeUrl;

?>
 <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
  <script>
 
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('476f5e28cd574a378338', {
      cluster: 'ap2',
      forceTLS: true
    });

    var channel = pusher.subscribe('my-channel');
    channel.bind('my-event', function(data) {
      var result=$.parseJSON(data);
      /*alert(result[1]);*/
      /*$(".notification-menu .title").text(result[1]);*/
      $(".notification-menu ul").prepend('<li><a href="'+result[0]+'" class="clearfix"><div class="image"><i class="fas fa-thumbs-down bg-danger text-light"></i></div><span class="title">'+result[1]+'</span><span class="message">'+result[2]+'</span></a></li>');
      var count=$(".notifications > li .notification-icon .badge").text();
      count=parseInt(count);
      count=count+1;
     $(".notifications > li .notification-icon .badge").text(count);
    });
  </script>
<header class="header header-nav-menu header-nav-top-line">
    <div class="logo-container">
        <a href="#" class="logo">
            <img src="<?=$baseUrl?>porto/img/logo.png" width="75" height="35" alt="Porto Admin" />
        </a>
        <button class="btn header-btn-collapse-nav d-lg-none" data-toggle="collapse" data-target=".header-nav">
            <i class="fas fa-bars"></i>
        </button>

        <!-- start: header nav menu -->
        <div class="header-nav collapse">
            <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
                <nav>
                    <ul class="nav nav-pills" id="mainNav">
                        <li class="">
                            <a class="nav-link" href="<?= $baseUrl?>site/index">
                                Dashboard
                            </a>
                        </li>
                        <?php
                        if(\Yii::$app->user->can('leads/index')){
                            ?>
                            <li class="dropdown">
                                <a class="nav-link dropdown-toggle">
                                    Leads
                                </a>
                                <ul class="dropdown-menu">
                                    <?php  if(\Yii::$app->user->can('leads/create')) {?>
                                    <li>
                                        <a class="nav-link" href="<?= $baseUrl?>leads/create">
                                           Add New Leads
                                        </a>
                                    </li>
                                    <?php } ?>
                                     
                                    <li>
                                        <a class="nav-link" href="<?= $baseUrl?>leads">
                                            My Assigned Leads
                                        </a>
                                    </li>
                                 
                                      <?php  if(\Yii::$app->user->can('MyregisterleadsView')) {?>
                                    <li>
                                        <a class="nav-link" href="<?= $baseUrl?>leads?type=myregister">
                                            My Register Leads
                                        </a>
                                    </li>
                                         <?php } ?>
                                    <?php  if(\Yii::$app->user->can('AllLeadsView')) {?>

                                        <li>
                                            <a class="nav-link" href="<?= $baseUrl?>leads?type=all">
                                                All Leads
                                            </a>
                                        </li>
                                    <?php } ?>

                                     <li><?php 
                                            if(\Yii::$app->user->can('teamlead')){?>
                                            <a class="nav-link" href="<?= $baseUrl?>leads?type=teamlead">
                                                Team Leads
                                            </a>
                                        </li>
                                    <?php } ?>


                                    <?php  if(\Yii::$app->user->can('DeleteLeadsView')) {?>
                                        <li>
                                            <a class="nav-link" href="<?= $baseUrl?>leads?type=delete">
                                                Deleted Leads
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <?php  if(\Yii::$app->user->can('leads_upload')) {?>
                                     <li>
                                        <a class="nav-link" href="<?= $baseUrl?>leads?type=upload">
                                            Upload Leads
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>


                        <?php } ?>

                        <?php
                        if(\Yii::$app->user->can('clients/index')){
                            ?>
                            <li class="">
                                <a class="nav-link uc" href="<?= $baseUrl?>clients">
                                    Clients
                                </a>
                            </li>
                        <?php } ?>
                        <?php
                        if(\Yii::$app->user->can('tasks/index')){
                            ?>

                            <li class="">
                                <a class="nav-link uc" href="<?= $baseUrl?>tasks">
                                    Tasks
                                </a>
                            </li>
                        <?php } ?>

                        <?php
                        if(\Yii::$app->user->can('projects/index')){
                            ?>

                            <li class="">
                                <a class="nav-link" href="<?= $baseUrl?>projects">
                                    Projects

                                </a>
                            </li>
                        <?php } ?>

                        <?php
                        if(\Yii::$app->user->can('property/index')){
                            ?>

                            <li class="">
                                <a class="nav-link uc" href="<?= $baseUrl?>property">
                                    Properties

                                </a>
                            </li>
                        <?php } ?>
                        <li class="dropdown">
                            <a class="nav-link dropdown-toggle">
                                Reports
                               
                            </a>
                            <ul class="dropdown-menu">
                            	<?php
								if(\Yii::$app->user->can('reports/project')){
								?>
                                <li class="dropdown">
                                    <a class="nav-link" href="<?= $baseUrl?>reports">
                                        Over All Poject
                                    </a>
                                </li>
                                <?php } ?>
                                <li class="dropdown">
                                    <a class="nav-link" href="<?= $baseUrl?>reports/property">
                                        Over All Property
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a class="nav-link" href="<?= $baseUrl?>reports/leads">
                                        Property Wise Leads
                                    </a>
                                </li>

                                <?php if(Yii::$app->user->can('reports/user-dashboard')) { ?>

                                <li class="dropdown">
                                    <a class="nav-link" href="<?= $baseUrl?>reports/user-dashboard">
                                        User Dashboard Report
                                    </a>
                                </li>

                                <?php } ?>

                                <?php if(Yii::$app->user->can('reports/follow-up')) { ?>

                                    <li class="dropdown">
                                        <a class="nav-link" href="<?= $baseUrl?>reports/follow-up">
                                            Follow Up Report
                                        </a>
                                    </li>

                                <?php } ?>
                                
                                <?php if(Yii::$app->user->can('reports/follow-up')) { ?>

                                    <li class="dropdown">
                                        <a class="nav-link" href="<?= $baseUrl?>reports/follow-user-wise">
                                            Follow Up UserWise Report
                                        </a>
                                    </li>

                                <?php } ?>

                                  <?php if(Yii::$app->user->can('reports/activity-up')) { ?>

                                    <li class="dropdown">
                                        <a class="nav-link" href="<?= $baseUrl?>reports/activity-up">
                                            Activity Report
                                        </a>
                                    </li>

                                    <?php } ?>
                                    
                                    <?php if(Yii::$app->user->can('reports/activity-user-wise')) { ?>

                                    <li class="dropdown">
                                        <a class="nav-link" href="<?= $baseUrl?>reports/activity-user-wise">
                                            Activity Report UserWise
                                        </a>
                                    </li>

                                    <?php } ?>
                               
                            </ul>
                        </li>


                        <li class="dropdown">
                            <a class="nav-link dropdown-toggle" href="#">
                                More
                                <i class="fas fa-caret-down"></i></a>
                            <ul class="dropdown-menu">
                                <?php if(Yii::$app->user->can('lead-status/index') || Yii::$app->user->can('lead-type/index') || Yii::$app->user->can('lead-source/index') || Yii::$app->user->can('leadresponse/index') || Yii::$app->user->can('follow-up-status/index')):?>
                                    <li class="dropdown-submenu">
                                        <a class="nav-link" href="#">
                                            Leads Setting
                                            <i class="fas fa-caret-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <?php    if(Yii::$app->user->can('lead-status/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>lead-status">
                                                        Lead Status
                                                    </a>
                                                </li>
                                            <?php }?>
                                            <?php    if(Yii::$app->user->can('lead-type/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>lead-type">
                                                        Lead Type
                                                    </a>
                                                </li>
                                            <?php }?>
                                            <?php    if(Yii::$app->user->can('lead-source/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>lead-source">
                                                        Lead Source
                                                    </a>
                                                </li>
                                            <?php }?>

                                            <?php    if(Yii::$app->user->can('follow-up-status/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>follow-up-status">
                                                        Follow Up Status
                                                    </a>
                                                </li>
                                            <?php }?>
                                              <?php    if(Yii::$app->user->can('leadresponse/index')){ ?>
                                            <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>leadresponse">
                                                        Lead Response
                                                    </a>
                                                </li>
                                            <?php } ?>

                                        </ul>
                                    </li>
                                <?php endif;?>
                                <?php if(Yii::$app->user->can('task-status/index') || Yii::$app->user->can('task-priority/index')):?>
                                    <li class="dropdown-submenu">
                                        <a class="nav-link" href="#">
                                            Tasks Setting
                                            <i class="fas fa-caret-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <?php    if(Yii::$app->user->can('task-status/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>task-status">
                                                        Tasks Status
                                                    </a>
                                                </li>
                                            <?php } ?>
                                            <?php    if(Yii::$app->user->can('task-priority/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>task-priority">
                                                        Task Priority
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                <?php endif;?>

                                <?php if(Yii::$app->user->can('project-category/index')):?>
                                    <li class="dropdown-submenu">
                                        <a class="nav-link" href="#">
                                            Project Setting
                                            <i class="fas fa-caret-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <?php    if(Yii::$app->user->can('project-category/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>project-category">
                                                        Project Category
                                                    </a>
                                                </li>
                                            <?php }?>
                                            <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>blocksector/index">
                                                        Block/sector
                                                    </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php endif;?>

                                <?php if(Yii::$app->user->can('property-category/index') || Yii::$app->user->can('property-location-type/index') || Yii::$app->user->can('property-purpose/index') || Yii::$app->user->can('extra-property-charges/index') || Yii::$app->user->can('property-status/index') ):?>
                                    <li class="dropdown-submenu">
                                        <a class="nav-link" href="#">
                                            Property Setting
                                            <i class="fas fa-caret-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <?php    if(Yii::$app->user->can('property-category/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>property-category">
                                                        Property Category
                                                    </a>
                                                </li>
                                            <?php } ?>
											
											
											<?php    if(Yii::$app->user->can('property-location-type/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>property-location-type">
                                                        Property Location Type
                                                    </a>
                                                </li>
                                            <?php } ?>

                                            <?php    if(Yii::$app->user->can('property-status/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>property-status">
                                                        Property Status
                                                    </a>
                                                </li>
                                            <?php } ?>


                                            <?php    if(Yii::$app->user->can('property-purpose/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>property-purpose">
                                                        Property Purpose
                                                    </a>
                                                </li>
                                            <?php } ?>
                                            <?php    if(Yii::$app->user->can('extra-property-charges/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>extra-property-charges">
                                                        Extra Property Charges
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                <?php endif;?>

                                <?php if(Yii::$app->user->can('feature-form/index') || Yii::$app->user->can('feature-list/index')
                                    || Yii::$app->user->can('country/index') || Yii::$app->user->can('state/index') || Yii::$app->user->can('city/index') || Yii::$app->user->can('email-template/index') || Yii::$app->user->can('sms-template/index') || Yii::$app->user->can('rbac/index')):?>
                                    <li class="dropdown-submenu">
                                        <a class="nav-link" href="#">
                                            System Setting
                                            <i class="fas fa-caret-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <?php    if(Yii::$app->user->can('feature-form/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>feature-form">
                                                        Features Form
                                                    </a>
                                                </li>
                                            <?php } ?>
                                            <?php    if(Yii::$app->user->can('feature-list/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>feature-list">
                                                        Features List
                                                    </a>
                                                </li>
                                            <?php } ?>
                                            <?php   /* if(Yii::$app->user->can('country/index')){*/ ?>
                                            <li>
                                                <a class="nav-link" href="<?= $baseUrl?>country">
                                                    Countries
                                                </a>
                                            </li>
                                            <?php /*}*/ ?>
                                            <?php    if(Yii::$app->user->can('state/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>state">
                                                        States
                                                    </a>
                                                </li>
                                            <?php } ?>
                                            <?php    if(Yii::$app->user->can('city/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>city">
                                                        Cities
                                                    </a>
                                                </li>
                                            <?php } ?>
                                            <?php    if(Yii::$app->user->can('email-template/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>email-template">
                                                        Email Template
                                                    </a>
                                                </li>
                                            <?php } ?>
                                            <?php    if(Yii::$app->user->can('sms-template/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>sms-template">
                                                        SMS Template
                                                    </a>
                                                </li>

                                            <?php } ?>
                                            <?php    if(Yii::$app->user->can('user/index')){ ?>


                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>user">
                                                        Users
                                                    </a>
                                                </li>
                                            <?php } ?>

                                            <?php    if(Yii::$app->user->can('rbac/index')){ ?>
                                                <li>
                                                    <a class="nav-link" href="<?= $baseUrl?>rbac">
                                                        Role Based Access Control
                                                    </a>
                                                </li>

                                            <?php } ?>
                                            
                                        </ul>
                                    </li>
                                <?php endif;?>

                            </ul>
                        </li>
                         <?php if(Yii::$app->user->can('sms-template/smspanal')){?>
                         <li>
                            <a class="nav-link" href="<?= $baseUrl?>sms-template/smspanal">
                                Sms 
                            </a>
                        </li> 
                    <?php }?>

                    </ul>
                </nav>
            </div>
        </div>
        <!-- end: header nav menu -->
    </div>

    <!-- start: search & user box -->
    <div class="header-right">



        <!--<a class="btn search-toggle d-none d-md-inline-block d-xl-none" data-toggle-class="active" data-target=".search"><i class="fas fa-search"></i></a>
        <form action="pages-search-results.html" class="search nav-form d-none d-xl-inline-block">
            <div class="input-group">
                <input type="text" class="form-control" name="q" id="q" placeholder="Search...">
                        <span class="input-group-append">
                            <button class="btn btn-default" type="submit"><i class="fas fa-search"></i></button>
                        </span>
            </div>
        </form>-->

         
        <span class="separator"></span>
      
        <ul class="notifications">
                       
                       <?php $notifications=\app\models\Notification::find()->where([
                        'and',
                        ['status'=>0],
                        ['user_id'=>Yii::$app->user->id]
                       ])->orderBy('id DESC')->all();
                           ?>
                       
                        <li>
                            <a href="#" class="dropdown-toggle notification-icon" id="notifications" data-toggle="dropdown">
                                <i class="fas fa-bell"></i>
                                <span class="badge"><?php if($notifications>0){echo count($notifications);}else{echo 0;}?></span>
                            </a>
            
                            <div class="dropdown-menu notification-menu">
                                <div class="notification-title">
                                    <span class="float-right badge badge-default"></span>
                                    Alerts
                                </div>
                                
                                
                                <div class="content" style="max-height: 300px;overflow-y: scroll;">
                                    <ul>
                                        <?php
                                        if(!empty($notifications)){
                                         foreach ($notifications as  $value){?>
                                        
                                        <li><a href="<?php echo Yii::$app->homeUrl;?>property/view?id=<?=$value->property_id;?>" class="clearfix"><div class="image"><i class="fas fa-thumbs-down bg-danger text-light"></i></div><span class="title"><?=$value->message;?></span><span class="message"><?= date("d/m/Y g:i:s A", strtotime($value->created_on));?></span></a></li>
                                       <?php }} else{
                                       
                                $notifications1=\app\models\Notification::find()->where(['user_id'=>Yii::$app->user->id
                                           ])->limit(40)->orderBy('id DESC')->all();
                                         foreach ($notifications1 as  $value):?>
                                         <li><a href="<?php echo Yii::$app->homeUrl;?>property/view?id=<?php echo $value->property_id;?>" class="clearfix"><div class="image"><i class="fas fa-thumbs-down bg-danger text-light"></i></div><span class="title"><?=$value->message;?></span><span class="message"><?php echo date("d-m-Y g:i:s A", strtotime($value->created_on));?></span></a></li>
                                     <?php endforeach;}?>
                                      

                                      
                                    </ul>
            
                                    <hr>
            
                                    <div class="text-right">
                                        <a href="#" class="view-more"></a>
                                    </div>
                                </div>
                            </div>
                        </li>
         </ul>



        <div id="userbox" class="userbox">
            <a href="#" data-toggle="dropdown">
                <figure class="profile-picture">
                    <img src="<?=$baseUrl?>porto/img/!logged-user.jpg" alt="<?=Yii::$app->user->identity->username?>" class="rounded-circle" data-lock-picture="<?=$baseUrl?>porto/img/!logged-user.jpg" />
                </figure>
                <div class="profile-info" data-lock-name="<?=Yii::$app->user->identity->username?>" data-lock-email="<?=Yii::$app->user->identity->email?>">
                    <span class="name"><?=Yii::$app->user->identity->username?></span>
                </div>

                <i class="fa custom-caret"></i>
            </a>

            <div class="dropdown-menu">
                <ul class="list-unstyled">
                    <li class="divider"></li>

                    <!--<li>
                        <a role="menuitem" tabindex="-1" href="#"><i class="fas fa-user"></i> My Profile</a>
                    </li>-->
                    <li>
                        <a role="menuitem" tabindex="-1" href="<?=$baseUrl?>user/profile" data-lock-screen="true"><i class="fas fa-lock"></i>Profile</a>
                    </li>

                    <li>
                        <a role="menuitem" tabindex="-1" href="<?=$baseUrl?>site/logout"><i class="fas fa-power-off"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end: search & user box -->
</header>
<script type="text/javascript">
    $(document).ready(function(){
      $("#notifications").click(function(){
        
        $(".notifications > li .notification-icon .badge").text(0);
        $.ajax({
             url:"<?php echo Yii::$app->homeUrl;?>ajax/notificationstatus",
             success:function(res){
               
             }
        });
      });
    });
</script>