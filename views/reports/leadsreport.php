<?php

use kartik\editable\Editable;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/*use kartik\daterange\DateRangePicker;*/
use yii\data\ActiveDataProvider;
use DateTime;
/* @var $this yii\web\View */
/* @var $searchModel app\models\leadsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Leads';
$this->params['breadcrumbs'][] = $this->title;
/*echo"<pre>";echo print_r($dataProvider->models);echo"</pre>";exit;*/

?>

<div class="row">
    <div class="col-lg-8">

    </div>

    <div class="col-lg-3">
        <form method="get">
            <div class="datepicker">
                <?php
                $this->title = 'Reports';

                $addon = <<< HTML
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <i class="fas fa-calendar-alt"></i>
                        </span>
                    </div>
HTML;

                /* echo '<div class="input-group drp-container">';
                 echo DateRangePicker::widget([
                         'name'=>'date_range_1',
                         'value'=>$_GET['date_range_1'],
                         'convertFormat'=>true,
                         'useWithAddon'=>true,
                         'pluginOptions'=>[


                             'opens'=>'left',
                             'ranges' => [

                                 "Today" => ["moment().startOf('day')", "moment()"],
                                 "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                                 "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],
                                 "This Month" => ["moment().startOf('month')", "moment().endOf('month')"],
                                 "Last Month" => ["moment().subtract(1, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"],

                             ],

                             'timePicker'=>true,
                             'timePickerIncrement'=>05,
                             'locale'=>['format'=>'d/m/Y h:i A']
                         ],
                     ]) . $addon;
                 echo '</div>';*/
                ?>

            </div>

    </div>
    <div class="col-lg-1">
<!--        <input type="submit" name="submit" id="submit" class="btn btn-success" style="margin-top: 10px;">
-->    </div>
    </form>
</div>


<div class="leads-index">
    <div class="row">
        <div class="col-md-12" style="background-color: white;">

            <div class="row" style="margin-left:45px">
                <?php
               
                foreach ($leadStaus as $status):
                    if(!isset($_GET['date_range_1'])){
                        $stu = \app\models\Leads::find()->where(['like', 'lead_status', $status->status])->count();
                        $totalleads=\app\models\Leads::find()->count();
                        $color=$status->color;
                         $percentage=$stu/$totalleads*100;
                    }
                    else
                    {

                        $stu = \app\models\Leads::find()->where([
                            'and',
                            ['like', 'lead_status', $status->status],
                            ['between','created_on',$start_date,$end_date]
                        ])->count();
                        
                        $color=$status->color;
                    }
                    ?>

                    <div class="col-xl-2 text-center">
                        <h2 class="card-title mt-3" style="color:<?= $color ?>"><b><?php echo  $status->status;?></b></h2><span class="badge"><?php echo $stu;?></span>
                        <div class="liquid-meter-wrapper liquid-meter-sm mt-3">
                            <div class="liquid-meter">
                                <meter min="0" max="100" value="<?= $percentage ?>" id="<?php echo $status->id;?>"></meter>
                            </div>

                        </div>

                    </div>
                    <script>

                        $(function(){

                            $('#<?php echo $status->id;?>').liquidMeter({
                                shape: 'circle',
                                color: '<?php echo $color;?>',
                                background: '#000',
                                fontSize: '24px',
                                fontWeight: '600',
                                stroke: '#F2F2F2',
                                textColor:'#fff',
                                liquidOpacity: 0.9,
                                liquidPalette: ['#333'],
                                speed: 2000,
                                animate: !$.browser.mobile
                            });

                        });

                    </script>

                <?php endforeach; ?>

            </div>


            <?php Pjax::begin(['id' => 'leadGridview']) ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                /*'filterModel' => $searchModel,*/
                'responsiveWrap' => false,

                'toolbar' =>  [
                    '{export}',
                    '{toggleData}',
                ],

                'export' => [
                    'fontAwesome' => true
                ],
                'exportConfig' => [
                    GridView::CSV => ['label' => 'Save as CSV', 'icon' => 'file-excel'],
                    GridView::EXCEL => ['label' => 'Save as EXCEL'],
                    GridView::TEXT => ['label' => 'Save as TEXT'],
                    GridView::PDF => ['label' => 'Save as PDF'],

                ],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i><span id="card-title">'.Yii::t ( 'app', ' List' ).'</span> </h5>',
                    'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                            'index'
                        ], [
                            'class' => 'btn btn-primary btn-sm'
                        ]),


                    'showFooter' => false,

                ],

                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute'=>'Project Name',

                        'value'=>function($model)
                        {
                            /*$projectid=\app\models\Property::find()->select('project_id')->where(['id'=>$model->id])->one();
                             $project_id=$projectid['project_id'];*/

                            $projectname=\app\models\Projects::find()->select('name')->where(['id'=>$model->project_id])->one();
                            if(!empty($projectname))
                            {
                                return $projectname->name;
                            }
                            else
                            {
                                return"Open";
                            }


                        },
                        'group' => true,

                    ],
                    [
                        'label'=>'Property Name',
                        'value'=>function($model)
                        {
                            $propertyname=\app\models\Property::find()->select('property_title')->where(['id'=>$model->property_id])->one();
                           
                            return $propertyname['property_title'];
                            
                            
                           
                        },
                        'group'=>true,
                        
                    ],
                    

                    [
                        'attribute'=>'lead_type',
                        'label'=>'Type',
                    ],
                    [
                        'label'=>'New',
                        'value'=>function($model)
                        {
                            $new=\app\models\Leads::find()->where([
                                'and',
                                ['=','property_id',$model->property_id],
                                ['=','lead_status','New']
                            ])->count();
                            return $new;
                        }
                    ],
                    [
                        'label'=>'Meeting',
                        'value'=>function($model)
                        {
                            $Meeting=\app\models\Leads::find()->where([
                                'and',
                                ['=','property_id',$model->property_id],
                                ['=','lead_status','Meeting']
                            ])->count();
                            return $Meeting;
                        },
                        /*'group'=>true,*/
                    ],
                    [
                        'label'=>'Interested',
                        'value'=>function($model)
                        {
                            $Interested=\app\models\Leads::find()->where([
                                'and',
                                ['=','property_id',$model->property_id],
                                ['=','lead_status','Interested']
                            ])->count();
                            return $Interested;
                        }
                    ],
                    [
                        'label'=>'Inactive',
                        'value'=>function($model)
                        {
                            $Inactive=\app\models\Leads::find()->where([
                                'and',
                                ['=','property_id',$model->property_id],
                                ['=','lead_status','Inactive']
                            ])->count();
                            return $Inactive;
                        }
                    ],
                    [
                        'label'=>'Sold',
                        'value'=>function($model)
                        {
                            $Sold=\app\models\Leads::find()->where([
                                'and',
                                ['=','property_id',$model->property_id],
                                ['=','lead_status','Sold']
                            ])->count();
                            return $Sold;
                        }
                    ],


                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>

    </div>
</div>