<?php
/**
 * Created by PhpStorm.
 * User: Multiline
 * Date: 4/24/2019
 * Time: 11:32 AM
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;

use kartik\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Activity Report';




?>
 

<style type="text/css">
    .modal-open .modal {
       overflow-y: hidden;
    }
    .modal-body
    {
        overflow-y: scroll;
        min-height: 400px !important;
    }

   .modal-content
    {
        height: 500px !important;
    }
</style>
<div class="card-body">


      <div class="follow-up-search">
      
        <?php $form = ActiveForm::begin([
            'action' => ['activity-up'],
            'method' => 'get',
        ]); ?>
        <div class="row">
            <?php if (Yii::$app->user->can('Admin') || Yii::$app->user->can('SuperAdmin') || Yii::$app->user->can('teamlead')) {?>
             <div class="col-md-3">

                <?php 
                  if(Yii::$app->user->can('teamlead')){
                 echo $form->field($model, 'lead_assign')->widget(Select2::classname(), [
                'data' =>  app\helpers\Helper::assignLead4(),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['multiple' => false, 'placeholder' => 'Select Users'],
                'pluginOptions' => [
                'allowClear' => true,
                ],
                ])->label('Lead Assign');  
                }else{
                    echo $form->field($model, 'lead_assign')->widget(Select2::classname(), [
                'data' =>  app\helpers\Helper::assignLead1(),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['multiple' => false, 'placeholder' => 'Select Users'],
                'pluginOptions' => [
                'allowClear' => true,
                ],
                ])->label('Lead Assign'); 
                 }
                ?>
               

            </div>
        <?php } ?>
        
            <div class="col-md-3">
                <?= $form->field($model, 'activity') ?>
            </div>


            
                       <div class="col-md-3">
                <?php
                $addon = <<< HTML
<span class="input-group-addon">
    <i class="glyphicon glyphicon-calendar"></i>
</span>
HTML;



                echo '<label class="control-label">Create On</label>';
                echo '<div class="drp-container">';
                echo DateRangePicker::widget([
                    'model'=>$model,
                    'attribute'=>'created_on',

                    'convertFormat'=>true,

                    'pluginOptions'=>[
                        'opens'=>'left',
                        'ranges' => [

                            "Today" => ["moment().startOf('day')", "moment()"],
                            "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                            "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],

                        ],

                        'timePicker'=>true,
                        'timePickerIncrement'=>05,
                        'locale'=>['format'=>'d/m/Y h:i A']
                    ],
                    'presetDropdown'=>false,
                    'hideInput'=>true
                ]);
                echo '</div>'; ?>

            </div>


        </div>


        <div class="form-group">
             <?= Html::label( 'Page Size', 'pagesize', array( 'style' => 'margin-left:10px; margin-top:8px;' ) ) ?>

            <?= Html::dropDownList(
    'pagesize', 
    ( isset($_GET['pagesize']) ? $_GET['pagesize'] : 20 ),  // set the default value for the dropdown list
    // set the key and value for the drop down list
    array( 
        20=>20,
        50 => 50, 
        100 => 100,
        200=>200,
        1000=>'All'
    ),
    // add the HTML attritutes for the dropdown list
    // I add pagesize as an id of dropdown list. later on, I will add into the Gridview widget.
    // so when the form is submitted, I can get the $_POST value in InvoiceSearch model.
    array( 
        'id' => 'pagesize', 
        'style' => 'margin-left:5px; margin-top:8px;'
        )
    ) 
?>
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default obaid']) ?>
           
        </div>

        <?php ActiveForm::end(); ?>

    </div>

    <div style="margin-top:20px">

    <?php
     if(Yii::$app->user->can('activity/download'))
     {
        $show='';
     }
     else
     {
        $show=false;
     }

    ?>

              

     <?= GridView::widget([
                'dataProvider' => $dataProvider,
                /*'filterModel' => $searchModel,*/
                'responsiveWrap' => false,
                'toolbar' =>  [
                    '{export}',
                    '{toggleData}',
                ],
                'export' => [
                'fontAwesome' => true
                ],
                'exportConfig' => [
                  
                    GridView::EXCEL => ['label' => 'Save as EXCEL'],
                   
                ],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i><span id="card-title">'.Yii::t ( 'app', ' List' ).'</span> </h5>',
                    'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                            'index'
                        ], [
                            'class' => 'btn btn-primary btn-sm'
                        ]),


                    'showFooter' => false,

                ],
        'columns' => [

          
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Lead ID',
                'format'=>'raw',
                'value' =>function($model)
                {
                    $id = $model->lead_id;
                    $url = Yii::$app->homeUrl.'leads/view?id='.$model->lead_id;
                    return "<a href='$url' target='_blank'>$id</a>";
                },
                /*'group'=>true*/

            ],

           [
            'label'=>'Name',
            'value'=>'lead.contact_name',

           ],

          

            [
               
                    'label' => 'Mobile',
                    'value'=>'lead.contact_mobile',
                    'group'=>true,
                   'hiddenFromExport' =>(Yii::$app->user->can('phonenumberdownload')),

            ],
    
            [
                'label' => 'Purpose',
                'value'=>'lead.lead_type',
                

            ],
            [
                'label' => 'Lead Status',
                'value'=>'lead.lead_status',
                

            ],
            [
                'label' => 'Assign To',
                'format'=>'raw',
                'value'=>function($model)
                {

                    if(empty($model->lead['lead_assign']))
                    {

                        return '';

                    }else {


                        $mo = explode(',', $model->lead['lead_assign']);
                        $e = '';
                        foreach ($mo as $key=>$data)
                        {
                            $user = \app\models\User::findOne($data);

                            $e .=$user->username.',';
                        }

                        $e = substr_replace($e, "", -1);
                        return $e;

                    }
                },
                

            ],
            [
                'label' => 'Assign By',
                'format'=>'raw',
                'value'=>function($model)
                {
                    if(empty($model->lead['assign_by']))
                    {

                        return '';

                    }else {


                        $mo = explode(',', $model->lead['assign_by']);
                        $e = '';
                        foreach ($mo as $key=>$data)
                        {
                            $user = \app\models\User::findOne($data);

                            $e .=$user->username.',';
                        }

                        $e = substr_replace($e, "", -1);
                        return $e;

                    }
                },
               

            ],
            [
                'label' => 'Register',
                'format'=>'raw',
                'value'=>function($model)
                {
                    if(empty($model->lead['created_by']))
                    {

                        return '';

                    }else {


                        $mo = explode(',', $model->lead['created_by']);
                        $e = '';
                        foreach ($mo as $key=>$data)
                        {
                            $user = \app\models\User::findOne($data);

                            $e .=$user->username.',';
                        }

                        $e = substr_replace($e, "", -1);
                        return $e;

                    }
                },
                

            ],
            
               [
                'label' => 'Last Activity',
                'format'=>'raw',
                'value' =>function($model)
                {
                     if(!empty($_GET['ActivitiesSearch']['created_on']))
                    {

                          if ( ! is_null($_GET['ActivitiesSearch']['created_on']) && strpos($_GET['ActivitiesSearch']['created_on'], ' - ') !== false ) {
                            list($start_date, $end_date) = explode(' - ',$_GET['ActivitiesSearch']['created_on']);
                            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
                             $start_date = $start_date->format('Y-m-d H:i:s');

                            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
                            $end_date = $end_date->format('Y-m-d H:i:s');
                            

                             $status= \app\models\Activities::find()->select('activity')->where(['between', 'activities.created_on', $start_date, $end_date])->andWhere(['lead_id'=>$model->lead_id])->orderBy(['activities.created_on'=> SORT_DESC])->one();
                                return $status->activity;



                   }
                        
                    }
                    else
                    {
                    $id = $model->lead->id;
                     $status= \app\models\Activities::find()->select('activity')->where(['lead_id'=>$model->lead_id])->orderBy(['created_on'=> SORT_DESC])->one();
                                return $status->activity;
                            }
                },
                 

              ],



              [
                 'label' => 'created on',
                'format'=>'raw',
                'value' =>function($model)
                {
                     if(!empty($_GET['ActivitiesSearch']['created_on']))
                    {

                          if ( ! is_null($_GET['ActivitiesSearch']['created_on']) && strpos($_GET['ActivitiesSearch']['created_on'], ' - ') !== false ) {
                            list($start_date, $end_date) = explode(' - ',$_GET['ActivitiesSearch']['created_on']);
                            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
                             $start_date = $start_date->format('Y-m-d H:i:s');

                            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
                            $end_date = $end_date->format('Y-m-d H:i:s');
                            

                             $status= \app\models\Activities::find()->select('created_on')->where(['between', 'activities.created_on', $start_date, $end_date])->andWhere(['lead_id'=>$model->lead_id])->orderBy(['activities.created_on'=> SORT_DESC])->one();
                                return $status->created_on;



                   }
                        
                    }
                    else
                    {
                    $id = $model->lead->id;

                                $status= \app\models\Activities::find()->select('created_on')->where(['lead_id'=>$model->lead_id])->orderBy(['created_on'=>SORT_DESC])->one();
                                return $status->created_on;
                            }
                           
                },
                
              ],
             // 'created_on',
            //'updated_on',
            //'updated_by',


                     [
                        'class' => '\kartik\grid\ActionColumn',
                        'header'=>'Actions',
                        'template'=>' {activity} ',
                        'buttons' => [
                           
                            'activity'=>function($url,$model)
                    {
                        
                         return '<a href="javascript:void(0)"  onclick="showactivity('.$model->lead->id.',event);" title="Activity"><span class="fas fa-eye"></span></a>';
                       
                    },
                          

                        ],

                    ]



        ],
        
     
    ]); ?>

    </div>

 





</div>
<script type="text/javascript">
    $(".obaid").click(function(){

   var uri = window.location.toString();

    if (uri.indexOf("?") > 0) {
        var clean_uri = uri.substring(0, uri.indexOf("?"));
        window.history.replaceState({}, document.title, clean_uri);
       $('input.form-control.range-value').attr('value','');
      
    }
    location.reload();
});


     function showactivity(id,event) {
    event.preventDefault();

    var dialog = bootbox.dialog({
        title:'<h4  id="myid" style="color:#85C0E7"><b>Leads Activities</b></h4>',
        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
        onEscape: function() {
            // you can do anything here you want when the user dismisses dialog
            $(".sp-palette-buttons-disabled").hide();
        }

    });

    dialog.init(function(){
        var request = $.ajax({
            url:'<?php echo Yii::$app->homeUrl;?>ajax/getleadactivities',
            method: "GET",
            data:{id:id}
        });

        request.done(function( msg ) {
            dialog.find('.bootbox-body').html(msg);
        });


    });

    

};



   
</script>