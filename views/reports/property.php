<?php
use app\models\Projects;
use app\models\User;
use app\models\Leads;

$this->title= $title;
if(!empty($blocsectortitle))
{
    //echo $blocsectortitle;exit;
}

?>

<style type="text/css">
    .inner-toolbar.clearfix{
        background: none !important;

        border-left: none !important;
    }
    .dashIconText {
        padding-top: 5px;
        padding-bottom: 10px;
        color:white !important;
        margin: 0;
        padding: 0;
        border: 0;
        font-size: 100%;
        font: inherit;
        vertical-align: baseline;

    }
    p {
        margin: 0 0 1.5em;
        line-height: 16px !important;
        color:white !important;
    }
    .round-corner {
        border: 2px !important;
        border-top-left-radius: .5em !important;
        border-top-right-radius: .5em !important;
        border-bottom-left-radius: .5em !important;
        border-bottom-right-radius: .5em !important;
        color: #FFF !important;
        width: 130px;
        height:100px !important;
        background-color: #930;
    }
    .dashIcon {
        float: left;
        border: solid 1px #CCCCCC;
        background-color: #F9F9F9;
        padding-top: 8px;
        margin-bottom: 10px;
        text-align: center;
        margin-right: 10px;
    }
    a.soldto{
        text-decoration: none;
        color:white;
    }

</style>


<!-- start: page -->
<section class="content-with-menu content-with-menu-has-toolbar media-gallery">
    <div class="content-with-menu-container" style="background-color: white;">
        <div class="inner-body mg-main">

            <div class="inner-toolbar clearfix">

                <ul>

                    <li class="right" data-sort-source data-sort-id="media-gallery">
                        <ul class="nav nav-pills nav-pills-primary">
                            <li style="margin-right:10px;">
                                <select class="form-control" id="filterbyproject">
                                    <option value="All">Filter By Projects</option>
                                    <?php
                                    if($this->title=="Open Property")
                                         {
                                            $openselected="selected";
                                         }
                                         elseif($this->title=="All Property")
                                         {
                                             $Allselected="selected";
                                         }
                                         ?>
                                    <!-- <option value="All" <?=$Allselected;?>>All</option> -->
                                    <option value="NULL" <?=$openselected;?>>Open Property</option>
                                    <?php foreach ($projects as $value):
                                       
                                        
                                         
                                        ?>

                                        <option value="<?= $value->id;?>"<?php if($_GET['projectid']==$value->id){echo "selected";}?>><?= $value->name;?></option>
                                    <?php endforeach;?>

                                </select>
                            </li>
                            
                             <li style="margin-right:10px;">
                                <select class="form-control" id="filterbyblocksector">
                                    <option value="">Filter By Block/Sector</option>
                                    <?php
                                    
                                     foreach ($blocksector as $value1):
                                        
                                      ?>
                         <option value="<?= $value1->id;?>" <?php if($_GET['blocksectorid']==$value1->id){echo "selected";}?>><?= $value1->blocksector;?></option>
                               
                                         <?php endforeach;?> 

                                </select>
                            </li>
                            <li style="padding-right: 10px;">
                                <button class="btn btn-info" id="searchproperty" name="searchproperty"><i class="fa fa-search"></i></button>
                            </li>
                            <li class="nav-item">
                                <label>Filter:</label>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" data-option-value="*" href="#all">All</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-option-value=".Available" href="#Available">Available <span class="badge" style="background-color:green;color:white"><?= $Available;?></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-option-value=".Sold" href="#Sold">Sold <span class="badge"style="background-color:#FFAD5B;color:white;"><?= $Sold;?></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-option-value=".Occupied" href="#Occupy">Occupy <span class="badge"style="background-color:rgb(176, 222, 9);color:white;"><?= $Occupy;?></span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="row mg-files" data-sort-destination data-sort-id="media-gallery">
                <?php
                
                foreach($result as $value):
                    $status=$value->status;
                    if(!empty($value->project_id))
                    {
                    $userasignid=Leads::find()->select('lead_assign')->where(['property_id'=>$value->id])->one();
                    $username=User::find()->select('first_name')->where(['id'=>$userasignid->lead_assign])->one();
                    $userfirstname=$username->first_name;
                    $description=$value->statusdesc;

                   }
                   else
                   {
                    $userasignid=Leads::find()->select('lead_assign')->where(['property_id'=>$value->id])->one();
                    $username=User::find()->select('first_name')->where(['id'=>$userasignid->lead_assign])->one();
                    $userfirstname=$username->first_name;
                    $description=$value->statusdesc;
                   }
                    if($status=='Available')
                    {
                        $color="#060";
                    }
                    if($status=='Sold')
                    {
                        $color="#FFAD5B";
                    }
                    if($status=='Occupied')
                    {
                        $color="rgb(176, 222, 9)";
                    }
                    ?>
                   
                    <div id="1" class="isotope-item <?php echo $status;?> dashIcon span-3 round-corner" style="color:#FFF !important; width:130px; height:75px; background-color:<?= $color;?>">

                        <div class="dashIconText" style="color:#FFF !important;"><span>
                                            <a style="color:white;font-weight:bold;"id="Room details" href="<?php echo Yii::$app->homeUrl?>property/view/?id=<?php echo $value->id;?>" class="room-no update-dialog-create">
                                                <?= $value->property_title;?></a></span>
                            <p>

                                <a style="color:#FFF;font-weight: bold"  Id=" guest="" details"="" class="update-dialog-create" "="" href="<?php echo Yii::$app->homeUrl?>property/view/?id=<?php echo $value->id;?>"><?= $status;?></a>
                            </p>
                            <?php
                            if(Yii::$app->user->can('propertyreport/soldto')):
                             if($description):?>
                            <a href="#" class="soldto"title="<?php if(!empty($description)){echo $description;}else{echo"Not Sold";}?>">Sold Desc</a>
                        <?php endif;endif;?>
                            <p style="margin-top: -10px;"><?php echo $userfirstname;?></p>
                        </div>
                    </div>

                <?php endforeach;?>


            </div>
        </div>
    </div>
</section>
<!-- end: page -->

<script type="text/javascript">
    $(document).ready(function(){


        $("#searchproperty").on('click',function(){
           var projectid=$("#filterbyproject").val();
           var blocksectorid=$("#filterbyblocksector").val();
           if(projectid=="")
           {
             $("#filterbyproject").css('border-color','red');
            setTimeout(function() {
                    $("#filterbyproject").css('border-color','');
                }, 2000);
           }
           if(projectid!=="" && blocksectorid=="")
           {
            var url="<?=Yii::$app->homeUrl?>reports/property?projectid="+projectid+"";
            document.location=url;
           }
           else if(projectid!=="" && blocksectorid!=="")
           {
            var url="<?=Yii::$app->homeUrl?>reports/property?projectid="+projectid+"&blocksectorid="+blocksectorid+"";
            document.location=url;
           }
        });

    });
</script>

<script type="text/javascript">
    /*! Magnific Popup - v1.1.0 - 2016-02-20
     * http://dimsemenov.com/plugins/magnific-popup/
     * Copyright (c) 2016 Dmitry Semenov; */
    ;(function (factory) {
        if (typeof define === 'function' && define.amd) {
            // AMD. Register as an anonymous module.
            define(['jquery'], factory);
        } else if (typeof exports === 'object') {
            // Node/CommonJS
            factory(require('jquery'));
        } else {
            // Browser globals
            factory(window.jQuery || window.Zepto);
        }
    }(function($) {

        /*>>core*/
        /**
         *
         * Magnific Popup Core JS file
         *
         */


        /**
         * Private static constants
         */
        var CLOSE_EVENT = 'Close',
            BEFORE_CLOSE_EVENT = 'BeforeClose',
            AFTER_CLOSE_EVENT = 'AfterClose',
            BEFORE_APPEND_EVENT = 'BeforeAppend',
            MARKUP_PARSE_EVENT = 'MarkupParse',
            OPEN_EVENT = 'Open',
            CHANGE_EVENT = 'Change',
            NS = 'mfp',
            EVENT_NS = '.' + NS,
            READY_CLASS = 'mfp-ready',
            REMOVING_CLASS = 'mfp-removing',
            PREVENT_CLOSE_CLASS = 'mfp-prevent-close';


        /**
         * Private vars
         */
        /*jshint -W079 */
        var mfp, // As we have only one instance of MagnificPopup object, we define it locally to not to use 'this'
            MagnificPopup = function(){},
            _isJQ = !!(window.jQuery),
            _prevStatus,
            _window = $(window),
            _document,
            _prevContentType,
            _wrapClasses,
            _currPopupType;


        /**
         * Private functions
         */
        var _mfpOn = function(name, f) {
                mfp.ev.on(NS + name + EVENT_NS, f);
            },
            _getEl = function(className, appendTo, html, raw) {
                var el = document.createElement('div');
                el.className = 'mfp-'+className;
                if(html) {
                    el.innerHTML = html;
                }
                if(!raw) {
                    el = $(el);
                    if(appendTo) {
                        el.appendTo(appendTo);
                    }
                } else if(appendTo) {
                    appendTo.appendChild(el);
                }
                return el;
            },
            _mfpTrigger = function(e, data) {
                mfp.ev.triggerHandler(NS + e, data);

                if(mfp.st.callbacks) {
                    // converts "mfpEventName" to "eventName" callback and triggers it if it's present
                    e = e.charAt(0).toLowerCase() + e.slice(1);
                    if(mfp.st.callbacks[e]) {
                        mfp.st.callbacks[e].apply(mfp, $.isArray(data) ? data : [data]);
                    }
                }
            },
            _getCloseBtn = function(type) {
                if(type !== _currPopupType || !mfp.currTemplate.closeBtn) {
                    mfp.currTemplate.closeBtn = $( mfp.st.closeMarkup.replace('%title%', mfp.st.tClose ) );
                    _currPopupType = type;
                }
                return mfp.currTemplate.closeBtn;
            },
        // Initialize Magnific Popup only when called at least once
            _checkInstance = function() {
                if(!$.magnificPopup.instance) {
                    /*jshint -W020 */
                    mfp = new MagnificPopup();
                    mfp.init();
                    $.magnificPopup.instance = mfp;
                }
            },
        // CSS transition detection, http://stackoverflow.com/questions/7264899/detect-css-transitions-using-javascript-and-without-modernizr
            supportsTransitions = function() {
                var s = document.createElement('p').style, // 's' for style. better to create an element if body yet to exist
                    v = ['ms','O','Moz','Webkit']; // 'v' for vendor

                if( s['transition'] !== undefined ) {
                    return true;
                }

                while( v.length ) {
                    if( v.pop() + 'Transition' in s ) {
                        return true;
                    }
                }

                return false;
            };



        /**
         * Public functions
         */
        MagnificPopup.prototype = {

            constructor: MagnificPopup,

            /**
             * Initializes Magnific Popup plugin.
             * This function is triggered only once when $.fn.magnificPopup or $.magnificPopup is executed
             */
            init: function() {
                var appVersion = navigator.appVersion;
                mfp.isLowIE = mfp.isIE8 = document.all && !document.addEventListener;
                mfp.isAndroid = (/android/gi).test(appVersion);
                mfp.isIOS = (/iphone|ipad|ipod/gi).test(appVersion);
                mfp.supportsTransition = supportsTransitions();

                // We disable fixed positioned lightbox on devices that don't handle it nicely.
                // If you know a better way of detecting this - let me know.
                mfp.probablyMobile = (mfp.isAndroid || mfp.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent) );
                _document = $(document);

                mfp.popupsCache = {};
            },

            /**
             * Opens popup
             * @param  data [description]
             */
            open: function(data) {

                var i;

                if(data.isObj === false) {
                    // convert jQuery collection to array to avoid conflicts later
                    mfp.items = data.items.toArray();

                    mfp.index = 0;
                    var items = data.items,
                        item;
                    for(i = 0; i < items.length; i++) {
                        item = items[i];
                        if(item.parsed) {
                            item = item.el[0];
                        }
                        if(item === data.el[0]) {
                            mfp.index = i;
                            break;
                        }
                    }
                } else {
                    mfp.items = $.isArray(data.items) ? data.items : [data.items];
                    mfp.index = data.index || 0;
                }

                // if popup is already opened - we just update the content
                if(mfp.isOpen) {
                    mfp.updateItemHTML();
                    return;
                }

                mfp.types = [];
                _wrapClasses = '';
                if(data.mainEl && data.mainEl.length) {
                    mfp.ev = data.mainEl.eq(0);
                } else {
                    mfp.ev = _document;
                }

                if(data.key) {
                    if(!mfp.popupsCache[data.key]) {
                        mfp.popupsCache[data.key] = {};
                    }
                    mfp.currTemplate = mfp.popupsCache[data.key];
                } else {
                    mfp.currTemplate = {};
                }



                mfp.st = $.extend(true, {}, $.magnificPopup.defaults, data );
                mfp.fixedContentPos = mfp.st.fixedContentPos === 'auto' ? !mfp.probablyMobile : mfp.st.fixedContentPos;

                if(mfp.st.modal) {
                    mfp.st.closeOnContentClick = false;
                    mfp.st.closeOnBgClick = false;
                    mfp.st.showCloseBtn = false;
                    mfp.st.enableEscapeKey = false;
                }


                // Building markup
                // main containers are created only once
                if(!mfp.bgOverlay) {

                    // Dark overlay
                    mfp.bgOverlay = _getEl('bg').on('click'+EVENT_NS, function() {
                        mfp.close();
                    });

                    mfp.wrap = _getEl('wrap').attr('tabindex', -1).on('click'+EVENT_NS, function(e) {
                        if(mfp._checkIfClose(e.target)) {
                            mfp.close();
                        }
                    });

                    mfp.container = _getEl('container', mfp.wrap);
                }

                mfp.contentContainer = _getEl('content');
                if(mfp.st.preloader) {
                    mfp.preloader = _getEl('preloader', mfp.container, mfp.st.tLoading);
                }


                // Initializing modules
                var modules = $.magnificPopup.modules;
                for(i = 0; i < modules.length; i++) {
                    var n = modules[i];
                    n = n.charAt(0).toUpperCase() + n.slice(1);
                    mfp['init'+n].call(mfp);
                }
                _mfpTrigger('BeforeOpen');


                if(mfp.st.showCloseBtn) {
                    // Close button
                    if(!mfp.st.closeBtnInside) {
                        mfp.wrap.append( _getCloseBtn() );
                    } else {
                        _mfpOn(MARKUP_PARSE_EVENT, function(e, template, values, item) {
                            values.close_replaceWith = _getCloseBtn(item.type);
                        });
                        _wrapClasses += ' mfp-close-btn-in';
                    }
                }

                if(mfp.st.alignTop) {
                    _wrapClasses += ' mfp-align-top';
                }



                if(mfp.fixedContentPos) {
                    mfp.wrap.css({
                        overflow: mfp.st.overflowY,
                        overflowX: 'hidden',
                        overflowY: mfp.st.overflowY
                    });
                } else {
                    mfp.wrap.css({
                        top: _window.scrollTop(),
                        position: 'absolute'
                    });
                }
                if( mfp.st.fixedBgPos === false || (mfp.st.fixedBgPos === 'auto' && !mfp.fixedContentPos) ) {
                    mfp.bgOverlay.css({
                        height: _document.height(),
                        position: 'absolute'
                    });
                }



                if(mfp.st.enableEscapeKey) {
                    // Close on ESC key
                    _document.on('keyup' + EVENT_NS, function(e) {
                        if(e.keyCode === 27) {
                            mfp.close();
                        }
                    });
                }

                _window.on('resize' + EVENT_NS, function() {
                    mfp.updateSize();
                });


                if(!mfp.st.closeOnContentClick) {
                    _wrapClasses += ' mfp-auto-cursor';
                }

                if(_wrapClasses)
                    mfp.wrap.addClass(_wrapClasses);


                // this triggers recalculation of layout, so we get it once to not to trigger twice
                var windowHeight = mfp.wH = _window.height();


                var windowStyles = {};

                if( mfp.fixedContentPos ) {
                    if(mfp._hasScrollBar(windowHeight)){
                        var s = mfp._getScrollbarSize();
                        if(s) {
                            windowStyles.marginRight = s;
                        }
                    }
                }

                if(mfp.fixedContentPos) {
                    if(!mfp.isIE7) {
                        windowStyles.overflow = 'hidden';
                    } else {
                        // ie7 double-scroll bug
                        $('body, html').css('overflow', 'hidden');
                    }
                }



                var classesToadd = mfp.st.mainClass;
                if(mfp.isIE7) {
                    classesToadd += ' mfp-ie7';
                }
                if(classesToadd) {
                    mfp._addClassToMFP( classesToadd );
                }

                // add content
                mfp.updateItemHTML();

                _mfpTrigger('BuildControls');

                // remove scrollbar, add margin e.t.c
                $('html').css(windowStyles);

                // add everything to DOM
                mfp.bgOverlay.add(mfp.wrap).prependTo( mfp.st.prependTo || $(document.body) );

                // Save last focused element
                mfp._lastFocusedEl = document.activeElement;

                // Wait for next cycle to allow CSS transition
                setTimeout(function() {

                    if(mfp.content) {
                        mfp._addClassToMFP(READY_CLASS);
                        mfp._setFocus();
                    } else {
                        // if content is not defined (not loaded e.t.c) we add class only for BG
                        mfp.bgOverlay.addClass(READY_CLASS);
                    }

                    // Trap the focus in popup
                    _document.on('focusin' + EVENT_NS, mfp._onFocusIn);

                }, 16);

                mfp.isOpen = true;
                mfp.updateSize(windowHeight);
                _mfpTrigger(OPEN_EVENT);

                return data;
            },

            /**
             * Closes the popup
             */
            close: function() {
                if(!mfp.isOpen) return;
                _mfpTrigger(BEFORE_CLOSE_EVENT);

                mfp.isOpen = false;
                // for CSS3 animation
                if(mfp.st.removalDelay && !mfp.isLowIE && mfp.supportsTransition )  {
                    mfp._addClassToMFP(REMOVING_CLASS);
                    setTimeout(function() {
                        mfp._close();
                    }, mfp.st.removalDelay);
                } else {
                    mfp._close();
                }
            },

            /**
             * Helper for close() function
             */
            _close: function() {
                _mfpTrigger(CLOSE_EVENT);

                var classesToRemove = REMOVING_CLASS + ' ' + READY_CLASS + ' ';

                mfp.bgOverlay.detach();
                mfp.wrap.detach();
                mfp.container.empty();

                if(mfp.st.mainClass) {
                    classesToRemove += mfp.st.mainClass + ' ';
                }

                mfp._removeClassFromMFP(classesToRemove);

                if(mfp.fixedContentPos) {
                    var windowStyles = {marginRight: ''};
                    if(mfp.isIE7) {
                        $('body, html').css('overflow', '');
                    } else {
                        windowStyles.overflow = '';
                    }
                    $('html').css(windowStyles);
                }

                _document.off('keyup' + EVENT_NS + ' focusin' + EVENT_NS);
                mfp.ev.off(EVENT_NS);

                // clean up DOM elements that aren't removed
                mfp.wrap.attr('class', 'mfp-wrap').removeAttr('style');
                mfp.bgOverlay.attr('class', 'mfp-bg');
                mfp.container.attr('class', 'mfp-container');

                // remove close button from target element
                if(mfp.st.showCloseBtn &&
                    (!mfp.st.closeBtnInside || mfp.currTemplate[mfp.currItem.type] === true)) {
                    if(mfp.currTemplate.closeBtn)
                        mfp.currTemplate.closeBtn.detach();
                }


                if(mfp.st.autoFocusLast && mfp._lastFocusedEl) {
                    $(mfp._lastFocusedEl).focus(); // put tab focus back
                }
                mfp.currItem = null;
                mfp.content = null;
                mfp.currTemplate = null;
                mfp.prevHeight = 0;

                _mfpTrigger(AFTER_CLOSE_EVENT);
            },

            updateSize: function(winHeight) {

                if(mfp.isIOS) {
                    // fixes iOS nav bars https://github.com/dimsemenov/Magnific-Popup/issues/2
                    var zoomLevel = document.documentElement.clientWidth / window.innerWidth;
                    var height = window.innerHeight * zoomLevel;
                    mfp.wrap.css('height', height);
                    mfp.wH = height;
                } else {
                    mfp.wH = winHeight || _window.height();
                }
                // Fixes #84: popup incorrectly positioned with position:relative on body
                if(!mfp.fixedContentPos) {
                    mfp.wrap.css('height', mfp.wH);
                }

                _mfpTrigger('Resize');

            },

            /**
             * Set content of popup based on current index
             */
            updateItemHTML: function() {
                var item = mfp.items[mfp.index];

                // Detach and perform modifications
                mfp.contentContainer.detach();

                if(mfp.content)
                    mfp.content.detach();

                if(!item.parsed) {
                    item = mfp.parseEl( mfp.index );
                }

                var type = item.type;

                _mfpTrigger('BeforeChange', [mfp.currItem ? mfp.currItem.type : '', type]);
                // BeforeChange event works like so:
                // _mfpOn('BeforeChange', function(e, prevType, newType) { });

                mfp.currItem = item;

                if(!mfp.currTemplate[type]) {
                    var markup = mfp.st[type] ? mfp.st[type].markup : false;

                    // allows to modify markup
                    _mfpTrigger('FirstMarkupParse', markup);

                    if(markup) {
                        mfp.currTemplate[type] = $(markup);
                    } else {
                        // if there is no markup found we just define that template is parsed
                        mfp.currTemplate[type] = true;
                    }
                }

                if(_prevContentType && _prevContentType !== item.type) {
                    mfp.container.removeClass('mfp-'+_prevContentType+'-holder');
                }

                var newContent = mfp['get' + type.charAt(0).toUpperCase() + type.slice(1)](item, mfp.currTemplate[type]);
                mfp.appendContent(newContent, type);

                item.preloaded = true;

                _mfpTrigger(CHANGE_EVENT, item);
                _prevContentType = item.type;

                // Append container back after its content changed
                mfp.container.prepend(mfp.contentContainer);

                _mfpTrigger('AfterChange');
            },


            /**
             * Set HTML content of popup
             */
            appendContent: function(newContent, type) {
                mfp.content = newContent;

                if(newContent) {
                    if(mfp.st.showCloseBtn && mfp.st.closeBtnInside &&
                        mfp.currTemplate[type] === true) {
                        // if there is no markup, we just append close button element inside
                        if(!mfp.content.find('.mfp-close').length) {
                            mfp.content.append(_getCloseBtn());
                        }
                    } else {
                        mfp.content = newContent;
                    }
                } else {
                    mfp.content = '';
                }

                _mfpTrigger(BEFORE_APPEND_EVENT);
                mfp.container.addClass('mfp-'+type+'-holder');

                mfp.contentContainer.append(mfp.content);
            },


            /**
             * Creates Magnific Popup data object based on given data
             * @param  {int} index Index of item to parse
             */
            parseEl: function(index) {
                var item = mfp.items[index],
                    type;

                if(item.tagName) {
                    item = { el: $(item) };
                } else {
                    type = item.type;
                    item = { data: item, src: item.src };
                }

                if(item.el) {
                    var types = mfp.types;

                    // check for 'mfp-TYPE' class
                    for(var i = 0; i < types.length; i++) {
                        if( item.el.hasClass('mfp-'+types[i]) ) {
                            type = types[i];
                            break;
                        }
                    }

                    item.src = item.el.attr('data-mfp-src');
                    if(!item.src) {
                        item.src = item.el.attr('href');
                    }
                }

                item.type = type || mfp.st.type || 'inline';
                item.index = index;
                item.parsed = true;
                mfp.items[index] = item;
                _mfpTrigger('ElementParse', item);

                return mfp.items[index];
            },


            /**
             * Initializes single popup or a group of popups
             */
            addGroup: function(el, options) {
                var eHandler = function(e) {
                    e.mfpEl = this;
                    mfp._openClick(e, el, options);
                };

                if(!options) {
                    options = {};
                }

                var eName = 'click.magnificPopup';
                options.mainEl = el;

                if(options.items) {
                    options.isObj = true;
                    el.off(eName).on(eName, eHandler);
                } else {
                    options.isObj = false;
                    if(options.delegate) {
                        el.off(eName).on(eName, options.delegate , eHandler);
                    } else {
                        options.items = el;
                        el.off(eName).on(eName, eHandler);
                    }
                }
            },
            _openClick: function(e, el, options) {
                var midClick = options.midClick !== undefined ? options.midClick : $.magnificPopup.defaults.midClick;


                if(!midClick && ( e.which === 2 || e.ctrlKey || e.metaKey || e.altKey || e.shiftKey ) ) {
                    return;
                }

                var disableOn = options.disableOn !== undefined ? options.disableOn : $.magnificPopup.defaults.disableOn;

                if(disableOn) {
                    if($.isFunction(disableOn)) {
                        if( !disableOn.call(mfp) ) {
                            return true;
                        }
                    } else { // else it's number
                        if( _window.width() < disableOn ) {
                            return true;
                        }
                    }
                }

                if(e.type) {
                    e.preventDefault();

                    // This will prevent popup from closing if element is inside and popup is already opened
                    if(mfp.isOpen) {
                        e.stopPropagation();
                    }
                }

                options.el = $(e.mfpEl);
                if(options.delegate) {
                    options.items = el.find(options.delegate);
                }
                mfp.open(options);
            },


            /**
             * Updates text on preloader
             */
            updateStatus: function(status, text) {

                if(mfp.preloader) {
                    if(_prevStatus !== status) {
                        mfp.container.removeClass('mfp-s-'+_prevStatus);
                    }

                    if(!text && status === 'loading') {
                        text = mfp.st.tLoading;
                    }

                    var data = {
                        status: status,
                        text: text
                    };
                    // allows to modify status
                    _mfpTrigger('UpdateStatus', data);

                    status = data.status;
                    text = data.text;

                    mfp.preloader.html(text);

                    mfp.preloader.find('a').on('click', function(e) {
                        e.stopImmediatePropagation();
                    });

                    mfp.container.addClass('mfp-s-'+status);
                    _prevStatus = status;
                }
            },


            /*
             "Private" helpers that aren't private at all
             */
            // Check to close popup or not
            // "target" is an element that was clicked
            _checkIfClose: function(target) {

                if($(target).hasClass(PREVENT_CLOSE_CLASS)) {
                    return;
                }

                var closeOnContent = mfp.st.closeOnContentClick;
                var closeOnBg = mfp.st.closeOnBgClick;

                if(closeOnContent && closeOnBg) {
                    return true;
                } else {

                    // We close the popup if click is on close button or on preloader. Or if there is no content.
                    if(!mfp.content || $(target).hasClass('mfp-close') || (mfp.preloader && target === mfp.preloader[0]) ) {
                        return true;
                    }

                    // if click is outside the content
                    if(  (target !== mfp.content[0] && !$.contains(mfp.content[0], target))  ) {
                        if(closeOnBg) {
                            // last check, if the clicked element is in DOM, (in case it's removed onclick)
                            if( $.contains(document, target) ) {
                                return true;
                            }
                        }
                    } else if(closeOnContent) {
                        return true;
                    }

                }
                return false;
            },
            _addClassToMFP: function(cName) {
                mfp.bgOverlay.addClass(cName);
                mfp.wrap.addClass(cName);
            },
            _removeClassFromMFP: function(cName) {
                this.bgOverlay.removeClass(cName);
                mfp.wrap.removeClass(cName);
            },
            _hasScrollBar: function(winHeight) {
                return (  (mfp.isIE7 ? _document.height() : document.body.scrollHeight) > (winHeight || _window.height()) );
            },
            _setFocus: function() {
                (mfp.st.focus ? mfp.content.find(mfp.st.focus).eq(0) : mfp.wrap).focus();
            },
            _onFocusIn: function(e) {
                if( e.target !== mfp.wrap[0] && !$.contains(mfp.wrap[0], e.target) ) {
                    mfp._setFocus();
                    return false;
                }
            },
            _parseMarkup: function(template, values, item) {
                var arr;
                if(item.data) {
                    values = $.extend(item.data, values);
                }
                _mfpTrigger(MARKUP_PARSE_EVENT, [template, values, item] );

                $.each(values, function(key, value) {
                    if(value === undefined || value === false) {
                        return true;
                    }
                    arr = key.split('_');
                    if(arr.length > 1) {
                        var el = template.find(EVENT_NS + '-'+arr[0]);

                        if(el.length > 0) {
                            var attr = arr[1];
                            if(attr === 'replaceWith') {
                                if(el[0] !== value[0]) {
                                    el.replaceWith(value);
                                }
                            } else if(attr === 'img') {
                                if(el.is('img')) {
                                    el.attr('src', value);
                                } else {
                                    el.replaceWith( $('<img>').attr('src', value).attr('class', el.attr('class')) );
                                }
                            } else {
                                el.attr(arr[1], value);
                            }
                        }

                    } else {
                        template.find(EVENT_NS + '-'+key).html(value);
                    }
                });
            },

            _getScrollbarSize: function() {
                // thx David
                if(mfp.scrollbarSize === undefined) {
                    var scrollDiv = document.createElement("div");
                    scrollDiv.style.cssText = 'width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;';
                    document.body.appendChild(scrollDiv);
                    mfp.scrollbarSize = scrollDiv.offsetWidth - scrollDiv.clientWidth;
                    document.body.removeChild(scrollDiv);
                }
                return mfp.scrollbarSize;
            }

        }; /* MagnificPopup core prototype end */




        /**
         * Public static functions
         */
        $.magnificPopup = {
            instance: null,
            proto: MagnificPopup.prototype,
            modules: [],

            open: function(options, index) {
                _checkInstance();

                if(!options) {
                    options = {};
                } else {
                    options = $.extend(true, {}, options);
                }

                options.isObj = true;
                options.index = index || 0;
                return this.instance.open(options);
            },

            close: function() {
                return $.magnificPopup.instance && $.magnificPopup.instance.close();
            },

            registerModule: function(name, module) {
                if(module.options) {
                    $.magnificPopup.defaults[name] = module.options;
                }
                $.extend(this.proto, module.proto);
                this.modules.push(name);
            },

            defaults: {

                // Info about options is in docs:
                // http://dimsemenov.com/plugins/magnific-popup/documentation.html#options

                disableOn: 0,

                key: null,

                midClick: false,

                mainClass: '',

                preloader: true,

                focus: '', // CSS selector of input to focus after popup is opened

                closeOnContentClick: false,

                closeOnBgClick: true,

                closeBtnInside: true,

                showCloseBtn: true,

                enableEscapeKey: true,

                modal: false,

                alignTop: false,

                removalDelay: 0,

                prependTo: null,

                fixedContentPos: 'auto',

                fixedBgPos: 'auto',

                overflowY: 'auto',

                closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>',

                tClose: 'Close (Esc)',

                tLoading: 'Loading...',

                autoFocusLast: true

            }
        };



        $.fn.magnificPopup = function(options) {
            _checkInstance();

            var jqEl = $(this);

            // We call some API method of first param is a string
            if (typeof options === "string" ) {

                if(options === 'open') {
                    var items,
                        itemOpts = _isJQ ? jqEl.data('magnificPopup') : jqEl[0].magnificPopup,
                        index = parseInt(arguments[1], 10) || 0;

                    if(itemOpts.items) {
                        items = itemOpts.items[index];
                    } else {
                        items = jqEl;
                        if(itemOpts.delegate) {
                            items = items.find(itemOpts.delegate);
                        }
                        items = items.eq( index );
                    }
                    mfp._openClick({mfpEl:items}, jqEl, itemOpts);
                } else {
                    if(mfp.isOpen)
                        mfp[options].apply(mfp, Array.prototype.slice.call(arguments, 1));
                }

            } else {
                // clone options obj
                options = $.extend(true, {}, options);

                /*
                 * As Zepto doesn't support .data() method for objects
                 * and it works only in normal browsers
                 * we assign "options" object directly to the DOM element. FTW!
                 */
                if(_isJQ) {
                    jqEl.data('magnificPopup', options);
                } else {
                    jqEl[0].magnificPopup = options;
                }

                mfp.addGroup(jqEl, options);

            }
            return jqEl;
        };

        /*>>core*/

        /*>>inline*/

        var INLINE_NS = 'inline',
            _hiddenClass,
            _inlinePlaceholder,
            _lastInlineElement,
            _putInlineElementsBack = function() {
                if(_lastInlineElement) {
                    _inlinePlaceholder.after( _lastInlineElement.addClass(_hiddenClass) ).detach();
                    _lastInlineElement = null;
                }
            };

        $.magnificPopup.registerModule(INLINE_NS, {
            options: {
                hiddenClass: 'hide', // will be appended with `mfp-` prefix
                markup: '',
                tNotFound: 'Content not found'
            },
            proto: {

                initInline: function() {
                    mfp.types.push(INLINE_NS);

                    _mfpOn(CLOSE_EVENT+'.'+INLINE_NS, function() {
                        _putInlineElementsBack();
                    });
                },

                getInline: function(item, template) {

                    _putInlineElementsBack();

                    if(item.src) {
                        var inlineSt = mfp.st.inline,
                            el = $(item.src);

                        if(el.length) {

                            // If target element has parent - we replace it with placeholder and put it back after popup is closed
                            var parent = el[0].parentNode;
                            if(parent && parent.tagName) {
                                if(!_inlinePlaceholder) {
                                    _hiddenClass = inlineSt.hiddenClass;
                                    _inlinePlaceholder = _getEl(_hiddenClass);
                                    _hiddenClass = 'mfp-'+_hiddenClass;
                                }
                                // replace target inline element with placeholder
                                _lastInlineElement = el.after(_inlinePlaceholder).detach().removeClass(_hiddenClass);
                            }

                            mfp.updateStatus('ready');
                        } else {
                            mfp.updateStatus('error', inlineSt.tNotFound);
                            el = $('<div>');
                        }

                        item.inlineElement = el;
                        return el;
                    }

                    mfp.updateStatus('ready');
                    mfp._parseMarkup(template, {}, item);
                    return template;
                }
            }
        });

        /*>>inline*/

        /*>>ajax*/
        var AJAX_NS = 'ajax',
            _ajaxCur,
            _removeAjaxCursor = function() {
                if(_ajaxCur) {
                    $(document.body).removeClass(_ajaxCur);
                }
            },
            _destroyAjaxRequest = function() {
                _removeAjaxCursor();
                if(mfp.req) {
                    mfp.req.abort();
                }
            };

        $.magnificPopup.registerModule(AJAX_NS, {

            options: {
                settings: null,
                cursor: 'mfp-ajax-cur',
                tError: '<a href="%url%">The content</a> could not be loaded.'
            },

            proto: {
                initAjax: function() {
                    mfp.types.push(AJAX_NS);
                    _ajaxCur = mfp.st.ajax.cursor;

                    _mfpOn(CLOSE_EVENT+'.'+AJAX_NS, _destroyAjaxRequest);
                    _mfpOn('BeforeChange.' + AJAX_NS, _destroyAjaxRequest);
                },
                getAjax: function(item) {

                    if(_ajaxCur) {
                        $(document.body).addClass(_ajaxCur);
                    }

                    mfp.updateStatus('loading');

                    var opts = $.extend({
                        url: item.src,
                        success: function(data, textStatus, jqXHR) {
                            var temp = {
                                data:data,
                                xhr:jqXHR
                            };

                            _mfpTrigger('ParseAjax', temp);

                            mfp.appendContent( $(temp.data), AJAX_NS );

                            item.finished = true;

                            _removeAjaxCursor();

                            mfp._setFocus();

                            setTimeout(function() {
                                mfp.wrap.addClass(READY_CLASS);
                            }, 16);

                            mfp.updateStatus('ready');

                            _mfpTrigger('AjaxContentAdded');
                        },
                        error: function() {
                            _removeAjaxCursor();
                            item.finished = item.loadError = true;
                            mfp.updateStatus('error', mfp.st.ajax.tError.replace('%url%', item.src));
                        }
                    }, mfp.st.ajax.settings);

                    mfp.req = $.ajax(opts);

                    return '';
                }
            }
        });

        /*>>ajax*/

        /*>>image*/
        var _imgInterval,
            _getTitle = function(item) {
                if(item.data && item.data.title !== undefined)
                    return item.data.title;

                var src = mfp.st.image.titleSrc;

                if(src) {
                    if($.isFunction(src)) {
                        return src.call(mfp, item);
                    } else if(item.el) {
                        return item.el.attr(src) || '';
                    }
                }
                return '';
            };

        $.magnificPopup.registerModule('image', {

            options: {
                markup: '<div class="mfp-figure">'+
                '<div class="mfp-close"></div>'+
                '<figure>'+
                '<div class="mfp-img"></div>'+
                '<figcaption>'+
                '<div class="mfp-bottom-bar">'+
                '<div class="mfp-title"></div>'+
                '<div class="mfp-counter"></div>'+
                '</div>'+
                '</figcaption>'+
                '</figure>'+
                '</div>',
                cursor: 'mfp-zoom-out-cur',
                titleSrc: 'title',
                verticalFit: true,
                tError: '<a href="%url%">The image</a> could not be loaded.'
            },

            proto: {
                initImage: function() {
                    var imgSt = mfp.st.image,
                        ns = '.image';

                    mfp.types.push('image');

                    _mfpOn(OPEN_EVENT+ns, function() {
                        if(mfp.currItem.type === 'image' && imgSt.cursor) {
                            $(document.body).addClass(imgSt.cursor);
                        }
                    });

                    _mfpOn(CLOSE_EVENT+ns, function() {
                        if(imgSt.cursor) {
                            $(document.body).removeClass(imgSt.cursor);
                        }
                        _window.off('resize' + EVENT_NS);
                    });

                    _mfpOn('Resize'+ns, mfp.resizeImage);
                    if(mfp.isLowIE) {
                        _mfpOn('AfterChange', mfp.resizeImage);
                    }
                },
                resizeImage: function() {
                    var item = mfp.currItem;
                    if(!item || !item.img) return;

                    if(mfp.st.image.verticalFit) {
                        var decr = 0;
                        // fix box-sizing in ie7/8
                        if(mfp.isLowIE) {
                            decr = parseInt(item.img.css('padding-top'), 10) + parseInt(item.img.css('padding-bottom'),10);
                        }
                        item.img.css('max-height', mfp.wH-decr);
                    }
                },
                _onImageHasSize: function(item) {
                    if(item.img) {

                        item.hasSize = true;

                        if(_imgInterval) {
                            clearInterval(_imgInterval);
                        }

                        item.isCheckingImgSize = false;

                        _mfpTrigger('ImageHasSize', item);

                        if(item.imgHidden) {
                            if(mfp.content)
                                mfp.content.removeClass('mfp-loading');

                            item.imgHidden = false;
                        }

                    }
                },

                /**
                 * Function that loops until the image has size to display elements that rely on it asap
                 */
                findImageSize: function(item) {

                    var counter = 0,
                        img = item.img[0],
                        mfpSetInterval = function(delay) {

                            if(_imgInterval) {
                                clearInterval(_imgInterval);
                            }
                            // decelerating interval that checks for size of an image
                            _imgInterval = setInterval(function() {
                                if(img.naturalWidth > 0) {
                                    mfp._onImageHasSize(item);
                                    return;
                                }

                                if(counter > 200) {
                                    clearInterval(_imgInterval);
                                }

                                counter++;
                                if(counter === 3) {
                                    mfpSetInterval(10);
                                } else if(counter === 40) {
                                    mfpSetInterval(50);
                                } else if(counter === 100) {
                                    mfpSetInterval(500);
                                }
                            }, delay);
                        };

                    mfpSetInterval(1);
                },

                getImage: function(item, template) {

                    var guard = 0,

                    // image load complete handler
                        onLoadComplete = function() {
                            if(item) {
                                if (item.img[0].complete) {
                                    item.img.off('.mfploader');

                                    if(item === mfp.currItem){
                                        mfp._onImageHasSize(item);

                                        mfp.updateStatus('ready');
                                    }

                                    item.hasSize = true;
                                    item.loaded = true;

                                    _mfpTrigger('ImageLoadComplete');

                                }
                                else {
                                    // if image complete check fails 200 times (20 sec), we assume that there was an error.
                                    guard++;
                                    if(guard < 200) {
                                        setTimeout(onLoadComplete,100);
                                    } else {
                                        onLoadError();
                                    }
                                }
                            }
                        },

                    // image error handler
                        onLoadError = function() {
                            if(item) {
                                item.img.off('.mfploader');
                                if(item === mfp.currItem){
                                    mfp._onImageHasSize(item);
                                    mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src) );
                                }

                                item.hasSize = true;
                                item.loaded = true;
                                item.loadError = true;
                            }
                        },
                        imgSt = mfp.st.image;


                    var el = template.find('.mfp-img');
                    if(el.length) {
                        var img = document.createElement('img');
                        img.className = 'mfp-img';
                        if(item.el && item.el.find('img').length) {
                            img.alt = item.el.find('img').attr('alt');
                        }
                        item.img = $(img).on('load.mfploader', onLoadComplete).on('error.mfploader', onLoadError);
                        img.src = item.src;

                        // without clone() "error" event is not firing when IMG is replaced by new IMG
                        // TODO: find a way to avoid such cloning
                        if(el.is('img')) {
                            item.img = item.img.clone();
                        }

                        img = item.img[0];
                        if(img.naturalWidth > 0) {
                            item.hasSize = true;
                        } else if(!img.width) {
                            item.hasSize = false;
                        }
                    }

                    mfp._parseMarkup(template, {
                        title: _getTitle(item),
                        img_replaceWith: item.img
                    }, item);

                    mfp.resizeImage();

                    if(item.hasSize) {
                        if(_imgInterval) clearInterval(_imgInterval);

                        if(item.loadError) {
                            template.addClass('mfp-loading');
                            mfp.updateStatus('error', imgSt.tError.replace('%url%', item.src) );
                        } else {
                            template.removeClass('mfp-loading');
                            mfp.updateStatus('ready');
                        }
                        return template;
                    }

                    mfp.updateStatus('loading');
                    item.loading = true;

                    if(!item.hasSize) {
                        item.imgHidden = true;
                        template.addClass('mfp-loading');
                        mfp.findImageSize(item);
                    }

                    return template;
                }
            }
        });

        /*>>image*/

        /*>>zoom*/
        var hasMozTransform,
            getHasMozTransform = function() {
                if(hasMozTransform === undefined) {
                    hasMozTransform = document.createElement('p').style.MozTransform !== undefined;
                }
                return hasMozTransform;
            };

        $.magnificPopup.registerModule('zoom', {

            options: {
                enabled: false,
                easing: 'ease-in-out',
                duration: 300,
                opener: function(element) {
                    return element.is('img') ? element : element.find('img');
                }
            },

            proto: {

                initZoom: function() {
                    var zoomSt = mfp.st.zoom,
                        ns = '.zoom',
                        image;

                    if(!zoomSt.enabled || !mfp.supportsTransition) {
                        return;
                    }

                    var duration = zoomSt.duration,
                        getElToAnimate = function(image) {
                            var newImg = image.clone().removeAttr('style').removeAttr('class').addClass('mfp-animated-image'),
                                transition = 'all '+(zoomSt.duration/1000)+'s ' + zoomSt.easing,
                                cssObj = {
                                    position: 'fixed',
                                    zIndex: 9999,
                                    left: 0,
                                    top: 0,
                                    '-webkit-backface-visibility': 'hidden'
                                },
                                t = 'transition';

                            cssObj['-webkit-'+t] = cssObj['-moz-'+t] = cssObj['-o-'+t] = cssObj[t] = transition;

                            newImg.css(cssObj);
                            return newImg;
                        },
                        showMainContent = function() {
                            mfp.content.css('visibility', 'visible');
                        },
                        openTimeout,
                        animatedImg;

                    _mfpOn('BuildControls'+ns, function() {
                        if(mfp._allowZoom()) {

                            clearTimeout(openTimeout);
                            mfp.content.css('visibility', 'hidden');

                            // Basically, all code below does is clones existing image, puts in on top of the current one and animated it

                            image = mfp._getItemToZoom();

                            if(!image) {
                                showMainContent();
                                return;
                            }

                            animatedImg = getElToAnimate(image);

                            animatedImg.css( mfp._getOffset() );

                            mfp.wrap.append(animatedImg);

                            openTimeout = setTimeout(function() {
                                animatedImg.css( mfp._getOffset( true ) );
                                openTimeout = setTimeout(function() {

                                    showMainContent();

                                    setTimeout(function() {
                                        animatedImg.remove();
                                        image = animatedImg = null;
                                        _mfpTrigger('ZoomAnimationEnded');
                                    }, 16); // avoid blink when switching images

                                }, duration); // this timeout equals animation duration

                            }, 16); // by adding this timeout we avoid short glitch at the beginning of animation


                            // Lots of timeouts...
                        }
                    });
                    _mfpOn(BEFORE_CLOSE_EVENT+ns, function() {
                        if(mfp._allowZoom()) {

                            clearTimeout(openTimeout);

                            mfp.st.removalDelay = duration;

                            if(!image) {
                                image = mfp._getItemToZoom();
                                if(!image) {
                                    return;
                                }
                                animatedImg = getElToAnimate(image);
                            }

                            animatedImg.css( mfp._getOffset(true) );
                            mfp.wrap.append(animatedImg);
                            mfp.content.css('visibility', 'hidden');

                            setTimeout(function() {
                                animatedImg.css( mfp._getOffset() );
                            }, 16);
                        }

                    });

                    _mfpOn(CLOSE_EVENT+ns, function() {
                        if(mfp._allowZoom()) {
                            showMainContent();
                            if(animatedImg) {
                                animatedImg.remove();
                            }
                            image = null;
                        }
                    });
                },

                _allowZoom: function() {
                    return mfp.currItem.type === 'image';
                },

                _getItemToZoom: function() {
                    if(mfp.currItem.hasSize) {
                        return mfp.currItem.img;
                    } else {
                        return false;
                    }
                },

                // Get element postion relative to viewport
                _getOffset: function(isLarge) {
                    var el;
                    if(isLarge) {
                        el = mfp.currItem.img;
                    } else {
                        el = mfp.st.zoom.opener(mfp.currItem.el || mfp.currItem);
                    }

                    var offset = el.offset();
                    var paddingTop = parseInt(el.css('padding-top'),10);
                    var paddingBottom = parseInt(el.css('padding-bottom'),10);
                    offset.top -= ( $(window).scrollTop() - paddingTop );


                    /*

                     Animating left + top + width/height looks glitchy in Firefox, but perfect in Chrome. And vice-versa.

                     */
                    var obj = {
                        width: el.width(),
                        // fix Zepto height+padding issue
                        height: (_isJQ ? el.innerHeight() : el[0].offsetHeight) - paddingBottom - paddingTop
                    };

                    // I hate to do this, but there is no another option
                    if( getHasMozTransform() ) {
                        obj['-moz-transform'] = obj['transform'] = 'translate(' + offset.left + 'px,' + offset.top + 'px)';
                    } else {
                        obj.left = offset.left;
                        obj.top = offset.top;
                    }
                    return obj;
                }

            }
        });



        /*>>zoom*/

        /*>>iframe*/

        var IFRAME_NS = 'iframe',
            _emptyPage = '//about:blank',

            _fixIframeBugs = function(isShowing) {
                if(mfp.currTemplate[IFRAME_NS]) {
                    var el = mfp.currTemplate[IFRAME_NS].find('iframe');
                    if(el.length) {
                        // reset src after the popup is closed to avoid "video keeps playing after popup is closed" bug
                        if(!isShowing) {
                            el[0].src = _emptyPage;
                        }

                        // IE8 black screen bug fix
                        if(mfp.isIE8) {
                            el.css('display', isShowing ? 'block' : 'none');
                        }
                    }
                }
            };

        $.magnificPopup.registerModule(IFRAME_NS, {

            options: {
                markup: '<div class="mfp-iframe-scaler">'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe>'+
                '</div>',

                srcAction: 'iframe_src',

                // we don't care and support only one default type of URL by default
                patterns: {
                    youtube: {
                        index: 'youtube.com',
                        id: 'v=',
                        src: '//www.youtube.com/embed/%id%?autoplay=1'
                    },
                    vimeo: {
                        index: 'vimeo.com/',
                        id: '/',
                        src: '//player.vimeo.com/video/%id%?autoplay=1'
                    },
                    gmaps: {
                        index: '//maps.google.',
                        src: '%id%&output=embed'
                    }
                }
            },

            proto: {
                initIframe: function() {
                    mfp.types.push(IFRAME_NS);

                    _mfpOn('BeforeChange', function(e, prevType, newType) {
                        if(prevType !== newType) {
                            if(prevType === IFRAME_NS) {
                                _fixIframeBugs(); // iframe if removed
                            } else if(newType === IFRAME_NS) {
                                _fixIframeBugs(true); // iframe is showing
                            }
                        }// else {
                        // iframe source is switched, don't do anything
                        //}
                    });

                    _mfpOn(CLOSE_EVENT + '.' + IFRAME_NS, function() {
                        _fixIframeBugs();
                    });
                },

                getIframe: function(item, template) {
                    var embedSrc = item.src;
                    var iframeSt = mfp.st.iframe;

                    $.each(iframeSt.patterns, function() {
                        if(embedSrc.indexOf( this.index ) > -1) {
                            if(this.id) {
                                if(typeof this.id === 'string') {
                                    embedSrc = embedSrc.substr(embedSrc.lastIndexOf(this.id)+this.id.length, embedSrc.length);
                                } else {
                                    embedSrc = this.id.call( this, embedSrc );
                                }
                            }
                            embedSrc = this.src.replace('%id%', embedSrc );
                            return false; // break;
                        }
                    });

                    var dataObj = {};
                    if(iframeSt.srcAction) {
                        dataObj[iframeSt.srcAction] = embedSrc;
                    }
                    mfp._parseMarkup(template, dataObj, item);

                    mfp.updateStatus('ready');

                    return template;
                }
            }
        });



        /*>>iframe*/

        /*>>gallery*/
        /**
         * Get looped index depending on number of slides
         */
        var _getLoopedId = function(index) {
                var numSlides = mfp.items.length;
                if(index > numSlides - 1) {
                    return index - numSlides;
                } else  if(index < 0) {
                    return numSlides + index;
                }
                return index;
            },
            _replaceCurrTotal = function(text, curr, total) {
                return text.replace(/%curr%/gi, curr + 1).replace(/%total%/gi, total);
            };

        $.magnificPopup.registerModule('gallery', {

            options: {
                enabled: false,
                arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
                preload: [0,2],
                navigateByImgClick: true,
                arrows: true,

                tPrev: 'Previous (Left arrow key)',
                tNext: 'Next (Right arrow key)',
                tCounter: '%curr% of %total%'
            },

            proto: {
                initGallery: function() {

                    var gSt = mfp.st.gallery,
                        ns = '.mfp-gallery';

                    mfp.direction = true; // true - next, false - prev

                    if(!gSt || !gSt.enabled ) return false;

                    _wrapClasses += ' mfp-gallery';

                    _mfpOn(OPEN_EVENT+ns, function() {

                        if(gSt.navigateByImgClick) {
                            mfp.wrap.on('click'+ns, '.mfp-img', function() {
                                if(mfp.items.length > 1) {
                                    mfp.next();
                                    return false;
                                }
                            });
                        }

                        _document.on('keydown'+ns, function(e) {
                            if (e.keyCode === 37) {
                                mfp.prev();
                            } else if (e.keyCode === 39) {
                                mfp.next();
                            }
                        });
                    });

                    _mfpOn('UpdateStatus'+ns, function(e, data) {
                        if(data.text) {
                            data.text = _replaceCurrTotal(data.text, mfp.currItem.index, mfp.items.length);
                        }
                    });

                    _mfpOn(MARKUP_PARSE_EVENT+ns, function(e, element, values, item) {
                        var l = mfp.items.length;
                        values.counter = l > 1 ? _replaceCurrTotal(gSt.tCounter, item.index, l) : '';
                    });

                    _mfpOn('BuildControls' + ns, function() {
                        if(mfp.items.length > 1 && gSt.arrows && !mfp.arrowLeft) {
                            var markup = gSt.arrowMarkup,
                                arrowLeft = mfp.arrowLeft = $( markup.replace(/%title%/gi, gSt.tPrev).replace(/%dir%/gi, 'left') ).addClass(PREVENT_CLOSE_CLASS),
                                arrowRight = mfp.arrowRight = $( markup.replace(/%title%/gi, gSt.tNext).replace(/%dir%/gi, 'right') ).addClass(PREVENT_CLOSE_CLASS);

                            arrowLeft.click(function() {
                                mfp.prev();
                            });
                            arrowRight.click(function() {
                                mfp.next();
                            });

                            mfp.container.append(arrowLeft.add(arrowRight));
                        }
                    });

                    _mfpOn(CHANGE_EVENT+ns, function() {
                        if(mfp._preloadTimeout) clearTimeout(mfp._preloadTimeout);

                        mfp._preloadTimeout = setTimeout(function() {
                            mfp.preloadNearbyImages();
                            mfp._preloadTimeout = null;
                        }, 16);
                    });


                    _mfpOn(CLOSE_EVENT+ns, function() {
                        _document.off(ns);
                        mfp.wrap.off('click'+ns);
                        mfp.arrowRight = mfp.arrowLeft = null;
                    });

                },
                next: function() {
                    mfp.direction = true;
                    mfp.index = _getLoopedId(mfp.index + 1);
                    mfp.updateItemHTML();
                },
                prev: function() {
                    mfp.direction = false;
                    mfp.index = _getLoopedId(mfp.index - 1);
                    mfp.updateItemHTML();
                },
                goTo: function(newIndex) {
                    mfp.direction = (newIndex >= mfp.index);
                    mfp.index = newIndex;
                    mfp.updateItemHTML();
                },
                preloadNearbyImages: function() {
                    var p = mfp.st.gallery.preload,
                        preloadBefore = Math.min(p[0], mfp.items.length),
                        preloadAfter = Math.min(p[1], mfp.items.length),
                        i;

                    for(i = 1; i <= (mfp.direction ? preloadAfter : preloadBefore); i++) {
                        mfp._preloadItem(mfp.index+i);
                    }
                    for(i = 1; i <= (mfp.direction ? preloadBefore : preloadAfter); i++) {
                        mfp._preloadItem(mfp.index-i);
                    }
                },
                _preloadItem: function(index) {
                    index = _getLoopedId(index);

                    if(mfp.items[index].preloaded) {
                        return;
                    }

                    var item = mfp.items[index];
                    if(!item.parsed) {
                        item = mfp.parseEl( index );
                    }

                    _mfpTrigger('LazyLoad', item);

                    if(item.type === 'image') {
                        item.img = $('<img class="mfp-img" />').on('load.mfploader', function() {
                            item.hasSize = true;
                        }).on('error.mfploader', function() {
                            item.hasSize = true;
                            item.loadError = true;
                            _mfpTrigger('LazyLoadError', item);
                        }).attr('src', item.src);
                    }


                    item.preloaded = true;
                }
            }
        });

        /*>>gallery*/

        /*>>retina*/

        var RETINA_NS = 'retina';

        $.magnificPopup.registerModule(RETINA_NS, {
            options: {
                replaceSrc: function(item) {
                    return item.src.replace(/\.\w+$/, function(m) { return '@2x' + m; });
                },
                ratio: 1 // Function or number.  Set to 1 to disable.
            },
            proto: {
                initRetina: function() {
                    if(window.devicePixelRatio > 1) {

                        var st = mfp.st.retina,
                            ratio = st.ratio;

                        ratio = !isNaN(ratio) ? ratio : ratio();

                        if(ratio > 1) {
                            _mfpOn('ImageHasSize' + '.' + RETINA_NS, function(e, item) {
                                item.img.css({
                                    'max-width': item.img[0].naturalWidth / ratio,
                                    'width': '100%'
                                });
                            });
                            _mfpOn('ElementParse' + '.' + RETINA_NS, function(e, item) {
                                item.src = st.replaceSrc(item, ratio);
                            });
                        }
                    }

                }
            }
        });

        /*>>retina*/
        _checkInstance(); }));




    /*!
     * Isotope PACKAGED v3.0.4
     *
     * Licensed GPLv3 for open source use
     * or Isotope Commercial License for commercial use
     *
     * http://isotope.metafizzy.co
     * Copyright 2017 Metafizzy
     */

    !function(t,e){"function"==typeof define&&define.amd?define("jquery-bridget/jquery-bridget",["jquery"],function(i){return e(t,i)}):"object"==typeof module&&module.exports?module.exports=e(t,require("jquery")):t.jQueryBridget=e(t,t.jQuery)}(window,function(t,e){"use strict";function i(i,s,a){function u(t,e,o){var n,s="$()."+i+'("'+e+'")';return t.each(function(t,u){var h=a.data(u,i);if(!h)return void r(i+" not initialized. Cannot call methods, i.e. "+s);var d=h[e];if(!d||"_"==e.charAt(0))return void r(s+" is not a valid method");var l=d.apply(h,o);n=void 0===n?l:n}),void 0!==n?n:t}function h(t,e){t.each(function(t,o){var n=a.data(o,i);n?(n.option(e),n._init()):(n=new s(o,e),a.data(o,i,n))})}a=a||e||t.jQuery,a&&(s.prototype.option||(s.prototype.option=function(t){a.isPlainObject(t)&&(this.options=a.extend(!0,this.options,t))}),a.fn[i]=function(t){if("string"==typeof t){var e=n.call(arguments,1);return u(this,t,e)}return h(this,t),this},o(a))}function o(t){!t||t&&t.bridget||(t.bridget=i)}var n=Array.prototype.slice,s=t.console,r="undefined"==typeof s?function(){}:function(t){s.error(t)};return o(e||t.jQuery),i}),function(t,e){"function"==typeof define&&define.amd?define("ev-emitter/ev-emitter",e):"object"==typeof module&&module.exports?module.exports=e():t.EvEmitter=e()}("undefined"!=typeof window?window:this,function(){function t(){}var e=t.prototype;return e.on=function(t,e){if(t&&e){var i=this._events=this._events||{},o=i[t]=i[t]||[];return o.indexOf(e)==-1&&o.push(e),this}},e.once=function(t,e){if(t&&e){this.on(t,e);var i=this._onceEvents=this._onceEvents||{},o=i[t]=i[t]||{};return o[e]=!0,this}},e.off=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){var o=i.indexOf(e);return o!=-1&&i.splice(o,1),this}},e.emitEvent=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){var o=0,n=i[o];e=e||[];for(var s=this._onceEvents&&this._onceEvents[t];n;){var r=s&&s[n];r&&(this.off(t,n),delete s[n]),n.apply(this,e),o+=r?0:1,n=i[o]}return this}},t}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("get-size/get-size",[],function(){return e()}):"object"==typeof module&&module.exports?module.exports=e():t.getSize=e()}(window,function(){"use strict";function t(t){var e=parseFloat(t),i=t.indexOf("%")==-1&&!isNaN(e);return i&&e}function e(){}function i(){for(var t={width:0,height:0,innerWidth:0,innerHeight:0,outerWidth:0,outerHeight:0},e=0;e<h;e++){var i=u[e];t[i]=0}return t}function o(t){var e=getComputedStyle(t);return e||a("Style returned "+e+". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"),e}function n(){if(!d){d=!0;var e=document.createElement("div");e.style.width="200px",e.style.padding="1px 2px 3px 4px",e.style.borderStyle="solid",e.style.borderWidth="1px 2px 3px 4px",e.style.boxSizing="border-box";var i=document.body||document.documentElement;i.appendChild(e);var n=o(e);s.isBoxSizeOuter=r=200==t(n.width),i.removeChild(e)}}function s(e){if(n(),"string"==typeof e&&(e=document.querySelector(e)),e&&"object"==typeof e&&e.nodeType){var s=o(e);if("none"==s.display)return i();var a={};a.width=e.offsetWidth,a.height=e.offsetHeight;for(var d=a.isBorderBox="border-box"==s.boxSizing,l=0;l<h;l++){var f=u[l],c=s[f],m=parseFloat(c);a[f]=isNaN(m)?0:m}var p=a.paddingLeft+a.paddingRight,y=a.paddingTop+a.paddingBottom,g=a.marginLeft+a.marginRight,v=a.marginTop+a.marginBottom,_=a.borderLeftWidth+a.borderRightWidth,I=a.borderTopWidth+a.borderBottomWidth,z=d&&r,x=t(s.width);x!==!1&&(a.width=x+(z?0:p+_));var S=t(s.height);return S!==!1&&(a.height=S+(z?0:y+I)),a.innerWidth=a.width-(p+_),a.innerHeight=a.height-(y+I),a.outerWidth=a.width+g,a.outerHeight=a.height+v,a}}var r,a="undefined"==typeof console?e:function(t){console.error(t)},u=["paddingLeft","paddingRight","paddingTop","paddingBottom","marginLeft","marginRight","marginTop","marginBottom","borderLeftWidth","borderRightWidth","borderTopWidth","borderBottomWidth"],h=u.length,d=!1;return s}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("desandro-matches-selector/matches-selector",e):"object"==typeof module&&module.exports?module.exports=e():t.matchesSelector=e()}(window,function(){"use strict";var t=function(){var t=window.Element.prototype;if(t.matches)return"matches";if(t.matchesSelector)return"matchesSelector";for(var e=["webkit","moz","ms","o"],i=0;i<e.length;i++){var o=e[i],n=o+"MatchesSelector";if(t[n])return n}}();return function(e,i){return e[t](i)}}),function(t,e){"function"==typeof define&&define.amd?define("fizzy-ui-utils/utils",["desandro-matches-selector/matches-selector"],function(i){return e(t,i)}):"object"==typeof module&&module.exports?module.exports=e(t,require("desandro-matches-selector")):t.fizzyUIUtils=e(t,t.matchesSelector)}(window,function(t,e){var i={};i.extend=function(t,e){for(var i in e)t[i]=e[i];return t},i.modulo=function(t,e){return(t%e+e)%e},i.makeArray=function(t){var e=[];if(Array.isArray(t))e=t;else if(t&&"object"==typeof t&&"number"==typeof t.length)for(var i=0;i<t.length;i++)e.push(t[i]);else e.push(t);return e},i.removeFrom=function(t,e){var i=t.indexOf(e);i!=-1&&t.splice(i,1)},i.getParent=function(t,i){for(;t.parentNode&&t!=document.body;)if(t=t.parentNode,e(t,i))return t},i.getQueryElement=function(t){return"string"==typeof t?document.querySelector(t):t},i.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},i.filterFindElements=function(t,o){t=i.makeArray(t);var n=[];return t.forEach(function(t){if(t instanceof HTMLElement){if(!o)return void n.push(t);e(t,o)&&n.push(t);for(var i=t.querySelectorAll(o),s=0;s<i.length;s++)n.push(i[s])}}),n},i.debounceMethod=function(t,e,i){var o=t.prototype[e],n=e+"Timeout";t.prototype[e]=function(){var t=this[n];t&&clearTimeout(t);var e=arguments,s=this;this[n]=setTimeout(function(){o.apply(s,e),delete s[n]},i||100)}},i.docReady=function(t){var e=document.readyState;"complete"==e||"interactive"==e?setTimeout(t):document.addEventListener("DOMContentLoaded",t)},i.toDashed=function(t){return t.replace(/(.)([A-Z])/g,function(t,e,i){return e+"-"+i}).toLowerCase()};var o=t.console;return i.htmlInit=function(e,n){i.docReady(function(){var s=i.toDashed(n),r="data-"+s,a=document.querySelectorAll("["+r+"]"),u=document.querySelectorAll(".js-"+s),h=i.makeArray(a).concat(i.makeArray(u)),d=r+"-options",l=t.jQuery;h.forEach(function(t){var i,s=t.getAttribute(r)||t.getAttribute(d);try{i=s&&JSON.parse(s)}catch(a){return void(o&&o.error("Error parsing "+r+" on "+t.className+": "+a))}var u=new e(t,i);l&&l.data(t,n,u)})})},i}),function(t,e){"function"==typeof define&&define.amd?define("outlayer/item",["ev-emitter/ev-emitter","get-size/get-size"],e):"object"==typeof module&&module.exports?module.exports=e(require("ev-emitter"),require("get-size")):(t.Outlayer={},t.Outlayer.Item=e(t.EvEmitter,t.getSize))}(window,function(t,e){"use strict";function i(t){for(var e in t)return!1;return e=null,!0}function o(t,e){t&&(this.element=t,this.layout=e,this.position={x:0,y:0},this._create())}function n(t){return t.replace(/([A-Z])/g,function(t){return"-"+t.toLowerCase()})}var s=document.documentElement.style,r="string"==typeof s.transition?"transition":"WebkitTransition",a="string"==typeof s.transform?"transform":"WebkitTransform",u={WebkitTransition:"webkitTransitionEnd",transition:"transitionend"}[r],h={transform:a,transition:r,transitionDuration:r+"Duration",transitionProperty:r+"Property",transitionDelay:r+"Delay"},d=o.prototype=Object.create(t.prototype);d.constructor=o,d._create=function(){this._transn={ingProperties:{},clean:{},onEnd:{}},this.css({position:"absolute"})},d.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},d.getSize=function(){this.size=e(this.element)},d.css=function(t){var e=this.element.style;for(var i in t){var o=h[i]||i;e[o]=t[i]}},d.getPosition=function(){var t=getComputedStyle(this.element),e=this.layout._getOption("originLeft"),i=this.layout._getOption("originTop"),o=t[e?"left":"right"],n=t[i?"top":"bottom"],s=this.layout.size,r=o.indexOf("%")!=-1?parseFloat(o)/100*s.width:parseInt(o,10),a=n.indexOf("%")!=-1?parseFloat(n)/100*s.height:parseInt(n,10);r=isNaN(r)?0:r,a=isNaN(a)?0:a,r-=e?s.paddingLeft:s.paddingRight,a-=i?s.paddingTop:s.paddingBottom,this.position.x=r,this.position.y=a},d.layoutPosition=function(){var t=this.layout.size,e={},i=this.layout._getOption("originLeft"),o=this.layout._getOption("originTop"),n=i?"paddingLeft":"paddingRight",s=i?"left":"right",r=i?"right":"left",a=this.position.x+t[n];e[s]=this.getXValue(a),e[r]="";var u=o?"paddingTop":"paddingBottom",h=o?"top":"bottom",d=o?"bottom":"top",l=this.position.y+t[u];e[h]=this.getYValue(l),e[d]="",this.css(e),this.emitEvent("layout",[this])},d.getXValue=function(t){var e=this.layout._getOption("horizontal");return this.layout.options.percentPosition&&!e?t/this.layout.size.width*100+"%":t+"px"},d.getYValue=function(t){var e=this.layout._getOption("horizontal");return this.layout.options.percentPosition&&e?t/this.layout.size.height*100+"%":t+"px"},d._transitionTo=function(t,e){this.getPosition();var i=this.position.x,o=this.position.y,n=parseInt(t,10),s=parseInt(e,10),r=n===this.position.x&&s===this.position.y;if(this.setPosition(t,e),r&&!this.isTransitioning)return void this.layoutPosition();var a=t-i,u=e-o,h={};h.transform=this.getTranslate(a,u),this.transition({to:h,onTransitionEnd:{transform:this.layoutPosition},isCleaning:!0})},d.getTranslate=function(t,e){var i=this.layout._getOption("originLeft"),o=this.layout._getOption("originTop");return t=i?t:-t,e=o?e:-e,"translate3d("+t+"px, "+e+"px, 0)"},d.goTo=function(t,e){this.setPosition(t,e),this.layoutPosition()},d.moveTo=d._transitionTo,d.setPosition=function(t,e){this.position.x=parseInt(t,10),this.position.y=parseInt(e,10)},d._nonTransition=function(t){this.css(t.to),t.isCleaning&&this._removeStyles(t.to);for(var e in t.onTransitionEnd)t.onTransitionEnd[e].call(this)},d.transition=function(t){if(!parseFloat(this.layout.options.transitionDuration))return void this._nonTransition(t);var e=this._transn;for(var i in t.onTransitionEnd)e.onEnd[i]=t.onTransitionEnd[i];for(i in t.to)e.ingProperties[i]=!0,t.isCleaning&&(e.clean[i]=!0);if(t.from){this.css(t.from);var o=this.element.offsetHeight;o=null}this.enableTransition(t.to),this.css(t.to),this.isTransitioning=!0};var l="opacity,"+n(a);d.enableTransition=function(){if(!this.isTransitioning){var t=this.layout.options.transitionDuration;t="number"==typeof t?t+"ms":t,this.css({transitionProperty:l,transitionDuration:t,transitionDelay:this.staggerDelay||0}),this.element.addEventListener(u,this,!1)}},d.onwebkitTransitionEnd=function(t){this.ontransitionend(t)},d.onotransitionend=function(t){this.ontransitionend(t)};var f={"-webkit-transform":"transform"};d.ontransitionend=function(t){if(t.target===this.element){var e=this._transn,o=f[t.propertyName]||t.propertyName;if(delete e.ingProperties[o],i(e.ingProperties)&&this.disableTransition(),o in e.clean&&(this.element.style[t.propertyName]="",delete e.clean[o]),o in e.onEnd){var n=e.onEnd[o];n.call(this),delete e.onEnd[o]}this.emitEvent("transitionEnd",[this])}},d.disableTransition=function(){this.removeTransitionStyles(),this.element.removeEventListener(u,this,!1),this.isTransitioning=!1},d._removeStyles=function(t){var e={};for(var i in t)e[i]="";this.css(e)};var c={transitionProperty:"",transitionDuration:"",transitionDelay:""};return d.removeTransitionStyles=function(){this.css(c)},d.stagger=function(t){t=isNaN(t)?0:t,this.staggerDelay=t+"ms"},d.removeElem=function(){this.element.parentNode.removeChild(this.element),this.css({display:""}),this.emitEvent("remove",[this])},d.remove=function(){return r&&parseFloat(this.layout.options.transitionDuration)?(this.once("transitionEnd",function(){this.removeElem()}),void this.hide()):void this.removeElem()},d.reveal=function(){delete this.isHidden,this.css({display:""});var t=this.layout.options,e={},i=this.getHideRevealTransitionEndProperty("visibleStyle");e[i]=this.onRevealTransitionEnd,this.transition({from:t.hiddenStyle,to:t.visibleStyle,isCleaning:!0,onTransitionEnd:e})},d.onRevealTransitionEnd=function(){this.isHidden||this.emitEvent("reveal")},d.getHideRevealTransitionEndProperty=function(t){var e=this.layout.options[t];if(e.opacity)return"opacity";for(var i in e)return i},d.hide=function(){this.isHidden=!0,this.css({display:""});var t=this.layout.options,e={},i=this.getHideRevealTransitionEndProperty("hiddenStyle");e[i]=this.onHideTransitionEnd,this.transition({from:t.visibleStyle,to:t.hiddenStyle,isCleaning:!0,onTransitionEnd:e})},d.onHideTransitionEnd=function(){this.isHidden&&(this.css({display:"none"}),this.emitEvent("hide"))},d.destroy=function(){this.css({position:"",left:"",right:"",top:"",bottom:"",transition:"",transform:""})},o}),function(t,e){"use strict";"function"==typeof define&&define.amd?define("outlayer/outlayer",["ev-emitter/ev-emitter","get-size/get-size","fizzy-ui-utils/utils","./item"],function(i,o,n,s){return e(t,i,o,n,s)}):"object"==typeof module&&module.exports?module.exports=e(t,require("ev-emitter"),require("get-size"),require("fizzy-ui-utils"),require("./item")):t.Outlayer=e(t,t.EvEmitter,t.getSize,t.fizzyUIUtils,t.Outlayer.Item)}(window,function(t,e,i,o,n){"use strict";function s(t,e){var i=o.getQueryElement(t);if(!i)return void(u&&u.error("Bad element for "+this.constructor.namespace+": "+(i||t)));this.element=i,h&&(this.$element=h(this.element)),this.options=o.extend({},this.constructor.defaults),this.option(e);var n=++l;this.element.outlayerGUID=n,f[n]=this,this._create();var s=this._getOption("initLayout");s&&this.layout()}function r(t){function e(){t.apply(this,arguments)}return e.prototype=Object.create(t.prototype),e.prototype.constructor=e,e}function a(t){if("number"==typeof t)return t;var e=t.match(/(^\d*\.?\d*)(\w*)/),i=e&&e[1],o=e&&e[2];if(!i.length)return 0;i=parseFloat(i);var n=m[o]||1;return i*n}var u=t.console,h=t.jQuery,d=function(){},l=0,f={};s.namespace="outlayer",s.Item=n,s.defaults={containerStyle:{position:"relative"},initLayout:!0,originLeft:!0,originTop:!0,resize:!0,resizeContainer:!0,transitionDuration:"0.4s",hiddenStyle:{opacity:0,transform:"scale(0.001)"},visibleStyle:{opacity:1,transform:"scale(1)"}};var c=s.prototype;o.extend(c,e.prototype),c.option=function(t){o.extend(this.options,t)},c._getOption=function(t){var e=this.constructor.compatOptions[t];return e&&void 0!==this.options[e]?this.options[e]:this.options[t]},s.compatOptions={initLayout:"isInitLayout",horizontal:"isHorizontal",layoutInstant:"isLayoutInstant",originLeft:"isOriginLeft",originTop:"isOriginTop",resize:"isResizeBound",resizeContainer:"isResizingContainer"},c._create=function(){this.reloadItems(),this.stamps=[],this.stamp(this.options.stamp),o.extend(this.element.style,this.options.containerStyle);var t=this._getOption("resize");t&&this.bindResize()},c.reloadItems=function(){this.items=this._itemize(this.element.children)},c._itemize=function(t){for(var e=this._filterFindItemElements(t),i=this.constructor.Item,o=[],n=0;n<e.length;n++){var s=e[n],r=new i(s,this);o.push(r)}return o},c._filterFindItemElements=function(t){return o.filterFindElements(t,this.options.itemSelector)},c.getItemElements=function(){return this.items.map(function(t){return t.element})},c.layout=function(){this._resetLayout(),this._manageStamps();var t=this._getOption("layoutInstant"),e=void 0!==t?t:!this._isLayoutInited;this.layoutItems(this.items,e),this._isLayoutInited=!0},c._init=c.layout,c._resetLayout=function(){this.getSize()},c.getSize=function(){this.size=i(this.element)},c._getMeasurement=function(t,e){var o,n=this.options[t];n?("string"==typeof n?o=this.element.querySelector(n):n instanceof HTMLElement&&(o=n),this[t]=o?i(o)[e]:n):this[t]=0},c.layoutItems=function(t,e){t=this._getItemsForLayout(t),this._layoutItems(t,e),this._postLayout()},c._getItemsForLayout=function(t){return t.filter(function(t){return!t.isIgnored})},c._layoutItems=function(t,e){if(this._emitCompleteOnItems("layout",t),t&&t.length){var i=[];t.forEach(function(t){var o=this._getItemLayoutPosition(t);o.item=t,o.isInstant=e||t.isLayoutInstant,i.push(o)},this),this._processLayoutQueue(i)}},c._getItemLayoutPosition=function(){return{x:0,y:0}},c._processLayoutQueue=function(t){this.updateStagger(),t.forEach(function(t,e){this._positionItem(t.item,t.x,t.y,t.isInstant,e)},this)},c.updateStagger=function(){var t=this.options.stagger;return null===t||void 0===t?void(this.stagger=0):(this.stagger=a(t),this.stagger)},c._positionItem=function(t,e,i,o,n){o?t.goTo(e,i):(t.stagger(n*this.stagger),t.moveTo(e,i))},c._postLayout=function(){this.resizeContainer()},c.resizeContainer=function(){var t=this._getOption("resizeContainer");if(t){var e=this._getContainerSize();e&&(this._setContainerMeasure(e.width,!0),this._setContainerMeasure(e.height,!1))}},c._getContainerSize=d,c._setContainerMeasure=function(t,e){if(void 0!==t){var i=this.size;i.isBorderBox&&(t+=e?i.paddingLeft+i.paddingRight+i.borderLeftWidth+i.borderRightWidth:i.paddingBottom+i.paddingTop+i.borderTopWidth+i.borderBottomWidth),t=Math.max(t,0),this.element.style[e?"width":"height"]=t+"px"}},c._emitCompleteOnItems=function(t,e){function i(){n.dispatchEvent(t+"Complete",null,[e])}function o(){r++,r==s&&i()}var n=this,s=e.length;if(!e||!s)return void i();var r=0;e.forEach(function(e){e.once(t,o)})},c.dispatchEvent=function(t,e,i){var o=e?[e].concat(i):i;if(this.emitEvent(t,o),h)if(this.$element=this.$element||h(this.element),e){var n=h.Event(e);n.type=t,this.$element.trigger(n,i)}else this.$element.trigger(t,i)},c.ignore=function(t){var e=this.getItem(t);e&&(e.isIgnored=!0)},c.unignore=function(t){var e=this.getItem(t);e&&delete e.isIgnored},c.stamp=function(t){t=this._find(t),t&&(this.stamps=this.stamps.concat(t),t.forEach(this.ignore,this))},c.unstamp=function(t){t=this._find(t),t&&t.forEach(function(t){o.removeFrom(this.stamps,t),this.unignore(t)},this)},c._find=function(t){if(t)return"string"==typeof t&&(t=this.element.querySelectorAll(t)),t=o.makeArray(t)},c._manageStamps=function(){this.stamps&&this.stamps.length&&(this._getBoundingRect(),this.stamps.forEach(this._manageStamp,this))},c._getBoundingRect=function(){var t=this.element.getBoundingClientRect(),e=this.size;this._boundingRect={left:t.left+e.paddingLeft+e.borderLeftWidth,top:t.top+e.paddingTop+e.borderTopWidth,right:t.right-(e.paddingRight+e.borderRightWidth),bottom:t.bottom-(e.paddingBottom+e.borderBottomWidth)}},c._manageStamp=d,c._getElementOffset=function(t){var e=t.getBoundingClientRect(),o=this._boundingRect,n=i(t),s={left:e.left-o.left-n.marginLeft,top:e.top-o.top-n.marginTop,right:o.right-e.right-n.marginRight,bottom:o.bottom-e.bottom-n.marginBottom};return s},c.handleEvent=o.handleEvent,c.bindResize=function(){t.addEventListener("resize",this),this.isResizeBound=!0},c.unbindResize=function(){t.removeEventListener("resize",this),this.isResizeBound=!1},c.onresize=function(){this.resize()},o.debounceMethod(s,"onresize",100),c.resize=function(){this.isResizeBound&&this.needsResizeLayout()&&this.layout()},c.needsResizeLayout=function(){var t=i(this.element),e=this.size&&t;return e&&t.innerWidth!==this.size.innerWidth},c.addItems=function(t){var e=this._itemize(t);return e.length&&(this.items=this.items.concat(e)),e},c.appended=function(t){var e=this.addItems(t);e.length&&(this.layoutItems(e,!0),this.reveal(e))},c.prepended=function(t){var e=this._itemize(t);if(e.length){var i=this.items.slice(0);this.items=e.concat(i),this._resetLayout(),this._manageStamps(),this.layoutItems(e,!0),this.reveal(e),this.layoutItems(i)}},c.reveal=function(t){if(this._emitCompleteOnItems("reveal",t),t&&t.length){var e=this.updateStagger();t.forEach(function(t,i){t.stagger(i*e),t.reveal()})}},c.hide=function(t){if(this._emitCompleteOnItems("hide",t),t&&t.length){var e=this.updateStagger();t.forEach(function(t,i){t.stagger(i*e),t.hide()})}},c.revealItemElements=function(t){var e=this.getItems(t);this.reveal(e)},c.hideItemElements=function(t){var e=this.getItems(t);this.hide(e)},c.getItem=function(t){for(var e=0;e<this.items.length;e++){var i=this.items[e];if(i.element==t)return i}},c.getItems=function(t){t=o.makeArray(t);var e=[];return t.forEach(function(t){var i=this.getItem(t);i&&e.push(i)},this),e},c.remove=function(t){var e=this.getItems(t);this._emitCompleteOnItems("remove",e),e&&e.length&&e.forEach(function(t){t.remove(),o.removeFrom(this.items,t)},this)},c.destroy=function(){var t=this.element.style;t.height="",t.position="",t.width="",this.items.forEach(function(t){t.destroy()}),this.unbindResize();var e=this.element.outlayerGUID;delete f[e],delete this.element.outlayerGUID,h&&h.removeData(this.element,this.constructor.namespace)},s.data=function(t){t=o.getQueryElement(t);var e=t&&t.outlayerGUID;return e&&f[e]},s.create=function(t,e){var i=r(s);return i.defaults=o.extend({},s.defaults),o.extend(i.defaults,e),i.compatOptions=o.extend({},s.compatOptions),i.namespace=t,i.data=s.data,i.Item=r(n),o.htmlInit(i,t),h&&h.bridget&&h.bridget(t,i),i};var m={ms:1,s:1e3};return s.Item=n,s}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/item",["outlayer/outlayer"],e):"object"==typeof module&&module.exports?module.exports=e(require("outlayer")):(t.Isotope=t.Isotope||{},t.Isotope.Item=e(t.Outlayer))}(window,function(t){"use strict";function e(){t.Item.apply(this,arguments)}var i=e.prototype=Object.create(t.Item.prototype),o=i._create;i._create=function(){this.id=this.layout.itemGUID++,o.call(this),this.sortData={}},i.updateSortData=function(){if(!this.isIgnored){this.sortData.id=this.id,this.sortData["original-order"]=this.id,this.sortData.random=Math.random();var t=this.layout.options.getSortData,e=this.layout._sorters;for(var i in t){var o=e[i];this.sortData[i]=o(this.element,this)}}};var n=i.destroy;return i.destroy=function(){n.apply(this,arguments),this.css({display:""})},e}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-mode",["get-size/get-size","outlayer/outlayer"],e):"object"==typeof module&&module.exports?module.exports=e(require("get-size"),require("outlayer")):(t.Isotope=t.Isotope||{},t.Isotope.LayoutMode=e(t.getSize,t.Outlayer))}(window,function(t,e){"use strict";function i(t){this.isotope=t,t&&(this.options=t.options[this.namespace],this.element=t.element,this.items=t.filteredItems,this.size=t.size)}var o=i.prototype,n=["_resetLayout","_getItemLayoutPosition","_manageStamp","_getContainerSize","_getElementOffset","needsResizeLayout","_getOption"];return n.forEach(function(t){o[t]=function(){return e.prototype[t].apply(this.isotope,arguments)}}),o.needsVerticalResizeLayout=function(){var e=t(this.isotope.element),i=this.isotope.size&&e;return i&&e.innerHeight!=this.isotope.size.innerHeight},o._getMeasurement=function(){this.isotope._getMeasurement.apply(this,arguments)},o.getColumnWidth=function(){this.getSegmentSize("column","Width")},o.getRowHeight=function(){this.getSegmentSize("row","Height")},o.getSegmentSize=function(t,e){var i=t+e,o="outer"+e;if(this._getMeasurement(i,o),!this[i]){var n=this.getFirstItemSize();this[i]=n&&n[o]||this.isotope.size["inner"+e]}},o.getFirstItemSize=function(){var e=this.isotope.filteredItems[0];return e&&e.element&&t(e.element)},o.layout=function(){this.isotope.layout.apply(this.isotope,arguments)},o.getSize=function(){this.isotope.getSize(),this.size=this.isotope.size},i.modes={},i.create=function(t,e){function n(){i.apply(this,arguments)}return n.prototype=Object.create(o),n.prototype.constructor=n,e&&(n.options=e),n.prototype.namespace=t,i.modes[t]=n,n},i}),function(t,e){"function"==typeof define&&define.amd?define("masonry/masonry",["outlayer/outlayer","get-size/get-size"],e):"object"==typeof module&&module.exports?module.exports=e(require("outlayer"),require("get-size")):t.Masonry=e(t.Outlayer,t.getSize)}(window,function(t,e){var i=t.create("masonry");i.compatOptions.fitWidth="isFitWidth";var o=i.prototype;return o._resetLayout=function(){this.getSize(),this._getMeasurement("columnWidth","outerWidth"),this._getMeasurement("gutter","outerWidth"),this.measureColumns(),this.colYs=[];for(var t=0;t<this.cols;t++)this.colYs.push(0);this.maxY=0,this.horizontalColIndex=0},o.measureColumns=function(){if(this.getContainerWidth(),!this.columnWidth){var t=this.items[0],i=t&&t.element;this.columnWidth=i&&e(i).outerWidth||this.containerWidth}var o=this.columnWidth+=this.gutter,n=this.containerWidth+this.gutter,s=n/o,r=o-n%o,a=r&&r<1?"round":"floor";s=Math[a](s),this.cols=Math.max(s,1)},o.getContainerWidth=function(){var t=this._getOption("fitWidth"),i=t?this.element.parentNode:this.element,o=e(i);this.containerWidth=o&&o.innerWidth},o._getItemLayoutPosition=function(t){t.getSize();var e=t.size.outerWidth%this.columnWidth,i=e&&e<1?"round":"ceil",o=Math[i](t.size.outerWidth/this.columnWidth);o=Math.min(o,this.cols);for(var n=this.options.horizontalOrder?"_getHorizontalColPosition":"_getTopColPosition",s=this[n](o,t),r={x:this.columnWidth*s.col,y:s.y},a=s.y+t.size.outerHeight,u=o+s.col,h=s.col;h<u;h++)this.colYs[h]=a;return r},o._getTopColPosition=function(t){var e=this._getTopColGroup(t),i=Math.min.apply(Math,e);return{col:e.indexOf(i),y:i}},o._getTopColGroup=function(t){if(t<2)return this.colYs;for(var e=[],i=this.cols+1-t,o=0;o<i;o++)e[o]=this._getColGroupY(o,t);return e},o._getColGroupY=function(t,e){if(e<2)return this.colYs[t];var i=this.colYs.slice(t,t+e);return Math.max.apply(Math,i)},o._getHorizontalColPosition=function(t,e){var i=this.horizontalColIndex%this.cols,o=t>1&&i+t>this.cols;i=o?0:i;var n=e.size.outerWidth&&e.size.outerHeight;return this.horizontalColIndex=n?i+t:this.horizontalColIndex,{col:i,y:this._getColGroupY(i,t)}},o._manageStamp=function(t){var i=e(t),o=this._getElementOffset(t),n=this._getOption("originLeft"),s=n?o.left:o.right,r=s+i.outerWidth,a=Math.floor(s/this.columnWidth);a=Math.max(0,a);var u=Math.floor(r/this.columnWidth);u-=r%this.columnWidth?0:1,u=Math.min(this.cols-1,u);for(var h=this._getOption("originTop"),d=(h?o.top:o.bottom)+i.outerHeight,l=a;l<=u;l++)this.colYs[l]=Math.max(d,this.colYs[l])},o._getContainerSize=function(){this.maxY=Math.max.apply(Math,this.colYs);var t={height:this.maxY};return this._getOption("fitWidth")&&(t.width=this._getContainerFitWidth()),t},o._getContainerFitWidth=function(){for(var t=0,e=this.cols;--e&&0===this.colYs[e];)t++;return(this.cols-t)*this.columnWidth-this.gutter},o.needsResizeLayout=function(){var t=this.containerWidth;return this.getContainerWidth(),t!=this.containerWidth},i}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-modes/masonry",["../layout-mode","masonry/masonry"],e):"object"==typeof module&&module.exports?module.exports=e(require("../layout-mode"),require("masonry-layout")):e(t.Isotope.LayoutMode,t.Masonry)}(window,function(t,e){"use strict";var i=t.create("masonry"),o=i.prototype,n={_getElementOffset:!0,layout:!0,_getMeasurement:!0};for(var s in e.prototype)n[s]||(o[s]=e.prototype[s]);var r=o.measureColumns;o.measureColumns=function(){this.items=this.isotope.filteredItems,r.call(this)};var a=o._getOption;return o._getOption=function(t){return"fitWidth"==t?void 0!==this.options.isFitWidth?this.options.isFitWidth:this.options.fitWidth:a.apply(this.isotope,arguments)},i}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-modes/fit-rows",["../layout-mode"],e):"object"==typeof exports?module.exports=e(require("../layout-mode")):e(t.Isotope.LayoutMode)}(window,function(t){"use strict";var e=t.create("fitRows"),i=e.prototype;return i._resetLayout=function(){this.x=0,this.y=0,this.maxY=0,this._getMeasurement("gutter","outerWidth")},i._getItemLayoutPosition=function(t){t.getSize();var e=t.size.outerWidth+this.gutter,i=this.isotope.size.innerWidth+this.gutter;0!==this.x&&e+this.x>i&&(this.x=0,this.y=this.maxY);var o={x:this.x,y:this.y};return this.maxY=Math.max(this.maxY,this.y+t.size.outerHeight),this.x+=e,o},i._getContainerSize=function(){return{height:this.maxY}},e}),function(t,e){"function"==typeof define&&define.amd?define("isotope/js/layout-modes/vertical",["../layout-mode"],e):"object"==typeof module&&module.exports?module.exports=e(require("../layout-mode")):e(t.Isotope.LayoutMode)}(window,function(t){"use strict";var e=t.create("vertical",{horizontalAlignment:0}),i=e.prototype;return i._resetLayout=function(){this.y=0},i._getItemLayoutPosition=function(t){t.getSize();var e=(this.isotope.size.innerWidth-t.size.outerWidth)*this.options.horizontalAlignment,i=this.y;return this.y+=t.size.outerHeight,{x:e,y:i}},i._getContainerSize=function(){return{height:this.y}},e}),function(t,e){"function"==typeof define&&define.amd?define(["outlayer/outlayer","get-size/get-size","desandro-matches-selector/matches-selector","fizzy-ui-utils/utils","isotope/js/item","isotope/js/layout-mode","isotope/js/layout-modes/masonry","isotope/js/layout-modes/fit-rows","isotope/js/layout-modes/vertical"],function(i,o,n,s,r,a){return e(t,i,o,n,s,r,a)}):"object"==typeof module&&module.exports?module.exports=e(t,require("outlayer"),require("get-size"),require("desandro-matches-selector"),require("fizzy-ui-utils"),require("isotope/js/item"),require("isotope/js/layout-mode"),require("isotope/js/layout-modes/masonry"),require("isotope/js/layout-modes/fit-rows"),require("isotope/js/layout-modes/vertical")):t.Isotope=e(t,t.Outlayer,t.getSize,t.matchesSelector,t.fizzyUIUtils,t.Isotope.Item,t.Isotope.LayoutMode)}(window,function(t,e,i,o,n,s,r){function a(t,e){return function(i,o){for(var n=0;n<t.length;n++){var s=t[n],r=i.sortData[s],a=o.sortData[s];if(r>a||r<a){var u=void 0!==e[s]?e[s]:e,h=u?1:-1;return(r>a?1:-1)*h}}return 0}}var u=t.jQuery,h=String.prototype.trim?function(t){return t.trim()}:function(t){return t.replace(/^\s+|\s+$/g,"")},d=e.create("isotope",{layoutMode:"masonry",isJQueryFiltering:!0,sortAscending:!0});d.Item=s,d.LayoutMode=r;var l=d.prototype;l._create=function(){this.itemGUID=0,this._sorters={},this._getSorters(),e.prototype._create.call(this),this.modes={},this.filteredItems=this.items,this.sortHistory=["original-order"];for(var t in r.modes)this._initLayoutMode(t)},l.reloadItems=function(){this.itemGUID=0,e.prototype.reloadItems.call(this)},l._itemize=function(){for(var t=e.prototype._itemize.apply(this,arguments),i=0;i<t.length;i++){var o=t[i];o.id=this.itemGUID++}return this._updateItemsSortData(t),t},l._initLayoutMode=function(t){var e=r.modes[t],i=this.options[t]||{};this.options[t]=e.options?n.extend(e.options,i):i,this.modes[t]=new e(this)},l.layout=function(){return!this._isLayoutInited&&this._getOption("initLayout")?void this.arrange():void this._layout()},l._layout=function(){var t=this._getIsInstant();this._resetLayout(),this._manageStamps(),this.layoutItems(this.filteredItems,t),this._isLayoutInited=!0},l.arrange=function(t){this.option(t),this._getIsInstant();var e=this._filter(this.items);this.filteredItems=e.matches,this._bindArrangeComplete(),this._isInstant?this._noTransition(this._hideReveal,[e]):this._hideReveal(e),this._sort(),this._layout()},l._init=l.arrange,l._hideReveal=function(t){this.reveal(t.needReveal),this.hide(t.needHide)},l._getIsInstant=function(){var t=this._getOption("layoutInstant"),e=void 0!==t?t:!this._isLayoutInited;return this._isInstant=e,e},l._bindArrangeComplete=function(){function t(){e&&i&&o&&n.dispatchEvent("arrangeComplete",null,[n.filteredItems])}var e,i,o,n=this;this.once("layoutComplete",function(){e=!0,t()}),this.once("hideComplete",function(){i=!0,t()}),this.once("revealComplete",function(){o=!0,t()})},l._filter=function(t){var e=this.options.filter;e=e||"*";for(var i=[],o=[],n=[],s=this._getFilterTest(e),r=0;r<t.length;r++){var a=t[r];if(!a.isIgnored){var u=s(a);u&&i.push(a),u&&a.isHidden?o.push(a):u||a.isHidden||n.push(a)}}return{matches:i,needReveal:o,needHide:n}},l._getFilterTest=function(t){return u&&this.options.isJQueryFiltering?function(e){return u(e.element).is(t)}:"function"==typeof t?function(e){return t(e.element)}:function(e){return o(e.element,t)}},l.updateSortData=function(t){
        var e;t?(t=n.makeArray(t),e=this.getItems(t)):e=this.items,this._getSorters(),this._updateItemsSortData(e)},l._getSorters=function(){var t=this.options.getSortData;for(var e in t){var i=t[e];this._sorters[e]=f(i)}},l._updateItemsSortData=function(t){for(var e=t&&t.length,i=0;e&&i<e;i++){var o=t[i];o.updateSortData()}};var f=function(){function t(t){if("string"!=typeof t)return t;var i=h(t).split(" "),o=i[0],n=o.match(/^\[(.+)\]$/),s=n&&n[1],r=e(s,o),a=d.sortDataParsers[i[1]];return t=a?function(t){return t&&a(r(t))}:function(t){return t&&r(t)}}function e(t,e){return t?function(e){return e.getAttribute(t)}:function(t){var i=t.querySelector(e);return i&&i.textContent}}return t}();d.sortDataParsers={parseInt:function(t){return parseInt(t,10)},parseFloat:function(t){return parseFloat(t)}},l._sort=function(){if(this.options.sortBy){var t=n.makeArray(this.options.sortBy);this._getIsSameSortBy(t)||(this.sortHistory=t.concat(this.sortHistory));var e=a(this.sortHistory,this.options.sortAscending);this.filteredItems.sort(e)}},l._getIsSameSortBy=function(t){for(var e=0;e<t.length;e++)if(t[e]!=this.sortHistory[e])return!1;return!0},l._mode=function(){var t=this.options.layoutMode,e=this.modes[t];if(!e)throw new Error("No layout mode: "+t);return e.options=this.options[t],e},l._resetLayout=function(){e.prototype._resetLayout.call(this),this._mode()._resetLayout()},l._getItemLayoutPosition=function(t){return this._mode()._getItemLayoutPosition(t)},l._manageStamp=function(t){this._mode()._manageStamp(t)},l._getContainerSize=function(){return this._mode()._getContainerSize()},l.needsResizeLayout=function(){return this._mode().needsResizeLayout()},l.appended=function(t){var e=this.addItems(t);if(e.length){var i=this._filterRevealAdded(e);this.filteredItems=this.filteredItems.concat(i)}},l.prepended=function(t){var e=this._itemize(t);if(e.length){this._resetLayout(),this._manageStamps();var i=this._filterRevealAdded(e);this.layoutItems(this.filteredItems),this.filteredItems=i.concat(this.filteredItems),this.items=e.concat(this.items)}},l._filterRevealAdded=function(t){var e=this._filter(t);return this.hide(e.needHide),this.reveal(e.matches),this.layoutItems(e.matches,!0),e.matches},l.insert=function(t){var e=this.addItems(t);if(e.length){var i,o,n=e.length;for(i=0;i<n;i++)o=e[i],this.element.appendChild(o.element);var s=this._filter(e).matches;for(i=0;i<n;i++)e[i].isLayoutInstant=!0;for(this.arrange(),i=0;i<n;i++)delete e[i].isLayoutInstant;this.reveal(s)}};var c=l.remove;return l.remove=function(t){t=n.makeArray(t);var e=this.getItems(t);c.call(this,t);for(var i=e&&e.length,o=0;i&&o<i;o++){var s=e[o];n.removeFrom(this.filteredItems,s)}},l.shuffle=function(){for(var t=0;t<this.items.length;t++){var e=this.items[t];e.sortData.random=Math.random()}this.options.sortBy="random",this._sort(),this._layout()},l._noTransition=function(t,e){var i=this.options.transitionDuration;this.options.transitionDuration=0;var o=t.apply(this,e);return this.options.transitionDuration=i,o},l.getFilteredItemElements=function(){return this.filteredItems.map(function(t){return t.element})},d});


    (function($) {

        /*
         Thumbnail: Select
         */
        $('.mg-option input[type=checkbox]').on('change', function( ev ) {
            var wrapper = $(this).parents('.thumbnail');
            if($(this).is(':checked')) {
                wrapper.addClass('thumbnail-selected');
            } else {
                wrapper.removeClass('thumbnail-selected');
            }
        });

        $('.mg-option input[type=checkbox]:checked').trigger('change');

        /*
         Toolbar: Select All
         */
        $('#mgSelectAll').on('click', function( ev ) {
            ev.preventDefault();
            var $this = $(this),
                $label = $this.find('> span');
            $checks = $('.mg-option input[type=checkbox]');

            if($this.attr('data-all-selected')) {
                $this.removeAttr('data-all-selected');
                $checks.prop('checked', false).trigger('change');
                $label.html($label.data('all-text'));
            } else {
                $this.attr('data-all-selected', 'true');
                $checks.prop('checked', true).trigger('change');
                $label.html($label.data('none-text'));
            }
        });

        /*
         Image Preview: Lightbox
         */
        $('.thumb-preview > a[href]').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-img-mobile',
            image: {
                verticalFit: true
            }
        });

        $('.thumb-preview .mg-zoom').on('click.lightbox', function( ev ) {
            ev.preventDefault();
            $(this).closest('.thumb-preview').find('a.thumb-image').triggerHandler('click');
        });

        /*
         Thumnail: Dropdown Options
         */
        $('.thumbnail .mg-toggle').parent()
            .on('show.bs.dropdown', function( ev ) {
                $(this).closest('.mg-thumb-options').css('overflow', 'visible');
            })
            .on('hidden.bs.dropdown', function( ev ) {
                $(this).closest('.mg-thumb-options').css('overflow', '');
            });

        $('.thumbnail').on('mouseenter', function() {
            var toggle = $(this).find('.mg-toggle');
            if ( toggle.parent().hasClass('open') ) {
                toggle.dropdown('toggle');
            }
        });

        /*
         Isotope: Sort Thumbnails
         */
        $("[data-sort-source]").each(function() {

            var source = $(this);
            var destination = $("[data-sort-destination][data-sort-id=" + $(this).attr("data-sort-id") + "]");

            if(destination.get(0)) {

                $(window).on('load', function() {

                    destination.isotope({
                        itemSelector: ".isotope-item",
                        layoutMode: 'fitRows'
                    });

                    $(window).on('sidebar-left-toggle inner-menu-toggle', function() {
                        destination.isotope();
                    });

                    source.find("a[data-option-value]").click(function(e) {

                        e.preventDefault();

                        var $this = $(this),
                            filter = $this.attr("data-option-value");

                        source.find(".active").removeClass("active");
                        $this.closest("li").addClass("active");

                        destination.isotope({
                            filter: filter
                        });

                        if(window.location.hash != "" || filter.replace(".","") != "*") {
                            window.location.hash = filter.replace(".","");
                        }

                        return false;

                    });

                    $(window).bind("hashchange", function(e) {

                        var hashFilter = "." + location.hash.replace("#",""),
                            hash = (hashFilter == "." || hashFilter == ".*" ? "*" : hashFilter);

                        source.find(".active").removeClass("active");
                        source.find("[data-option-value='" + hash + "']").closest("li").addClass("active");

                        destination.isotope({
                            filter: hash
                        });

                    });

                    var hashFilter = "." + (location.hash.replace("#","") || "*");
                    var initFilterEl = source.find("a[data-option-value='" + hashFilter + "']");

                    if(initFilterEl.get(0)) {
                        source.find("[data-option-value='" + hashFilter + "']").click();
                    } else {
                        source.find(".active a").click();
                    }

                });

            }

        });

    }(jQuery));
</script>

