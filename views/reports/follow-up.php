<?php
/**
 * Created by PhpStorm.
 * User: Multiline
 * Date: 4/24/2019
 * Time: 11:32 AM
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Follow Up Report';


?>  
 
<div class="card-body">

    <div class="follow-up-search">

        <?php $form = ActiveForm::begin([
            'action' => ['follow-up'],
            'method' => 'get',
        ]); ?>
        <div class="row">
            <?php  if(Yii::$app->user->can('follow_up_user_search')){?>
            <div class="col-md-3">

                <?php 
                 if(Yii::$app->user->can('teamlead')){
                 $team_lead=Yii::$app->user->identity->lead_assign;
                        $split = preg_split('/(?:"[^"]*"|)\K\s*(,\s*|$)/', $team_lead);
                        $result = array_filter($split);

                        echo $form->field($model, 'lead_assign')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\User::find()->where(
                                ['id'=>$result])->all(),'id','username'),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => ['placeholder'  => 'Select Users'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Lead Assign'); 
                }else{
                    echo $form->field($model, 'lead_assign')->widget(Select2::classname(), [
                'data' =>  app\helpers\Helper::assignLead2(),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['multiple' => false, 'placeholder' => 'Select Users'],
                'pluginOptions' => [
                'allowClear' => true,
                ],
                ])->label('Lead Assign'); 
                 }
                ?>
               

            </div>
        <?php } ?>
        
            <div class="col-md-3">
                <?= $form->field($model, 'note') ?>
            </div>


                <div class="col-md-3">
                <?php
                $addon = <<< HTML
<span class="input-group-addon">
    <i class="glyphicon glyphicon-calendar"></i>
</span>
HTML;



                echo '<label class="control-label">Follow Up Date Range</label>';
                echo '<div class="drp-container">';
                echo DateRangePicker::widget([
                    'model'=>$model,
                    'attribute'=>'follow_date_time',

                    'convertFormat'=>true,

                    'pluginOptions'=>[
                        'opens'=>'left',
                        'ranges' => [

                            "Today" => ["moment().startOf('day')", "moment()"],
                            "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                            "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],

                        ],

                        'timePicker'=>true,
                        'timePickerIncrement'=>05,
                        'locale'=>['format'=>'d/m/Y h:i A']
                    ],
                    'presetDropdown'=>false,
                    'hideInput'=>true
                ]);
                echo '</div>'; ?>

            </div>



           


                       <div class="col-md-3">
                <?php
                $addon = <<< HTML
<span class="input-group-addon">
    <i class="glyphicon glyphicon-calendar"></i>
</span>
HTML;



                echo '<label class="control-label">Create On</label>';
                echo '<div class="drp-container">';
                echo DateRangePicker::widget([
                    'model'=>$model,
                    'attribute'=>'created_on',

                    'convertFormat'=>true,

                    'pluginOptions'=>[
                        'opens'=>'left',
                        'ranges' => [

                            "Today" => ["moment().startOf('day')", "moment()"],
                            "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                            "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],

                        ],

                        'timePicker'=>true,
                        'timePickerIncrement'=>05,
                        'locale'=>['format'=>'d/m/Y h:i A']
                    ],
                    'presetDropdown'=>false,
                    'hideInput'=>true
                ]);
                echo '</div>'; ?>

            </div>

            <div class="col-md-3">

                    <?= $form->field($model, 'status')->widget(Select2::classname(), [
                        'data' =>  ArrayHelper::map(\app\models\FollowUpStatus::find()->all(), 'status', 'status'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['multiple' => false, 'placeholder' => 'Select Status'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ]); ?>

            </div>


        </div>




        <?php // echo $form->field($model, 'created_on') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'updated_on') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <div class="form-group">
              <?= Html::label( 'Page Size', 'pagesize', array( 'style' => 'margin-left:10px; margin-top:8px;' ) ) ?>

            <?= Html::dropDownList(
    'pagesize', 
    ( isset($_GET['pagesize']) ? $_GET['pagesize'] : 20 ),  // set the default value for the dropdown list
    // set the key and value for the drop down list
    array( 
        20=>20,
        50 => 50, 
        100 => 100,
        200=>200,
        1000=>'All'
    ),
    // add the HTML attritutes for the dropdown list
    // I add pagesize as an id of dropdown list. later on, I will add into the Gridview widget.
    // so when the form is submitted, I can get the $_POST value in InvoiceSearch model.
    array( 
        'id' => 'pagesize', 
        'style' => 'margin-left:5px; margin-top:8px;'
        )
    ) 
?>



            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default obaid']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

    <div style="margin-top:20px">


 <?php
     if(Yii::$app->user->can('followup/download'))
     {
        $show='';
     }
     else
     {
        $show=false;
     }

    ?>




    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'responsiveWrap' => false,
        'tableOptions' => [
            'class' => 'table table-condensed table-sm',
        ],
          'toolbar' =>  [
                    '{export}',
                    '{toggleData}',
                ],
                'export' => [
                'fontAwesome' => true
                ],
                'exportConfig' => [
                  
                    GridView::EXCEL => ['label' => 'Save as EXCEL'],
                   
                ],


        'panel' => [
            //'type' => GridView::TYPE_PRIMARY,
            //'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
            //'before'=>$create.' '.$ordering.' ',
            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Lead ID',
                'format'=>'raw',
                'value' =>function($model)
                {
                    $id = $model->lead->id;
                    $url = Yii::$app->homeUrl.'leads/view?id='.$model->lead->id;
                    return "<a href='$url' target='_blank'>$id</a>";
                },
                'group'=>true

            ],

            [
                'label' => 'Name',
                'value'=>'lead.contact_name',
                'group'=>true

            ],

            

            [
                'label' => 'Mobile',
                'value'=>'lead.contact_mobile',
                'group'=>true,
                'hiddenFromExport' =>(Yii::$app->user->can('phonenumberdownload')),
                

            ],
            [
                'label' => 'Purpose',
                'value'=>'lead.lead_type',

            ],
            [
                'label' => 'Lead Status',
                'value'=>'lead.lead_status'

            ],
            [
                'label' => 'Assign To',
                'format'=>'raw',
                'value'=>function($model)
                {

                    if(empty($model->lead['lead_assign']))
                    {

                        return '';

                    }else {


                        $mo = explode(',', $model->lead['lead_assign']);
                        $e = '';
                        foreach ($mo as $key=>$data)
                        {
                            $user = \app\models\User::findOne($data);

                            $e .=$user->username.',';
                        }

                        $e = substr_replace($e, "", -1);
                        return $e;

                    }
                },

            ],
            [
                'label' => 'Assign By',
                'format'=>'raw',
                'value'=>function($model)
                {
                    if(empty($model->lead['assign_by']))
                    {

                        return '';

                    }else {


                        $mo = explode(',', $model->lead['assign_by']);
                        $e = '';
                        foreach ($mo as $key=>$data)
                        {
                            $user = \app\models\User::findOne($data);

                            $e .=$user->username.',';
                        }

                        $e = substr_replace($e, "", -1);
                        return $e;

                    }
                },

            ],
            [
                'label' => 'Register',
                'format'=>'raw',
                'value'=>function($model)
                {
                    if(empty($model->lead['created_by']))
                    {

                        return '';

                    }else {


                        $mo = explode(',', $model->lead['created_by']);
                        $e = '';
                        foreach ($mo as $key=>$data)
                        {
                            $user = \app\models\User::findOne($data);

                            $e .=$user->username.',';
                        }

                        $e = substr_replace($e, "", -1);
                        return $e;

                    }
                },

            ],


            [
                'attribute'=>'follow_date_time',
                'label'=>'Follow Up DateTime',
                /*'format' => ['date', 'php:d/m/Y H:i a']*/
                'format'=>'raw',
                'value' => function($model){
                    if($model->follow_date_time)
                    {
                        return date("d/m/Y h:i A", strtotime($model->follow_date_time));

                    }else
                    {
                        return '';
                    }
                },


            ],
            'note',
            'status',
              [
                'attribute'=>'created_on',
                'label'=>'Created Date',
                /*'format' => ['date', 'php:d/m/Y H:i a']*/
                'format'=>'raw',
                'value' => function($model){
                    if($model->created_on)
                    {
                        return date("d/m/Y h:i A", strtotime($model->created_on));

                    }else
                    {
                        return '';
                    }
                },


            ],

            //'created_by',
            //'updated_on',
            //'updated_by',


        ],
    ]); ?>

    </div>







</div>
<script type="text/javascript">
    $(".obaid").click(function(){

   var uri = window.location.toString();

    if (uri.indexOf("?") > 0) {
        var clean_uri = uri.substring(0, uri.indexOf("?"));
        window.history.replaceState({}, document.title, clean_uri);
       $('input.form-control.range-value').attr('value','');
      
    }
    location.reload();
});
   
</script>