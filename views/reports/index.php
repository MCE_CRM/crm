<?php

/* @var $this yii\web\View */

/*use kartik\daterange\DateRangePicker;*/
use app\models\Tasks;
use app\models\Property;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use app\models\Leads;
use app\models\LeadStatus;

$this->title = 'Dashboard';

?>
<style type="text/css">
    .card-header.text-white.bg-info {
        background-color: black !important;
    }
    .border-info {
        border-color:white !important;
    }

    #chartdiv,#chartdiv1 {
        width: 100%;
        height: 300px;

</style>

<div class="row">
    <div class="col-lg-8">

    </div>
    <div class="col-lg-3">
        <form method="get">
            <div class="datepicker">
                <?php
                $this->title = 'Reports';

                $addon = <<< HTML
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fas fa-calendar-alt"></i>
                                </span>
                            </div>
HTML;

                /*echo '<div class="input-group drp-container">';
                echo DateRangePicker::widget([
                        'name'=>'date_range_1',
                        'value'=>$_GET['date_range_1'],
                        'convertFormat'=>true,
                        'useWithAddon'=>true,
                        'pluginOptions'=>[


                            'opens'=>'left',
                            'ranges' => [

                                "Today" => ["moment().startOf('day')", "moment()"],
                                "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                                "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],
                                "This Month" => ["moment().startOf('month')", "moment().endOf('month')"],
                                "Last Month" => ["moment().subtract(1, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"],

                            ],

                            'timePicker'=>true,
                            'timePickerIncrement'=>05,
                            'locale'=>['format'=>'d/m/Y h:i A']
                        ],
                    ]) . $addon;
                echo '</div>';*/
                ?>

            </div>

    </div>
    <div class="col-lg-1">
<!--        <input type="submit" name="submit" id="submit" class="btn btn-success" style="margin-top: 10px;">
-->    </div>
    </form>
</div>
<div class="row">

    <div class="col-lg-12 mb-12">

        <section class="card">

            <div class="card-body">
                <div class="col-lg-12">
                    <div id="chartdiv1">


                    </div>
                </div>
                <h4><i>Projects Name</i></h4>

            </div>
        </section>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <?php Pjax::begin(); ?>
        <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'responsiveWrap' => false,
            'toolbar' => [
                '{export}',
                '{toggleData}',
            ],
            'export' => [
                'fontAwesome' => true
            ],
            'exportConfig' => [
                GridView::CSV => ['label' => 'Save as CSV', 'icon' => 'file-excel'],
                GridView::EXCEL => ['label' => 'Save as EXCEL'],
                GridView::TEXT => ['label' => 'Save as TEXT'],
                GridView::PDF => ['label' => 'Save as PDF'],

            ],
            'panel' => [
                'type' => GridView::TYPE_INFO,
                'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
                'before' => Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                'index'
                ], [
                'class' => 'btn btn-primary btn-sm'
                ]),


                'showFooter' => false,

            ],
            'columns' => [


                ['class' => 'yii\grid\SerialColumn'],

                [
                    'label'=>'Project Name',
                    'value'=>'name',
                    'headerOptions' => ['style' => 'width:10%']

                ],

                [
                    'label'=>'Address',
                    'value'=>'address',
                    'headerOptions' => ['style' => 'width:20%']


                ],
                [
                    'label'=>'Sold',
                    'value'=>function($model)
                    {

                        $count = Property::find()->where(['project_id'=>$model->id])->andWhere(['status'=>'Sold'])->count();
                        return $count;
                    }

                ],
                [
                    'label'=>'Available',
                    'value'=>function($model)
                    {
                        $count1 = Property::find()->where(['project_id'=>$model->id])->andWhere(['status'=>'Available'])->count();
                        return $count1;
                    }

                ],
                [
                    'label'=>'Occupied',
                    'value'=>function($model)
                    {
                        $count2 = Property::find()->where(['project_id'=>$model->id])->andWhere(['status'=>'Occupied'])->count();
                        return $count2;
                    }
                ],
                [
                    'label'=>'OverAll Leads',
                    'value'=>function($model)
                    {
                        $countstatus=Leads::find()->where(['project_id'=>$model->id])->count();
                        return $countstatus;
                    }
                ],
                [
                    'class' => '\kartik\grid\ActionColumn',
                    'template'=>'{update}',
                    'buttons' => [
                        'update' => function ($url, $model)

                        {
                            return '<a href="#"><span class="fas fa-eye"  onclick="getprojectfeature('.$model->id.',\'projects\',\'Feature\',event)"></span></a>';
                        } ,

                    ],
                ],
            ]

        ]); ?>

    </div>
</div>



<script>

    $(function(){



        var chart = AmCharts.makeChart("chartdiv1", {
            "type": "serial",
            "theme": "black",
            "depth3D": 20,
            "angle": 10,
            "legend": {
                "horizontalGap": 20,
                "maxColumns": 1,
                "position": "right",
                "useGraphSettings": true,
                "markerSize": 10
            },
            "dataLoader": {
                 "url": "<?=Yii::$app->homeUrl?>ajax/get-property-status?date_range_1=<?=$_GET['date_range_1'];?>"
            },

            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.3,
                "gridAlpha": 0,
                "dashLength": 0,
                "autoGridCount": false,
                "gridCount": 20,
                'integersOnly':true
            }],
            "graphs": [{
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Sold",
                "type": "column",
                "color": "#000000",
                "valueField": "Sold",
                "fixedColumnWidth": 55
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Avaliable",
                "type": "column",
                "color": "#000000",
                "valueField": "Available",
                "fixedColumnWidth": 55
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Occupied",
                "type": "column",
                "color": "#000000",
                "valueField": "Occupied",
                "fixedColumnWidth": 55
            }],
            "categoryField": "name",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "position": "left",
                "labelRotation": 75

            },
            "export": {
                "enabled": true
            }

        });
        ;


    });
</script>


