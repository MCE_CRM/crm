
<?php

use app\models\User;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\sortable\Sortable;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LeadStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */




$this->title = 'Project Category';
$this->params['breadcrumbs'][] = $this->title;

$create = '';
$ordering = '';
if(\Yii::$app->user->can('project-category/create')){
    $create = Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New Category'), [
        'create'
    ], ['data-pjax' => 0,'onclick'=>'addnew(event,this)',
        'class' => 'btn btn-default btn-sm add-new'
    ]);
}


if (\Yii::$app->user->can('project-category/ordering')) {

$ordering = '<a href="'.Yii::$app->homeUrl.'project-category/ordering" class="btn btn-success btn-sm"><i class="fa fa-sort"> Ordering</i></a>';
}



?>
<div class="leadstatus-index">
    <?php


    Pjax::begin(['timeout' => '30000']);
    
    
    ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'toolbar' =>  [
            //'{export}',
            '{toggleData}',
        ],

        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
            'before'=>$create.' '.$ordering.' '.Html::a('<i class="fa fa-sync"></i> '.Yii::t ( 'app', 'Reset List' ), ['index'], ['class' => 'btn btn-info btn-sm']).' ',
            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'name',
            //'active',
            [
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute'=>'active',
                'value'=>function($model)
                {
                    return statusLabel($model->active);
                },
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => [
                    0 => 'Inactive',
                    1 => 'Active',
                ],
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',
            ],
            //'sort_order',
            [
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' =>  'created_on',
                'value' => function($model){
                    return date("d/m/Y", strtotime($model->created_on));
                },
                'filterType'=>GridView::FILTER_DATE,
                'filterWidgetOptions'=> [
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy',
                    ],
                ],
                //'filter'=>false

            ],
            [
                'headerOptions' => ['style' => 'width:15%','class' => 'text-center'],
                'attribute' => 'created_by',
                'contentOptions' => ['class' => 'text-center'],
                'value'=>'user.username',
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',

            ],
            [

                //'attribute' =>  'created_on',
                'header'=>'Last Update',
                'headerOptions' => ['style' => 'width:15%','class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'format'=>'raw',
                'visible'=> \Yii::$app->user->can('project-category/last-update'),
                'value' => function($model){
                    if($model->updated_by)
                    {
                        return date("d/m/Y h:i A", strtotime($model->updated_on)).'<br>'.\app\helpers\Helper::getUser($model->updated_by);

                    }else
                    {
                        return '';
                    }
                },
                //'filter'=>false

            ],
            //'created_by',
            //'updated_on',
            //'updated_by',

            [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{update}  {defaultValue} {delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        if (\Yii::$app->user->can('project-category/delete')) {
                            return Html::a('<span class="fas fa-trash"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-pjax' => '1',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                        'pjax' => 1,],
                                ]
                            );
                        }
                    },
                    'update' => function ($url, $model)

                    {
                     if(\Yii::$app->user->can('project-category/update')){
                        return '<a href="#"><span class="fa fa-pencil-alt"  onclick="updateRecord('.$model->id.',\'project-category\',\'Update Project Category\',event)"></span></a>';

                    }else
                     {
                         return '';
                     }
                    } ,
                    'defaultValue' => function ($url, $model) {

                        if (\Yii::$app->user->can('project-category/default-set')) {

                            if (\app\models\DefaultValueModule::checkDefaultValue('project-category', $model->id)) {
                                return Html::a('<span class="fa fa-eraser"></span>', Yii::$app->urlManager->createUrl(['project-category/index', 'del_id' => $model->id]), [
                                    'title' => Yii::t('app', 'Delete Default'),
                                ]);
                            } else {
                                return Html::a('<span class="fa fa-tag"></span>', Yii::$app->urlManager->createUrl(['project-category/index', 'id' => $model->id]), [
                                    'title' => Yii::t('app', 'Make Default'),
                                ]);
                            }
                        }
                    }

                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>

