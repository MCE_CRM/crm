<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Send Email';
$this->params['breadcrumbs'][] = $this->title;


?>


<div class="site-contact">

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Thank you for contacting us. We will respond to you as soon as possible.
        </div>

    <?php else: ?>


        <div class="row">

            <div class="col-lg-12">

                <?php


                $form = \yii\widgets\ActiveForm::begin([
                    'id' => 'form',
                    'errorCssClass' => 'has-danger',

                ]);



                ?>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'email')->label("To") ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'cc') ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label" for="emailform-subject">Select Template</label>
                        <select id="inputState" onchange="changeFunc(value);" class="form-control">
                            <option selected>Select Template</option>
                            <?php
                            $getTemplateName = \app\models\EmailTemplate::find()->select('id,template_name')->where(['active'=>1])->andWhere(['developer'=>0])->all();
                            foreach ($getTemplateName as $template_name)
                            {?>
                                <option value="<?php echo $template_name->id?>"><?php echo $template_name->template_name?></option>
                            <?php } ?>
                        </select>

                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'subject') ?>
                    </div>
                </div>


                <?= $form->field($model, 'body',['options' => ['id' => 'some_id']])->widget(CKEditor::className(), [
                    'options' => ['rows' => 6],
                    'preset' => 'basic',
                    'id'=>'desc',
                    'name' =>'desc',
                ]) ?>



                <div class="form-group">
                    <?= Html::submitButton('Send', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>


        </div>

    <?php endif; ?>
</div>

<script>



    function changeFunc($i) {
        var id = $i;
        $.ajax({
            type: "GET",
            url: baseUrl+"/ajax/get-email-template?id="+id,
            success: function(data)
            {
                var myArr = JSON.parse(data);
                if(myArr.status==1)
                {

                    $("#emailform-subject").val(myArr.subject);
                    CKEDITOR.instances['EmailForm[body]'].setData(myArr.body);

                }else
                {
                    $("#emailform-subject").val('');
                    CKEDITOR.instances['EmailForm[body]'].setData('');

                }


            }
        });
    }


</script>