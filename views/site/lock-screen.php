<?php use yii\bootstrap\ActiveForm;
use yii\helpers\Html;


$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- start: page -->
<section class="body-sign body-locked">
    <div class="center-sign">
        <div class="panel card-sign">
            <div class="card-body">

                <div class="site-login">
                    <!--<h1><?/*= Html::encode($this->title) */?></h1>-->


                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'action' => ['site/login'],
                        // 'layout' => 'horizontal',
                        'fieldConfig' => [
                            'template' => "{label}{input}{error}",
                            'options' => ['class' => 'form-group mb-3'],
                            'labelOptions' => ['class' => 'control-label'],
                        ],
                        'errorCssClass' => 'has-danger'
                    ]); ?>

                    <div class="current-user text-center">
                        <img src="<?= \app\helpers\Helper::getBaseUrl().'files/profile_image/'.$model->image ?>" alt="<?= $model->username?>" class="rounded-circle user-image" />
                        <h2 class="user-name text-dark m-0"><?= $first_name?> <?= $last_name?></h2>
                        <p class="user-email m-0"><?= $email ?></p>
                    </div>

                    <?php
                    echo Html::activeHiddenInput($model, 'username');
                    ?>
                    <?php
                    // Input group
                    echo $form->field($model, 'password', [
                        'inputTemplate' => '<div class="input-group">{input}<span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-lock"></i>
										</span>
									</span></div>',
                    ])->passwordInput(['class' => 'form-control form-control-lg']);

                    ?>

                    <div class="row">
                        <div class="col-6">
                            <p class="mt-1 mb-3">
                                <a href="<?= Yii::$app->homeUrl?>site/login">Not <?= $first_name?> <?= $last_name?>?</a>
                            </p>
                        </div>
                        <div class="col-6">
                            <?= Html::submitButton('Unlock', ['class' => 'btn btn-primary pull-right', 'name' => 'login-button']) ?>

                        </div>
                    </div>


                    <!--<?/* $form->field($model, 'password')->passwordInput()*/?> !-->



                    <?php ActiveForm::end(); ?>

                </div>


            </div>
        </div>
    </div>
</section>
<!-- end: page -->


