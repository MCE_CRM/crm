<?php
/**
 * Created by PhpStorm.
 * User: MCE_Barshan
 * Date: 4/8/2020
 * Time: 5:06 PM
 */
?>



<div class="_41cc3033 _7a3728c3">
    <div class="bbfbe3d2">
        <div class="d27a9599">
            <div class="_5f5a3b34"><span aria-label="Summary text">1 to 25 of 19,915 Plots</span>
                <div class="_759ed1d9">
                    <svg xmlns="http://www.w3.org/2000/svg" class="_2f985bf9" viewBox="0 0 32 32">
                        <path d="M26.9 5.9c.6 0 1.1.5 1.1 1.1v15.2l2.2-1.8c.5-.4 1.2-.3 1.6.1.4.5.3 1.2-.1 1.6L30 23.5l-1.2 1-.9.8-.9.7-1-.6-.4-.4-1.7-1.4-1.7-1.4c-.5-.4-.5-1.1-.1-1.6s1.1-.5 1.6-.1l2.2 1.8V7c-.1-.6.3-1.1 1-1.1zm-8.3.2c.7 0 1.3.6 1.3 1.3 0 .7-.6 1.3-1.3 1.3H1.3C.6 8.6 0 8.1 0 7.4c0-.7.6-1.3 1.3-1.3h17.3zm0 5.8c.7 0 1.3.6 1.3 1.3 0 .7-.6 1.3-1.3 1.3H5.1c-.7 0-1.3-.6-1.3-1.3 0-.7.6-1.3 1.3-1.3h13.5zm0 5.7c.7 0 1.3.6 1.3 1.3 0 .7-.6 1.3-1.3 1.3H8.9c-.7 0-1.3-.6-1.3-1.3 0-.7.6-1.3 1.3-1.3h9.7zm0 5.7c.7 0 1.3.6 1.3 1.3 0 .7-.6 1.3-1.3 1.3h-5.9c-.7 0-1.3-.6-1.3-1.3 0-.7.6-1.3 1.3-1.3h5.9z"></path>
                    </svg>
                    <div role="button" aria-haspopup="true" aria-label="Sort by filter" class="b3e5761f"><span class="ef5cccac"><span class="fontCompensation">Popular</span></span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6" class="eedc221b">
                            <path class="cls-1" d="M12 6L6 0 0 6h12z"></path>
                        </svg>
                    </div>
                </div>
            </div>
            <div class="f92cb4ee"></div><span class="_2cc0603b"><span role="button" class="ae44851f" aria-label="List view"><svg xmlns="http://www.w3.org/2000/svg" class="e38171df _676048b2" viewBox="0 0 32 32"><path d="M4.6 4.3c0 1.3-1 2.3-2.3 2.3S0 5.6 0 4.3C0 3 1 2 2.3 2s2.3 1 2.3 2.3M11.5 2h18.3C31 2 32 3 32 4.3c0 1.3-1 2.3-2.3 2.3H11.5a2.6 2.6 0 0 1-2.3-2.3c0-1.3 1-2.3 2.3-2.3zM4.6 16c0 1.3-1 2.3-2.3 2.3S0 17.3 0 16s1-2.3 2.3-2.3 2.3 1 2.3 2.3m6.9-2.3h18.3c1.2.1 2.2 1.1 2.3 2.3 0 1.3-1 2.3-2.3 2.3H11.5A2.6 2.6 0 0 1 9.2 16c0-1.3 1-2.3 2.3-2.3zm-6.9 14c0 1.3-1 2.3-2.3 2.3S0 29 0 27.7s1-2.3 2.3-2.3 2.3 1 2.3 2.3m6.9-2.3h18.3c1.2.1 2.2 1.1 2.3 2.3 0 1.3-1 2.3-2.3 2.3H11.5a2.6 2.6 0 0 1-2.3-2.3c0-1.3 1-2.3 2.3-2.3z"></path></svg></span><span role="button" class="ae44851f" aria-label="Grid view"><svg xmlns="http://www.w3.org/2000/svg" class="e38171df " viewBox="0 0 32 32"><path d="M0 7V1.8C0 .8.8 0 1.8 0H7c1 0 1.8.8 1.8 1.8V7C8.8 8 8 8.8 7 8.8H1.8C.8 8.8 0 8 0 7zm25 1.8h5.2c1 0 1.8-.8 1.8-1.8V1.8c0-1-.8-1.8-1.8-1.8H25c-1 0-1.8.8-1.8 1.8V7c0 1 .8 1.8 1.8 1.8zm-11.6 0h5.2c1 0 1.8-.8 1.8-1.8V1.8c0-1-.8-1.8-1.8-1.8h-5.2c-1 0-1.8.8-1.8 1.8V7c0 1 .8 1.8 1.8 1.8zm-1.8 9.7c0 1 .8 1.8 1.8 1.8h5.2c1 0 1.8-.8 1.8-1.8v-5.2c0-1-.8-1.8-1.8-1.8h-5.2c-1 0-1.8.8-1.8 1.8v5.2zm0 11.7c0 1 .8 1.8 1.8 1.8h5.2c1 0 1.8-.8 1.8-1.8V25c0-1-.8-1.8-1.8-1.8h-5.2c-1 0-1.8.8-1.8 1.8v5.2zm18.6-7H25c-1 0-1.8.8-1.8 1.8v5.2c0 1 .8 1.8 1.8 1.8h5.2c1 0 1.8-.8 1.8-1.8V25c0-1-.8-1.8-1.8-1.8zm0-11.6H25c-1 0-1.8.8-1.8 1.8v5.2c0 1 .8 1.8 1.8 1.8h5.2c1 0 1.8-.8 1.8-1.8v-5.2c0-1-.8-1.8-1.8-1.8zM0 30.2c0 1 .8 1.8 1.8 1.8H7c1 0 1.8-.8 1.8-1.8V25c0-1-.8-1.8-1.8-1.8H1.8C.8 23.2 0 24 0 25v5.2zm0-11.6c0 1 .8 1.8 1.8 1.8H7c1 0 1.8-.8 1.8-1.8v-5.2c0-1-.8-1.8-1.8-1.8H1.8c-1 0-1.8.8-1.8 1.8v5.2z"></path></svg></span></span>
        </div>
        <ul class="_357a9937">
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/rawalpindi_capital_smart_city_5_marla_residential_plot_for_sale_in_capital_smart_city-22188596-11891-2.html" class="_7ac32433" title="5 Marla Residential Plot For Sale In Capital Smart City">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3 _01e2e273 be4c340f">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <source type="image/webp" srcset="https://media.zameen.com/thumbnails/81472943-400x300.webp"><img role="presentation" alt="5 Marla Residential Plot For Sale In Capital Smart City" title="5 Marla Residential Plot For Sale In Capital Smart City" aria-label="Listing photo" src="https://media.zameen.com/thumbnails/81472943-400x300.jpeg" class="_8f499ba9"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="super hot label">super hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5">
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f773b145" viewBox="0 0 13 9">
                                                <g fill="#FFF">
                                                    <path d="M7.57 5.22c0 .6-.5 1-1.1 1s-1.1-.5-1.1-1 .5-1 1.1-1 1.1.4 1.1 1"></path>
                                                    <path d="M11.87 1.42h-2.9l-.7-1.4h-3.6l-.7 1.4h-2.9c-.6 0-1.1.4-1.1 1v5.5c0 .6.5 1 1.1 1h10.8c.6 0 1.1-.5 1.1-1v-5.5c0-.6-.4-1-1.1-1 .1 0 .1 0 0 0zm-5.4 6.2c-1.4 0-2.5-1.1-2.5-2.4s1.1-2.4 2.5-2.4 2.5 1.1 2.5 2.4c.1 1.3-1.1 2.4-2.5 2.4.1 0 0 0 0 0z"></path>
                                                </g>
                                            </svg><span class="_78f72f87">5</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/rawalpindi_capital_smart_city_5_marla_residential_plot_for_sale_in_capital_smart_city-22188596-11891-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=5%20Marla%20Residential%20Plot%20For%20Sale%20In%20Capital%20Smart%20City - url: https://www.zameen.com/Property/rawalpindi_capital_smart_city_5_marla_residential_plot_for_sale_in_capital_smart_city-22188596-11891-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=5%20Marla%20Residential%20Plot%20For%20Sale%20In%20Capital%20Smart%20City%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Frawalpindi_capital_smart_city_5_marla_residential_plot_for_sale_in_capital_smart_city-22188596-11891-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=5%20Marla%20Residential%20Plot%20For%20Sale%20In%20Capital%20Smart%20City&amp;body=https://www.zameen.com/Property/rawalpindi_capital_smart_city_5_marla_residential_plot_for_sale_in_capital_smart_city-22188596-11891-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=5 Marla Residential Plot For Sale In Capital Smart City&amp;body=https://www.zameen.com/Property/rawalpindi_capital_smart_city_5_marla_residential_plot_for_sale_in_capital_smart_city-22188596-11891-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed _01e2e273 c7b81b5c">
                        <div class="_7ac32433" title="5 Marla Residential Plot For Sale In Capital Smart City">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">24.15 Lakh</span></div>
                                    </div>
                                </div>
                                <div class="_8e3c1d32 tether-target tether-abutted tether-abutted-top tether-out-of-bounds tether-out-of-bounds-top tether-element-attached-bottom tether-element-attached-center tether-target-attached-top tether-target-attached-center"></div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="c1a24809" viewBox="0 0 32 32">
                                            <g fill="#C32C2B">
                                                <path d="M6.4 27.4h-.2l-.1-.3c-3-3.8-3.5-8.4-1.6-13.6-.6-1-1-2-1.1-3.2l-.2-.3-.3.1c-2.6 4.3-4.8 10.6-.5 16 1.8 1.8 4.3 3 6.9 3.3a8 8 0 0 1-2.9-2z"></path>
                                                <path d="M15.6 29.6c-3.1.1-6.1-1-8.3-3.3-4.3-5.4-2.1-11.7.5-16h.4c.1 0 .2 0 .2.2.1 1.7.8 3.2 2 4.4.4-1.4 1.1-2.8 2-4 1.8-2.3 2.5-5.2 2-8l.2-.4h.1l.2.1.5.6c1.2 1.4 2.3 3 3.2 4.5.9 2 1.5 3.8 1.8 6 .7-.6 1.3-1.3 1.7-2 .3-1 .3-2-.1-3l.1-.4h.4a16 16 0 0 1 4.9 11.8 10 10 0 0 1-6.8 9c-1.7.3-3.3.5-5 .5zm-.9-13.8c-.8.8-1.5 1.8-2.1 2.8a9.1 9.1 0 0 0-1.4 4.2c0 3.6 2 6.2 4.6 6.2 1.4 0 2.8-.6 3.7-1.7 1-1 1.4-2.7 1.2-4.2-.4-1.6-1.1-3.2-2.2-4.5-.2.6-.5 1.1-.9 1.5l-.6.5h-.1c-.5 0-.9-.3-1.3-.7a5.8 5.8 0 0 1-.9-4z"></path>
                                                <path d="M31.9 18a15 15 0 0 0-4.8-9.8h-.4c-.1.1-.2.2-.1.3.3 1 .4 2 .1 3l-.1.1c1 2 1.7 4.1 1.9 6.3.6 4.7-1.8 9.4-6 11.6.9 0 1.8-.3 2.7-.6C29 27.5 31.7 24 32 20l-.1-2z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <g fill="#34A048">
                                                <path d="M25.3 16.8V6.5l-5-1.3-4.1-1h-.3l-4 1c-1.8.3-3.5.8-5.3 1.2V17c-.1 2.1.7 4.2 2 5.8 2 2.3 4.5 4.1 7.2 5.2 2.5-1 4.7-2.5 6.6-4.4 2-1.7 3-4.2 2.9-6.7zm-3.2-4.4l-2.5 3.2-4 5.2c-.8 1-1.3 1-2.1 0L9.9 16a1 1 0 0 1 0-1.5c.4-.4 1.1-.3 1.5.1l2.6 2.3.6.5.3-.5 5.6-5.5.7-.5c.4 0 .9.1 1.1.5.1.2.1.7-.2 1.1z"></path>
                                                <path d="M29.1 16.8V5a2 2 0 0 0-1.7-2.2l-6-1.5L16.8.1h-1.7l-4.5 1.1-6.1 1.5C3.5 3 2.7 4 2.8 5v11.9c-.1 3 1 6 2.9 8.2a25 25 0 0 0 9.3 6.7l.9.2c.3 0 .7 0 1-.2a24 24 0 0 0 8.4-5.7c2.6-2.3 4-5.7 3.8-9.2zm-4.8 8.5c-2.3 2.3-5 4.1-7.9 5.3-.2.1-.5.1-.8 0a22 22 0 0 1-8.8-6.3c-1.8-2-2.7-4.7-2.6-7.4V5c0-.5.1-.8.6-.9l10.6-2.6h1C20 2.4 23.5 3.2 27 4c.5.1.6.4.6.8v11.8c.3 3.4-1 6.5-3.3 8.7z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <div class="_0402297d "><span class="_6b1c4ee5"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.7 10.2" class="_85195ac8"><path fill="#f5d652" d="M10.6 3.9a.6.6 0 0 0-.5-.5L7.2 3 5.9.3a.6.6 0 0 0-.4-.3.6.6 0 0 0-.2 0 .6.6 0 0 0-.5.3L3.5 3l-3 .4a.6.6 0 0 0-.3 1l2.1 2.2-.5 2.9a.6.6 0 0 0 1 .6l2.5-1.3L8 10a.6.6 0 0 0 .5 0 .7.7 0 0 0 .1 0 .6.6 0 0 0 .3-.6l-.5-3 2-2a.6.6 0 0 0 .2-.6z"></path></svg></span><span class="e7869745">Titanium</span></div>
                                    </div>
                                </div>
                                <div class="_8e3c1d32 tether-target tether-element-attached-bottom tether-element-attached-center tether-target-attached-top tether-target-attached-center"></div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Capital Smart City, Rawalpindi</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="5 Marla Residential Plot For Sale In Capital Smart City"><div class="_1e0ca152 _026d7bff"><div><span>5 Marla</span></div>
                            </div>
                        </div>
                        </span>
                        </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">5 Marla Residential Plot For Sale In Capital Smart City</h2></div>
                        <div class="ee550b27">Invest in Pakistan's First Smart City Today - 5 Marla Residential Plots Available! Located near... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 3 hours ago</span><span aria-label="Listing updated date" class="d77ff1d8">(Updated: 1 hour ago)</span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-22188596" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-22188596">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/20470630-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/20470630-240x180.jpeg class=_54945be7 _1b380bc2 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/20470630-240x180.webp" srcset="https://media.zameen.com/thumbnails/20470630-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/20470630-240x180.jpeg" class="_54945be7 _1b380bc2 lazy loaded" src="https://media.zameen.com/thumbnails/20470630-240x180.jpeg" data-was-processed="true"></picture>
                                    <div class="_8f0b72fd">
                                        <picture class="_219b7e0a">
                                            <source type="image/webp" srcset="https://media.zameen.com/thumbnails/20470630-600x450.webp"><img role="presentation" aria-label="Listing agency photo" src="https://media.zameen.com/thumbnails/20470630-600x450.jpeg" class="_53426593"></picture>
                                        <div class="_713d6e48">Registered since: 2015</div>
                                        <div class="_4ed4ba45">329 Properties for Sale</div>
                                        <div class="_4ed4ba45">14 Properties for Rent</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li class="_53ec901b">
                <div class="_7bc4114b" id="g-ad-8935" style="width: 780px; height: 50px; display: none;" data-google-query-id="CIKw3srn2OgCFcgUGwodTIYCIg">
                    <div id="google_ads_iframe_/31946216/Long_Strip_NS_0__container__" style="border: 0pt none; width: 780px; height: 50px;"></div>
                </div>
            </li>
            <li class="_53ec901b">
                <div class="_7bc4114b" id="g-ad-3672" style="width: 650px; height: 80px; display: none;" data-google-query-id="CLeS3srn2OgCFUYWGwodY8cBeg">
                    <div id="google_ads_iframe_/31946216/Strip_1_650x80_0__container__" style="border: 0pt none; width: 650px; height: 80px;"></div>
                </div>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/chakri_road_blue_world_city_blue_world_city_7_marla_overseas_block_plot_for_sale-22346405-11768-2.html" class="_7ac32433" title="Blue World City 7 Marla overseas Block Plot for sale">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3 _01e2e273 be4c340f">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <source type="image/webp" srcset="https://media.zameen.com/thumbnails/82029911-400x300.webp"><img role="presentation" alt="Blue World City 7 Marla overseas Block Plot for sale" title="Blue World City 7 Marla overseas Block Plot for sale" aria-label="Listing photo" src="https://media.zameen.com/thumbnails/82029911-400x300.jpeg" class="_8f499ba9"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="super hot label">super hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5">
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f773b145" viewBox="0 0 13 9">
                                                <g fill="#FFF">
                                                    <path d="M7.57 5.22c0 .6-.5 1-1.1 1s-1.1-.5-1.1-1 .5-1 1.1-1 1.1.4 1.1 1"></path>
                                                    <path d="M11.87 1.42h-2.9l-.7-1.4h-3.6l-.7 1.4h-2.9c-.6 0-1.1.4-1.1 1v5.5c0 .6.5 1 1.1 1h10.8c.6 0 1.1-.5 1.1-1v-5.5c0-.6-.4-1-1.1-1 .1 0 .1 0 0 0zm-5.4 6.2c-1.4 0-2.5-1.1-2.5-2.4s1.1-2.4 2.5-2.4 2.5 1.1 2.5 2.4c.1 1.3-1.1 2.4-2.5 2.4.1 0 0 0 0 0z"></path>
                                                </g>
                                            </svg><span class="_78f72f87">10</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/chakri_road_blue_world_city_blue_world_city_7_marla_overseas_block_plot_for_sale-22346405-11768-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Blue%20World%20City%207%20Marla%20overseas%20Block%20Plot%20for%20sale - url: https://www.zameen.com/Property/chakri_road_blue_world_city_blue_world_city_7_marla_overseas_block_plot_for_sale-22346405-11768-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Blue%20World%20City%207%20Marla%20overseas%20Block%20Plot%20for%20sale%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Fchakri_road_blue_world_city_blue_world_city_7_marla_overseas_block_plot_for_sale-22346405-11768-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Blue%20World%20City%207%20Marla%20overseas%20Block%20Plot%20for%20sale&amp;body=https://www.zameen.com/Property/chakri_road_blue_world_city_blue_world_city_7_marla_overseas_block_plot_for_sale-22346405-11768-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Blue World City 7 Marla overseas Block Plot for sale&amp;body=https://www.zameen.com/Property/chakri_road_blue_world_city_blue_world_city_7_marla_overseas_block_plot_for_sale-22346405-11768-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed _01e2e273 c7b81b5c">
                        <div class="_7ac32433" title="Blue World City 7 Marla overseas Block Plot for sale">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">13.5 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="c1a24809" viewBox="0 0 32 32">
                                            <g fill="#C32C2B">
                                                <path d="M6.4 27.4h-.2l-.1-.3c-3-3.8-3.5-8.4-1.6-13.6-.6-1-1-2-1.1-3.2l-.2-.3-.3.1c-2.6 4.3-4.8 10.6-.5 16 1.8 1.8 4.3 3 6.9 3.3a8 8 0 0 1-2.9-2z"></path>
                                                <path d="M15.6 29.6c-3.1.1-6.1-1-8.3-3.3-4.3-5.4-2.1-11.7.5-16h.4c.1 0 .2 0 .2.2.1 1.7.8 3.2 2 4.4.4-1.4 1.1-2.8 2-4 1.8-2.3 2.5-5.2 2-8l.2-.4h.1l.2.1.5.6c1.2 1.4 2.3 3 3.2 4.5.9 2 1.5 3.8 1.8 6 .7-.6 1.3-1.3 1.7-2 .3-1 .3-2-.1-3l.1-.4h.4a16 16 0 0 1 4.9 11.8 10 10 0 0 1-6.8 9c-1.7.3-3.3.5-5 .5zm-.9-13.8c-.8.8-1.5 1.8-2.1 2.8a9.1 9.1 0 0 0-1.4 4.2c0 3.6 2 6.2 4.6 6.2 1.4 0 2.8-.6 3.7-1.7 1-1 1.4-2.7 1.2-4.2-.4-1.6-1.1-3.2-2.2-4.5-.2.6-.5 1.1-.9 1.5l-.6.5h-.1c-.5 0-.9-.3-1.3-.7a5.8 5.8 0 0 1-.9-4z"></path>
                                                <path d="M31.9 18a15 15 0 0 0-4.8-9.8h-.4c-.1.1-.2.2-.1.3.3 1 .4 2 .1 3l-.1.1c1 2 1.7 4.1 1.9 6.3.6 4.7-1.8 9.4-6 11.6.9 0 1.8-.3 2.7-.6C29 27.5 31.7 24 32 20l-.1-2z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <g fill="#34A048">
                                                <path d="M25.3 16.8V6.5l-5-1.3-4.1-1h-.3l-4 1c-1.8.3-3.5.8-5.3 1.2V17c-.1 2.1.7 4.2 2 5.8 2 2.3 4.5 4.1 7.2 5.2 2.5-1 4.7-2.5 6.6-4.4 2-1.7 3-4.2 2.9-6.7zm-3.2-4.4l-2.5 3.2-4 5.2c-.8 1-1.3 1-2.1 0L9.9 16a1 1 0 0 1 0-1.5c.4-.4 1.1-.3 1.5.1l2.6 2.3.6.5.3-.5 5.6-5.5.7-.5c.4 0 .9.1 1.1.5.1.2.1.7-.2 1.1z"></path>
                                                <path d="M29.1 16.8V5a2 2 0 0 0-1.7-2.2l-6-1.5L16.8.1h-1.7l-4.5 1.1-6.1 1.5C3.5 3 2.7 4 2.8 5v11.9c-.1 3 1 6 2.9 8.2a25 25 0 0 0 9.3 6.7l.9.2c.3 0 .7 0 1-.2a24 24 0 0 0 8.4-5.7c2.6-2.3 4-5.7 3.8-9.2zm-4.8 8.5c-2.3 2.3-5 4.1-7.9 5.3-.2.1-.5.1-.8 0a22 22 0 0 1-8.8-6.3c-1.8-2-2.7-4.7-2.6-7.4V5c0-.5.1-.8.6-.9l10.6-2.6h1C20 2.4 23.5 3.2 27 4c.5.1.6.4.6.8v11.8c.3 3.4-1 6.5-3.3 8.7z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <div class="_0402297d "><span class="_6b1c4ee5"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.7 10.2" class="_85195ac8"><path fill="#f5d652" d="M10.6 3.9a.6.6 0 0 0-.5-.5L7.2 3 5.9.3a.6.6 0 0 0-.4-.3.6.6 0 0 0-.2 0 .6.6 0 0 0-.5.3L3.5 3l-3 .4a.6.6 0 0 0-.3 1l2.1 2.2-.5 2.9a.6.6 0 0 0 1 .6l2.5-1.3L8 10a.6.6 0 0 0 .5 0 .7.7 0 0 0 .1 0 .6.6 0 0 0 .3-.6l-.5-3 2-2a.6.6 0 0 0 .2-.6z"></path></svg></span><span class="e7869745">Titanium</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Blue World City, Chakri Road</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Blue World City 7 Marla overseas Block Plot for sale"><div class="_1e0ca152 _026d7bff"><div><span>7 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Blue World City 7 Marla overseas Block Plot for sale</h2></div>
                        <div class="ee550b27">Blue world city all plots for sale with amazing discount limited files in old rates 7.10. 14. Marla... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 3 hours ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-22346405" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-22346405">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/30315994-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/30315994-240x180.jpeg class=_54945be7 _1b380bc2 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/30315994-240x180.webp" srcset="https://media.zameen.com/thumbnails/30315994-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/30315994-240x180.jpeg" class="_54945be7 _1b380bc2 lazy loaded" src="https://media.zameen.com/thumbnails/30315994-240x180.jpeg" data-was-processed="true"></picture>
                                    <div class="_8f0b72fd">
                                        <picture class="_219b7e0a">
                                            <source type="image/webp" srcset="https://media.zameen.com/thumbnails/30315994-600x450.webp"><img role="presentation" aria-label="Listing agency photo" src="https://media.zameen.com/thumbnails/30315994-600x450.jpeg" class="_53426593"></picture>
                                        <div class="_713d6e48">Registered since: 2018</div>
                                        <div class="_4ed4ba45">135 Properties for Sale</div>
                                        <div class="_4ed4ba45">0 Properties for Rent</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/chakri_road_blue_world_city_5_marla_residential_plot_old_booking_available_in_blue_world_city-22333674-11768-2.html" class="_7ac32433" title="5 Marla Residential Plot Old Booking Available in Blue World City">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3 _01e2e273 be4c340f">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <source type="image/webp" srcset="https://media.zameen.com/thumbnails/81974127-400x300.webp"><img role="presentation" alt="5 Marla Residential Plot Old Booking Available in Blue World City" title="5 Marla Residential Plot Old Booking Available in Blue World City" aria-label="Listing photo" src="https://media.zameen.com/thumbnails/81974127-400x300.jpeg" class="_8f499ba9"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="super hot label">super hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5">
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f773b145" viewBox="0 0 13 9">
                                                <g fill="#FFF">
                                                    <path d="M7.57 5.22c0 .6-.5 1-1.1 1s-1.1-.5-1.1-1 .5-1 1.1-1 1.1.4 1.1 1"></path>
                                                    <path d="M11.87 1.42h-2.9l-.7-1.4h-3.6l-.7 1.4h-2.9c-.6 0-1.1.4-1.1 1v5.5c0 .6.5 1 1.1 1h10.8c.6 0 1.1-.5 1.1-1v-5.5c0-.6-.4-1-1.1-1 .1 0 .1 0 0 0zm-5.4 6.2c-1.4 0-2.5-1.1-2.5-2.4s1.1-2.4 2.5-2.4 2.5 1.1 2.5 2.4c.1 1.3-1.1 2.4-2.5 2.4.1 0 0 0 0 0z"></path>
                                                </g>
                                            </svg><span class="_78f72f87">11</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/chakri_road_blue_world_city_5_marla_residential_plot_old_booking_available_in_blue_world_city-22333674-11768-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=5%20Marla%20Residential%20Plot%20Old%20Booking%20Available%20in%20Blue%20World%20City - url: https://www.zameen.com/Property/chakri_road_blue_world_city_5_marla_residential_plot_old_booking_available_in_blue_world_city-22333674-11768-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=5%20Marla%20Residential%20Plot%20Old%20Booking%20Available%20in%20Blue%20World%20City%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Fchakri_road_blue_world_city_5_marla_residential_plot_old_booking_available_in_blue_world_city-22333674-11768-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=5%20Marla%20Residential%20Plot%20Old%20Booking%20Available%20in%20Blue%20World%20City&amp;body=https://www.zameen.com/Property/chakri_road_blue_world_city_5_marla_residential_plot_old_booking_available_in_blue_world_city-22333674-11768-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=5 Marla Residential Plot Old Booking Available in Blue World City&amp;body=https://www.zameen.com/Property/chakri_road_blue_world_city_5_marla_residential_plot_old_booking_available_in_blue_world_city-22333674-11768-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed _01e2e273 c7b81b5c">
                        <div class="_7ac32433" title="5 Marla Residential Plot Old Booking Available in Blue World City">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">8 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="c1a24809" viewBox="0 0 32 32">
                                            <g fill="#C32C2B">
                                                <path d="M6.4 27.4h-.2l-.1-.3c-3-3.8-3.5-8.4-1.6-13.6-.6-1-1-2-1.1-3.2l-.2-.3-.3.1c-2.6 4.3-4.8 10.6-.5 16 1.8 1.8 4.3 3 6.9 3.3a8 8 0 0 1-2.9-2z"></path>
                                                <path d="M15.6 29.6c-3.1.1-6.1-1-8.3-3.3-4.3-5.4-2.1-11.7.5-16h.4c.1 0 .2 0 .2.2.1 1.7.8 3.2 2 4.4.4-1.4 1.1-2.8 2-4 1.8-2.3 2.5-5.2 2-8l.2-.4h.1l.2.1.5.6c1.2 1.4 2.3 3 3.2 4.5.9 2 1.5 3.8 1.8 6 .7-.6 1.3-1.3 1.7-2 .3-1 .3-2-.1-3l.1-.4h.4a16 16 0 0 1 4.9 11.8 10 10 0 0 1-6.8 9c-1.7.3-3.3.5-5 .5zm-.9-13.8c-.8.8-1.5 1.8-2.1 2.8a9.1 9.1 0 0 0-1.4 4.2c0 3.6 2 6.2 4.6 6.2 1.4 0 2.8-.6 3.7-1.7 1-1 1.4-2.7 1.2-4.2-.4-1.6-1.1-3.2-2.2-4.5-.2.6-.5 1.1-.9 1.5l-.6.5h-.1c-.5 0-.9-.3-1.3-.7a5.8 5.8 0 0 1-.9-4z"></path>
                                                <path d="M31.9 18a15 15 0 0 0-4.8-9.8h-.4c-.1.1-.2.2-.1.3.3 1 .4 2 .1 3l-.1.1c1 2 1.7 4.1 1.9 6.3.6 4.7-1.8 9.4-6 11.6.9 0 1.8-.3 2.7-.6C29 27.5 31.7 24 32 20l-.1-2z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <g fill="#34A048">
                                                <path d="M25.3 16.8V6.5l-5-1.3-4.1-1h-.3l-4 1c-1.8.3-3.5.8-5.3 1.2V17c-.1 2.1.7 4.2 2 5.8 2 2.3 4.5 4.1 7.2 5.2 2.5-1 4.7-2.5 6.6-4.4 2-1.7 3-4.2 2.9-6.7zm-3.2-4.4l-2.5 3.2-4 5.2c-.8 1-1.3 1-2.1 0L9.9 16a1 1 0 0 1 0-1.5c.4-.4 1.1-.3 1.5.1l2.6 2.3.6.5.3-.5 5.6-5.5.7-.5c.4 0 .9.1 1.1.5.1.2.1.7-.2 1.1z"></path>
                                                <path d="M29.1 16.8V5a2 2 0 0 0-1.7-2.2l-6-1.5L16.8.1h-1.7l-4.5 1.1-6.1 1.5C3.5 3 2.7 4 2.8 5v11.9c-.1 3 1 6 2.9 8.2a25 25 0 0 0 9.3 6.7l.9.2c.3 0 .7 0 1-.2a24 24 0 0 0 8.4-5.7c2.6-2.3 4-5.7 3.8-9.2zm-4.8 8.5c-2.3 2.3-5 4.1-7.9 5.3-.2.1-.5.1-.8 0a22 22 0 0 1-8.8-6.3c-1.8-2-2.7-4.7-2.6-7.4V5c0-.5.1-.8.6-.9l10.6-2.6h1C20 2.4 23.5 3.2 27 4c.5.1.6.4.6.8v11.8c.3 3.4-1 6.5-3.3 8.7z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <div class="_0402297d "><span class="_6b1c4ee5"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.7 10.2" class="_85195ac8"><path fill="#f5d652" d="M10.6 3.9a.6.6 0 0 0-.5-.5L7.2 3 5.9.3a.6.6 0 0 0-.4-.3.6.6 0 0 0-.2 0 .6.6 0 0 0-.5.3L3.5 3l-3 .4a.6.6 0 0 0-.3 1l2.1 2.2-.5 2.9a.6.6 0 0 0 1 .6l2.5-1.3L8 10a.6.6 0 0 0 .5 0 .7.7 0 0 0 .1 0 .6.6 0 0 0 .3-.6l-.5-3 2-2a.6.6 0 0 0 .2-.6z"></path></svg></span><span class="e7869745">Titanium</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Blue World City, Chakri Road</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="5 Marla Residential Plot Old Booking Available in Blue World City"><div class="_1e0ca152 _026d7bff"><div><span>5 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">5 Marla Residential Plot Old Booking Available in Blue World City</h2></div>
                        <div class="ee550b27">Blue World City is a project of Blue Group of Companies. Located Near New Islamabad International... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 5 hours ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-22333674" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-22333674">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/30315994-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/30315994-240x180.jpeg class=_54945be7 _1b380bc2 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/30315994-240x180.webp" srcset="https://media.zameen.com/thumbnails/30315994-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/30315994-240x180.jpeg" class="_54945be7 _1b380bc2 lazy loaded" src="https://media.zameen.com/thumbnails/30315994-240x180.jpeg" data-was-processed="true"></picture>
                                    <div class="_8f0b72fd">
                                        <picture class="_219b7e0a">
                                            <source type="image/webp" srcset="https://media.zameen.com/thumbnails/30315994-600x450.webp"><img role="presentation" aria-label="Listing agency photo" src="https://media.zameen.com/thumbnails/30315994-600x450.jpeg" class="_53426593"></picture>
                                        <div class="_713d6e48">Registered since: 2018</div>
                                        <div class="_4ed4ba45">135 Properties for Sale</div>
                                        <div class="_4ed4ba45">0 Properties for Rent</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li class="_53ec901b">
                <div class="_7bc4114b" id="g-ad-9823" style="width: 650px; height: 80px; display: none;" data-google-query-id="CP-d3srn2OgCFddmGwodJCQIiw">
                    <div id="google_ads_iframe_/31946216/Strip_2_650x80_0__container__" style="border: 0pt none; width: 650px; height: 80px;"></div>
                </div>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/chakri_road_blue_world_city_7_marla_plot_in_overseas_block_in_blue_world_city-22329897-11768-2.html" class="_7ac32433" title="7 Marla Plot in Overseas Block In Blue world City">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3 _01e2e273 be4c340f">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <source type="image/webp" srcset="https://media.zameen.com/thumbnails/81968473-400x300.webp"><img role="presentation" alt="7 Marla Plot in Overseas Block In Blue world City" title="7 Marla Plot in Overseas Block In Blue world City" aria-label="Listing photo" src="https://media.zameen.com/thumbnails/81968473-400x300.jpeg" class="_8f499ba9"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="super hot label">super hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5">
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f773b145" viewBox="0 0 13 9">
                                                <g fill="#FFF">
                                                    <path d="M7.57 5.22c0 .6-.5 1-1.1 1s-1.1-.5-1.1-1 .5-1 1.1-1 1.1.4 1.1 1"></path>
                                                    <path d="M11.87 1.42h-2.9l-.7-1.4h-3.6l-.7 1.4h-2.9c-.6 0-1.1.4-1.1 1v5.5c0 .6.5 1 1.1 1h10.8c.6 0 1.1-.5 1.1-1v-5.5c0-.6-.4-1-1.1-1 .1 0 .1 0 0 0zm-5.4 6.2c-1.4 0-2.5-1.1-2.5-2.4s1.1-2.4 2.5-2.4 2.5 1.1 2.5 2.4c.1 1.3-1.1 2.4-2.5 2.4.1 0 0 0 0 0z"></path>
                                                </g>
                                            </svg><span class="_78f72f87">46</span></div>
                                    </div>
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f5678b67" viewBox="0 0 32 32">
                                                <path d="M25 18.5v-2a3 3 0 0 0-3-3H3a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h19a3 3 0 0 0 3-3v-2l7 5v-16l-7 5zM11 7c0 2.5-2 4.5-4.5 4.5S2 9.5 2 7s2-4.5 4.5-4.5S11 4.5 11 7m12 0c0 2.5-2 4.5-4.5 4.5S14 9.5 14 7s2-4.5 4.5-4.5S23 4.5 23 7" fill="#FFF"></path>
                                            </svg><span class="_78f72f87">1</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/chakri_road_blue_world_city_7_marla_plot_in_overseas_block_in_blue_world_city-22329897-11768-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=7%20Marla%20Plot%20in%20Overseas%20Block%20In%20Blue%20world%20City - url: https://www.zameen.com/Property/chakri_road_blue_world_city_7_marla_plot_in_overseas_block_in_blue_world_city-22329897-11768-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=7%20Marla%20Plot%20in%20Overseas%20Block%20In%20Blue%20world%20City%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Fchakri_road_blue_world_city_7_marla_plot_in_overseas_block_in_blue_world_city-22329897-11768-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=7%20Marla%20Plot%20in%20Overseas%20Block%20In%20Blue%20world%20City&amp;body=https://www.zameen.com/Property/chakri_road_blue_world_city_7_marla_plot_in_overseas_block_in_blue_world_city-22329897-11768-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=7 Marla Plot in Overseas Block In Blue world City&amp;body=https://www.zameen.com/Property/chakri_road_blue_world_city_7_marla_plot_in_overseas_block_in_blue_world_city-22329897-11768-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="_6681ac2b">
                                <button class="_22c5fb33">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="svg-icon-sprite" viewBox="0 0 74 47">
                                        <path fill="#FFF" d="M44.4 21.7c1 .5 1.4 1.6.9 2.6a2 2 0 0 1-.9 1l-6.2 3-6.8 3.4c-.6.3-1.3.3-1.9 0a2 2 0 0 1-1-1.8V17a2 2 0 0 1 1-1.8c.6-.4 1.3-.4 1.9 0l6.8 3.3 6.2 3.3z"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed _01e2e273 c7b81b5c">
                        <div class="_7ac32433" title="7 Marla Plot in Overseas Block In Blue world City">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">1.35 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="c1a24809" viewBox="0 0 32 32">
                                            <g fill="#C32C2B">
                                                <path d="M6.4 27.4h-.2l-.1-.3c-3-3.8-3.5-8.4-1.6-13.6-.6-1-1-2-1.1-3.2l-.2-.3-.3.1c-2.6 4.3-4.8 10.6-.5 16 1.8 1.8 4.3 3 6.9 3.3a8 8 0 0 1-2.9-2z"></path>
                                                <path d="M15.6 29.6c-3.1.1-6.1-1-8.3-3.3-4.3-5.4-2.1-11.7.5-16h.4c.1 0 .2 0 .2.2.1 1.7.8 3.2 2 4.4.4-1.4 1.1-2.8 2-4 1.8-2.3 2.5-5.2 2-8l.2-.4h.1l.2.1.5.6c1.2 1.4 2.3 3 3.2 4.5.9 2 1.5 3.8 1.8 6 .7-.6 1.3-1.3 1.7-2 .3-1 .3-2-.1-3l.1-.4h.4a16 16 0 0 1 4.9 11.8 10 10 0 0 1-6.8 9c-1.7.3-3.3.5-5 .5zm-.9-13.8c-.8.8-1.5 1.8-2.1 2.8a9.1 9.1 0 0 0-1.4 4.2c0 3.6 2 6.2 4.6 6.2 1.4 0 2.8-.6 3.7-1.7 1-1 1.4-2.7 1.2-4.2-.4-1.6-1.1-3.2-2.2-4.5-.2.6-.5 1.1-.9 1.5l-.6.5h-.1c-.5 0-.9-.3-1.3-.7a5.8 5.8 0 0 1-.9-4z"></path>
                                                <path d="M31.9 18a15 15 0 0 0-4.8-9.8h-.4c-.1.1-.2.2-.1.3.3 1 .4 2 .1 3l-.1.1c1 2 1.7 4.1 1.9 6.3.6 4.7-1.8 9.4-6 11.6.9 0 1.8-.3 2.7-.6C29 27.5 31.7 24 32 20l-.1-2z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <g fill="#34A048">
                                                <path d="M25.3 16.8V6.5l-5-1.3-4.1-1h-.3l-4 1c-1.8.3-3.5.8-5.3 1.2V17c-.1 2.1.7 4.2 2 5.8 2 2.3 4.5 4.1 7.2 5.2 2.5-1 4.7-2.5 6.6-4.4 2-1.7 3-4.2 2.9-6.7zm-3.2-4.4l-2.5 3.2-4 5.2c-.8 1-1.3 1-2.1 0L9.9 16a1 1 0 0 1 0-1.5c.4-.4 1.1-.3 1.5.1l2.6 2.3.6.5.3-.5 5.6-5.5.7-.5c.4 0 .9.1 1.1.5.1.2.1.7-.2 1.1z"></path>
                                                <path d="M29.1 16.8V5a2 2 0 0 0-1.7-2.2l-6-1.5L16.8.1h-1.7l-4.5 1.1-6.1 1.5C3.5 3 2.7 4 2.8 5v11.9c-.1 3 1 6 2.9 8.2a25 25 0 0 0 9.3 6.7l.9.2c.3 0 .7 0 1-.2a24 24 0 0 0 8.4-5.7c2.6-2.3 4-5.7 3.8-9.2zm-4.8 8.5c-2.3 2.3-5 4.1-7.9 5.3-.2.1-.5.1-.8 0a22 22 0 0 1-8.8-6.3c-1.8-2-2.7-4.7-2.6-7.4V5c0-.5.1-.8.6-.9l10.6-2.6h1C20 2.4 23.5 3.2 27 4c.5.1.6.4.6.8v11.8c.3 3.4-1 6.5-3.3 8.7z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <div class="_0402297d "><span class="_6b1c4ee5"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.7 10.2" class="_85195ac8"><path fill="#f5d652" d="M10.6 3.9a.6.6 0 0 0-.5-.5L7.2 3 5.9.3a.6.6 0 0 0-.4-.3.6.6 0 0 0-.2 0 .6.6 0 0 0-.5.3L3.5 3l-3 .4a.6.6 0 0 0-.3 1l2.1 2.2-.5 2.9a.6.6 0 0 0 1 .6l2.5-1.3L8 10a.6.6 0 0 0 .5 0 .7.7 0 0 0 .1 0 .6.6 0 0 0 .3-.6l-.5-3 2-2a.6.6 0 0 0 .2-.6z"></path></svg></span><span class="e7869745">Titanium</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Blue World City, Chakri Road</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="7 Marla Plot in Overseas Block In Blue world City"><div class="_1e0ca152 _026d7bff"><div><span>7 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">7 Marla Plot in Overseas Block In Blue world City</h2></div>
                        <div class="ee550b27">Distinctive Feature of Overseas Block in Blue World City: Blue World City Islamabad is a housing... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 1 day ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-22329897" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-22329897">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/20470561-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/20470561-240x180.jpeg class=_54945be7 _1b380bc2 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/20470561-240x180.webp" srcset="https://media.zameen.com/thumbnails/20470561-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/20470561-240x180.jpeg" class="_54945be7 _1b380bc2 lazy loaded" src="https://media.zameen.com/thumbnails/20470561-240x180.jpeg" data-was-processed="true"></picture>
                                    <div class="_8f0b72fd">
                                        <picture class="_219b7e0a">
                                            <source type="image/webp" srcset="https://media.zameen.com/thumbnails/20470561-600x450.webp"><img role="presentation" aria-label="Listing agency photo" src="https://media.zameen.com/thumbnails/20470561-600x450.jpeg" class="_53426593"></picture>
                                        <div class="_713d6e48">Registered since: 2015</div>
                                        <div class="_4ed4ba45">324 Properties for Sale</div>
                                        <div class="_4ed4ba45">0 Properties for Rent</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <article class="_7b6b357c">
                <div class="_210240b5"><span>Riverwalk</span><span class="b46424f9">New Project</span></div>
                <div class="_73bd3919">
                    <a href="/new-projects/riverwalk-916.html" class="_8d9bab4b">
                        <div class="_5d194818"></div>
                    </a>
                </div>
                <div class="a4c98264">
                    <div>
                        <div class="_66fe3c1c">
                            <div class="_150e2e12">
                                <picture class="_219b7e0a">
                                    <noscript>
                                        <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/2276230-400x300.webp />
                                        <img role="presentation" src=https://media.zameen.com/thumbnails/2276230-400x300.jpeg class=_68943e06 alt=Riverwalk title=Riverwalk aria-label="Fallback listing photo" />
                                    </noscript>
                                    <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/2276230-400x300.webp" srcset="https://media.zameen.com/thumbnails/2276230-400x300.webp"><img role="presentation" alt="Riverwalk" title="Riverwalk" aria-label="Listing photo" data-src="https://media.zameen.com/thumbnails/2276230-400x300.jpeg" class="_68943e06 lazy loaded" src="https://media.zameen.com/thumbnails/2276230-400x300.jpeg" data-was-processed="true"></picture>
                            </div>
                            <div class="_6d2ea5a7">
                                <div class="f59902d8">
                                    <div class="f1475fe5">
                                        <div class="b20d55ae">
                                            <div class="_3448627a">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f773b145" viewBox="0 0 13 9">
                                                    <g fill="#FFF">
                                                        <path d="M7.57 5.22c0 .6-.5 1-1.1 1s-1.1-.5-1.1-1 .5-1 1.1-1 1.1.4 1.1 1"></path>
                                                        <path d="M11.87 1.42h-2.9l-.7-1.4h-3.6l-.7 1.4h-2.9c-.6 0-1.1.4-1.1 1v5.5c0 .6.5 1 1.1 1h10.8c.6 0 1.1-.5 1.1-1v-5.5c0-.6-.4-1-1.1-1 .1 0 .1 0 0 0zm-5.4 6.2c-1.4 0-2.5-1.1-2.5-2.4s1.1-2.4 2.5-2.4 2.5 1.1 2.5 2.4c.1 1.3-1.1 2.4-2.5 2.4.1 0 0 0 0 0z"></path>
                                                    </g>
                                                </svg><span class="_78f72f87">10</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="_7fe76c69">
                                    <div class="_54799766">
                                        <div class="a091f4d0">
                                            <div class="b20d55ae _5e324ffe _664578bf">
                                                <button class="_5715b2fb" title="Map">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                        <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                    </svg>
                                                </button>
                                            </div>
                                            <div class="b20d55ae _5e324ffe _664578bf">
                                                <div class="_3d017150" aria-label="Share property">
                                                    <button class="_5715b2fb" title="Share it">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                            <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                        </svg>
                                                    </button>
                                                    <div>
                                                        <div style="display: none;">
                                                            <div class="_12e55ca6  ">
                                                                <ul class="_55bd3d93">
                                                                    <li class="_6fdf20b6">
                                                                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/riverwalk-916.html" title="Share on Facebook" class="c6481bc9">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                                <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                                <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                            </svg><span>Share on Facebook</span></a>
                                                                    </li>
                                                                    <li class="_6fdf20b6">
                                                                        <a target="_blank" href="https://twitter.com/intent/tweet?text=Riverwalk - url: https://www.zameen.com/Property/riverwalk-916.html" title="Share on Twitter" class="c6481bc9">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                                <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                            </svg><span>Share on Twitter</span></a>
                                                                    </li>
                                                                    <li class="_6fdf20b6"></li>
                                                                    <li class="_6fdf20b6">
                                                                        <a target="_blank" href="https://wa.me/?text=Riverwalk%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Friverwalk-916.html" title="Share on WhatsApp" class="c6481bc9">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                                <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                                <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                                <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                            </svg><span>Share on WhatsApp</span></a>
                                                                    </li>
                                                                    <li class="_6fdf20b6">
                                                                        <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Riverwalk&amp;body=https://www.zameen.com/Property/riverwalk-916.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                                <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                                <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                            </svg><span>Send via GMail</span></a>
                                                                    </li>
                                                                    <li class="_6fdf20b6">
                                                                        <a target="_self" href="mailto:?subject=Riverwalk&amp;body=https://www.zameen.com/Property/riverwalk-916.html" title="Send via E-Mail" class="c6481bc9">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                                <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                            </svg><span>Send via E-Mail</span></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_70cc0e82">
                            <div class="_62bc75d8">
                                <picture class="_219b7e0a">
                                    <noscript>
                                        <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/2276230-120x90.webp />
                                        <img role="presentation" src=https://media.zameen.com/thumbnails/2276230-120x90.jpeg class=a lt=1 Riverwalk title=1 Riverwalk aria-label="Fallback listing photo" />
                                    </noscript>
                                    <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/2276230-120x90.webp" srcset="https://media.zameen.com/thumbnails/2276230-120x90.webp"><img role="presentation" alt="1 Riverwalk" title="1 Riverwalk" aria-label="mini-photo-2276230" data-src="https://media.zameen.com/thumbnails/2276230-120x90.jpeg" class="lazy loaded" src="https://media.zameen.com/thumbnails/2276230-120x90.jpeg" data-was-processed="true"></picture>
                            </div>
                            <div class="_62bc75d8">
                                <picture class="_219b7e0a">
                                    <noscript>
                                        <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/2276262-120x90.webp />
                                        <img role="presentation" src=https://media.zameen.com/thumbnails/2276262-120x90.jpeg class=a lt=2 Riverwalk title=2 Riverwalk aria-label="Fallback listing photo" />
                                    </noscript>
                                    <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/2276262-120x90.webp" srcset="https://media.zameen.com/thumbnails/2276262-120x90.webp"><img role="presentation" alt="2 Riverwalk" title="2 Riverwalk" aria-label="mini-photo-2276262" data-src="https://media.zameen.com/thumbnails/2276262-120x90.jpeg" class="lazy loaded" src="https://media.zameen.com/thumbnails/2276262-120x90.jpeg" data-was-processed="true"></picture>
                            </div>
                            <div class="_62bc75d8">
                                <picture class="_219b7e0a">
                                    <noscript>
                                        <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/77887022-120x90.webp />
                                        <img role="presentation" src=https://media.zameen.com/thumbnails/77887022-120x90.jpeg class=a lt=3 Riverwalk title=3 Riverwalk aria-label="Fallback listing photo" />
                                    </noscript>
                                    <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/77887022-120x90.webp" srcset="https://media.zameen.com/thumbnails/77887022-120x90.webp"><img role="presentation" alt="3 Riverwalk" title="3 Riverwalk" aria-label="mini-photo-77887022" data-src="https://media.zameen.com/thumbnails/77887022-120x90.jpeg" class="lazy loaded" src="https://media.zameen.com/thumbnails/77887022-120x90.jpeg" data-was-processed="true"></picture>
                            </div>
                        </div>
                    </div>
                    <div class="a2c7c87e">
                        <h2 class="_975a5723">Islamabad Expressway - Islamabad</h2><span class="_51544d2f"><span class="_7c3ad451">PKR</span>70.15 Lakh&nbsp;to&nbsp;2.15 Crore</span>
                        <div class="f946428d"><span class="_8b9b7c59">3 Beds&nbsp;Flats</span><span class="_8b9b7c59"><span>5.5 Marla</span></span><span class="_8b9b7c59">PKR81.65 Lakh</span></div>
                        <div class="f946428d"><span class="_8b9b7c59">3 Beds&nbsp;Flats</span><span class="_8b9b7c59"><span>5.3 Marla</span></span><span class="_8b9b7c59">PKR79.35 Lakh</span></div>
                        <div class="f946428d"><span class="_8b9b7c59">5 Beds&nbsp;Flats</span><span class="_8b9b7c59"><span>16.8 Marla</span></span><span class="_8b9b7c59">PKR2.15 Crore</span></div>
                        <div class="_8cce37e8">
                            <a href="/new-projects/riverwalk-916.html" class="_9cc43f20">
                                <svg xmlns="http://www.w3.org/2000/svg" class="_10b6420d" viewBox="0 0 32.1 32">
                                    <g data-name="Layer 2">
                                        <path d="M15.6 32a101.4 101.4 0 0 1-12.3-.7A3.8 3.8 0 0 1 0 27.2a17.3 17.3 0 0 1 8.7-12.5l.6-.3.5.5a10 10 0 0 0 6.2 2.4h.1a8.7 8.7 0 0 0 6.1-2.4l.5-.5.6.3A17.3 17.3 0 0 1 32 27.1a3.6 3.6 0 0 1-3 4.2h-.3a111 111 0 0 1-12.8.7zM3.4 29.5a103.6 103.6 0 0 0 12.5.7 104.8 104.8 0 0 0 12.5-.7 1.8 1.8 0 0 0 1.6-2 15 15 0 0 0-7.2-10.7 10.7 10.7 0 0 1-6.9 2.4A11.6 11.6 0 0 1 9 16.8a15.2 15.2 0 0 0-7.2 10.6 2 2 0 0 0 1.6 2.1zM16 16.2a8.1 8.1 0 1 1 8-8.1 8 8 0 0 1-8 8.1zm0-14.3a6.2 6.2 0 1 0 6.2 6.2A6.2 6.2 0 0 0 16 1.9z" data-name="Layer 1"></path>
                                    </g>
                                </svg><span class="ece6016f">Developer</span></a>
                            <a href="/new-projects/riverwalk-916.html" class="_9cc43f20">
                                <svg xmlns="http://www.w3.org/2000/svg" class="_10b6420d" viewBox="0 0 11.6 12">
                                    <path fill="#555460" d="M11.5 4.5L6 .7c-.1-.1-.2-.1-.3 0L.2 4.6c-.1 0-.2.1-.2.2v1.8c0 .2.1.3.3.3.1 0 .1 0 .2-.1l.7-.5V11c0 .2.1.3.3.3h3.1c.2 0 .3-.1.3-.3V8.3h1.8v2.8c0 .2.1.3.3.3h3.1c.2 0 .3-.1.3-.3V6.4l.7.5c.3.1.5 0 .5-.2V4.8c.1-.1 0-.2-.1-.3zm-1.7 6.2H7.4V8c0-.2-.1-.3-.3-.3H4.6c-.2 0-.3.1-.3.3v2.8H1.8V5.9l4-2.8 4 2.8v4.8zM11 6L6 2.5a.3.3 0 0 0-.4 0L.6 6V4.9l5.2-3.7L11 4.9V6z"></path>
                                </svg><span class="ece6016f">Property Types</span></a>
                        </div>
                        <div class="_9510912e">
                            <button class="_5b77d672 da62f2ae _7b380faf" type="button" aria-label="Call">Call</button>
                            <button class="_85d9f2e2 a8197536 _0a6511c6" aria-label="Send email"> Email</button>
                            <div class="_2381ffe7 _69e43419"><a href="/new-projects/reserve-unit-916.html" class="_99383342">Reserve</a></div>
                        </div>
                    </div>
                </div>
            </article>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/rawalpindi_capital_smart_city_5_marla_plot_file_for_sale_in_general_and_overseas_block_capital_smart_city-22329894-11891-2.html" class="_7ac32433" title="5 Marla Plot File For Sale In General And Overseas Block Capital Smart City">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3 _01e2e273 be4c340f">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/81968325-400x300.webp />
                                    <img role="presentation" src=https://media.zameen.com/thumbnails/81968325-400x300.jpeg class=_8f499ba9 alt=5 Marla Plot File For Sale In General And Overseas Block Capital Smart City title=5 Marla Plot File For Sale In General And Overseas Block Capital Smart City aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/81968325-400x300.webp" srcset="https://media.zameen.com/thumbnails/81968325-400x300.webp"><img role="presentation" alt="5 Marla Plot File For Sale In General And Overseas Block Capital Smart City" title="5 Marla Plot File For Sale In General And Overseas Block Capital Smart City" aria-label="Listing photo" data-src="https://media.zameen.com/thumbnails/81968325-400x300.jpeg" class="_8f499ba9 lazy loaded" src="https://media.zameen.com/thumbnails/81968325-400x300.jpeg" data-was-processed="true"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="super hot label">super hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5">
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f773b145" viewBox="0 0 13 9">
                                                <g fill="#FFF">
                                                    <path d="M7.57 5.22c0 .6-.5 1-1.1 1s-1.1-.5-1.1-1 .5-1 1.1-1 1.1.4 1.1 1"></path>
                                                    <path d="M11.87 1.42h-2.9l-.7-1.4h-3.6l-.7 1.4h-2.9c-.6 0-1.1.4-1.1 1v5.5c0 .6.5 1 1.1 1h10.8c.6 0 1.1-.5 1.1-1v-5.5c0-.6-.4-1-1.1-1 .1 0 .1 0 0 0zm-5.4 6.2c-1.4 0-2.5-1.1-2.5-2.4s1.1-2.4 2.5-2.4 2.5 1.1 2.5 2.4c.1 1.3-1.1 2.4-2.5 2.4.1 0 0 0 0 0z"></path>
                                                </g>
                                            </svg><span class="_78f72f87">50</span></div>
                                    </div>
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f5678b67" viewBox="0 0 32 32">
                                                <path d="M25 18.5v-2a3 3 0 0 0-3-3H3a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h19a3 3 0 0 0 3-3v-2l7 5v-16l-7 5zM11 7c0 2.5-2 4.5-4.5 4.5S2 9.5 2 7s2-4.5 4.5-4.5S11 4.5 11 7m12 0c0 2.5-2 4.5-4.5 4.5S14 9.5 14 7s2-4.5 4.5-4.5S23 4.5 23 7" fill="#FFF"></path>
                                            </svg><span class="_78f72f87">1</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/rawalpindi_capital_smart_city_5_marla_plot_file_for_sale_in_general_and_overseas_block_capital_smart_city-22329894-11891-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=5%20Marla%20Plot%20File%20For%20Sale%20In%20General%20And%20Overseas%20Block%20Capital%20Smart%20City - url: https://www.zameen.com/Property/rawalpindi_capital_smart_city_5_marla_plot_file_for_sale_in_general_and_overseas_block_capital_smart_city-22329894-11891-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=5%20Marla%20Plot%20File%20For%20Sale%20In%20General%20And%20Overseas%20Block%20Capital%20Smart%20City%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Frawalpindi_capital_smart_city_5_marla_plot_file_for_sale_in_general_and_overseas_block_capital_smart_city-22329894-11891-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=5%20Marla%20Plot%20File%20For%20Sale%20In%20General%20And%20Overseas%20Block%20Capital%20Smart%20City&amp;body=https://www.zameen.com/Property/rawalpindi_capital_smart_city_5_marla_plot_file_for_sale_in_general_and_overseas_block_capital_smart_city-22329894-11891-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=5 Marla Plot File For Sale In General And Overseas Block Capital Smart City&amp;body=https://www.zameen.com/Property/rawalpindi_capital_smart_city_5_marla_plot_file_for_sale_in_general_and_overseas_block_capital_smart_city-22329894-11891-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="_6681ac2b">
                                <button class="_22c5fb33">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="svg-icon-sprite" viewBox="0 0 74 47">
                                        <path fill="#FFF" d="M44.4 21.7c1 .5 1.4 1.6.9 2.6a2 2 0 0 1-.9 1l-6.2 3-6.8 3.4c-.6.3-1.3.3-1.9 0a2 2 0 0 1-1-1.8V17a2 2 0 0 1 1-1.8c.6-.4 1.3-.4 1.9 0l6.8 3.3 6.2 3.3z"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed _01e2e273 c7b81b5c">
                        <div class="_7ac32433" title="5 Marla Plot File For Sale In General And Overseas Block Capital Smart City">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">24.15 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="c1a24809" viewBox="0 0 32 32">
                                            <g fill="#C32C2B">
                                                <path d="M6.4 27.4h-.2l-.1-.3c-3-3.8-3.5-8.4-1.6-13.6-.6-1-1-2-1.1-3.2l-.2-.3-.3.1c-2.6 4.3-4.8 10.6-.5 16 1.8 1.8 4.3 3 6.9 3.3a8 8 0 0 1-2.9-2z"></path>
                                                <path d="M15.6 29.6c-3.1.1-6.1-1-8.3-3.3-4.3-5.4-2.1-11.7.5-16h.4c.1 0 .2 0 .2.2.1 1.7.8 3.2 2 4.4.4-1.4 1.1-2.8 2-4 1.8-2.3 2.5-5.2 2-8l.2-.4h.1l.2.1.5.6c1.2 1.4 2.3 3 3.2 4.5.9 2 1.5 3.8 1.8 6 .7-.6 1.3-1.3 1.7-2 .3-1 .3-2-.1-3l.1-.4h.4a16 16 0 0 1 4.9 11.8 10 10 0 0 1-6.8 9c-1.7.3-3.3.5-5 .5zm-.9-13.8c-.8.8-1.5 1.8-2.1 2.8a9.1 9.1 0 0 0-1.4 4.2c0 3.6 2 6.2 4.6 6.2 1.4 0 2.8-.6 3.7-1.7 1-1 1.4-2.7 1.2-4.2-.4-1.6-1.1-3.2-2.2-4.5-.2.6-.5 1.1-.9 1.5l-.6.5h-.1c-.5 0-.9-.3-1.3-.7a5.8 5.8 0 0 1-.9-4z"></path>
                                                <path d="M31.9 18a15 15 0 0 0-4.8-9.8h-.4c-.1.1-.2.2-.1.3.3 1 .4 2 .1 3l-.1.1c1 2 1.7 4.1 1.9 6.3.6 4.7-1.8 9.4-6 11.6.9 0 1.8-.3 2.7-.6C29 27.5 31.7 24 32 20l-.1-2z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <g fill="#34A048">
                                                <path d="M25.3 16.8V6.5l-5-1.3-4.1-1h-.3l-4 1c-1.8.3-3.5.8-5.3 1.2V17c-.1 2.1.7 4.2 2 5.8 2 2.3 4.5 4.1 7.2 5.2 2.5-1 4.7-2.5 6.6-4.4 2-1.7 3-4.2 2.9-6.7zm-3.2-4.4l-2.5 3.2-4 5.2c-.8 1-1.3 1-2.1 0L9.9 16a1 1 0 0 1 0-1.5c.4-.4 1.1-.3 1.5.1l2.6 2.3.6.5.3-.5 5.6-5.5.7-.5c.4 0 .9.1 1.1.5.1.2.1.7-.2 1.1z"></path>
                                                <path d="M29.1 16.8V5a2 2 0 0 0-1.7-2.2l-6-1.5L16.8.1h-1.7l-4.5 1.1-6.1 1.5C3.5 3 2.7 4 2.8 5v11.9c-.1 3 1 6 2.9 8.2a25 25 0 0 0 9.3 6.7l.9.2c.3 0 .7 0 1-.2a24 24 0 0 0 8.4-5.7c2.6-2.3 4-5.7 3.8-9.2zm-4.8 8.5c-2.3 2.3-5 4.1-7.9 5.3-.2.1-.5.1-.8 0a22 22 0 0 1-8.8-6.3c-1.8-2-2.7-4.7-2.6-7.4V5c0-.5.1-.8.6-.9l10.6-2.6h1C20 2.4 23.5 3.2 27 4c.5.1.6.4.6.8v11.8c.3 3.4-1 6.5-3.3 8.7z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <div class="_0402297d "><span class="_6b1c4ee5"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.7 10.2" class="_85195ac8"><path fill="#f5d652" d="M10.6 3.9a.6.6 0 0 0-.5-.5L7.2 3 5.9.3a.6.6 0 0 0-.4-.3.6.6 0 0 0-.2 0 .6.6 0 0 0-.5.3L3.5 3l-3 .4a.6.6 0 0 0-.3 1l2.1 2.2-.5 2.9a.6.6 0 0 0 1 .6l2.5-1.3L8 10a.6.6 0 0 0 .5 0 .7.7 0 0 0 .1 0 .6.6 0 0 0 .3-.6l-.5-3 2-2a.6.6 0 0 0 .2-.6z"></path></svg></span><span class="e7869745">Titanium</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Capital Smart City, Rawalpindi</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="5 Marla Plot File For Sale In General And Overseas Block Capital Smart City"><div class="_1e0ca152 _026d7bff"><div><span>5 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">5 Marla Plot File For Sale In General And Overseas Block Capital Smart City</h2></div>
                        <div class="ee550b27">Located at only 10 mins from Islamabad airport, 3 Km away from Thalian Interchange on Lahore-... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 1 day ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-22329894" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-22329894">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/20470561-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/20470561-240x180.jpeg class=_54945be7 _1b380bc2 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/20470561-240x180.webp" srcset="https://media.zameen.com/thumbnails/20470561-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/20470561-240x180.jpeg" class="_54945be7 _1b380bc2 lazy loaded" src="https://media.zameen.com/thumbnails/20470561-240x180.jpeg" data-was-processed="true"></picture>
                                    <div class="_8f0b72fd">
                                        <picture class="_219b7e0a">
                                            <noscript>
                                                <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/20470561-600x450.webp />
                                                <img role="presentation" src=https://media.zameen.com/thumbnails/20470561-600x450.jpeg class=_53426593 alt=t itle=a ria-label="Fallback listing photo" />
                                            </noscript>
                                            <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/20470561-600x450.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/20470561-600x450.jpeg" class="_53426593 lazy loaded"></picture>
                                        <div class="_713d6e48">Registered since: 2015</div>
                                        <div class="_4ed4ba45">324 Properties for Sale</div>
                                        <div class="_4ed4ba45">0 Properties for Rent</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li class="_53ec901b">
                <div class="_7bc4114b" id="g-ad-4117" style="width: 650px; height: 80px; display: none;" data-google-query-id="COSp4crn2OgCFVXV1QodAz0DAg">
                    <div id="google_ads_iframe_/31946216/Strip_3_650x80_0__container__" style="border: 0pt none; width: 650px; height: 80px;"></div>
                </div>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/bahria_town_rawalpindi_bahria_town_phase_7_residential_plot_for_sale_in__intellectual_village_bahria_town_phase_7-22148880-3047-2.html" class="_7ac32433" title="Residential Plot For Sale In  Intellectual Village Bahria Town Phase 7">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3 _01e2e273 be4c340f">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://images.zameen.com/smap/400/300/33.531951/73.115215/image.jpeg?quality=80&imageformat=jpeg />
                                    <img role="presentation" src=https://images.zameen.com/smap/400/300/33.531951/73.115215/image.jpeg?quality=80&imageformat=jpeg class=_8f499ba9 alt=Residential Plot For Sale In Intellectual Village Bahria Town Phase 7 title=Residential Plot For Sale In Intellectual Village Bahria Town Phase 7 aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://images.zameen.com/smap/400/300/33.531951/73.115215/image.jpeg?quality=80&amp;imageformat=jpeg" srcset="https://images.zameen.com/smap/400/300/33.531951/73.115215/image.jpeg?quality=80&amp;imageformat=jpeg"><img role="presentation" alt="Residential Plot For Sale In  Intellectual Village Bahria Town Phase 7" title="Residential Plot For Sale In  Intellectual Village Bahria Town Phase 7" aria-label="Listing photo" data-src="https://images.zameen.com/smap/400/300/33.531951/73.115215/image.jpeg?quality=80&amp;imageformat=jpeg" class="_8f499ba9 lazy loaded" src="https://images.zameen.com/smap/400/300/33.531951/73.115215/image.jpeg?quality=80&amp;imageformat=jpeg" data-was-processed="true"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="super hot label">super hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5">
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f5678b67" viewBox="0 0 32 32">
                                                <path d="M25 18.5v-2a3 3 0 0 0-3-3H3a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h19a3 3 0 0 0 3-3v-2l7 5v-16l-7 5zM11 7c0 2.5-2 4.5-4.5 4.5S2 9.5 2 7s2-4.5 4.5-4.5S11 4.5 11 7m12 0c0 2.5-2 4.5-4.5 4.5S14 9.5 14 7s2-4.5 4.5-4.5S23 4.5 23 7" fill="#FFF"></path>
                                            </svg><span class="_78f72f87">1</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/bahria_town_rawalpindi_bahria_town_phase_7_residential_plot_for_sale_in__intellectual_village_bahria_town_phase_7-22148880-3047-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Residential%20Plot%20For%20Sale%20In%20%20Intellectual%20Village%20Bahria%20Town%20Phase%207 - url: https://www.zameen.com/Property/bahria_town_rawalpindi_bahria_town_phase_7_residential_plot_for_sale_in__intellectual_village_bahria_town_phase_7-22148880-3047-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Residential%20Plot%20For%20Sale%20In%20%20Intellectual%20Village%20Bahria%20Town%20Phase%207%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Fbahria_town_rawalpindi_bahria_town_phase_7_residential_plot_for_sale_in__intellectual_village_bahria_town_phase_7-22148880-3047-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Residential%20Plot%20For%20Sale%20In%20%20Intellectual%20Village%20Bahria%20Town%20Phase%207&amp;body=https://www.zameen.com/Property/bahria_town_rawalpindi_bahria_town_phase_7_residential_plot_for_sale_in__intellectual_village_bahria_town_phase_7-22148880-3047-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Residential Plot For Sale In  Intellectual Village Bahria Town Phase 7&amp;body=https://www.zameen.com/Property/bahria_town_rawalpindi_bahria_town_phase_7_residential_plot_for_sale_in__intellectual_village_bahria_town_phase_7-22148880-3047-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="_6681ac2b">
                                <button class="_22c5fb33">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="svg-icon-sprite" viewBox="0 0 74 47">
                                        <path fill="#FFF" d="M44.4 21.7c1 .5 1.4 1.6.9 2.6a2 2 0 0 1-.9 1l-6.2 3-6.8 3.4c-.6.3-1.3.3-1.9 0a2 2 0 0 1-1-1.8V17a2 2 0 0 1 1-1.8c.6-.4 1.3-.4 1.9 0l6.8 3.3 6.2 3.3z"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed _01e2e273 c7b81b5c">
                        <div class="_7ac32433" title="Residential Plot For Sale In  Intellectual Village Bahria Town Phase 7">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">3.6 Crore</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="c1a24809" viewBox="0 0 32 32">
                                            <g fill="#C32C2B">
                                                <path d="M6.4 27.4h-.2l-.1-.3c-3-3.8-3.5-8.4-1.6-13.6-.6-1-1-2-1.1-3.2l-.2-.3-.3.1c-2.6 4.3-4.8 10.6-.5 16 1.8 1.8 4.3 3 6.9 3.3a8 8 0 0 1-2.9-2z"></path>
                                                <path d="M15.6 29.6c-3.1.1-6.1-1-8.3-3.3-4.3-5.4-2.1-11.7.5-16h.4c.1 0 .2 0 .2.2.1 1.7.8 3.2 2 4.4.4-1.4 1.1-2.8 2-4 1.8-2.3 2.5-5.2 2-8l.2-.4h.1l.2.1.5.6c1.2 1.4 2.3 3 3.2 4.5.9 2 1.5 3.8 1.8 6 .7-.6 1.3-1.3 1.7-2 .3-1 .3-2-.1-3l.1-.4h.4a16 16 0 0 1 4.9 11.8 10 10 0 0 1-6.8 9c-1.7.3-3.3.5-5 .5zm-.9-13.8c-.8.8-1.5 1.8-2.1 2.8a9.1 9.1 0 0 0-1.4 4.2c0 3.6 2 6.2 4.6 6.2 1.4 0 2.8-.6 3.7-1.7 1-1 1.4-2.7 1.2-4.2-.4-1.6-1.1-3.2-2.2-4.5-.2.6-.5 1.1-.9 1.5l-.6.5h-.1c-.5 0-.9-.3-1.3-.7a5.8 5.8 0 0 1-.9-4z"></path>
                                                <path d="M31.9 18a15 15 0 0 0-4.8-9.8h-.4c-.1.1-.2.2-.1.3.3 1 .4 2 .1 3l-.1.1c1 2 1.7 4.1 1.9 6.3.6 4.7-1.8 9.4-6 11.6.9 0 1.8-.3 2.7-.6C29 27.5 31.7 24 32 20l-.1-2z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Bahria Town Phase 7, Bahria Town Rawalpindi</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Residential Plot For Sale In  Intellectual Village Bahria Town Phase 7"><div class="_1e0ca152 _026d7bff"><div><span>1 Kanal</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Residential Plot For Sale In  Intellectual Village Bahria Town Phase 7</h2></div>
                        <div class="ee550b27">Plot# 314 is available for sale. If you're looking for 4500 Square Feet properties that fit your... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 3 hours ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-22148880" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-22148880">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/bahria_town_phase_8_bahria_town_phase_8_sector_f_2_bahria_town_phase_8_f2_sector_10_marla_plot_boulevard_category-22136651-8250-2.html" class="_7ac32433" title="Bahria Town Phase 8 F2 Sector 10 Marla Plot Boulevard Category">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3 _01e2e273 be4c340f">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/81190823-400x300.webp />
                                    <img role="presentation" src=https://media.zameen.com/thumbnails/81190823-400x300.jpeg class=_8f499ba9 alt=Bahria Town Phase 8 F2 Sector 10 Marla Plot Boulevard Category title=Bahria Town Phase 8 F2 Sector 10 Marla Plot Boulevard Category aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/81190823-400x300.webp" srcset="https://media.zameen.com/thumbnails/81190823-400x300.webp"><img role="presentation" alt="Bahria Town Phase 8 F2 Sector 10 Marla Plot Boulevard Category" title="Bahria Town Phase 8 F2 Sector 10 Marla Plot Boulevard Category" aria-label="Listing photo" data-src="https://media.zameen.com/thumbnails/81190823-400x300.jpeg" class="_8f499ba9 lazy loaded" src="https://media.zameen.com/thumbnails/81190823-400x300.jpeg" data-was-processed="true"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="super hot label">super hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5">
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f773b145" viewBox="0 0 13 9">
                                                <g fill="#FFF">
                                                    <path d="M7.57 5.22c0 .6-.5 1-1.1 1s-1.1-.5-1.1-1 .5-1 1.1-1 1.1.4 1.1 1"></path>
                                                    <path d="M11.87 1.42h-2.9l-.7-1.4h-3.6l-.7 1.4h-2.9c-.6 0-1.1.4-1.1 1v5.5c0 .6.5 1 1.1 1h10.8c.6 0 1.1-.5 1.1-1v-5.5c0-.6-.4-1-1.1-1 .1 0 .1 0 0 0zm-5.4 6.2c-1.4 0-2.5-1.1-2.5-2.4s1.1-2.4 2.5-2.4 2.5 1.1 2.5 2.4c.1 1.3-1.1 2.4-2.5 2.4.1 0 0 0 0 0z"></path>
                                                </g>
                                            </svg><span class="_78f72f87">1</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_sector_f_2_bahria_town_phase_8_f2_sector_10_marla_plot_boulevard_category-22136651-8250-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Bahria%20Town%20Phase%208%20F2%20Sector%2010%20Marla%20Plot%20Boulevard%20Category - url: https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_sector_f_2_bahria_town_phase_8_f2_sector_10_marla_plot_boulevard_category-22136651-8250-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Bahria%20Town%20Phase%208%20F2%20Sector%2010%20Marla%20Plot%20Boulevard%20Category%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Fbahria_town_phase_8_bahria_town_phase_8_sector_f_2_bahria_town_phase_8_f2_sector_10_marla_plot_boulevard_category-22136651-8250-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Bahria%20Town%20Phase%208%20F2%20Sector%2010%20Marla%20Plot%20Boulevard%20Category&amp;body=https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_sector_f_2_bahria_town_phase_8_f2_sector_10_marla_plot_boulevard_category-22136651-8250-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Bahria Town Phase 8 F2 Sector 10 Marla Plot Boulevard Category&amp;body=https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_sector_f_2_bahria_town_phase_8_f2_sector_10_marla_plot_boulevard_category-22136651-8250-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed _01e2e273 c7b81b5c">
                        <div class="_7ac32433" title="Bahria Town Phase 8 F2 Sector 10 Marla Plot Boulevard Category">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">43 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="c1a24809" viewBox="0 0 32 32">
                                            <g fill="#C32C2B">
                                                <path d="M6.4 27.4h-.2l-.1-.3c-3-3.8-3.5-8.4-1.6-13.6-.6-1-1-2-1.1-3.2l-.2-.3-.3.1c-2.6 4.3-4.8 10.6-.5 16 1.8 1.8 4.3 3 6.9 3.3a8 8 0 0 1-2.9-2z"></path>
                                                <path d="M15.6 29.6c-3.1.1-6.1-1-8.3-3.3-4.3-5.4-2.1-11.7.5-16h.4c.1 0 .2 0 .2.2.1 1.7.8 3.2 2 4.4.4-1.4 1.1-2.8 2-4 1.8-2.3 2.5-5.2 2-8l.2-.4h.1l.2.1.5.6c1.2 1.4 2.3 3 3.2 4.5.9 2 1.5 3.8 1.8 6 .7-.6 1.3-1.3 1.7-2 .3-1 .3-2-.1-3l.1-.4h.4a16 16 0 0 1 4.9 11.8 10 10 0 0 1-6.8 9c-1.7.3-3.3.5-5 .5zm-.9-13.8c-.8.8-1.5 1.8-2.1 2.8a9.1 9.1 0 0 0-1.4 4.2c0 3.6 2 6.2 4.6 6.2 1.4 0 2.8-.6 3.7-1.7 1-1 1.4-2.7 1.2-4.2-.4-1.6-1.1-3.2-2.2-4.5-.2.6-.5 1.1-.9 1.5l-.6.5h-.1c-.5 0-.9-.3-1.3-.7a5.8 5.8 0 0 1-.9-4z"></path>
                                                <path d="M31.9 18a15 15 0 0 0-4.8-9.8h-.4c-.1.1-.2.2-.1.3.3 1 .4 2 .1 3l-.1.1c1 2 1.7 4.1 1.9 6.3.6 4.7-1.8 9.4-6 11.6.9 0 1.8-.3 2.7-.6C29 27.5 31.7 24 32 20l-.1-2z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <g fill="#34A048">
                                                <path d="M25.3 16.8V6.5l-5-1.3-4.1-1h-.3l-4 1c-1.8.3-3.5.8-5.3 1.2V17c-.1 2.1.7 4.2 2 5.8 2 2.3 4.5 4.1 7.2 5.2 2.5-1 4.7-2.5 6.6-4.4 2-1.7 3-4.2 2.9-6.7zm-3.2-4.4l-2.5 3.2-4 5.2c-.8 1-1.3 1-2.1 0L9.9 16a1 1 0 0 1 0-1.5c.4-.4 1.1-.3 1.5.1l2.6 2.3.6.5.3-.5 5.6-5.5.7-.5c.4 0 .9.1 1.1.5.1.2.1.7-.2 1.1z"></path>
                                                <path d="M29.1 16.8V5a2 2 0 0 0-1.7-2.2l-6-1.5L16.8.1h-1.7l-4.5 1.1-6.1 1.5C3.5 3 2.7 4 2.8 5v11.9c-.1 3 1 6 2.9 8.2a25 25 0 0 0 9.3 6.7l.9.2c.3 0 .7 0 1-.2a24 24 0 0 0 8.4-5.7c2.6-2.3 4-5.7 3.8-9.2zm-4.8 8.5c-2.3 2.3-5 4.1-7.9 5.3-.2.1-.5.1-.8 0a22 22 0 0 1-8.8-6.3c-1.8-2-2.7-4.7-2.6-7.4V5c0-.5.1-.8.6-.9l10.6-2.6h1C20 2.4 23.5 3.2 27 4c.5.1.6.4.6.8v11.8c.3 3.4-1 6.5-3.3 8.7z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Bahria Town Phase 8 - Sector F-2, Bahria Town Phase 8</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Bahria Town Phase 8 F2 Sector 10 Marla Plot Boulevard Category"><div class="_1e0ca152 _026d7bff"><div><span>10 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Bahria Town Phase 8 F2 Sector 10 Marla Plot Boulevard Category</h2></div>
                        <div class="ee550b27">Bahria Town Phase 8 F2 Sector 10 Marla Plot Boulevard Category Open Transfer(No Transfer Fee) +... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 1 day ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-22136651" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-22136651">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/72146816-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/72146816-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/72146816-240x180.webp" srcset="https://media.zameen.com/thumbnails/72146816-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/72146816-240x180.jpeg" class="_54945be7 lazy loaded" src="https://media.zameen.com/thumbnails/72146816-240x180.jpeg" data-was-processed="true"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li class="_53ec901b">
                <div class="_7bc4114b" id="g-ad-5359" style="width: 650px; height: 80px; display: none;" data-google-query-id="CMq75Mrn2OgCFUjg1QodvMEC_A">
                    <div id="google_ads_iframe_/31946216/Strip_4_650x80_0__container__" style="border: 0pt none; width: 650px; height: 80px;"></div>
                </div>
            </li>
            <article class="_7b6b357c">
                <div class="_210240b5"><span>Grand Millennium</span><span class="b46424f9">New Project</span></div>
                <div class="_73bd3919">
                    <a href="/new-projects/grand_millennium-1791.html" class="_8d9bab4b">
                        <div class="_5d194818"></div>
                    </a>
                </div>
                <div class="a4c98264">
                    <div>
                        <div class="_66fe3c1c">
                            <div class="_150e2e12">
                                <picture class="_219b7e0a">
                                    <noscript>
                                        <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/74059933-400x300.webp />
                                        <img role="presentation" src=https://media.zameen.com/thumbnails/74059933-400x300.jpeg class=_68943e06 alt=Grand Millennium title=Grand Millennium aria-label="Fallback listing photo" />
                                    </noscript>
                                    <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/74059933-400x300.webp" srcset="https://media.zameen.com/thumbnails/74059933-400x300.webp"><img role="presentation" alt="Grand Millennium" title="Grand Millennium" aria-label="Listing photo" data-src="https://media.zameen.com/thumbnails/74059933-400x300.jpeg" class="_68943e06 lazy loaded" src="https://media.zameen.com/thumbnails/74059933-400x300.jpeg" data-was-processed="true"></picture>
                            </div>
                            <div class="_6d2ea5a7">
                                <div class="f59902d8">
                                    <div class="f1475fe5">
                                        <div class="b20d55ae">
                                            <div class="_3448627a">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f773b145" viewBox="0 0 13 9">
                                                    <g fill="#FFF">
                                                        <path d="M7.57 5.22c0 .6-.5 1-1.1 1s-1.1-.5-1.1-1 .5-1 1.1-1 1.1.4 1.1 1"></path>
                                                        <path d="M11.87 1.42h-2.9l-.7-1.4h-3.6l-.7 1.4h-2.9c-.6 0-1.1.4-1.1 1v5.5c0 .6.5 1 1.1 1h10.8c.6 0 1.1-.5 1.1-1v-5.5c0-.6-.4-1-1.1-1 .1 0 .1 0 0 0zm-5.4 6.2c-1.4 0-2.5-1.1-2.5-2.4s1.1-2.4 2.5-2.4 2.5 1.1 2.5 2.4c.1 1.3-1.1 2.4-2.5 2.4.1 0 0 0 0 0z"></path>
                                                    </g>
                                                </svg><span class="_78f72f87">15</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="_7fe76c69">
                                    <div class="_54799766">
                                        <div class="a091f4d0">
                                            <div class="b20d55ae _5e324ffe _664578bf">
                                                <button class="_5715b2fb" title="Map">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                        <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                    </svg>
                                                </button>
                                            </div>
                                            <div class="b20d55ae _5e324ffe _664578bf">
                                                <div class="_3d017150" aria-label="Share property">
                                                    <button class="_5715b2fb" title="Share it">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                            <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                        </svg>
                                                    </button>
                                                    <div>
                                                        <div style="display: none;">
                                                            <div class="_12e55ca6  ">
                                                                <ul class="_55bd3d93">
                                                                    <li class="_6fdf20b6">
                                                                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/grand_millennium-1791.html" title="Share on Facebook" class="c6481bc9">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                                <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                                <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                            </svg><span>Share on Facebook</span></a>
                                                                    </li>
                                                                    <li class="_6fdf20b6">
                                                                        <a target="_blank" href="https://twitter.com/intent/tweet?text=Grand%20Millennium - url: https://www.zameen.com/Property/grand_millennium-1791.html" title="Share on Twitter" class="c6481bc9">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                                <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                            </svg><span>Share on Twitter</span></a>
                                                                    </li>
                                                                    <li class="_6fdf20b6"></li>
                                                                    <li class="_6fdf20b6">
                                                                        <a target="_blank" href="https://wa.me/?text=Grand%20Millennium%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Fgrand_millennium-1791.html" title="Share on WhatsApp" class="c6481bc9">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                                <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                                <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                                <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                            </svg><span>Share on WhatsApp</span></a>
                                                                    </li>
                                                                    <li class="_6fdf20b6">
                                                                        <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Grand%20Millennium&amp;body=https://www.zameen.com/Property/grand_millennium-1791.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                                <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                                <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                            </svg><span>Send via GMail</span></a>
                                                                    </li>
                                                                    <li class="_6fdf20b6">
                                                                        <a target="_self" href="mailto:?subject=Grand Millennium&amp;body=https://www.zameen.com/Property/grand_millennium-1791.html" title="Send via E-Mail" class="c6481bc9">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                                <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                            </svg><span>Send via E-Mail</span></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_70cc0e82">
                            <div class="_62bc75d8">
                                <picture class="_219b7e0a">
                                    <noscript>
                                        <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/74059933-120x90.webp />
                                        <img role="presentation" src=https://media.zameen.com/thumbnails/74059933-120x90.jpeg class=a lt=1 Grand Millennium title=1 Grand Millennium aria-label="Fallback listing photo" />
                                    </noscript>
                                    <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/74059933-120x90.webp" srcset="https://media.zameen.com/thumbnails/74059933-120x90.webp"><img role="presentation" alt="1 Grand Millennium" title="1 Grand Millennium" aria-label="mini-photo-74059933" data-src="https://media.zameen.com/thumbnails/74059933-120x90.jpeg" class="lazy loaded" src="https://media.zameen.com/thumbnails/74059933-120x90.jpeg" data-was-processed="true"></picture>
                            </div>
                            <div class="_62bc75d8">
                                <picture class="_219b7e0a">
                                    <noscript>
                                        <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/74059934-120x90.webp />
                                        <img role="presentation" src=https://media.zameen.com/thumbnails/74059934-120x90.jpeg class=a lt=2 Grand Millennium title=2 Grand Millennium aria-label="Fallback listing photo" />
                                    </noscript>
                                    <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/74059934-120x90.webp" srcset="https://media.zameen.com/thumbnails/74059934-120x90.webp"><img role="presentation" alt="2 Grand Millennium" title="2 Grand Millennium" aria-label="mini-photo-74059934" data-src="https://media.zameen.com/thumbnails/74059934-120x90.jpeg" class="lazy loaded" src="https://media.zameen.com/thumbnails/74059934-120x90.jpeg" data-was-processed="true"></picture>
                            </div>
                            <div class="_62bc75d8">
                                <picture class="_219b7e0a">
                                    <noscript>
                                        <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/74059935-120x90.webp />
                                        <img role="presentation" src=https://media.zameen.com/thumbnails/74059935-120x90.jpeg class=a lt=3 Grand Millennium title=3 Grand Millennium aria-label="Fallback listing photo" />
                                    </noscript>
                                    <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/74059935-120x90.webp" srcset="https://media.zameen.com/thumbnails/74059935-120x90.webp"><img role="presentation" alt="3 Grand Millennium" title="3 Grand Millennium" aria-label="mini-photo-74059935" data-src="https://media.zameen.com/thumbnails/74059935-120x90.jpeg" class="lazy loaded" src="https://media.zameen.com/thumbnails/74059935-120x90.jpeg" data-was-processed="true"></picture>
                            </div>
                        </div>
                    </div>
                    <div class="a2c7c87e">
                        <h2 class="_975a5723">Bahria Business District - Bahria Town Rawalpindi</h2><span class="_51544d2f"><span class="_7c3ad451">PKR</span>61.8 Lakh&nbsp;to&nbsp;1.88 Crore</span>
                        <div class="f946428d"><span class="_8b9b7c59">1 Beds&nbsp;Flats</span><span class="_8b9b7c59"><span>3.3 Marla</span></span><span class="_8b9b7c59">PKR90.75 Lakh</span></div>
                        <div class="f946428d"><span class="_8b9b7c59">3 Beds&nbsp;Flats</span><span class="_8b9b7c59"><span>6.9 Marla</span></span><span class="_8b9b7c59">PKR1.88 Crore</span></div>
                        <div class="f946428d"><span class="_8b9b7c59">2 Beds&nbsp;Flats</span><span class="_8b9b7c59"><span>5.8 Marla</span></span><span class="_8b9b7c59">PKR1.64 Crore</span></div>
                        <div class="_28f12c58">
                            <button class="_5b77d672 da62f2ae _7b380faf" type="button" aria-label="Call">Call</button>
                            <button class="_85d9f2e2 a8197536 _0a6511c6" aria-label="Send email"> Email</button>
                            <a href="/new-projects/grand_millennium-1791.html" class="_9cc43f20">
                                <svg xmlns="http://www.w3.org/2000/svg" class="_10b6420d" viewBox="0 0 32.1 32">
                                    <g data-name="Layer 2">
                                        <path d="M15.6 32a101.4 101.4 0 0 1-12.3-.7A3.8 3.8 0 0 1 0 27.2a17.3 17.3 0 0 1 8.7-12.5l.6-.3.5.5a10 10 0 0 0 6.2 2.4h.1a8.7 8.7 0 0 0 6.1-2.4l.5-.5.6.3A17.3 17.3 0 0 1 32 27.1a3.6 3.6 0 0 1-3 4.2h-.3a111 111 0 0 1-12.8.7zM3.4 29.5a103.6 103.6 0 0 0 12.5.7 104.8 104.8 0 0 0 12.5-.7 1.8 1.8 0 0 0 1.6-2 15 15 0 0 0-7.2-10.7 10.7 10.7 0 0 1-6.9 2.4A11.6 11.6 0 0 1 9 16.8a15.2 15.2 0 0 0-7.2 10.6 2 2 0 0 0 1.6 2.1zM16 16.2a8.1 8.1 0 1 1 8-8.1 8 8 0 0 1-8 8.1zm0-14.3a6.2 6.2 0 1 0 6.2 6.2A6.2 6.2 0 0 0 16 1.9z" data-name="Layer 1"></path>
                                    </g>
                                </svg><span class="ece6016f">Developer</span></a>
                            <a href="/new-projects/grand_millennium-1791.html" class="_9cc43f20">
                                <svg xmlns="http://www.w3.org/2000/svg" class="_10b6420d" viewBox="0 0 11.6 12">
                                    <path fill="#555460" d="M11.5 4.5L6 .7c-.1-.1-.2-.1-.3 0L.2 4.6c-.1 0-.2.1-.2.2v1.8c0 .2.1.3.3.3.1 0 .1 0 .2-.1l.7-.5V11c0 .2.1.3.3.3h3.1c.2 0 .3-.1.3-.3V8.3h1.8v2.8c0 .2.1.3.3.3h3.1c.2 0 .3-.1.3-.3V6.4l.7.5c.3.1.5 0 .5-.2V4.8c.1-.1 0-.2-.1-.3zm-1.7 6.2H7.4V8c0-.2-.1-.3-.3-.3H4.6c-.2 0-.3.1-.3.3v2.8H1.8V5.9l4-2.8 4 2.8v4.8zM11 6L6 2.5a.3.3 0 0 0-.4 0L.6 6V4.9l5.2-3.7L11 4.9V6z"></path>
                                </svg><span class="ece6016f">Property Types</span></a>
                        </div>
                    </div>
                </div>
            </article>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/bahria_town_rawalpindi_bahria_business_district_open_transfer_business_district_south_plot_606_liberty_bahria_town_phase_8_rawalpindi-20872745-9033-2.html" class="_7ac32433" title="Open Transfer Business District South Plot # 606 Liberty Bahria Town Phase 8 Rawalpindi">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3 _01e2e273 be4c340f">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/75053500-400x300.webp />
                                    <img role="presentation" src=https://media.zameen.com/thumbnails/75053500-400x300.jpeg class=_8f499ba9 alt=Open Transfer Business District South Plot # 606 Liberty Bahria Town Phase 8 Rawalpindi title=Open Transfer Business District South Plot # 606 Liberty Bahria Town Phase 8 Rawalpindi aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/75053500-400x300.webp" srcset="https://media.zameen.com/thumbnails/75053500-400x300.webp"><img role="presentation" alt="Open Transfer Business District South Plot # 606 Liberty Bahria Town Phase 8 Rawalpindi" title="Open Transfer Business District South Plot # 606 Liberty Bahria Town Phase 8 Rawalpindi" aria-label="Listing photo" data-src="https://media.zameen.com/thumbnails/75053500-400x300.jpeg" class="_8f499ba9 lazy loaded" src="https://media.zameen.com/thumbnails/75053500-400x300.jpeg" data-was-processed="true"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="super hot label">super hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5">
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f773b145" viewBox="0 0 13 9">
                                                <g fill="#FFF">
                                                    <path d="M7.57 5.22c0 .6-.5 1-1.1 1s-1.1-.5-1.1-1 .5-1 1.1-1 1.1.4 1.1 1"></path>
                                                    <path d="M11.87 1.42h-2.9l-.7-1.4h-3.6l-.7 1.4h-2.9c-.6 0-1.1.4-1.1 1v5.5c0 .6.5 1 1.1 1h10.8c.6 0 1.1-.5 1.1-1v-5.5c0-.6-.4-1-1.1-1 .1 0 .1 0 0 0zm-5.4 6.2c-1.4 0-2.5-1.1-2.5-2.4s1.1-2.4 2.5-2.4 2.5 1.1 2.5 2.4c.1 1.3-1.1 2.4-2.5 2.4.1 0 0 0 0 0z"></path>
                                                </g>
                                            </svg><span class="_78f72f87">9</span></div>
                                    </div>
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f5678b67" viewBox="0 0 32 32">
                                                <path d="M25 18.5v-2a3 3 0 0 0-3-3H3a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h19a3 3 0 0 0 3-3v-2l7 5v-16l-7 5zM11 7c0 2.5-2 4.5-4.5 4.5S2 9.5 2 7s2-4.5 4.5-4.5S11 4.5 11 7m12 0c0 2.5-2 4.5-4.5 4.5S14 9.5 14 7s2-4.5 4.5-4.5S23 4.5 23 7" fill="#FFF"></path>
                                            </svg><span class="_78f72f87">1</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="_1e0ca152 ">
                                        <div>
                                            <div class="b20d55ae a45ecd74">
                                                <div class="da769a1b">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.4" height="9.5" viewBox="0 0 12.4 9.5" class="cec1916f">
                                                        <path fill="inherit" d="M.1 5.2C0 5 0 4.8.1 4.6l.7-.7c.2-.2.5-.2.6 0L4 6.7c.1.1.2.1.3 0L10.7.1c.2-.2.5-.2.6 0l.7.7c.2.2.2.5 0 .6l-7.4 8c-.2.1-.5.2-.6 0l-3.7-4-.2-.2z"></path>
                                                    </svg><span class="b21fda39">Verified</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/bahria_town_rawalpindi_bahria_business_district_open_transfer_business_district_south_plot_606_liberty_bahria_town_phase_8_rawalpindi-20872745-9033-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Open%20Transfer%20Business%20District%20South%20Plot%20%23%20606%20Liberty%20Bahria%20Town%20Phase%208%20Rawalpindi - url: https://www.zameen.com/Property/bahria_town_rawalpindi_bahria_business_district_open_transfer_business_district_south_plot_606_liberty_bahria_town_phase_8_rawalpindi-20872745-9033-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Open%20Transfer%20Business%20District%20South%20Plot%20%23%20606%20Liberty%20Bahria%20Town%20Phase%208%20Rawalpindi%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Fbahria_town_rawalpindi_bahria_business_district_open_transfer_business_district_south_plot_606_liberty_bahria_town_phase_8_rawalpindi-20872745-9033-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Open%20Transfer%20Business%20District%20South%20Plot%20%23%20606%20Liberty%20Bahria%20Town%20Phase%208%20Rawalpindi&amp;body=https://www.zameen.com/Property/bahria_town_rawalpindi_bahria_business_district_open_transfer_business_district_south_plot_606_liberty_bahria_town_phase_8_rawalpindi-20872745-9033-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Open Transfer Business District South Plot # 606 Liberty Bahria Town Phase 8 Rawalpindi&amp;body=https://www.zameen.com/Property/bahria_town_rawalpindi_bahria_business_district_open_transfer_business_district_south_plot_606_liberty_bahria_town_phase_8_rawalpindi-20872745-9033-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="_6681ac2b">
                                <button class="_22c5fb33">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="svg-icon-sprite" viewBox="0 0 74 47">
                                        <path fill="#FFF" d="M44.4 21.7c1 .5 1.4 1.6.9 2.6a2 2 0 0 1-.9 1l-6.2 3-6.8 3.4c-.6.3-1.3.3-1.9 0a2 2 0 0 1-1-1.8V17a2 2 0 0 1 1-1.8c.6-.4 1.3-.4 1.9 0l6.8 3.3 6.2 3.3z"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed _01e2e273 c7b81b5c">
                        <div class="_7ac32433" title="Open Transfer Business District South Plot # 606 Liberty Bahria Town Phase 8 Rawalpindi">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">13.28 Crore</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.4" height="9.5" viewBox="0 0 12.4 9.5" class="c1a24809 d5f0c97c">
                                            <path fill="inherit" d="M.1 5.2C0 5 0 4.8.1 4.6l.7-.7c.2-.2.5-.2.6 0L4 6.7c.1.1.2.1.3 0L10.7.1c.2-.2.5-.2.6 0l.7.7c.2.2.2.5 0 .6l-7.4 8c-.2.1-.5.2-.6 0l-3.7-4-.2-.2z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="c1a24809" viewBox="0 0 32 32">
                                            <g fill="#C32C2B">
                                                <path d="M6.4 27.4h-.2l-.1-.3c-3-3.8-3.5-8.4-1.6-13.6-.6-1-1-2-1.1-3.2l-.2-.3-.3.1c-2.6 4.3-4.8 10.6-.5 16 1.8 1.8 4.3 3 6.9 3.3a8 8 0 0 1-2.9-2z"></path>
                                                <path d="M15.6 29.6c-3.1.1-6.1-1-8.3-3.3-4.3-5.4-2.1-11.7.5-16h.4c.1 0 .2 0 .2.2.1 1.7.8 3.2 2 4.4.4-1.4 1.1-2.8 2-4 1.8-2.3 2.5-5.2 2-8l.2-.4h.1l.2.1.5.6c1.2 1.4 2.3 3 3.2 4.5.9 2 1.5 3.8 1.8 6 .7-.6 1.3-1.3 1.7-2 .3-1 .3-2-.1-3l.1-.4h.4a16 16 0 0 1 4.9 11.8 10 10 0 0 1-6.8 9c-1.7.3-3.3.5-5 .5zm-.9-13.8c-.8.8-1.5 1.8-2.1 2.8a9.1 9.1 0 0 0-1.4 4.2c0 3.6 2 6.2 4.6 6.2 1.4 0 2.8-.6 3.7-1.7 1-1 1.4-2.7 1.2-4.2-.4-1.6-1.1-3.2-2.2-4.5-.2.6-.5 1.1-.9 1.5l-.6.5h-.1c-.5 0-.9-.3-1.3-.7a5.8 5.8 0 0 1-.9-4z"></path>
                                                <path d="M31.9 18a15 15 0 0 0-4.8-9.8h-.4c-.1.1-.2.2-.1.3.3 1 .4 2 .1 3l-.1.1c1 2 1.7 4.1 1.9 6.3.6 4.7-1.8 9.4-6 11.6.9 0 1.8-.3 2.7-.6C29 27.5 31.7 24 32 20l-.1-2z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <g fill="#34A048">
                                                <path d="M25.3 16.8V6.5l-5-1.3-4.1-1h-.3l-4 1c-1.8.3-3.5.8-5.3 1.2V17c-.1 2.1.7 4.2 2 5.8 2 2.3 4.5 4.1 7.2 5.2 2.5-1 4.7-2.5 6.6-4.4 2-1.7 3-4.2 2.9-6.7zm-3.2-4.4l-2.5 3.2-4 5.2c-.8 1-1.3 1-2.1 0L9.9 16a1 1 0 0 1 0-1.5c.4-.4 1.1-.3 1.5.1l2.6 2.3.6.5.3-.5 5.6-5.5.7-.5c.4 0 .9.1 1.1.5.1.2.1.7-.2 1.1z"></path>
                                                <path d="M29.1 16.8V5a2 2 0 0 0-1.7-2.2l-6-1.5L16.8.1h-1.7l-4.5 1.1-6.1 1.5C3.5 3 2.7 4 2.8 5v11.9c-.1 3 1 6 2.9 8.2a25 25 0 0 0 9.3 6.7l.9.2c.3 0 .7 0 1-.2a24 24 0 0 0 8.4-5.7c2.6-2.3 4-5.7 3.8-9.2zm-4.8 8.5c-2.3 2.3-5 4.1-7.9 5.3-.2.1-.5.1-.8 0a22 22 0 0 1-8.8-6.3c-1.8-2-2.7-4.7-2.6-7.4V5c0-.5.1-.8.6-.9l10.6-2.6h1C20 2.4 23.5 3.2 27 4c.5.1.6.4.6.8v11.8c.3 3.4-1 6.5-3.3 8.7z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Bahria Business District, Bahria Town Rawalpindi</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Open Transfer Business District South Plot # 606 Liberty Bahria Town Phase 8 Rawalpindi"><div class="_1e0ca152 _026d7bff"><div><span>2 Kanal</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Open Transfer Business District South Plot # 606 Liberty Bahria Town Phase 8 Rawalpindi</h2></div>
                        <div class="ee550b27">•plot# 606 •street# BD-SOUTH BLVD CORNER •size: 133X60 2. kanal •plot price:132,800,000 •down... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 1 day ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-20872745" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-20872745">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/66237336-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/66237336-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/66237336-240x180.webp" srcset="https://media.zameen.com/thumbnails/66237336-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/66237336-240x180.jpeg" class="_54945be7 lazy loaded" src="https://media.zameen.com/thumbnails/66237336-240x180.jpeg" data-was-processed="true"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li class="_53ec901b">
                <div class="_7bc4114b" id="g-ad-7709" style="width: 650px; height: 80px; display: none;" data-google-query-id="CIrQ5Mrn2OgCFdcYGwodBYkNlg">
                    <div id="google_ads_iframe_/31946216/Strip_5_650x80_0__container__" style="border: 0pt none; width: 650px; height: 80px;"></div>
                </div>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/bahria_town_phase_8_bahria_town_phase_8_eden_lake_view_block_open_transfer_eden_lake_view_block_commercial_plot_1b_bahria_town_phase_8_rawalpindi-20868678-3073-2.html" class="_7ac32433" title="Open Transfer Eden Lake View Block Commercial Plot 1B Bahria Town Phase 8 Rawalpindi">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3 _01e2e273 be4c340f">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/74863032-400x300.webp />
                                    <img role="presentation" src=https://media.zameen.com/thumbnails/74863032-400x300.jpeg class=_8f499ba9 alt=Open Transfer Eden Lake View Block Commercial Plot 1B Bahria Town Phase 8 Rawalpindi title=Open Transfer Eden Lake View Block Commercial Plot 1B Bahria Town Phase 8 Rawalpindi aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/74863032-400x300.webp" srcset="https://media.zameen.com/thumbnails/74863032-400x300.webp"><img role="presentation" alt="Open Transfer Eden Lake View Block Commercial Plot 1B Bahria Town Phase 8 Rawalpindi" title="Open Transfer Eden Lake View Block Commercial Plot 1B Bahria Town Phase 8 Rawalpindi" aria-label="Listing photo" data-src="https://media.zameen.com/thumbnails/74863032-400x300.jpeg" class="_8f499ba9 lazy loaded" src="https://media.zameen.com/thumbnails/74863032-400x300.jpeg" data-was-processed="true"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="super hot label">super hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5">
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f773b145" viewBox="0 0 13 9">
                                                <g fill="#FFF">
                                                    <path d="M7.57 5.22c0 .6-.5 1-1.1 1s-1.1-.5-1.1-1 .5-1 1.1-1 1.1.4 1.1 1"></path>
                                                    <path d="M11.87 1.42h-2.9l-.7-1.4h-3.6l-.7 1.4h-2.9c-.6 0-1.1.4-1.1 1v5.5c0 .6.5 1 1.1 1h10.8c.6 0 1.1-.5 1.1-1v-5.5c0-.6-.4-1-1.1-1 .1 0 .1 0 0 0zm-5.4 6.2c-1.4 0-2.5-1.1-2.5-2.4s1.1-2.4 2.5-2.4 2.5 1.1 2.5 2.4c.1 1.3-1.1 2.4-2.5 2.4.1 0 0 0 0 0z"></path>
                                                </g>
                                            </svg><span class="_78f72f87">9</span></div>
                                    </div>
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f5678b67" viewBox="0 0 32 32">
                                                <path d="M25 18.5v-2a3 3 0 0 0-3-3H3a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h19a3 3 0 0 0 3-3v-2l7 5v-16l-7 5zM11 7c0 2.5-2 4.5-4.5 4.5S2 9.5 2 7s2-4.5 4.5-4.5S11 4.5 11 7m12 0c0 2.5-2 4.5-4.5 4.5S14 9.5 14 7s2-4.5 4.5-4.5S23 4.5 23 7" fill="#FFF"></path>
                                            </svg><span class="_78f72f87">1</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="_1e0ca152 ">
                                        <div>
                                            <div class="b20d55ae a45ecd74">
                                                <div class="da769a1b">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.4" height="9.5" viewBox="0 0 12.4 9.5" class="cec1916f">
                                                        <path fill="inherit" d="M.1 5.2C0 5 0 4.8.1 4.6l.7-.7c.2-.2.5-.2.6 0L4 6.7c.1.1.2.1.3 0L10.7.1c.2-.2.5-.2.6 0l.7.7c.2.2.2.5 0 .6l-7.4 8c-.2.1-.5.2-.6 0l-3.7-4-.2-.2z"></path>
                                                    </svg><span class="b21fda39">Verified</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_eden_lake_view_block_open_transfer_eden_lake_view_block_commercial_plot_1b_bahria_town_phase_8_rawalpindi-20868678-3073-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Open%20Transfer%20Eden%20Lake%20View%20Block%20Commercial%20Plot%201B%20Bahria%20Town%20Phase%208%20Rawalpindi - url: https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_eden_lake_view_block_open_transfer_eden_lake_view_block_commercial_plot_1b_bahria_town_phase_8_rawalpindi-20868678-3073-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Open%20Transfer%20Eden%20Lake%20View%20Block%20Commercial%20Plot%201B%20Bahria%20Town%20Phase%208%20Rawalpindi%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Fbahria_town_phase_8_bahria_town_phase_8_eden_lake_view_block_open_transfer_eden_lake_view_block_commercial_plot_1b_bahria_town_phase_8_rawalpindi-20868678-3073-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Open%20Transfer%20Eden%20Lake%20View%20Block%20Commercial%20Plot%201B%20Bahria%20Town%20Phase%208%20Rawalpindi&amp;body=https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_eden_lake_view_block_open_transfer_eden_lake_view_block_commercial_plot_1b_bahria_town_phase_8_rawalpindi-20868678-3073-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Open Transfer Eden Lake View Block Commercial Plot 1B Bahria Town Phase 8 Rawalpindi&amp;body=https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_eden_lake_view_block_open_transfer_eden_lake_view_block_commercial_plot_1b_bahria_town_phase_8_rawalpindi-20868678-3073-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="_6681ac2b">
                                <button class="_22c5fb33">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="svg-icon-sprite" viewBox="0 0 74 47">
                                        <path fill="#FFF" d="M44.4 21.7c1 .5 1.4 1.6.9 2.6a2 2 0 0 1-.9 1l-6.2 3-6.8 3.4c-.6.3-1.3.3-1.9 0a2 2 0 0 1-1-1.8V17a2 2 0 0 1 1-1.8c.6-.4 1.3-.4 1.9 0l6.8 3.3 6.2 3.3z"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed _01e2e273 c7b81b5c">
                        <div class="_7ac32433" title="Open Transfer Eden Lake View Block Commercial Plot 1B Bahria Town Phase 8 Rawalpindi">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">76.72 Crore</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.4" height="9.5" viewBox="0 0 12.4 9.5" class="c1a24809 d5f0c97c">
                                            <path fill="inherit" d="M.1 5.2C0 5 0 4.8.1 4.6l.7-.7c.2-.2.5-.2.6 0L4 6.7c.1.1.2.1.3 0L10.7.1c.2-.2.5-.2.6 0l.7.7c.2.2.2.5 0 .6l-7.4 8c-.2.1-.5.2-.6 0l-3.7-4-.2-.2z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="c1a24809" viewBox="0 0 32 32">
                                            <g fill="#C32C2B">
                                                <path d="M6.4 27.4h-.2l-.1-.3c-3-3.8-3.5-8.4-1.6-13.6-.6-1-1-2-1.1-3.2l-.2-.3-.3.1c-2.6 4.3-4.8 10.6-.5 16 1.8 1.8 4.3 3 6.9 3.3a8 8 0 0 1-2.9-2z"></path>
                                                <path d="M15.6 29.6c-3.1.1-6.1-1-8.3-3.3-4.3-5.4-2.1-11.7.5-16h.4c.1 0 .2 0 .2.2.1 1.7.8 3.2 2 4.4.4-1.4 1.1-2.8 2-4 1.8-2.3 2.5-5.2 2-8l.2-.4h.1l.2.1.5.6c1.2 1.4 2.3 3 3.2 4.5.9 2 1.5 3.8 1.8 6 .7-.6 1.3-1.3 1.7-2 .3-1 .3-2-.1-3l.1-.4h.4a16 16 0 0 1 4.9 11.8 10 10 0 0 1-6.8 9c-1.7.3-3.3.5-5 .5zm-.9-13.8c-.8.8-1.5 1.8-2.1 2.8a9.1 9.1 0 0 0-1.4 4.2c0 3.6 2 6.2 4.6 6.2 1.4 0 2.8-.6 3.7-1.7 1-1 1.4-2.7 1.2-4.2-.4-1.6-1.1-3.2-2.2-4.5-.2.6-.5 1.1-.9 1.5l-.6.5h-.1c-.5 0-.9-.3-1.3-.7a5.8 5.8 0 0 1-.9-4z"></path>
                                                <path d="M31.9 18a15 15 0 0 0-4.8-9.8h-.4c-.1.1-.2.2-.1.3.3 1 .4 2 .1 3l-.1.1c1 2 1.7 4.1 1.9 6.3.6 4.7-1.8 9.4-6 11.6.9 0 1.8-.3 2.7-.6C29 27.5 31.7 24 32 20l-.1-2z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <g fill="#34A048">
                                                <path d="M25.3 16.8V6.5l-5-1.3-4.1-1h-.3l-4 1c-1.8.3-3.5.8-5.3 1.2V17c-.1 2.1.7 4.2 2 5.8 2 2.3 4.5 4.1 7.2 5.2 2.5-1 4.7-2.5 6.6-4.4 2-1.7 3-4.2 2.9-6.7zm-3.2-4.4l-2.5 3.2-4 5.2c-.8 1-1.3 1-2.1 0L9.9 16a1 1 0 0 1 0-1.5c.4-.4 1.1-.3 1.5.1l2.6 2.3.6.5.3-.5 5.6-5.5.7-.5c.4 0 .9.1 1.1.5.1.2.1.7-.2 1.1z"></path>
                                                <path d="M29.1 16.8V5a2 2 0 0 0-1.7-2.2l-6-1.5L16.8.1h-1.7l-4.5 1.1-6.1 1.5C3.5 3 2.7 4 2.8 5v11.9c-.1 3 1 6 2.9 8.2a25 25 0 0 0 9.3 6.7l.9.2c.3 0 .7 0 1-.2a24 24 0 0 0 8.4-5.7c2.6-2.3 4-5.7 3.8-9.2zm-4.8 8.5c-2.3 2.3-5 4.1-7.9 5.3-.2.1-.5.1-.8 0a22 22 0 0 1-8.8-6.3c-1.8-2-2.7-4.7-2.6-7.4V5c0-.5.1-.8.6-.9l10.6-2.6h1C20 2.4 23.5 3.2 27 4c.5.1.6.4.6.8v11.8c.3 3.4-1 6.5-3.3 8.7z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Bahria Town Phase 8 - Eden Lake View Block, Bahria Town Phase 8</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Open Transfer Eden Lake View Block Commercial Plot 1B Bahria Town Phase 8 Rawalpindi"><div class="_1e0ca152 _026d7bff"><div><span>19 Kanal</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Open Transfer Eden Lake View Block Commercial Plot 1B Bahria Town Phase 8 Rawalpindi</h2></div>
                        <div class="ee550b27">•Plot# 1-B •Size: 19.18-Kanal •Down Payment: Rs 249,340,000 •Plot Price: 767,200,000 •On Easy 3.5... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 1 day ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-20868678" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-20868678">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/66237336-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/66237336-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/66237336-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/66237336-240x180.jpeg" class="_54945be7 lazy loaded"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/bahria_town_phase_8_bahria_hamlet_open_transfer_bahria_hamlet_commercial__plot_1a__bahria_town_phase_8_rawalpindi-20868996-10354-2.html" class="_7ac32433" title="Open Transfer Bahria Hamlet Commercial  Plot 1A  Bahria Town Phase 8 Rawalpindi">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3 _01e2e273 be4c340f">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/74863206-400x300.webp />
                                    <img role="presentation" src=https://media.zameen.com/thumbnails/74863206-400x300.jpeg class=_8f499ba9 alt=Open Transfer Bahria Hamlet Commercial Plot 1A Bahria Town Phase 8 Rawalpindi title=Open Transfer Bahria Hamlet Commercial Plot 1A Bahria Town Phase 8 Rawalpindi aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/74863206-400x300.webp"><img role="presentation" alt="Open Transfer Bahria Hamlet Commercial  Plot 1A  Bahria Town Phase 8 Rawalpindi" title="Open Transfer Bahria Hamlet Commercial  Plot 1A  Bahria Town Phase 8 Rawalpindi" aria-label="Listing photo" data-src="https://media.zameen.com/thumbnails/74863206-400x300.jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="super hot label">super hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5">
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f773b145" viewBox="0 0 13 9">
                                                <g fill="#FFF">
                                                    <path d="M7.57 5.22c0 .6-.5 1-1.1 1s-1.1-.5-1.1-1 .5-1 1.1-1 1.1.4 1.1 1"></path>
                                                    <path d="M11.87 1.42h-2.9l-.7-1.4h-3.6l-.7 1.4h-2.9c-.6 0-1.1.4-1.1 1v5.5c0 .6.5 1 1.1 1h10.8c.6 0 1.1-.5 1.1-1v-5.5c0-.6-.4-1-1.1-1 .1 0 .1 0 0 0zm-5.4 6.2c-1.4 0-2.5-1.1-2.5-2.4s1.1-2.4 2.5-2.4 2.5 1.1 2.5 2.4c.1 1.3-1.1 2.4-2.5 2.4.1 0 0 0 0 0z"></path>
                                                </g>
                                            </svg><span class="_78f72f87">9</span></div>
                                    </div>
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f5678b67" viewBox="0 0 32 32">
                                                <path d="M25 18.5v-2a3 3 0 0 0-3-3H3a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h19a3 3 0 0 0 3-3v-2l7 5v-16l-7 5zM11 7c0 2.5-2 4.5-4.5 4.5S2 9.5 2 7s2-4.5 4.5-4.5S11 4.5 11 7m12 0c0 2.5-2 4.5-4.5 4.5S14 9.5 14 7s2-4.5 4.5-4.5S23 4.5 23 7" fill="#FFF"></path>
                                            </svg><span class="_78f72f87">1</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="_1e0ca152 ">
                                        <div>
                                            <div class="b20d55ae a45ecd74">
                                                <div class="da769a1b">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.4" height="9.5" viewBox="0 0 12.4 9.5" class="cec1916f">
                                                        <path fill="inherit" d="M.1 5.2C0 5 0 4.8.1 4.6l.7-.7c.2-.2.5-.2.6 0L4 6.7c.1.1.2.1.3 0L10.7.1c.2-.2.5-.2.6 0l.7.7c.2.2.2.5 0 .6l-7.4 8c-.2.1-.5.2-.6 0l-3.7-4-.2-.2z"></path>
                                                    </svg><span class="b21fda39">Verified</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/bahria_town_phase_8_bahria_hamlet_open_transfer_bahria_hamlet_commercial__plot_1a__bahria_town_phase_8_rawalpindi-20868996-10354-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Open%20Transfer%20Bahria%20Hamlet%20Commercial%20%20Plot%201A%20%20Bahria%20Town%20Phase%208%20Rawalpindi - url: https://www.zameen.com/Property/bahria_town_phase_8_bahria_hamlet_open_transfer_bahria_hamlet_commercial__plot_1a__bahria_town_phase_8_rawalpindi-20868996-10354-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Open%20Transfer%20Bahria%20Hamlet%20Commercial%20%20Plot%201A%20%20Bahria%20Town%20Phase%208%20Rawalpindi%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Fbahria_town_phase_8_bahria_hamlet_open_transfer_bahria_hamlet_commercial__plot_1a__bahria_town_phase_8_rawalpindi-20868996-10354-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Open%20Transfer%20Bahria%20Hamlet%20Commercial%20%20Plot%201A%20%20Bahria%20Town%20Phase%208%20Rawalpindi&amp;body=https://www.zameen.com/Property/bahria_town_phase_8_bahria_hamlet_open_transfer_bahria_hamlet_commercial__plot_1a__bahria_town_phase_8_rawalpindi-20868996-10354-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Open Transfer Bahria Hamlet Commercial  Plot 1A  Bahria Town Phase 8 Rawalpindi&amp;body=https://www.zameen.com/Property/bahria_town_phase_8_bahria_hamlet_open_transfer_bahria_hamlet_commercial__plot_1a__bahria_town_phase_8_rawalpindi-20868996-10354-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="_6681ac2b">
                                <button class="_22c5fb33">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="svg-icon-sprite" viewBox="0 0 74 47">
                                        <path fill="#FFF" d="M44.4 21.7c1 .5 1.4 1.6.9 2.6a2 2 0 0 1-.9 1l-6.2 3-6.8 3.4c-.6.3-1.3.3-1.9 0a2 2 0 0 1-1-1.8V17a2 2 0 0 1 1-1.8c.6-.4 1.3-.4 1.9 0l6.8 3.3 6.2 3.3z"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed _01e2e273 c7b81b5c">
                        <div class="_7ac32433" title="Open Transfer Bahria Hamlet Commercial  Plot 1A  Bahria Town Phase 8 Rawalpindi">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">8.24 Crore</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="12.4" height="9.5" viewBox="0 0 12.4 9.5" class="c1a24809 d5f0c97c">
                                            <path fill="inherit" d="M.1 5.2C0 5 0 4.8.1 4.6l.7-.7c.2-.2.5-.2.6 0L4 6.7c.1.1.2.1.3 0L10.7.1c.2-.2.5-.2.6 0l.7.7c.2.2.2.5 0 .6l-7.4 8c-.2.1-.5.2-.6 0l-3.7-4-.2-.2z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="c1a24809" viewBox="0 0 32 32">
                                            <g fill="#C32C2B">
                                                <path d="M6.4 27.4h-.2l-.1-.3c-3-3.8-3.5-8.4-1.6-13.6-.6-1-1-2-1.1-3.2l-.2-.3-.3.1c-2.6 4.3-4.8 10.6-.5 16 1.8 1.8 4.3 3 6.9 3.3a8 8 0 0 1-2.9-2z"></path>
                                                <path d="M15.6 29.6c-3.1.1-6.1-1-8.3-3.3-4.3-5.4-2.1-11.7.5-16h.4c.1 0 .2 0 .2.2.1 1.7.8 3.2 2 4.4.4-1.4 1.1-2.8 2-4 1.8-2.3 2.5-5.2 2-8l.2-.4h.1l.2.1.5.6c1.2 1.4 2.3 3 3.2 4.5.9 2 1.5 3.8 1.8 6 .7-.6 1.3-1.3 1.7-2 .3-1 .3-2-.1-3l.1-.4h.4a16 16 0 0 1 4.9 11.8 10 10 0 0 1-6.8 9c-1.7.3-3.3.5-5 .5zm-.9-13.8c-.8.8-1.5 1.8-2.1 2.8a9.1 9.1 0 0 0-1.4 4.2c0 3.6 2 6.2 4.6 6.2 1.4 0 2.8-.6 3.7-1.7 1-1 1.4-2.7 1.2-4.2-.4-1.6-1.1-3.2-2.2-4.5-.2.6-.5 1.1-.9 1.5l-.6.5h-.1c-.5 0-.9-.3-1.3-.7a5.8 5.8 0 0 1-.9-4z"></path>
                                                <path d="M31.9 18a15 15 0 0 0-4.8-9.8h-.4c-.1.1-.2.2-.1.3.3 1 .4 2 .1 3l-.1.1c1 2 1.7 4.1 1.9 6.3.6 4.7-1.8 9.4-6 11.6.9 0 1.8-.3 2.7-.6C29 27.5 31.7 24 32 20l-.1-2z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <g fill="#34A048">
                                                <path d="M25.3 16.8V6.5l-5-1.3-4.1-1h-.3l-4 1c-1.8.3-3.5.8-5.3 1.2V17c-.1 2.1.7 4.2 2 5.8 2 2.3 4.5 4.1 7.2 5.2 2.5-1 4.7-2.5 6.6-4.4 2-1.7 3-4.2 2.9-6.7zm-3.2-4.4l-2.5 3.2-4 5.2c-.8 1-1.3 1-2.1 0L9.9 16a1 1 0 0 1 0-1.5c.4-.4 1.1-.3 1.5.1l2.6 2.3.6.5.3-.5 5.6-5.5.7-.5c.4 0 .9.1 1.1.5.1.2.1.7-.2 1.1z"></path>
                                                <path d="M29.1 16.8V5a2 2 0 0 0-1.7-2.2l-6-1.5L16.8.1h-1.7l-4.5 1.1-6.1 1.5C3.5 3 2.7 4 2.8 5v11.9c-.1 3 1 6 2.9 8.2a25 25 0 0 0 9.3 6.7l.9.2c.3 0 .7 0 1-.2a24 24 0 0 0 8.4-5.7c2.6-2.3 4-5.7 3.8-9.2zm-4.8 8.5c-2.3 2.3-5 4.1-7.9 5.3-.2.1-.5.1-.8 0a22 22 0 0 1-8.8-6.3c-1.8-2-2.7-4.7-2.6-7.4V5c0-.5.1-.8.6-.9l10.6-2.6h1C20 2.4 23.5 3.2 27 4c.5.1.6.4.6.8v11.8c.3 3.4-1 6.5-3.3 8.7z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Bahria Hamlet, Bahria Town Phase 8</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Open Transfer Bahria Hamlet Commercial  Plot 1A  Bahria Town Phase 8 Rawalpindi"><div class="_1e0ca152 _026d7bff"><div><span>3 Kanal</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Open Transfer Bahria Hamlet Commercial  Plot 1A  Bahria Town Phase 8 Rawalpindi</h2></div>
                        <div class="ee550b27">•Plot# 1-A •Street# 08 Blvd •Size: 164x120 Square Feet (3.93 Kanal) •Plot Price: 88,425,000 (8... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 1 day ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-20868996" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-20868996">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/66237336-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/66237336-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/66237336-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/66237336-240x180.jpeg" class="_54945be7 lazy loaded"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li class="_53ec901b">
                <div class="_7bc4114b" id="g-ad-7674" style="width: 650px; height: 80px; display: none;" data-google-query-id="CNi86Mrn2OgCFRfV1QodeMwADw">
                    <div id="google_ads_iframe_/31946216/Strip_6_650x80_0__container__" style="border: 0pt none; width: 650px; height: 80px;"></div>
                </div>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/rawalpindi_capital_smart_city_book_your_residential_plot_in_capital_smart_city_in_the_lowest_prices-22188614-11891-2.html" class="_7ac32433" title="Book your Residential Plot in Capital Smart City in the Lowest Prices">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/81473030-400x300.webp />
                                    <img role="presentation" src=https://media.zameen.com/thumbnails/81473030-400x300.jpeg class=_8f499ba9 alt=Book your Residential Plot in Capital Smart City in the Lowest Prices title=Book your Residential Plot in Capital Smart City in the Lowest Prices aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/81473030-400x300.webp"><img role="presentation" alt="Book your Residential Plot in Capital Smart City in the Lowest Prices" title="Book your Residential Plot in Capital Smart City in the Lowest Prices" aria-label="Listing photo" data-src="https://media.zameen.com/thumbnails/81473030-400x300.jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="hot label">hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5">
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f773b145" viewBox="0 0 13 9">
                                                <g fill="#FFF">
                                                    <path d="M7.57 5.22c0 .6-.5 1-1.1 1s-1.1-.5-1.1-1 .5-1 1.1-1 1.1.4 1.1 1"></path>
                                                    <path d="M11.87 1.42h-2.9l-.7-1.4h-3.6l-.7 1.4h-2.9c-.6 0-1.1.4-1.1 1v5.5c0 .6.5 1 1.1 1h10.8c.6 0 1.1-.5 1.1-1v-5.5c0-.6-.4-1-1.1-1 .1 0 .1 0 0 0zm-5.4 6.2c-1.4 0-2.5-1.1-2.5-2.4s1.1-2.4 2.5-2.4 2.5 1.1 2.5 2.4c.1 1.3-1.1 2.4-2.5 2.4.1 0 0 0 0 0z"></path>
                                                </g>
                                            </svg><span class="_78f72f87">7</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/rawalpindi_capital_smart_city_book_your_residential_plot_in_capital_smart_city_in_the_lowest_prices-22188614-11891-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Book%20your%20Residential%20Plot%20in%20Capital%20Smart%20City%20in%20the%20Lowest%20Prices - url: https://www.zameen.com/Property/rawalpindi_capital_smart_city_book_your_residential_plot_in_capital_smart_city_in_the_lowest_prices-22188614-11891-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Book%20your%20Residential%20Plot%20in%20Capital%20Smart%20City%20in%20the%20Lowest%20Prices%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Frawalpindi_capital_smart_city_book_your_residential_plot_in_capital_smart_city_in_the_lowest_prices-22188614-11891-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Book%20your%20Residential%20Plot%20in%20Capital%20Smart%20City%20in%20the%20Lowest%20Prices&amp;body=https://www.zameen.com/Property/rawalpindi_capital_smart_city_book_your_residential_plot_in_capital_smart_city_in_the_lowest_prices-22188614-11891-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Book your Residential Plot in Capital Smart City in the Lowest Prices&amp;body=https://www.zameen.com/Property/rawalpindi_capital_smart_city_book_your_residential_plot_in_capital_smart_city_in_the_lowest_prices-22188614-11891-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed">
                        <div class="_7ac32433" title="Book your Residential Plot in Capital Smart City in the Lowest Prices">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">41.4 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <path fill="#C22B2A" d="M11.1 0c.8 2.3.6 4.9-.3 7.1-1.1 2-2.3 4-3.8 5.8a16.3 16.3 0 0 0-2.8 7.4c.1 6.5 5.4 11.8 12 11.7 3 .2 5.9-.9 8.1-2.9 1.8-1.8 3-4.2 3.3-6.8.6-4.2-.5-8.5-2.9-11.9.1 1.3 0 2.6-.4 3.8a5.3 5.3 0 0 1-3.6 3.4 4 4 0 0 1-4.1-1.3 5.5 5.5 0 0 1-.5-5.4c.7-1.7.7-3.6.1-5.3a8.6 8.6 0 0 0-2.6-3.7A9.2 9.2 0 0 0 11.1 0z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <g fill="#34A048">
                                                <path d="M25.3 16.8V6.5l-5-1.3-4.1-1h-.3l-4 1c-1.8.3-3.5.8-5.3 1.2V17c-.1 2.1.7 4.2 2 5.8 2 2.3 4.5 4.1 7.2 5.2 2.5-1 4.7-2.5 6.6-4.4 2-1.7 3-4.2 2.9-6.7zm-3.2-4.4l-2.5 3.2-4 5.2c-.8 1-1.3 1-2.1 0L9.9 16a1 1 0 0 1 0-1.5c.4-.4 1.1-.3 1.5.1l2.6 2.3.6.5.3-.5 5.6-5.5.7-.5c.4 0 .9.1 1.1.5.1.2.1.7-.2 1.1z"></path>
                                                <path d="M29.1 16.8V5a2 2 0 0 0-1.7-2.2l-6-1.5L16.8.1h-1.7l-4.5 1.1-6.1 1.5C3.5 3 2.7 4 2.8 5v11.9c-.1 3 1 6 2.9 8.2a25 25 0 0 0 9.3 6.7l.9.2c.3 0 .7 0 1-.2a24 24 0 0 0 8.4-5.7c2.6-2.3 4-5.7 3.8-9.2zm-4.8 8.5c-2.3 2.3-5 4.1-7.9 5.3-.2.1-.5.1-.8 0a22 22 0 0 1-8.8-6.3c-1.8-2-2.7-4.7-2.6-7.4V5c0-.5.1-.8.6-.9l10.6-2.6h1C20 2.4 23.5 3.2 27 4c.5.1.6.4.6.8v11.8c.3 3.4-1 6.5-3.3 8.7z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <div class="_0402297d "><span class="_6b1c4ee5"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.7 10.2" class="_85195ac8"><path fill="#f5d652" d="M10.6 3.9a.6.6 0 0 0-.5-.5L7.2 3 5.9.3a.6.6 0 0 0-.4-.3.6.6 0 0 0-.2 0 .6.6 0 0 0-.5.3L3.5 3l-3 .4a.6.6 0 0 0-.3 1l2.1 2.2-.5 2.9a.6.6 0 0 0 1 .6l2.5-1.3L8 10a.6.6 0 0 0 .5 0 .7.7 0 0 0 .1 0 .6.6 0 0 0 .3-.6l-.5-3 2-2a.6.6 0 0 0 .2-.6z"></path></svg></span><span class="e7869745">Titanium</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Capital Smart City, Rawalpindi</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Book your Residential Plot in Capital Smart City in the Lowest Prices"><div class="_1e0ca152 _026d7bff"><div><span>10 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Book your Residential Plot in Capital Smart City in the Lowest Prices</h2></div>
                        <div class="ee550b27">Capital Smart City Residential Plot for Sale | 10 Marla Located near Islamabad airport and... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 3 hours ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-22188614" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-22188614">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/20470630-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/20470630-240x180.jpeg class=_54945be7 _1b380bc2 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/20470630-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/20470630-240x180.jpeg" class="_54945be7 _1b380bc2 lazy loaded"></picture>
                                    <div class="_8f0b72fd">
                                        <picture class="_219b7e0a">
                                            <noscript>
                                                <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/20470630-600x450.webp />
                                                <img role="presentation" src=https://media.zameen.com/thumbnails/20470630-600x450.jpeg class=_53426593 alt=t itle=a ria-label="Fallback listing photo" />
                                            </noscript>
                                            <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/20470630-600x450.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/20470630-600x450.jpeg" class="_53426593 lazy loaded"></picture>
                                        <div class="_713d6e48">Registered since: 2015</div>
                                        <div class="_4ed4ba45">329 Properties for Sale</div>
                                        <div class="_4ed4ba45">14 Properties for Rent</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <article class="_7b6b357c">
                <div class="_210240b5"><span>Countree Farmhouses &amp; Resort</span><span class="b46424f9">New Project</span></div>
                <div class="_73bd3919">
                    <a href="/new-projects/countree_farmhouses_resort-1556.html" class="_8d9bab4b">
                        <div class="_5d194818"></div>
                    </a>
                </div>
                <div class="a4c98264">
                    <div>
                        <div class="_66fe3c1c">
                            <div class="_150e2e12">
                                <picture class="_219b7e0a">
                                    <noscript>
                                        <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/76225752-400x300.webp />
                                        <img role="presentation" src=https://media.zameen.com/thumbnails/76225752-400x300.jpeg class=_68943e06 alt=Countree Farmhouses & Resort title=Countree Farmhouses & Resort aria-label="Fallback listing photo" />
                                    </noscript>
                                    <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/76225752-400x300.webp"><img role="presentation" alt="Countree Farmhouses &amp; Resort" title="Countree Farmhouses &amp; Resort" aria-label="Listing photo" data-src="https://media.zameen.com/thumbnails/76225752-400x300.jpeg" class="_68943e06 lazy loaded"></picture>
                            </div>
                            <div class="_6d2ea5a7">
                                <div class="f59902d8">
                                    <div class="f1475fe5">
                                        <div class="b20d55ae">
                                            <div class="_3448627a">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f773b145" viewBox="0 0 13 9">
                                                    <g fill="#FFF">
                                                        <path d="M7.57 5.22c0 .6-.5 1-1.1 1s-1.1-.5-1.1-1 .5-1 1.1-1 1.1.4 1.1 1"></path>
                                                        <path d="M11.87 1.42h-2.9l-.7-1.4h-3.6l-.7 1.4h-2.9c-.6 0-1.1.4-1.1 1v5.5c0 .6.5 1 1.1 1h10.8c.6 0 1.1-.5 1.1-1v-5.5c0-.6-.4-1-1.1-1 .1 0 .1 0 0 0zm-5.4 6.2c-1.4 0-2.5-1.1-2.5-2.4s1.1-2.4 2.5-2.4 2.5 1.1 2.5 2.4c.1 1.3-1.1 2.4-2.5 2.4.1 0 0 0 0 0z"></path>
                                                    </g>
                                                </svg><span class="_78f72f87">18</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="_7fe76c69">
                                    <div class="_54799766">
                                        <div class="a091f4d0">
                                            <div class="b20d55ae _5e324ffe _664578bf">
                                                <button class="_5715b2fb" title="Map">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                        <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                    </svg>
                                                </button>
                                            </div>
                                            <div class="b20d55ae _5e324ffe _664578bf">
                                                <div class="_3d017150" aria-label="Share property">
                                                    <button class="_5715b2fb" title="Share it">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                            <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                        </svg>
                                                    </button>
                                                    <div>
                                                        <div style="display: none;">
                                                            <div class="_12e55ca6  ">
                                                                <ul class="_55bd3d93">
                                                                    <li class="_6fdf20b6">
                                                                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/countree_farmhouses_resort-1556.html" title="Share on Facebook" class="c6481bc9">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                                <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                                <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                            </svg><span>Share on Facebook</span></a>
                                                                    </li>
                                                                    <li class="_6fdf20b6">
                                                                        <a target="_blank" href="https://twitter.com/intent/tweet?text=Countree%20Farmhouses%20%26%20Resort - url: https://www.zameen.com/Property/countree_farmhouses_resort-1556.html" title="Share on Twitter" class="c6481bc9">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                                <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                            </svg><span>Share on Twitter</span></a>
                                                                    </li>
                                                                    <li class="_6fdf20b6"></li>
                                                                    <li class="_6fdf20b6">
                                                                        <a target="_blank" href="https://wa.me/?text=Countree%20Farmhouses%20%26%20Resort%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Fcountree_farmhouses_resort-1556.html" title="Share on WhatsApp" class="c6481bc9">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                                <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                                <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                                <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                            </svg><span>Share on WhatsApp</span></a>
                                                                    </li>
                                                                    <li class="_6fdf20b6">
                                                                        <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Countree%20Farmhouses%20%26%20Resort&amp;body=https://www.zameen.com/Property/countree_farmhouses_resort-1556.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                                <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                                <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                            </svg><span>Send via GMail</span></a>
                                                                    </li>
                                                                    <li class="_6fdf20b6">
                                                                        <a target="_self" href="mailto:?subject=Countree Farmhouses &amp; Resort&amp;body=https://www.zameen.com/Property/countree_farmhouses_resort-1556.html" title="Send via E-Mail" class="c6481bc9">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                                <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                            </svg><span>Send via E-Mail</span></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_70cc0e82">
                            <div class="_62bc75d8">
                                <picture class="_219b7e0a">
                                    <noscript>
                                        <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/76225752-120x90.webp />
                                        <img role="presentation" src=https://media.zameen.com/thumbnails/76225752-120x90.jpeg class=a lt=1 Countree Farmhouses & Resort title=1 Countree Farmhouses & Resort aria-label="Fallback listing photo" />
                                    </noscript>
                                    <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/76225752-120x90.webp"><img role="presentation" alt="1 Countree Farmhouses &amp; Resort" title="1 Countree Farmhouses &amp; Resort" aria-label="mini-photo-76225752" data-src="https://media.zameen.com/thumbnails/76225752-120x90.jpeg" class=" lazy loaded"></picture>
                            </div>
                            <div class="_62bc75d8">
                                <picture class="_219b7e0a">
                                    <noscript>
                                        <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/76225745-120x90.webp />
                                        <img role="presentation" src=https://media.zameen.com/thumbnails/76225745-120x90.jpeg class=a lt=2 Countree Farmhouses & Resort title=2 Countree Farmhouses & Resort aria-label="Fallback listing photo" />
                                    </noscript>
                                    <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/76225745-120x90.webp"><img role="presentation" alt="2 Countree Farmhouses &amp; Resort" title="2 Countree Farmhouses &amp; Resort" aria-label="mini-photo-76225745" data-src="https://media.zameen.com/thumbnails/76225745-120x90.jpeg" class=" lazy loaded"></picture>
                            </div>
                            <div class="_62bc75d8">
                                <picture class="_219b7e0a">
                                    <noscript>
                                        <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/76225746-120x90.webp />
                                        <img role="presentation" src=https://media.zameen.com/thumbnails/76225746-120x90.jpeg class=a lt=3 Countree Farmhouses & Resort title=3 Countree Farmhouses & Resort aria-label="Fallback listing photo" />
                                    </noscript>
                                    <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/76225746-120x90.webp"><img role="presentation" alt="3 Countree Farmhouses &amp; Resort" title="3 Countree Farmhouses &amp; Resort" aria-label="mini-photo-76225746" data-src="https://media.zameen.com/thumbnails/76225746-120x90.jpeg" class=" lazy loaded"></picture>
                            </div>
                        </div>
                    </div>
                    <div class="a2c7c87e">
                        <h2 class="_975a5723">Chakri Road - Rawalpindi</h2><span class="_51544d2f"><span class="_7c3ad451">PKR</span>32 Lakh&nbsp;to&nbsp;64 Lakh</span>
                        <div class="f946428d"><span class="_8b9b7c59">&nbsp;Residential Plots</span><span class="_8b9b7c59"><span>160 Marla</span></span><span class="_8b9b7c59">PKR64 Lakh</span></div>
                        <div class="f946428d"><span class="_8b9b7c59">&nbsp;Residential Plots</span><span class="_8b9b7c59"><span>80 Marla</span></span><span class="_8b9b7c59">PKR32 Lakh</span></div>
                        <div class="f946428d"><span class="_8b9b7c59">&nbsp;Residential Plots</span><span class="_8b9b7c59"><span>120 Marla</span></span><span class="_8b9b7c59">PKR48 Lakh</span></div>
                        <div class="_28f12c58">
                            <button class="_5b77d672 da62f2ae _7b380faf" type="button" aria-label="Call">Call</button>
                            <button class="_85d9f2e2 a8197536 _0a6511c6" aria-label="Send email"> Email</button>
                            <a href="/new-projects/countree_farmhouses_resort-1556.html" class="_9cc43f20">
                                <svg xmlns="http://www.w3.org/2000/svg" class="_10b6420d" viewBox="0 0 32.1 32">
                                    <g data-name="Layer 2">
                                        <path d="M15.6 32a101.4 101.4 0 0 1-12.3-.7A3.8 3.8 0 0 1 0 27.2a17.3 17.3 0 0 1 8.7-12.5l.6-.3.5.5a10 10 0 0 0 6.2 2.4h.1a8.7 8.7 0 0 0 6.1-2.4l.5-.5.6.3A17.3 17.3 0 0 1 32 27.1a3.6 3.6 0 0 1-3 4.2h-.3a111 111 0 0 1-12.8.7zM3.4 29.5a103.6 103.6 0 0 0 12.5.7 104.8 104.8 0 0 0 12.5-.7 1.8 1.8 0 0 0 1.6-2 15 15 0 0 0-7.2-10.7 10.7 10.7 0 0 1-6.9 2.4A11.6 11.6 0 0 1 9 16.8a15.2 15.2 0 0 0-7.2 10.6 2 2 0 0 0 1.6 2.1zM16 16.2a8.1 8.1 0 1 1 8-8.1 8 8 0 0 1-8 8.1zm0-14.3a6.2 6.2 0 1 0 6.2 6.2A6.2 6.2 0 0 0 16 1.9z" data-name="Layer 1"></path>
                                    </g>
                                </svg><span class="ece6016f">Developer</span></a>
                            <a href="/new-projects/countree_farmhouses_resort-1556.html" class="_9cc43f20">
                                <svg xmlns="http://www.w3.org/2000/svg" class="_10b6420d" viewBox="0 0 11.6 12">
                                    <path fill="#555460" d="M11.5 4.5L6 .7c-.1-.1-.2-.1-.3 0L.2 4.6c-.1 0-.2.1-.2.2v1.8c0 .2.1.3.3.3.1 0 .1 0 .2-.1l.7-.5V11c0 .2.1.3.3.3h3.1c.2 0 .3-.1.3-.3V8.3h1.8v2.8c0 .2.1.3.3.3h3.1c.2 0 .3-.1.3-.3V6.4l.7.5c.3.1.5 0 .5-.2V4.8c.1-.1 0-.2-.1-.3zm-1.7 6.2H7.4V8c0-.2-.1-.3-.3-.3H4.6c-.2 0-.3.1-.3.3v2.8H1.8V5.9l4-2.8 4 2.8v4.8zM11 6L6 2.5a.3.3 0 0 0-.4 0L.6 6V4.9l5.2-3.7L11 4.9V6z"></path>
                                </svg><span class="ece6016f">Property Types</span></a>
                        </div>
                    </div>
                </div>
            </article>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/capital_smart_city_capital_smart_city_overseas_block_capital_smart_city_villas-22324821-16249-2.html" class="_7ac32433" title="Capital Smart City Villas">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/81950031-400x300.webp />
                                    <img role="presentation" src=https://media.zameen.com/thumbnails/81950031-400x300.jpeg class=_8f499ba9 alt=Capital Smart City Villas title=Capital Smart City Villas aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/81950031-400x300.webp"><img role="presentation" alt="Capital Smart City Villas" title="Capital Smart City Villas" aria-label="Listing photo" data-src="https://media.zameen.com/thumbnails/81950031-400x300.jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="hot label">hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5">
                                    <div class="b20d55ae">
                                        <div class="_3448627a">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="bb8cec23 f773b145" viewBox="0 0 13 9">
                                                <g fill="#FFF">
                                                    <path d="M7.57 5.22c0 .6-.5 1-1.1 1s-1.1-.5-1.1-1 .5-1 1.1-1 1.1.4 1.1 1"></path>
                                                    <path d="M11.87 1.42h-2.9l-.7-1.4h-3.6l-.7 1.4h-2.9c-.6 0-1.1.4-1.1 1v5.5c0 .6.5 1 1.1 1h10.8c.6 0 1.1-.5 1.1-1v-5.5c0-.6-.4-1-1.1-1 .1 0 .1 0 0 0zm-5.4 6.2c-1.4 0-2.5-1.1-2.5-2.4s1.1-2.4 2.5-2.4 2.5 1.1 2.5 2.4c.1 1.3-1.1 2.4-2.5 2.4.1 0 0 0 0 0z"></path>
                                                </g>
                                            </svg><span class="_78f72f87">8</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/capital_smart_city_capital_smart_city_overseas_block_capital_smart_city_villas-22324821-16249-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Capital%20Smart%20City%20Villas - url: https://www.zameen.com/Property/capital_smart_city_capital_smart_city_overseas_block_capital_smart_city_villas-22324821-16249-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Capital%20Smart%20City%20Villas%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Fcapital_smart_city_capital_smart_city_overseas_block_capital_smart_city_villas-22324821-16249-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Capital%20Smart%20City%20Villas&amp;body=https://www.zameen.com/Property/capital_smart_city_capital_smart_city_overseas_block_capital_smart_city_villas-22324821-16249-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Capital Smart City Villas&amp;body=https://www.zameen.com/Property/capital_smart_city_capital_smart_city_overseas_block_capital_smart_city_villas-22324821-16249-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed">
                        <div class="_7ac32433" title="Capital Smart City Villas">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">30 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <path fill="#C22B2A" d="M11.1 0c.8 2.3.6 4.9-.3 7.1-1.1 2-2.3 4-3.8 5.8a16.3 16.3 0 0 0-2.8 7.4c.1 6.5 5.4 11.8 12 11.7 3 .2 5.9-.9 8.1-2.9 1.8-1.8 3-4.2 3.3-6.8.6-4.2-.5-8.5-2.9-11.9.1 1.3 0 2.6-.4 3.8a5.3 5.3 0 0 1-3.6 3.4 4 4 0 0 1-4.1-1.3 5.5 5.5 0 0 1-.5-5.4c.7-1.7.7-3.6.1-5.3a8.6 8.6 0 0 0-2.6-3.7A9.2 9.2 0 0 0 11.1 0z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <g fill="#34A048">
                                                <path d="M25.3 16.8V6.5l-5-1.3-4.1-1h-.3l-4 1c-1.8.3-3.5.8-5.3 1.2V17c-.1 2.1.7 4.2 2 5.8 2 2.3 4.5 4.1 7.2 5.2 2.5-1 4.7-2.5 6.6-4.4 2-1.7 3-4.2 2.9-6.7zm-3.2-4.4l-2.5 3.2-4 5.2c-.8 1-1.3 1-2.1 0L9.9 16a1 1 0 0 1 0-1.5c.4-.4 1.1-.3 1.5.1l2.6 2.3.6.5.3-.5 5.6-5.5.7-.5c.4 0 .9.1 1.1.5.1.2.1.7-.2 1.1z"></path>
                                                <path d="M29.1 16.8V5a2 2 0 0 0-1.7-2.2l-6-1.5L16.8.1h-1.7l-4.5 1.1-6.1 1.5C3.5 3 2.7 4 2.8 5v11.9c-.1 3 1 6 2.9 8.2a25 25 0 0 0 9.3 6.7l.9.2c.3 0 .7 0 1-.2a24 24 0 0 0 8.4-5.7c2.6-2.3 4-5.7 3.8-9.2zm-4.8 8.5c-2.3 2.3-5 4.1-7.9 5.3-.2.1-.5.1-.8 0a22 22 0 0 1-8.8-6.3c-1.8-2-2.7-4.7-2.6-7.4V5c0-.5.1-.8.6-.9l10.6-2.6h1C20 2.4 23.5 3.2 27 4c.5.1.6.4.6.8v11.8c.3 3.4-1 6.5-3.3 8.7z"></path>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <div class="_0402297d "><span class="_6b1c4ee5"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.7 10.2" class="_85195ac8"><path fill="#f5d652" d="M10.6 3.9a.6.6 0 0 0-.5-.5L7.2 3 5.9.3a.6.6 0 0 0-.4-.3.6.6 0 0 0-.2 0 .6.6 0 0 0-.5.3L3.5 3l-3 .4a.6.6 0 0 0-.3 1l2.1 2.2-.5 2.9a.6.6 0 0 0 1 .6l2.5-1.3L8 10a.6.6 0 0 0 .5 0 .7.7 0 0 0 .1 0 .6.6 0 0 0 .3-.6l-.5-3 2-2a.6.6 0 0 0 .2-.6z"></path></svg></span><span class="e7869745">Titanium</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Capital Smart City - Overseas Block, Capital Smart City</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Capital Smart City Villas"><div class="_1e0ca152 _026d7bff"><div><span>9.1 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Capital Smart City Villas</h2></div>
                        <div class="ee550b27">DO YOU HAVE A SMART PHONE? DO YOU HAVE A SMART CAR? Than WHY DON'T YOU HAVE A SMART VILLAS??????... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 3 hours ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-22324821" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-22324821">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/30315994-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/30315994-240x180.jpeg class=_54945be7 _1b380bc2 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/30315994-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/30315994-240x180.jpeg" class="_54945be7 _1b380bc2 lazy loaded"></picture>
                                    <div class="_8f0b72fd">
                                        <picture class="_219b7e0a">
                                            <noscript>
                                                <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/30315994-600x450.webp />
                                                <img role="presentation" src=https://media.zameen.com/thumbnails/30315994-600x450.jpeg class=_53426593 alt=t itle=a ria-label="Fallback listing photo" />
                                            </noscript>
                                            <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/30315994-600x450.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/30315994-600x450.jpeg" class="_53426593 lazy loaded"></picture>
                                        <div class="_713d6e48">Registered since: 2018</div>
                                        <div class="_4ed4ba45">135 Properties for Sale</div>
                                        <div class="_4ed4ba45">0 Properties for Rent</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li class="_53ec901b">
                <div class="_7bc4114b" id="g-ad-1766" style="width: 650px; height: 80px; display: none;" data-google-query-id="CKy26Mrn2OgCFaGpUQod7gAIBA">
                    <div id="google_ads_iframe_/31946216/Strip_7_650x80_0__container__" style="border: 0pt none; width: 650px; height: 80px;"></div>
                </div>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/rawalpindi_adiala_road_agricultural_land__available_for_sale-21209653-478-2.html" class="_7ac32433" title="Agricultural Land  Available For Sale">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://images.zameen.com/smap/400/300/33.550869/73.056182/image.jpeg?quality=80&imageformat=jpeg />
                                    <img role="presentation" src=https://images.zameen.com/smap/400/300/33.550869/73.056182/image.jpeg?quality=80&imageformat=jpeg class=_8f499ba9 alt=Agricultural Land Available For Sale title=Agricultural Land Available For Sale aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://images.zameen.com/smap/400/300/33.550869/73.056182/image.jpeg?quality=80&amp;imageformat=jpeg"><img role="presentation" alt="Agricultural Land  Available For Sale" title="Agricultural Land  Available For Sale" aria-label="Listing photo" data-src="https://images.zameen.com/smap/400/300/33.550869/73.056182/image.jpeg?quality=80&amp;imageformat=jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="hot label">hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5"></div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/rawalpindi_adiala_road_agricultural_land__available_for_sale-21209653-478-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Agricultural%20Land%20%20Available%20For%20Sale - url: https://www.zameen.com/Property/rawalpindi_adiala_road_agricultural_land__available_for_sale-21209653-478-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Agricultural%20Land%20%20Available%20For%20Sale%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Frawalpindi_adiala_road_agricultural_land__available_for_sale-21209653-478-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Agricultural%20Land%20%20Available%20For%20Sale&amp;body=https://www.zameen.com/Property/rawalpindi_adiala_road_agricultural_land__available_for_sale-21209653-478-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Agricultural Land  Available For Sale&amp;body=https://www.zameen.com/Property/rawalpindi_adiala_road_agricultural_land__available_for_sale-21209653-478-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed">
                        <div class="_7ac32433" title="Agricultural Land  Available For Sale">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">8 Crore</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <path fill="#C22B2A" d="M11.1 0c.8 2.3.6 4.9-.3 7.1-1.1 2-2.3 4-3.8 5.8a16.3 16.3 0 0 0-2.8 7.4c.1 6.5 5.4 11.8 12 11.7 3 .2 5.9-.9 8.1-2.9 1.8-1.8 3-4.2 3.3-6.8.6-4.2-.5-8.5-2.9-11.9.1 1.3 0 2.6-.4 3.8a5.3 5.3 0 0 1-3.6 3.4 4 4 0 0 1-4.1-1.3 5.5 5.5 0 0 1-.5-5.4c.7-1.7.7-3.6.1-5.3a8.6 8.6 0 0 0-2.6-3.7A9.2 9.2 0 0 0 11.1 0z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Adiala Road, Rawalpindi</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Agricultural Land  Available For Sale"><div class="_1e0ca152 _026d7bff"><div><span>200 Kanal</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Agricultural Land  Available For Sale</h2></div>
                        <div class="ee550b27">Agricultural Land Available For Sale Main Adiala Road Rawalpindi. Price: 4 Lac Par Kanal. Best... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 1 hour ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-21209653" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-21209653">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/20471945-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/20471945-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/20471945-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/20471945-240x180.jpeg" class="_54945be7 lazy loaded"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/bahria_greens_overseas_enclave_bahria_greens_overseas_enclave_sector_3_plot_for_sale_in_bahria_town_phase_8_sector_overseas_3-18363900-8302-2.html" class="_7ac32433" title="Plot For Sale In Bahria Town Phase 8 Sector Overseas 3">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://images.zameen.com/smap/400/300/33.509017/73.081219/image.jpeg?quality=80&imageformat=jpeg />
                                    <img role="presentation" src=https://images.zameen.com/smap/400/300/33.509017/73.081219/image.jpeg?quality=80&imageformat=jpeg class=_8f499ba9 alt=Plot For Sale In Bahria Town Phase 8 Sector Overseas 3 title=Plot For Sale In Bahria Town Phase 8 Sector Overseas 3 aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://images.zameen.com/smap/400/300/33.509017/73.081219/image.jpeg?quality=80&amp;imageformat=jpeg"><img role="presentation" alt="Plot For Sale In Bahria Town Phase 8 Sector Overseas 3" title="Plot For Sale In Bahria Town Phase 8 Sector Overseas 3" aria-label="Listing photo" data-src="https://images.zameen.com/smap/400/300/33.509017/73.081219/image.jpeg?quality=80&amp;imageformat=jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="hot label">hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5"></div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/bahria_greens_overseas_enclave_bahria_greens_overseas_enclave_sector_3_plot_for_sale_in_bahria_town_phase_8_sector_overseas_3-18363900-8302-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Plot%20For%20Sale%20In%20Bahria%20Town%20Phase%208%20Sector%20Overseas%203 - url: https://www.zameen.com/Property/bahria_greens_overseas_enclave_bahria_greens_overseas_enclave_sector_3_plot_for_sale_in_bahria_town_phase_8_sector_overseas_3-18363900-8302-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Plot%20For%20Sale%20In%20Bahria%20Town%20Phase%208%20Sector%20Overseas%203%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Fbahria_greens_overseas_enclave_bahria_greens_overseas_enclave_sector_3_plot_for_sale_in_bahria_town_phase_8_sector_overseas_3-18363900-8302-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Plot%20For%20Sale%20In%20Bahria%20Town%20Phase%208%20Sector%20Overseas%203&amp;body=https://www.zameen.com/Property/bahria_greens_overseas_enclave_bahria_greens_overseas_enclave_sector_3_plot_for_sale_in_bahria_town_phase_8_sector_overseas_3-18363900-8302-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Plot For Sale In Bahria Town Phase 8 Sector Overseas 3&amp;body=https://www.zameen.com/Property/bahria_greens_overseas_enclave_bahria_greens_overseas_enclave_sector_3_plot_for_sale_in_bahria_town_phase_8_sector_overseas_3-18363900-8302-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed">
                        <div class="_7ac32433" title="Plot For Sale In Bahria Town Phase 8 Sector Overseas 3">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">1.1 Crore</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <path fill="#C22B2A" d="M11.1 0c.8 2.3.6 4.9-.3 7.1-1.1 2-2.3 4-3.8 5.8a16.3 16.3 0 0 0-2.8 7.4c.1 6.5 5.4 11.8 12 11.7 3 .2 5.9-.9 8.1-2.9 1.8-1.8 3-4.2 3.3-6.8.6-4.2-.5-8.5-2.9-11.9.1 1.3 0 2.6-.4 3.8a5.3 5.3 0 0 1-3.6 3.4 4 4 0 0 1-4.1-1.3 5.5 5.5 0 0 1-.5-5.4c.7-1.7.7-3.6.1-5.3a8.6 8.6 0 0 0-2.6-3.7A9.2 9.2 0 0 0 11.1 0z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Bahria Greens - Overseas Enclave - Sector 3, Bahria Greens - Overseas Enclave</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Plot For Sale In Bahria Town Phase 8 Sector Overseas 3"><div class="_1e0ca152 _026d7bff"><div><span>15 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Plot For Sale In Bahria Town Phase 8 Sector Overseas 3</h2></div>
                        <div class="ee550b27">Beautiful height location, back open and Dead-end corner plot 10 marla plot with 5 marla extra land... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 2 hours ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-18363900" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-18363900">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/24873402-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/24873402-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/24873402-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/24873402-240x180.jpeg" class="_54945be7 lazy loaded"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/bahria_town_phase_8_bahria_town_phase_8_rafi_block_residential_5_marla_mail_blueward_with_map_possession_paid_for_sale-18873311-3071-2.html" class="_7ac32433" title="Residential 5 Marla mail Blueward With Map Possession Paid For Sale">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://images.zameen.com/smap/400/300/33.50297/73.102169/image.jpeg?quality=80&imageformat=jpeg />
                                    <img role="presentation" src=https://images.zameen.com/smap/400/300/33.50297/73.102169/image.jpeg?quality=80&imageformat=jpeg class=_8f499ba9 alt=Residential 5 Marla mail Blueward With Map Possession Paid For Sale title=Residential 5 Marla mail Blueward With Map Possession Paid For Sale aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://images.zameen.com/smap/400/300/33.50297/73.102169/image.jpeg?quality=80&amp;imageformat=jpeg"><img role="presentation" alt="Residential 5 Marla mail Blueward With Map Possession Paid For Sale" title="Residential 5 Marla mail Blueward With Map Possession Paid For Sale" aria-label="Listing photo" data-src="https://images.zameen.com/smap/400/300/33.50297/73.102169/image.jpeg?quality=80&amp;imageformat=jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="hot label">hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5"></div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_rafi_block_residential_5_marla_mail_blueward_with_map_possession_paid_for_sale-18873311-3071-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Residential%205%20Marla%20mail%20Blueward%20With%20Map%20Possession%20Paid%20For%20Sale - url: https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_rafi_block_residential_5_marla_mail_blueward_with_map_possession_paid_for_sale-18873311-3071-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Residential%205%20Marla%20mail%20Blueward%20With%20Map%20Possession%20Paid%20For%20Sale%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Fbahria_town_phase_8_bahria_town_phase_8_rafi_block_residential_5_marla_mail_blueward_with_map_possession_paid_for_sale-18873311-3071-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Residential%205%20Marla%20mail%20Blueward%20With%20Map%20Possession%20Paid%20For%20Sale&amp;body=https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_rafi_block_residential_5_marla_mail_blueward_with_map_possession_paid_for_sale-18873311-3071-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Residential 5 Marla mail Blueward With Map Possession Paid For Sale&amp;body=https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_rafi_block_residential_5_marla_mail_blueward_with_map_possession_paid_for_sale-18873311-3071-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed">
                        <div class="_7ac32433" title="Residential 5 Marla mail Blueward With Map Possession Paid For Sale">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">38 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <path fill="#C22B2A" d="M11.1 0c.8 2.3.6 4.9-.3 7.1-1.1 2-2.3 4-3.8 5.8a16.3 16.3 0 0 0-2.8 7.4c.1 6.5 5.4 11.8 12 11.7 3 .2 5.9-.9 8.1-2.9 1.8-1.8 3-4.2 3.3-6.8.6-4.2-.5-8.5-2.9-11.9.1 1.3 0 2.6-.4 3.8a5.3 5.3 0 0 1-3.6 3.4 4 4 0 0 1-4.1-1.3 5.5 5.5 0 0 1-.5-5.4c.7-1.7.7-3.6.1-5.3a8.6 8.6 0 0 0-2.6-3.7A9.2 9.2 0 0 0 11.1 0z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Bahria Town Phase 8 - Rafi Block, Bahria Town Phase 8</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Residential 5 Marla mail Blueward With Map Possession Paid For Sale"><div class="_1e0ca152 _026d7bff"><div><span>5 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Residential 5 Marla mail Blueward With Map Possession Paid For Sale</h2></div>
                        <div class="ee550b27">Bahria Town Phase 8 Rafi Block 5 Marla . The time spent to make a fruitful investment like this... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 2 hours ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-18873311" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-18873311">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/24873402-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/24873402-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/24873402-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/24873402-240x180.jpeg" class="_54945be7 lazy loaded"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/bahria_town_phase_8_bahria_town_phase_8_block_f_bahria_town_phase_8_residential_boulevard_corner_plot_for_sale-18873357-3054-2.html" class="_7ac32433" title="Bahria Town Phase 8 Residential Boulevard Corner Plot For Sale">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://images.zameen.com/smap/400/300/33.482606/73.092105/image.jpeg?quality=80&imageformat=jpeg />
                                    <img role="presentation" src=https://images.zameen.com/smap/400/300/33.482606/73.092105/image.jpeg?quality=80&imageformat=jpeg class=_8f499ba9 alt=Bahria Town Phase 8 Residential Boulevard Corner Plot For Sale title=Bahria Town Phase 8 Residential Boulevard Corner Plot For Sale aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://images.zameen.com/smap/400/300/33.482606/73.092105/image.jpeg?quality=80&amp;imageformat=jpeg"><img role="presentation" alt="Bahria Town Phase 8 Residential Boulevard Corner Plot For Sale" title="Bahria Town Phase 8 Residential Boulevard Corner Plot For Sale" aria-label="Listing photo" data-src="https://images.zameen.com/smap/400/300/33.482606/73.092105/image.jpeg?quality=80&amp;imageformat=jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="hot label">hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5"></div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_block_f_bahria_town_phase_8_residential_boulevard_corner_plot_for_sale-18873357-3054-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Bahria%20Town%20Phase%208%20Residential%20Boulevard%20Corner%20Plot%20For%20Sale - url: https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_block_f_bahria_town_phase_8_residential_boulevard_corner_plot_for_sale-18873357-3054-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Bahria%20Town%20Phase%208%20Residential%20Boulevard%20Corner%20Plot%20For%20Sale%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Fbahria_town_phase_8_bahria_town_phase_8_block_f_bahria_town_phase_8_residential_boulevard_corner_plot_for_sale-18873357-3054-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Bahria%20Town%20Phase%208%20Residential%20Boulevard%20Corner%20Plot%20For%20Sale&amp;body=https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_block_f_bahria_town_phase_8_residential_boulevard_corner_plot_for_sale-18873357-3054-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Bahria Town Phase 8 Residential Boulevard Corner Plot For Sale&amp;body=https://www.zameen.com/Property/bahria_town_phase_8_bahria_town_phase_8_block_f_bahria_town_phase_8_residential_boulevard_corner_plot_for_sale-18873357-3054-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed">
                        <div class="_7ac32433" title="Bahria Town Phase 8 Residential Boulevard Corner Plot For Sale">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">75 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <path fill="#C22B2A" d="M11.1 0c.8 2.3.6 4.9-.3 7.1-1.1 2-2.3 4-3.8 5.8a16.3 16.3 0 0 0-2.8 7.4c.1 6.5 5.4 11.8 12 11.7 3 .2 5.9-.9 8.1-2.9 1.8-1.8 3-4.2 3.3-6.8.6-4.2-.5-8.5-2.9-11.9.1 1.3 0 2.6-.4 3.8a5.3 5.3 0 0 1-3.6 3.4 4 4 0 0 1-4.1-1.3 5.5 5.5 0 0 1-.5-5.4c.7-1.7.7-3.6.1-5.3a8.6 8.6 0 0 0-2.6-3.7A9.2 9.2 0 0 0 11.1 0z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Bahria Town Phase 8 - Block F, Bahria Town Phase 8</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Bahria Town Phase 8 Residential Boulevard Corner Plot For Sale"><div class="_1e0ca152 _026d7bff"><div><span>10 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Bahria Town Phase 8 Residential Boulevard Corner Plot For Sale</h2></div>
                        <div class="ee550b27">8 marla ext land map paid Bahria Town Phase 8 Boulevard back plot for sale on beautiful location. ... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 2 hours ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-18873357" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-18873357">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/24873402-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/24873402-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/24873402-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/24873402-240x180.jpeg" class="_54945be7 lazy loaded"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/bahria_town_rawalpindi_bahria_intellectual_village_bahria_town_intellectual_village_2_kanal_2_marla_3_side_plot_for_sale_on_beautiful_location-18873672-8256-2.html" class="_7ac32433" title="Bahria Town Intellectual Village 2 Kanal 2 Marla 3 Side Plot For Sale On Beautiful Location">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://images.zameen.com/smap/400/300/33.526943/73.093743/image.jpeg?quality=80&imageformat=jpeg />
                                    <img role="presentation" src=https://images.zameen.com/smap/400/300/33.526943/73.093743/image.jpeg?quality=80&imageformat=jpeg class=_8f499ba9 alt=Bahria Town Intellectual Village 2 Kanal 2 Marla 3 Side Plot For Sale On Beautiful Location title=Bahria Town Intellectual Village 2 Kanal 2 Marla 3 Side Plot For Sale On Beautiful Location aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://images.zameen.com/smap/400/300/33.526943/73.093743/image.jpeg?quality=80&amp;imageformat=jpeg"><img role="presentation" alt="Bahria Town Intellectual Village 2 Kanal 2 Marla 3 Side Plot For Sale On Beautiful Location" title="Bahria Town Intellectual Village 2 Kanal 2 Marla 3 Side Plot For Sale On Beautiful Location" aria-label="Listing photo" data-src="https://images.zameen.com/smap/400/300/33.526943/73.093743/image.jpeg?quality=80&amp;imageformat=jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51">
                                <div class="_71b6d25e" aria-label="hot label">hot</div>
                            </div>
                            <div class="f59902d8">
                                <div class="f1475fe5"></div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/bahria_town_rawalpindi_bahria_intellectual_village_bahria_town_intellectual_village_2_kanal_2_marla_3_side_plot_for_sale_on_beautiful_location-18873672-8256-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Bahria%20Town%20Intellectual%20Village%202%20Kanal%202%20Marla%203%20Side%20Plot%20For%20Sale%20On%20Beautiful%20Location - url: https://www.zameen.com/Property/bahria_town_rawalpindi_bahria_intellectual_village_bahria_town_intellectual_village_2_kanal_2_marla_3_side_plot_for_sale_on_beautiful_location-18873672-8256-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Bahria%20Town%20Intellectual%20Village%202%20Kanal%202%20Marla%203%20Side%20Plot%20For%20Sale%20On%20Beautiful%20Location%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Fbahria_town_rawalpindi_bahria_intellectual_village_bahria_town_intellectual_village_2_kanal_2_marla_3_side_plot_for_sale_on_beautiful_location-18873672-8256-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Bahria%20Town%20Intellectual%20Village%202%20Kanal%202%20Marla%203%20Side%20Plot%20For%20Sale%20On%20Beautiful%20Location&amp;body=https://www.zameen.com/Property/bahria_town_rawalpindi_bahria_intellectual_village_bahria_town_intellectual_village_2_kanal_2_marla_3_side_plot_for_sale_on_beautiful_location-18873672-8256-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Bahria Town Intellectual Village 2 Kanal 2 Marla 3 Side Plot For Sale On Beautiful Location&amp;body=https://www.zameen.com/Property/bahria_town_rawalpindi_bahria_intellectual_village_bahria_town_intellectual_village_2_kanal_2_marla_3_side_plot_for_sale_on_beautiful_location-18873672-8256-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed">
                        <div class="_7ac32433" title="Bahria Town Intellectual Village 2 Kanal 2 Marla 3 Side Plot For Sale On Beautiful Location">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">3 Crore</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7">
                            <div class="_1e0ca152 _0af330ea">
                                <div>
                                    <div class="_38842953">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="c1a24809">
                                            <path fill="#C22B2A" d="M11.1 0c.8 2.3.6 4.9-.3 7.1-1.1 2-2.3 4-3.8 5.8a16.3 16.3 0 0 0-2.8 7.4c.1 6.5 5.4 11.8 12 11.7 3 .2 5.9-.9 8.1-2.9 1.8-1.8 3-4.2 3.3-6.8.6-4.2-.5-8.5-2.9-11.9.1 1.3 0 2.6-.4 3.8a5.3 5.3 0 0 1-3.6 3.4 4 4 0 0 1-4.1-1.3 5.5 5.5 0 0 1-.5-5.4c.7-1.7.7-3.6.1-5.3a8.6 8.6 0 0 0-2.6-3.7A9.2 9.2 0 0 0 11.1 0z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Bahria Intellectual Village, Bahria Town Rawalpindi</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Bahria Town Intellectual Village 2 Kanal 2 Marla 3 Side Plot For Sale On Beautiful Location"><div class="_1e0ca152 _026d7bff"><div><span>2 Kanal</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Bahria Town Intellectual Village 2 Kanal 2 Marla 3 Side Plot For Sale On Beautiful Location</h2></div>
                        <div class="ee550b27">Bahria town Intellectual Village 2 kanal 2 Marla plot. The time spent to make a fruitful... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 2 hours ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-18873672" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-18873672">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/24873402-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/24873402-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/24873402-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/24873402-240x180.jpeg" class="_54945be7 lazy loaded"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li class="_53ec901b">
                <div class="_7bc4114b" id="g-ad-9076" style="width: 650px; height: 80px; display: none;" data-google-query-id="CPiW6srn2OgCFVPiGwodruYEaA">
                    <div id="google_ads_iframe_/31946216/Strip_8_650x80_0__container__" style="border: 0pt none; width: 650px; height: 80px;"></div>
                </div>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/rawalpindi_gulshan_abad_pair_plot_is_available_for_sale-21806622-1058-2.html" class="_7ac32433" title="Pair Plot Is Available For Sale">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg />
                                    <img role="presentation" src=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg class=_8f499ba9 alt=Pair Plot Is Available For Sale title=Pair Plot Is Available For Sale aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg"><img role="presentation" alt="Pair Plot Is Available For Sale" title="Pair Plot Is Available For Sale" aria-label="Listing photo" data-src="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51"></div>
                            <div class="f59902d8">
                                <div class="f1475fe5"></div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/rawalpindi_gulshan_abad_pair_plot_is_available_for_sale-21806622-1058-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Pair%20Plot%20Is%20Available%20For%20Sale - url: https://www.zameen.com/Property/rawalpindi_gulshan_abad_pair_plot_is_available_for_sale-21806622-1058-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Pair%20Plot%20Is%20Available%20For%20Sale%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Frawalpindi_gulshan_abad_pair_plot_is_available_for_sale-21806622-1058-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Pair%20Plot%20Is%20Available%20For%20Sale&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_pair_plot_is_available_for_sale-21806622-1058-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Pair Plot Is Available For Sale&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_pair_plot_is_available_for_sale-21806622-1058-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed">
                        <div class="_7ac32433" title="Pair Plot Is Available For Sale">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">45 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7"></div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Gulshan Abad, Rawalpindi</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Pair Plot Is Available For Sale"><div class="_1e0ca152 _026d7bff"><div><span>10 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Pair Plot Is Available For Sale</h2></div>
                        <div class="ee550b27">10 Marla Pair Plot Available in in Street # 79 A. Sector # 3 of Gulshan Abad Adiala Road,100 '/,... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 16 minutes ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-21806622" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-21806622">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/20471965-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/20471965-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/20471965-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/20471965-240x180.jpeg" class="_54945be7 lazy loaded"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/rawalpindi_gulshan_abad_pair_plot_is_available_for_sale-21807206-1058-2.html" class="_7ac32433" title="Pair Plot Is Available For Sale">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg />
                                    <img role="presentation" src=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg class=_8f499ba9 alt=Pair Plot Is Available For Sale title=Pair Plot Is Available For Sale aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg"><img role="presentation" alt="Pair Plot Is Available For Sale" title="Pair Plot Is Available For Sale" aria-label="Listing photo" data-src="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51"></div>
                            <div class="f59902d8">
                                <div class="f1475fe5"></div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/rawalpindi_gulshan_abad_pair_plot_is_available_for_sale-21807206-1058-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Pair%20Plot%20Is%20Available%20For%20Sale - url: https://www.zameen.com/Property/rawalpindi_gulshan_abad_pair_plot_is_available_for_sale-21807206-1058-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Pair%20Plot%20Is%20Available%20For%20Sale%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Frawalpindi_gulshan_abad_pair_plot_is_available_for_sale-21807206-1058-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Pair%20Plot%20Is%20Available%20For%20Sale&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_pair_plot_is_available_for_sale-21807206-1058-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Pair Plot Is Available For Sale&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_pair_plot_is_available_for_sale-21807206-1058-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed">
                        <div class="_7ac32433" title="Pair Plot Is Available For Sale">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">50 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7"></div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Gulshan Abad, Rawalpindi</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Pair Plot Is Available For Sale"><div class="_1e0ca152 _026d7bff"><div><span>10 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Pair Plot Is Available For Sale</h2></div>
                        <div class="ee550b27">Pair Plot is Available for Sale. 10 Marla Level Plot is Available for Sale Suitated in Gulshan... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 16 minutes ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-21807206" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-21807206">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/20471965-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/20471965-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/20471965-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/20471965-240x180.jpeg" class="_54945be7 lazy loaded"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li class="_53ec901b">
                <div class="_7bc4114b" id="g-ad-9702" style="width: 650px; height: 80px; display: none;" data-google-query-id="CJye6crn2OgCFcLGUQodijcI-Q">
                    <div id="google_ads_iframe_/31946216/Strip_9_650x80_0__container__" style="border: 0pt none; width: 650px; height: 80px;"></div>
                </div>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/rawalpindi_gulshan_abad_the_good_location_plot_for_investment_purpose-21807375-1058-2.html" class="_7ac32433" title="The Good Location Plot For Investment Purpose">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg />
                                    <img role="presentation" src=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg class=_8f499ba9 alt=The Good Location Plot For Investment Purpose title=The Good Location Plot For Investment Purpose aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg"><img role="presentation" alt="The Good Location Plot For Investment Purpose" title="The Good Location Plot For Investment Purpose" aria-label="Listing photo" data-src="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51"></div>
                            <div class="f59902d8">
                                <div class="f1475fe5"></div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/rawalpindi_gulshan_abad_the_good_location_plot_for_investment_purpose-21807375-1058-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=The%20Good%20Location%20Plot%20For%20Investment%20Purpose - url: https://www.zameen.com/Property/rawalpindi_gulshan_abad_the_good_location_plot_for_investment_purpose-21807375-1058-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=The%20Good%20Location%20Plot%20For%20Investment%20Purpose%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Frawalpindi_gulshan_abad_the_good_location_plot_for_investment_purpose-21807375-1058-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=The%20Good%20Location%20Plot%20For%20Investment%20Purpose&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_the_good_location_plot_for_investment_purpose-21807375-1058-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=The Good Location Plot For Investment Purpose&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_the_good_location_plot_for_investment_purpose-21807375-1058-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed">
                        <div class="_7ac32433" title="The Good Location Plot For Investment Purpose">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">85 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7"></div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Gulshan Abad, Rawalpindi</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="The Good Location Plot For Investment Purpose"><div class="_1e0ca152 _026d7bff"><div><span>1 Kanal</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">The Good Location Plot For Investment Purpose</h2></div>
                        <div class="ee550b27">1 kanal Plot situated in Street # 61,Sector #2 of Gulshan Abad Adiala Road,100 '/, Level Plot... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 16 minutes ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-21807375" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-21807375">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/20471965-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/20471965-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/20471965-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/20471965-240x180.jpeg" class="_54945be7 lazy loaded"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/rawalpindi_gulshan_abad_residential_pair_plot_available_for_sale-22123740-1058-2.html" class="_7ac32433" title="Residential Pair Plot Available For Sale">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg />
                                    <img role="presentation" src=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg class=_8f499ba9 alt=Residential Pair Plot Available For Sale title=Residential Pair Plot Available For Sale aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg"><img role="presentation" alt="Residential Pair Plot Available For Sale" title="Residential Pair Plot Available For Sale" aria-label="Listing photo" data-src="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51"></div>
                            <div class="f59902d8">
                                <div class="f1475fe5"></div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/rawalpindi_gulshan_abad_residential_pair_plot_available_for_sale-22123740-1058-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Residential%20Pair%20Plot%20Available%20For%20Sale - url: https://www.zameen.com/Property/rawalpindi_gulshan_abad_residential_pair_plot_available_for_sale-22123740-1058-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Residential%20Pair%20Plot%20Available%20For%20Sale%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Frawalpindi_gulshan_abad_residential_pair_plot_available_for_sale-22123740-1058-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Residential%20Pair%20Plot%20Available%20For%20Sale&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_residential_pair_plot_available_for_sale-22123740-1058-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Residential Pair Plot Available For Sale&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_residential_pair_plot_available_for_sale-22123740-1058-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed">
                        <div class="_7ac32433" title="Residential Pair Plot Available For Sale">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">50 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7"></div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Gulshan Abad, Rawalpindi</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Residential Pair Plot Available For Sale"><div class="_1e0ca152 _026d7bff"><div><span>10 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Residential Pair Plot Available For Sale</h2></div>
                        <div class="ee550b27">Residential Pair Plot Available For Sale Situated in Gulshan Abad, Sectors#3 Reasonable price... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 16 minutes ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-22123740" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-22123740">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/20471965-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/20471965-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/20471965-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/20471965-240x180.jpeg" class="_54945be7 lazy loaded"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/rawalpindi_gulshan_abad_we_are_thrilled_to_offer_a_residential_plot-21204112-1058-2.html" class="_7ac32433" title="We Are Thrilled To Offer A Residential Plot">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg />
                                    <img role="presentation" src=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg class=_8f499ba9 alt=We Are Thrilled To Offer A Residential Plot title=We Are Thrilled To Offer A Residential Plot aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg"><img role="presentation" alt="We Are Thrilled To Offer A Residential Plot" title="We Are Thrilled To Offer A Residential Plot" aria-label="Listing photo" data-src="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51"></div>
                            <div class="f59902d8">
                                <div class="f1475fe5"></div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/rawalpindi_gulshan_abad_we_are_thrilled_to_offer_a_residential_plot-21204112-1058-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=We%20Are%20Thrilled%20To%20Offer%20A%20Residential%20Plot - url: https://www.zameen.com/Property/rawalpindi_gulshan_abad_we_are_thrilled_to_offer_a_residential_plot-21204112-1058-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=We%20Are%20Thrilled%20To%20Offer%20A%20Residential%20Plot%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Frawalpindi_gulshan_abad_we_are_thrilled_to_offer_a_residential_plot-21204112-1058-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=We%20Are%20Thrilled%20To%20Offer%20A%20Residential%20Plot&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_we_are_thrilled_to_offer_a_residential_plot-21204112-1058-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=We Are Thrilled To Offer A Residential Plot&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_we_are_thrilled_to_offer_a_residential_plot-21204112-1058-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed">
                        <div class="_7ac32433" title="We Are Thrilled To Offer A Residential Plot">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">25 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7"></div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Gulshan Abad, Rawalpindi</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="We Are Thrilled To Offer A Residential Plot"><div class="_1e0ca152 _026d7bff"><div><span>10 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">We Are Thrilled To Offer A Residential Plot</h2></div>
                        <div class="ee550b27">10 Marla Plot situated in Sector # 4 of Gulshan Abad Rawalpindi. It is Golden Opportunity to be... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 16 minutes ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-21204112" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-21204112">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/20471965-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/20471965-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/20471965-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/20471965-240x180.jpeg" class="_54945be7 lazy loaded"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li class="_53ec901b">
                <div class="_7bc4114b" id="g-ad-6348" style="width: 650px; height: 80px; display: none;" data-google-query-id="CIzA78rn2OgCFdOnGwodrqcLng">
                    <div id="google_ads_iframe_/31946216/Strip_10_650x80_0__container__" style="border: 0pt none; width: 650px; height: 80px;"></div>
                </div>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/rawalpindi_gulshan_abad_an_ultimate_chance_for_long_term_investment_in_the_shape_of_residential_plot-21204115-1058-2.html" class="_7ac32433" title="An Ultimate Chance For Long Term Investment In The Shape Of Residential Plot">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg />
                                    <img role="presentation" src=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg class=_8f499ba9 alt=An Ultimate Chance For Long Term Investment In The Shape Of Residential Plot title=An Ultimate Chance For Long Term Investment In The Shape Of Residential Plot aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg"><img role="presentation" alt="An Ultimate Chance For Long Term Investment In The Shape Of Residential Plot" title="An Ultimate Chance For Long Term Investment In The Shape Of Residential Plot" aria-label="Listing photo" data-src="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51"></div>
                            <div class="f59902d8">
                                <div class="f1475fe5"></div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/rawalpindi_gulshan_abad_an_ultimate_chance_for_long_term_investment_in_the_shape_of_residential_plot-21204115-1058-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=An%20Ultimate%20Chance%20For%20Long%20Term%20Investment%20In%20The%20Shape%20Of%20Residential%20Plot - url: https://www.zameen.com/Property/rawalpindi_gulshan_abad_an_ultimate_chance_for_long_term_investment_in_the_shape_of_residential_plot-21204115-1058-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=An%20Ultimate%20Chance%20For%20Long%20Term%20Investment%20In%20The%20Shape%20Of%20Residential%20Plot%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Frawalpindi_gulshan_abad_an_ultimate_chance_for_long_term_investment_in_the_shape_of_residential_plot-21204115-1058-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=An%20Ultimate%20Chance%20For%20Long%20Term%20Investment%20In%20The%20Shape%20Of%20Residential%20Plot&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_an_ultimate_chance_for_long_term_investment_in_the_shape_of_residential_plot-21204115-1058-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=An Ultimate Chance For Long Term Investment In The Shape Of Residential Plot&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_an_ultimate_chance_for_long_term_investment_in_the_shape_of_residential_plot-21204115-1058-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed">
                        <div class="_7ac32433" title="An Ultimate Chance For Long Term Investment In The Shape Of Residential Plot">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">35 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7"></div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Gulshan Abad, Rawalpindi</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="An Ultimate Chance For Long Term Investment In The Shape Of Residential Plot"><div class="_1e0ca152 _026d7bff"><div><span>16 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">An Ultimate Chance For Long Term Investment In The Shape Of Residential Plot</h2></div>
                        <div class="ee550b27">16 Marla Plot situated in Gulshan Abad Rawalpindi. The Owner of Above Said Plot is in Need of... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 16 minutes ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-21204115" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-21204115">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/20471965-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/20471965-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/20471965-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/20471965-240x180.jpeg" class="_54945be7 lazy loaded"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/rawalpindi_gulshan_abad_corner_plot_is_available_for_sale-18978243-1058-2.html" class="_7ac32433" title="Corner Plot Is Available For Sale">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg />
                                    <img role="presentation" src=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg class=_8f499ba9 alt=Corner Plot Is Available For Sale title=Corner Plot Is Available For Sale aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg"><img role="presentation" alt="Corner Plot Is Available For Sale" title="Corner Plot Is Available For Sale" aria-label="Listing photo" data-src="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51"></div>
                            <div class="f59902d8">
                                <div class="f1475fe5"></div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/rawalpindi_gulshan_abad_corner_plot_is_available_for_sale-18978243-1058-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=Corner%20Plot%20Is%20Available%20For%20Sale - url: https://www.zameen.com/Property/rawalpindi_gulshan_abad_corner_plot_is_available_for_sale-18978243-1058-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=Corner%20Plot%20Is%20Available%20For%20Sale%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Frawalpindi_gulshan_abad_corner_plot_is_available_for_sale-18978243-1058-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=Corner%20Plot%20Is%20Available%20For%20Sale&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_corner_plot_is_available_for_sale-18978243-1058-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=Corner Plot Is Available For Sale&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_corner_plot_is_available_for_sale-18978243-1058-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed">
                        <div class="_7ac32433" title="Corner Plot Is Available For Sale">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">75 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7"></div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Gulshan Abad, Rawalpindi</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="Corner Plot Is Available For Sale"><div class="_1e0ca152 _026d7bff"><div><span>1 Kanal</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">Corner Plot Is Available For Sale</h2></div>
                        <div class="ee550b27">Corner One Kanal Plot Is Available For Sale. Situated In Gulshan Abad Sector 2. With All Basic... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 16 minutes ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-18978243" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-18978243">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/20471965-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/20471965-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/20471965-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/20471965-240x180.jpeg" class="_54945be7 lazy loaded"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li role="article" class="ef447dde">
                <article class="f0349ab4">
                    <div class="f74e80f3">
                        <a href="/Property/rawalpindi_gulshan_abad_the_good_location_plot_for_investment_purpose-20334908-1058-2.html" class="_7ac32433" title="The Good Location Plot For Investment Purpose">
                            <div class="_413d73d2" aria-label="Listing link"></div>
                        </a>
                    </div>
                    <div class="a71a1ca3">
                        <div class="_150e2e12">
                            <picture class="_219b7e0a">
                                <noscript>
                                    <source type="image/webp" srcSet=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg />
                                    <img role="presentation" src=https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&imageformat=jpeg class=_8f499ba9 alt=The Good Location Plot For Investment Purpose title=The Good Location Plot For Investment Purpose aria-label="Fallback listing photo" />
                                </noscript>
                                <source type="image/webp" data-srcset="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg"><img role="presentation" alt="The Good Location Plot For Investment Purpose" title="The Good Location Plot For Investment Purpose" aria-label="Listing photo" data-src="https://images.zameen.com/smap/400/300/33.530556/73.061264/image.jpeg?quality=80&amp;imageformat=jpeg" class="_8f499ba9 lazy loaded"></picture>
                        </div>
                        <div class="_6d2ea5a7">
                            <div class="_7b192d51"></div>
                            <div class="f59902d8">
                                <div class="f1475fe5"></div>
                            </div>
                            <div class="_7fe76c69">
                                <div class="_54799766">
                                    <div class="a091f4d0">
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="_5715b2fb" title="Map">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="_0f4cf282" viewBox="0 0 9 13">
                                                    <path fill="#FFF" d="M4.5 0C2 0 0 2 0 4.4 0 7.8 4.5 13 4.5 13S9 7.8 9 4.4C9 2 7 0 4.5 0zm0 6.3c-1.1 0-1.9-.9-1.9-1.9a2 2 0 0 1 1.9-1.9c1.1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <div class="_3d017150" aria-label="Share property">
                                                <button class="_5715b2fb" title="Share it">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="_2828459f" viewBox="0 0 13 12">
                                                        <path fill="#FFF" d="M9.7 2V0L13 3.3 9.7 6.6V4.3c-3.9.5-5.8 4.8-5.8 4.8 0-3.6 2.5-6.7 5.8-7.1zm1.2 9.2H.8V2.5H7a12 12 0 0 1 2-.8H.4c-.2 0-.4.2-.4.4v9.5c0 .3.2.4.4.4h10.9c.2 0 .4-.2.4-.4V5.2l-.8.8v5.2z"></path>
                                                    </svg>
                                                </button>
                                                <div>
                                                    <div style="display: none;">
                                                        <div class="_12e55ca6  ">
                                                            <ul class="_55bd3d93">
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.zameen.com/Property/rawalpindi_gulshan_abad_the_good_location_plot_for_investment_purpose-20334908-1058-2.html" title="Share on Facebook" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_52f0a589">
                                                                            <path fill="#3C5A99" d="M30.2 32c1 0 1.8-.8 1.8-1.8V1.7c0-1-.8-1.8-1.8-1.8H1.7C.7-.1-.1.7-.1 1.7v28.6c0 1 .8 1.8 1.8 1.8l28.5-.1z"></path>
                                                                            <path fill="#fff" d="M22.1 32V19.6h4.2l.6-4.8h-4.8v-3.1c0-1.4.4-2.4 2.4-2.4H27V5c-.4-.1-2-.2-3.7-.2-3.7 0-6.2 2.3-6.2 6.4v3.6h-4.2v4.8H17V32h5.1z"></path>
                                                                        </svg><span>Share on Facebook</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://twitter.com/intent/tweet?text=The%20Good%20Location%20Plot%20For%20Investment%20Purpose - url: https://www.zameen.com/Property/rawalpindi_gulshan_abad_the_good_location_plot_for_investment_purpose-20334908-1058-2.html" title="Share on Twitter" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="18339 5898.7 18.8 15.2">
                                                                            <path fill="#65bbf2" d="M18352.1 5898.7a4 4 0 0 0-3.9 4.1v.6c-3.9-.7-5.8-1.8-8-4-1.2 2.5.2 4.5 1.7 5.6a3.6 3.6 0 0 1-2.5-.7h-.1a6.1 6.1 0 0 0 4 4c-1.4 0-2.3.2-3.2-.4-.1 0-.2 0-.1.1.7 2 2.1 2.6 4.3 2.6a8.2 8.2 0 0 1-5.2 1.7.1.1 0 0 0 0 .2c1 .8 3.4 1.4 6.8 1.4 5.7 0 10.3-5 10.3-11.2v-.3a2.9 2.9 0 0 0 1.6-2l-2 .7v-.1a5 5 0 0 0 1.7-2.2l-2.5.9a.4.4 0 0 1-.3 0 7 7 0 0 0-2.6-1"></path>
                                                                        </svg><span>Share on Twitter</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6"></li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://wa.me/?text=The%20Good%20Location%20Plot%20For%20Investment%20Purpose%20https%3A%2F%2Fwww.zameen.com%2FProperty%2Frawalpindi_gulshan_abad_the_good_location_plot_for_investment_purpose-20334908-1058-2.html" title="Share on WhatsApp" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 28 28" class="_52f0a589 _5d315485">
                                                                            <path fill="#fff" d="M3 29l1.9-6.7a12.4 12.4 0 0 1-1.8-6.4C3.1 8.8 9.1 3 16.6 3a13.7 13.7 0 0 1 9.5 3.8 12.6 12.6 0 0 1 3.9 9.1 13.2 13.2 0 0 1-13.4 12.9 13 13 0 0 1-6.4-1.6z"></path>
                                                                            <path fill="#25d366" d="M10.5 24.9l.4.2a11.3 11.3 0 0 0 5.7 1.5 11 11 0 0 0 11.2-10.7A11 11 0 0 0 16.6 5.2 11 11 0 0 0 5.4 15.9a10.6 10.6 0 0 0 1.7 5.7l.3.4-1.1 4z"></path>
                                                                            <path fill="#fff" d="M13.6 9.6c-.3-.6-.6-.6-.8-.6h-.7a1.3 1.3 0 0 0-.9.4 3.9 3.9 0 0 0-1.2 2.9 6.9 6.9 0 0 0 1.4 3.6c.2.2 2.4 3.8 5.9 5.2 2.9 1.1 3.5.9 4.1.9a3.6 3.6 0 0 0 2.3-1.6 2.5 2.5 0 0 0 .2-1.6 3.2 3.2 0 0 0-.7-.4 25.6 25.6 0 0 0-2.4-1.1c-.3-.1-.5-.2-.8.2l-1.1 1.4c-.2.3-.4.3-.7.1a9.7 9.7 0 0 1-2.8-1.7 9.4 9.4 0 0 1-1.9-2.4c-.2-.3 0-.5.2-.7l.5-.6a1.1 1.1 0 0 0 .3-.6.6.6 0 0 0 0-.6 14.8 14.8 0 0 0-.9-2.8z"></path>
                                                                        </svg><span>Share on WhatsApp</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_blank" href="https://mail.google.com/mail/u/0/?view=cm&amp;fs=1&amp;to&amp;su=The%20Good%20Location%20Plot%20For%20Investment%20Purpose&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_the_good_location_plot_for_investment_purpose-20334908-1058-2.html&amp;ui=2&amp;tf=1" title="Send via GMail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#EAEAEA" d="M30 28H2a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h28a2 2 0 0 1 2 2v20a2 2 0 0 1-2 2z"></path>
                                                                            <path fill="#D54C3F" d="M30 4h-.5l-.2.1c-.3.1-.6.3-.8.6l-12.5 9L3.2 4.4H3l-.2-.2-.4-.2H2a2 2 0 0 0-2 2v20c0 1.1.9 2 2 2h2V9.9l12 8.8 12-8.8V28h2a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path>
                                                                        </svg><span>Send via GMail</span></a>
                                                                </li>
                                                                <li class="_6fdf20b6">
                                                                    <a target="_self" href="mailto:?subject=The Good Location Plot For Investment Purpose&amp;body=https://www.zameen.com/Property/rawalpindi_gulshan_abad_the_good_location_plot_for_investment_purpose-20334908-1058-2.html" title="Send via E-Mail" class="c6481bc9">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" class="_52f0a589" viewBox="0 0 32 32">
                                                                            <path fill="#444" d="M2.3 5.5C1 5.5 0 6.5 0 7.8v16.4c0 1.3 1 2.3 2.3 2.3h27.3c1.3 0 2.3-1 2.3-2.3V7.8c0-1.3-1-2.3-2.3-2.3H2.3zM4 9.4l.6.2 10.3 9.5c.6.6 1.5.6 2.2 0l10.3-9.5c.3-.3.8-.3 1.1 0 .3.3.3.8 0 1.1l-5.8 5.4 5.8 5.2c.4.3.4.8.1 1.1a1 1 0 0 1-1.2 0l-5.9-5.3-3.3 3.1a3.2 3.2 0 0 1-4.3 0l-3.3-3.1-5.9 5.3a1 1 0 0 1-1.2 0c-.3-.3-.2-.9.1-1.1l5.8-5.2-5.8-5.4a.8.8 0 0 1-.2-.8c0-.3.3-.5.6-.5z"></path>
                                                                        </svg><span>Send via E-Mail</span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b20d55ae _5e324ffe _664578bf">
                                            <button class="efdddcd4 ba3d1766" title="Add to favourites">
                                                <svg xmlns="http://www.w3.org/2000/svg" overflow="visible" viewBox="0 0 20 18.4" width="12" height="11" class="_455091c2">
                                                    <path d="M10 18.4L8.6 17C3.4 12.4 0 9.3 0 5.5A5.5 5.5 0 0 1 5.5 0 6 6 0 0 1 10 2a6 6 0 0 1 4.5-2A5.5 5.5 0 0 1 20 5.5c0 3.8-3.4 6.9-8.6 11.5z"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_1d4d62ed">
                        <div class="_7ac32433" title="The Good Location Plot For Investment Purpose">
                            <div class="_1e0ca152 _026d7bff">
                                <div>
                                    <div class="cd6d5974 _532a0352">
                                        <div class="c4fc20ba"><span class="c2cc9762" aria-label="Listing currency">PKR</span><span class="_14bafbc4"></span><span class="f343d9ce" aria-label="Listing price">26 Lakh</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="_1c5c76c7"></div>
                        <div class="fd9b3915">
                            <div class="_162e6469" aria-label="Listing location">Gulshan Abad, Rawalpindi</div>
                            <div class="_1da99a35">
                                <div class="_22b2f6ed"><span class="_4720d1a0 "><span class="_0c8a5353 d2db01cb"></span><span class="b6a29bc0" aria-label="Area"><div class="_7ac32433" title="The Good Location Plot For Investment Purpose"><div class="_1e0ca152 _026d7bff"><div><span>10 Marla</span></div>
                </div>
            </div>
            </span>
            </span>
                                </div>
                            </div>
                            <h2 class="c0df3811" aria-label="Listing title">The Good Location Plot For Investment Purpose</h2></div>
                        <div class="ee550b27">Plot Is Available For Sale. 10 Marla Residential Plot is available for Sale 35x72. Situated... more</div>
                        <div class="ae30f392">
                            <div class="_2b6db39e">
                                <div class="_08b01580"><span aria-label="Listing creation date" class="d77ff1d8">Added: 16 minutes ago</span><span aria-label="Listing updated date" class="d77ff1d8"></span></div>
                                <div class="ea88ac38">
                                    <button class="_5b77d672 da62f2ae _8d1154ff" type="button" aria-label="Call">Call</button>
                                    <button class="_85d9f2e2 a8197536 a8375d37" aria-label="Send email"> Email</button>
                                    <div class="_4fab07ae e8755457">
                                        <input type="checkbox" id="email-basket-checkbox-input-20334908" class="_86801f82">
                                        <label class="_9a281bec" for="email-basket-checkbox-input-20334908">EMAIL BASKET</label>
                                    </div>
                                </div>
                            </div>
                            <div class="_90188574">
                                <div class="f23f4390">
                                    <picture class="_219b7e0a">
                                        <noscript>
                                            <source type="image/webp" srcSet=https://media.zameen.com/thumbnails/20471965-240x180.webp />
                                            <img role="presentation" src=https://media.zameen.com/thumbnails/20471965-240x180.jpeg class=_54945be7 alt=t itle=a ria-label="Fallback listing photo" />
                                        </noscript>
                                        <source type="image/webp" data-srcset="https://media.zameen.com/thumbnails/20471965-240x180.webp"><img role="presentation" aria-label="Listing agency photo" data-src="https://media.zameen.com/thumbnails/20471965-240x180.jpeg" class="_54945be7 lazy loaded"></picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
            <li class="_71225ea8"></li>
        </ul>
        <div class="f6a5c5bb" role="navigation">
            <ul class="_92c36ba1">
                <li>
                    <div title="" class="d02376f9 _3ccbf7e4">1</div>
                </li>
                <li>
                    <a href="/Plots/Rawalpindi-41-2.html" title="Page 2" class="b7880daf">
                        <div title="Page 2" class="d02376f9 ">2</div>
                    </a>
                </li>
                <li>
                    <a href="/Plots/Rawalpindi-41-3.html" title="Page 3" class="b7880daf">
                        <div title="Page 3" class="d02376f9 ">3</div>
                    </a>
                </li>
                <li>
                    <a href="/Plots/Rawalpindi-41-4.html" title="Page 4" class="b7880daf">
                        <div title="Page 4" class="d02376f9 ">4</div>
                    </a>
                </li>
                <li>
                    <a href="/Plots/Rawalpindi-41-5.html" title="Page 5" class="b7880daf">
                        <div title="Page 5" class="d02376f9 ">5</div>
                    </a>
                </li>
                <li>
                    <a href="/Plots/Rawalpindi-41-6.html" title="Page 6" class="b7880daf">
                        <div title="Page 6" class="d02376f9 ">6</div>
                    </a>
                </li>
                <li>
                    <a href="/Plots/Rawalpindi-41-7.html" title="Page 7" class="b7880daf">
                        <div title="Page 7" class="d02376f9 ">7</div>
                    </a>
                </li>
                <li>
                    <a href="/Plots/Rawalpindi-41-8.html" title="Page 8" class="b7880daf">
                        <div title="Page 8" class="d02376f9 ">8</div>
                    </a>
                </li>
                <li>
                    <a href="/Plots/Rawalpindi-41-2.html" title="Next" class="b7880daf">
                        <div title="Next" class="d02376f9 ">
                            <svg xmlns="http://www.w3.org/2000/svg" class="b5193a6f" viewBox="0 0 32 32">
                                <g fill="#95989A">
                                    <path d="M3.18 30V1.9c0-1.1.9-2 2-1.9.4 0 .7.1 1 .3l21.8 14c.5.4.9 1 .9 1.6 0 .6-.4 1.2-.9 1.6L6.18 31.6c-.3.2-.7.3-1 .3-.5 0-1-.2-1.4-.6-.4-.3-.6-.8-.6-1.3z"></path>
                                    <path d="M3.18 30V1.9c0-1.1.9-2 2-1.9.4 0 .7.1 1 .3l21.8 14c.5.4.9 1 .9 1.6 0 .6-.4 1.2-.9 1.6L6.18 31.6c-.3.2-.7.3-1 .3-.5 0-1-.2-1.4-.6-.4-.3-.6-.8-.6-1.3z"></path>
                                </g>
                            </svg>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="ecb56a26">
        <div class="_60bb27db"><span class="_00b7d3b2">Be the first to hear about new properties</span>
            <button class="_8bb4a162">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_8b4015df">
                    <path d="M28.7 5.6H3.3C1.5 5.6 0 7 0 8.9v14.2c0 1.8 1.5 3.3 3.3 3.3h25.3c1.8 0 3.3-1.5 3.3-3.3V8.9c.1-1.9-1.4-3.3-3.2-3.3zm-6.8 9.7l8.5-7c.1.2.1.4.1.6v14.2c0 .2 0 .3-.1.5l-8.5-8.3zM28.7 7c.3 0 .5.1.8.2L16 18.2 2.6 7.2c.2-.1.5-.2.7-.2h25.4zM1.6 23.6c-.1-.2-.1-.3-.1-.5V8.9c0-.2 0-.4.1-.6l8.5 7-8.5 8.3zM3.3 25c-.3 0-.6-.1-.8-.2l8.8-8.5 4.7 3.9 4.7-3.9 8.8 8.5c-.3.1-.5.2-.8.2H3.3z"></path>
                </svg><span class="_9696fb26" aria-label="Alert properties button">Alert me of new properties</span></button>
        </div>
        <div class="_9d1238cf" aria-label="Side Banner List">
            <section class="_0ff73f1e">
                <a href="/ads/89/" rel="nofollow" target="_parent|_blank"><img data-src="/newsite/ads_server/v3/images/new_skin_img/4794_Pak_Associates.jpg" alt="ad" class="lazy loaded" src="/newsite/ads_server/v3/images/new_skin_img/4794_Pak_Associates.jpg" data-was-processed="true"></a>
            </section>
            <section class="_0ff73f1e">
                <a href="/ads/1201/" rel="nofollow" target="_parent|_blank"><img data-src="/newsite/ads_server/v3/images/new_skin_img/21334_Mazhar_Enterprises_Real_Estate@5d7f52fb9b36b.jpg" alt="ad" class="lazy loaded" src="/newsite/ads_server/v3/images/new_skin_img/21334_Mazhar_Enterprises_Real_Estate@5d7f52fb9b36b.jpg" data-was-processed="true"></a>
            </section>
        </div>
    </div>
</div>
