<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<section style="background-image: url('<?php echo Yii::$app->homeUrl;?>porto/img/login-bg.jpg');width: 100%;
   height: 100%;
   background-attachment: fixed;
   background-position: center center;
   background-size: cover;">
<section class="body-sign">
    <div class="center-sign">
        <a href="/" class="logo float-left" style="width:100%;text-align: center;">
            <img src="<?=Yii::$app->homeUrl ?>porto/img/logo.png" style="width:300px;height: 133px;"height="54" alt="Porto Admin" />
        </a>

        <div class="panel card-sign">
            <div class="card-title-sign mt-3 text-right">
                <h2 class="title text-uppercase font-weight-bold m-0"><i class="fas fa-user mr-1"></i> Sign In</h2>
            </div>
            <div class="card-body">
                <div class="site-login">
                    <!--<h1><?/*= Html::encode($this->title) */?></h1>-->


                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        // 'layout' => 'horizontal',
                        'fieldConfig' => [
                            'template' => "{label}{input}{error}",
                            'options' => ['class' => 'form-group mb-3'],
                            'labelOptions' => ['class' => 'control-label'],
                        ],
                        'errorCssClass' => 'has-danger'
                    ]); ?>

                    <?php
                    // Input group
                    echo $form->field($model, 'username', [
                        'inputTemplate' => '<div class="input-group">{input}<span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-user"></i>
										</span>
									</span></div>',
                    ])->textInput(['class' => 'form-control form-control-lg']);

                    ?>

                    <?php
                    // Input group
                    echo $form->field($model, 'password', [
                        'inputTemplate' => '<div class="input-group">{input}<span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-lock"></i>
										</span>
									</span></div>',
                    ])->passwordInput(['class' => 'form-control form-control-lg']);

                    ?>


                    <div class="row">
                        <div class="col-sm-8">
                            <?= $form->field($model, 'rememberMe')->checkbox([
                                'template' => "<div class=\"checkbox-custom checkbox-default\">{input} {label}</div>",
                            ]) ?>
                        </div>
                        <div class="col-sm-4 text-right">
                            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                        </div>
                    </div>




                    <!--<?/* $form->field($model, 'password')->passwordInput()*/?> !-->



                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>

        <p class="text-center  mt-3 mb-3" style="color:white">&copy; Copyright 2017. All Rights Reserved.</p>
    </div>
</section>
</section>

