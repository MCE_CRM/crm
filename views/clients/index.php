<?php

use app\models\User;
use kartik\date\DatePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clients';
$this->params['breadcrumbs'][] = $this->title;
?>

<style type="text/css">
    .isDisabled {
  color: currentColor;
  cursor: not-allowed;
  opacity: 0.5;
  text-decoration: none;
}

 .modal-open .modal {
    overflow-x: hidden;
     overflow-y:hidden !important;
    }
   
   .modal-body {
    max-height:400px !important;
    overflow-y:scroll;
   }


</style>

<div class="clients-index">

     <div class="card-body">
        <div class="text-right" style="width:50% !important;float:right">
            
            
            <button type="button" onclick="gridShow(this)" class="mb-1 mt-1 mr-1 btn btn-default"><i class="fas fa-box"></i> Grid View</button>
            <button type="button" onclick="calenderShow(this)" class="mb-1 mt-1 mr-1 btn btn-default"><i class="fas fa-calendar-alt"></i> Follow Up Calender</button>
        </div>

        <div class="text-left">
        <div class="follow-up-search">
      
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>
     



        <div class="form-group">
             <?= Html::label( 'Page Size', 'pagesize', array( 'style' => 'margin-left:10px; margin-top:8px;' ) ) ?>

            <?= Html::dropDownList(
    'pagesize', 
    ( isset($_GET['pagesize']) ? $_GET['pagesize'] : 20 ),  // set the default value for the dropdown list
    // set the key and value for the drop down list
    array( 
        20=>20,
        100 => 50, 
        200 => 100,
        500=>200,
        1000=>1000
    ),
    // add the HTML attritutes for the dropdown list
    // I add pagesize as an id of dropdown list. later on, I will add into the Gridview widget.
    // so when the form is submitted, I can get the $_POST value in InvoiceSearch model.
    array( 
        'id' => 'pagesize', 
        'style' => 'margin-left:5px; margin-top:8px;'
        )
    ) 
?>
          
        <!--    <select name="country">
               <option value="">Select Country</option>
               <option value="177" <?php if($_GET['country']==177){echo "selected";}?>>Pakistan</option>
               <option value="771"  <?php if($_GET['country']==771){echo "selected";}?>>International</option>
           </select >-->
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary mysearchbtn']) ?>
        
           
        </div>

        <?php ActiveForm::end(); ?>

    </div>

<!--    <h1><?/*= Html::encode($this->title) */?></h1>
-->    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   <!-- <p>
        <?/*= Html::a('Create Clients', ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->
<?php
     if(Yii::$app->user->can('clients/download'))
     {
        $show='';
     }
     else
     {
        $show=false;
     }
?>
   

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'toolbar' =>  [
            '{export}',
            '{toggleData}',
        ],
        /*'export' => [
            'fontAwesome' => true
        ],*/
        'export'=>$show,
        'exportConfig' => [
            GridView::CSV => ['label' => 'Save as CSV', 'icon' => 'file-excel'],
            GridView::EXCEL => ['label' => 'Save as EXCEL'],
            GridView::TEXT => ['label' => 'Save as TEXT'],
            GridView::PDF => ['label' => 'Save as PDF'],

        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
            'before' => Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New Client'), [
                'create'
            ],

             ['data-pjax' => 0,'onclick'=>'addnew(event,this)',
                'class' => 'btn btn-success btn-sm add-new'
            ]) ,

            'after' => Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                    'index'
                ], [
                    'class' => 'btn btn-primary btn-sm'
                ]),


            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            [
                'attribute'=>'Propety title',
                'value'=>'propert.property_title',
                'format'=>'raw',
                'filter'=> Select2::widget([

                    'name' => 'ClientsSearch[property_id]',
                    'options' => ['placeholder' => 'All ...'],
                    'model' => $searchModel,
                    'value' => $searchModel->propert->property_title,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['ajax/property-list']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],
                ]),
            
            ],
            'email:email',
            'phone',
            'mobile',
            [
                'attribute'=>'country',
                'value'=>'countr.country',
                'format'=>'raw',
                'width'=>'200px',
                'filter'=> Select2::widget([

                    'name' => 'ClientsSearch[country]',
                    'options' => ['placeholder' => 'All ...'],
                    'model' => $searchModel,
                    'value' => $searchModel->countr->country,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['ajax/country-list']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],
                ]),
            ],
            [
                'attribute'=>'state',
                'value'=>'stat.state',
                'format'=>'raw',
                'width'=>'200px',
                'filter'=> Select2::widget([

                    'name' => 'ClientsSearch[state]',
                    'options' => ['placeholder' => 'All ...'],
                    'model' => $searchModel,
                    'value' => $searchModel->stat->state,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['ajax/state-list']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],
                ]),
            ],
            [
                'attribute'=>'city',
                'value'=>'cit.city',
                'format'=>'raw',
                'width'=>'200px',
                'filter'=> Select2::widget([

                    'name' => 'ClientsSearch[city]',
                    'options' => ['placeholder' => 'All ...'],
                    'model' => $searchModel,
                    'value' => $searchModel->cit->city,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['ajax/city-list']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],
                ]),
            ],
            [
                'attribute'=>'show_to',

                'format'=>'raw',
                'value'=>function($model) {
                    if ((Yii::$app->user->can("clients/assign") && ($model->created_by == Yii::$app->user->id)) || Yii::$app->user->can("clients/assign-other")) {


                        if ($model->show_to) {
                            
                            $model->show_to = explode(',', $model->show_to);
                            $model->show_to=array_unique($model->show_to);
                            $e = '';

                            foreach ($model->show_to as $key => $data) {
                                $user = \app\models\User::findOne($data);

                                $e .= $user->username . ',<br>';
                            }

                            $e = substr_replace($e, "", -1);

                            return '<a href="JavaScript:Void(0);"  onclick="assign(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">' . $e . '</button>';

                        } else {
                            return '<a href="JavaScript:Void(0);"  onclick="assign(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">Not Set</button>';

                        }
                    }else {
                        if ($model->show_to) {
                           
                             $model->show_to = explode(',', $model->show_to);
                            $model->show_to=array_unique($model->show_to);
                            $e = '';
                            foreach ($model->show_to as $key => $data) {
                                $user = \app\models\User::findOne($data);

                                $e .= $user->username . ',<br>';
                            }

                            $e = substr_replace($e, "", -1);

                            return '<a href="JavaScript:Void(0);"   class="kv-editable-value kv-editable-link assign">' . $e . '</button>';

                        } else {
                            return '<a href="JavaScript:Void(0);"   class="kv-editable-value kv-editable-link assign">Not Set</button>';
                        }
                    }

                }
            ],

            //'zipcode',
            //'address',
            [
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' =>  'created_on',
                'value' => function($model){
                    return date("d/m/Y", strtotime($model->created_on));
                },
                'filterType'=>GridView::FILTER_DATE,
                'filterWidgetOptions'=> [
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy',
                    ],
                ],
                //'filter'=>false

            ],
            [
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'attribute' => 'created_by',
                'contentOptions' => ['class' => 'text-center'],
                'value'=>'user.username',
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',

            ],
            [

                //'attribute' =>  'created_on',
                'header'=>'Last Update',
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'format'=>'raw',
                'visible'=> \Yii::$app->user->can('clients/last-update'),
                'value' => function($model){
                    if($model->updated_by)
                    {
                        return date("d/m/Y h:i A", strtotime($model->updated_on)).'<br>'.\app\helpers\Helper::getUser($model->updated_by);

                    }else
                    {
                        return '';
                    }
                },
                //'filter'=>false

            ],
            /*[
                'headerOptions' => ['style' => 'width:5%','class' => 'text-center'],
                'attribute'=>'Type',
                'value'=>'LeadType'
            ],*/
            /*'LeadType',*/
              [
                    'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                    'attribute' => 'LeadType',
                    'contentOptions' => ['class' => 'text-center'],
                    'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                    'filter' =>array("Leads Client"=>"Leads Client","Mnually"=>"Mnually","NULL"=>"NULL"),
                    'filterWidgetOptions' => [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'All...'],
                    'format'=>'raw',

                ],
            //'updated_on',
            //'updated_by',

            [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{update} {delete} {activity} {addleads}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        if (\Yii::$app->user->can('clients/delete')) {
                            return Html::a('<span class="fas fa-trash"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-pjax' => '1',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                        'pjax' => 1,],
                                ]
                            );
                        }
                    },
                    'update' => function ($url, $model)

                    {
                        if(\Yii::$app->user->can('clients/update') || ($model->created_by == Yii::$app->user->id) ){
                            return '<a href="#"><span class="fa fa-pencil-alt"  onclick="updateRecord('.$model->id.',\'clients\',\'Update Client\',event)"></span></a>';

                        }else
                        {
                            return '';
                        }
                    } ,
                       'activity'=>function($url,$model)
                    {
                        
                         return '<a href="javascript:void(0)"  onclick="smshistory('.$model->id.',event);" title="sms history"><span class="fa fa-history"></span></a>';
                        
                    },
                    'addleads'=>function($url,$model)
                    {
                        $url = ''.Yii::$app->homeUrl.'leads/create/?name='.$model['name'].'&mobile='.$model['mobile'].'&email='.$model['email'].'&country='.$model['country'].'&city='.$model['city'].'&state='.$model['state'].'&address='.$model['address'].'&leadtype='.$model['LeadType'].'&project='.$model['project_id'].'&blocksector='.$model['blocksector'].'&property='.$model['property_id'];
                         return '<a href="'.$url.'">Add Leads</a>';

                       
                    }

                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
<script>


    function assign(event,id) {

        event.preventDefault();
        var url = '<?=Yii::$app->homeUrl?>ajax/assign-client?id='+id+'';

        var dialog = bootbox.dialog({
            title: 'Show Other',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',

        });

        dialog.init(function(){
            var request = $.ajax({
                url: url,
                method: "GET",
            });

            request.done(function( msg ) {
                dialog.find('.bootbox-body').html(msg);
            });

            $(document).on("submit", "#form", function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                $(this).submit(function() {
                    return false;
                });



                $form = $(this); //wrap this in jQuery

                var url = $form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#form").serialize(),
                    // serializes the form's elements.
                    success: function(data)
                    {
                        if(data==true)
                        {
                            $.pjax.defaults.timeout = 5000;
                            $.pjax.reload({container:'#p0'});
                            bootbox.hideAll();
                        }


                    }
                });
                // avoid to execute the actual submit of the form.

            });

        });

    };

   

   function smshistory(id,event)
   {
     event.preventDefault();

    var dialog = bootbox.dialog({
        title:'<h4 style="color:#85C0E7"><b>Client Sms History</b></h4>',
        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
        onEscape: function() {
            // you can do anything here you want when the user dismisses dialog
            $(".sp-palette-buttons-disabled").hide();
        }

    });

    dialog.init(function(){
        var request = $.ajax({
            url:'<?php echo Yii::$app->homeUrl;?>ajax/clientsmshistory',
            method: "GET",
            data:{id:id}
        });

        request.done(function( msg ) {
            dialog.find('.bootbox-body').html(msg);
        });


    });

   };

</script>
