<?php

use app\models\DefaultValueModule;
use kartik\widgets\DepDrop;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<style>

    .control-label{
        font-size: 0.9rem !important;
        color: black;
    }

</style>


<?php
$country = '';
if($model->isNewRecord)
{
    if($default_country = DefaultValueModule::getDefaultValueId('country'))
    {
        $c= \app\models\Country::findOne($default_country);
        $country = $c->country;
        $model->country = $default_country;


        $default_project= DefaultValueModule::getDefaultValueId('project_id');
    
        $p= \app\models\Projects::findOne($default_project);
        $project= $p->name;
        $model->project_id = $default_project;



    }
}
else
{
    $c= \app\models\Country::findOne($model->country);
    $country = $c->country;

    $s = \app\models\State::findOne($model->state);
    $state = $s->state;

    $ci = \app\models\City::findOne($model->city);
    $city = $ci->city;

}




?>

<div class="clients-form">

    <?php
    $form = ActiveForm::begin([
        'id' => 'form',
        'enableAjaxValidation' => true,
        'validationUrl' => ($model->isNewRecord) ?Yii::$app->homeUrl.'clients/validate':Yii::$app->homeUrl.'clients/validate?id='.$model->id.'',
        'errorCssClass' => 'has-danger',
    ]);
    ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?=

            $form->field($model, 'country')->widget(Select2::classname(), [
                'initValueText' => $country, // set the initial display text
                'options' => ['placeholder' => 'Search for a Country ...'],
                'theme' => Select2::THEME_BOOTSTRAP,
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 1,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => \yii\helpers\Url::to(['ajax/country-list']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                ],
            ]);



            ?>

        </div>
        <div class="col-md-6">
            <?php

            echo $form->field($model, 'state')->widget(DepDrop::classname(), [
                'options'=>['id'=>'subcat-id'],
                'data'=> [$model->state =>$state],
                'type'=>DepDrop::TYPE_SELECT2,
                'select2Options'=>['theme' => Select2::THEME_BOOTSTRAP,'pluginOptions'=>['allowClear'=>true]],

                'pluginOptions'=>[
                    'depends'=>['clients-country'],
                    'initDepends' => ['clients-country'],
                    'initialize'=>true,
                    'placeholder'=>'Select...',
                    'url'=>Url::to(['/ajax/get-country-state'])
                ]
            ]);

            ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?php

            echo $form->field($model, 'city')->widget(DepDrop::classname(), [
                'options'=>['id'=>'subcat2-id'],
                'data'=> [$model->city =>$city],

                'type'=>DepDrop::TYPE_SELECT2,
                'select2Options'=>['theme' => Select2::THEME_BOOTSTRAP,'pluginOptions'=>['allowClear'=>true]],


                'pluginOptions'=>[
                    'depends'=>['subcat-id'],

                    'placeholder'=>'Select...',
                    'url'=>Url::to(['/ajax/get-state-city'])
                ]
            ]);

            ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

        </div>
         <div class="col-lg-12 ">
                    <?php


                    // Normal select with ActiveForm & model
                    echo $form->field($model, 'project_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\Projects::find()->where(['active'=>1])->all(),'id','name'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['placeholder' => 'Select a Project ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
               /*   echo  $form->field($model, 'project_id')->widget(Select2::classname(), [
                //'initValueText' => $country, // set the initial display text
                'options' => ['placeholder' => 'Search for a Project...'],
                'theme' => Select2::THEME_BOOTSTRAP,
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 1,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => \yii\helpers\Url::to(['ajax/project-list']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                ],
            ]);*/




                    ?>

        </div>

        <div class="col-md-6">
                        <?=
                 $form->field($model, 'blocksector')->widget(DepDrop::classname(), [
                'data'=> [$model->state =>$state],
                'type'=>DepDrop::TYPE_SELECT2,
                'select2Options'=>['theme' => Select2::THEME_BOOTSTRAP,'pluginOptions'=>['allowClear'=>true]],

                'pluginOptions'=>[
                    'depends'=>['clients-project_id'],
                    'initDepends' => ['clients-project_id'],
                    'initialize'=>true,
                    'placeholder'=>'Select...',
                    'url'=>Url::to(['/ajax/get-country-sector'])
                ]
            ]);
                       /* $form->field($model, 'blocksector')->widget(Select2::classname(), [
                            //'initValueText' => $country, // set the initial display text
                            'options' => ['placeholder' => 'Search for a Block/sector ...'],
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                ],
                                'ajax' => [
                                    'url' => \yii\helpers\Url::to(['ajax/blocksector-list']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                            ],
                        ]);*/



                        ?>

                    </div>

         <div class="col-lg-6">
                    <?php

                    echo $form->field($model, 'property_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\Property::find()->where(['like', 'status', 'Available'])->all(),'id','property_title'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['placeholder' => 'Select a Property ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);


                    //$model = \app\models\ExtraCharges::find()->select(['id','name'])->where(['id'=>1])->andWhere(['id2'=>2])->count();

                    ?>

        </div>


    </div>
     
    
    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

    <?php //echo $form->field($model, 'created_on')->textInput() ?>

    <?php //echo $form->field($model, 'created_by')->textInput() ?>

    <?php //echo $form->field($model, 'updated_on')->textInput() ?>

    <?php //echo $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $('#clients-country').parents('.bootbox').removeAttr('tabindex');
    $('#subcat-id').parents('.bootbox').removeAttr('tabindex');
    $('#clients-city').parents('.bootbox').removeAttr('tabindex');
</script>