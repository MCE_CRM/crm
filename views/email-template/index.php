<?php

use app\models\User;
use kartik\date\DatePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmailTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Email Templates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-template-index">


    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'pjax' => true,

        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
            'before' => Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New Email Template'), [
                'create'
            ], ['data-pjax' => 0,
                'class' => 'btn btn-default btn-sm add-new'
            ]),
            'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                    'index'
                ], [
                    'class' => 'btn btn-primary btn-sm'
                ]),


            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'template_name',
            [
                'attribute' => 'template_name',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $widget)
                {
                    return '<a href="'.Yii::$app->homeUrl.'email-template/update?id=' . $model->id . '">' . $model->template_name . '</a>';
                }
            ],
            'template_subject:ntext',
            //'template_body:ntext',
            [
                'attribute' => 'template_body',
                'format' => 'raw'
            ],
            [
                'attribute'=>'active',
                'value'=>function($model)
                {
                    return statusLabel($model->active);
                },
                'format'=>'raw',
            ],
            [

                'attribute' =>  'created_on',
                'value' => function($model){
                    return date("d/m/Y", strtotime($model->created_on));
                },
                'filterType'=>GridView::FILTER_DATE,
                'filterWidgetOptions'=> [
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy',
                    ],
                ],
                //'filter'=>false

            ],
            [

                'attribute' => 'created_by',
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'value'=>function($model)
                {
                    return \app\helpers\Helper::getUser($model->created_by);
                },
                //'value'=>'user.username',
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',

            ],
            [

                //'attribute' =>  'created_on',
                'header'=>'Last Update',
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'format'=>'raw',
                'value' => function($model){
                    if($model->updated_by)
                    {
                        return date("d/m/Y", strtotime($model->updated_on)).'<br>'.\app\helpers\Helper::getUser($model->updated_by);

                    }else
                    {
                        return '';
                    }
                },
                //'filter'=>false

            ],

            [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{update}',
                'buttons' => [
                    'update' => function ($url, $model)

                    {
                        return '<a href="'.$url.'"><span class="fa fa-pencil-alt" ></span></a>';
                    } ,
                ],
            ],
        ],
    ]); Pjax::end(); ?>
</div>
