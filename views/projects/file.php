<?php
/**
 * Created by PhpStorm.
 * User: Multiline
 * Date: 9/4/2018
 * Time: 2:03 PM
 */


$filesModel = \app\models\Files::find()->where(['project_id'=>$model->id])->all();

?>
<style>

    .table-info, .table-info > th, .table-info > td{
        color:#000 !important;
        background-color: white !important;
    }




    .table > thead > tr > td.info, .table > tbody > tr > td.info, .table > tfoot > tr > td.info, .table > thead > tr > th.info, .table > tbody > tr > th.info, .table > tfoot > tr > th.info, .table > thead > tr.info > td, .table > tbody > tr.info > td, .table > tfoot > tr.info > td, .table > thead > tr.info > th, .table > tbody > tr.info > th, .table > tfoot > tr.info > th{
        color:#000 !important;
        background-color: white !important;
    }
</style>

<div class="row">
    <div class="col-md-1">

    </div>
    <div class="col-md-10">
        <table class="table table-responsive-lg table-bordered table-striped table-sm mb-0">
            <thead>
            <tr>
                <th>Sno</th>
                <th>FileName</th>
                <th>Title</th>
                <th>Type</th>
                <th>Size</th>
                <th>Added On</th>
                <th>Added By</th>
                <th class="text-right">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 0;
            foreach ($filesModel as $files)
            {
            $i++;

            ?>
            <tr>

                <td><?= $i?></td>
                <td><?= $files->file_name?></td>
                <td><?= $files->custom_name?></td>
                <td><?= $files->type?></td>
                <td><?= \app\helpers\Helper::getSize($files->size) ?></td>
                <td><?= \app\helpers\Helper::DateTime($files->created_on) ?></td>
                <td><?= \app\helpers\Helper::getCreated($files->created_by)?></td>
                <td class="actions">
                    <a href="<?= \app\helpers\Helper::getBaseUrl()?>files/project<?= $model->id?>/<?= $files->file_name?>" class="html5lightbox"><i class="fas fa-play"></i></a>

                    <a href="<?= \app\helpers\Helper::getBaseUrl()?>files/project<?= $model->id?>/<?= $files->file_name?>" download><i class="fas fa-download"></i></a>
                    <a href="javascript:void(0)" onclick="edit(<?= $files->id ?>,'<?= $files->custom_name ?>')" ><i class="fas fa-pencil-alt"></i></a>
                    <a href="javascript:void(0)" onclick="del(<?= $files->id ?>)"><i class="far fa-trash-alt"></i></a>
                </td>
            </tr>

            <?php } ?>

            </tbody>
        </table>

    </div>
    <div class="col-md-1">

    </div>
</div>

<script>

    function edit(id,custom ) {
        bootbox.prompt("Update", function(result){

            if(result !== custom)
            {
                $.ajax({
                    url: "<?= Yii::$app->homeUrl?>files/update",
                    type: "POST",
                    data: { name: result,id:id},
                    success: function(result){

                        if(result==true)
                        {
                            bootbox.hideAll();
                            $.pjax.reload({container:'#p0'});
                        }

                    }});

            }

        });

        $('.bootbox-input').val(custom);


    }

    function del(id) {

        bootbox.confirm({
            message: "Are You Sure Delete This?",
            buttons: {
                confirm: {
                    label: 'OK',
                    className: 'btn btn-primary'
                },
                cancel: {
                    label: 'Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if(result==true)
                {
                    $.ajax({
                        url: "<?= Yii::$app->homeUrl?>files/delete",
                        type: "POST",
                        data: {id:id},
                        success: function(result){

                            if(result==true)
                            {
                                bootbox.hideAll();
                                $.pjax.reload({container:'#p0'});
                            }

                        }});


                }
            }
        });


    }




</script>





