<?php
use kartik\editable\Editable;
use app\models\User;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use app\models\Blocksector;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;

$create = '';
if(\Yii::$app->user->can('projects/create')){
    $create = Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New Project'), [
        'create'
    ], ['data-pjax' => 0,
        'class' => 'btn btn-default btn-sm add-new'
    ]);
}

?>

    <div class="projects-index">

        <?php

        Pjax::begin(['timeout' => '30000']);

        
        ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'responsiveWrap' => false,
            'toolbar' =>  [
                //'{export}',
                '{toggleData}',
            ],
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
                'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
                'before'=>$create.' '. Html::a('<i class="fa fa-sync"></i> '.Yii::t ( 'app', 'Reset List' ), ['index'], ['class' => 'btn btn-info btn-sm']).' ',
                'showFooter' => false,

            ],

            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'name',
                [
                    'attribute'=>'blocksector',
                    'value'=>function($model)
                    {
                       
                       $blockscrtorname=Blocksector::find()->where(['project_id'=>$model->id])->all();
                        $e = '';
                            foreach ($blockscrtorname as  $data) {
                                

                                $e .= $data->blocksector .',<br>';


                            }

                           /* $e = substr_replace($e, "", -1);*/

                            return $e;
                
                           
                     },

                   
                    'format'=>'raw',
                    'width'=>'200px',
                    'filter'=> Select2::widget([

                        'name' => 'ProjectsSearch[blocksector]',
                        'options' => ['placeholder' => 'All ...'],
                        'model' => $searchModel,
                        'value' => $searchModel->countr->country,
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => \yii\helpers\Url::to(['ajax/blocksector-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ]),
                ],
                [
                    'attribute'=>'country',
                    'value'=>'countr.country',
                    'format'=>'raw',
                    'width'=>'200px',
                    'filter'=> Select2::widget([

                        'name' => 'ProjectsSearch[country]',
                        'options' => ['placeholder' => 'All ...'],
                        'model' => $searchModel,
                        'value' => $searchModel->countr->country,
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => \yii\helpers\Url::to(['ajax/country-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ]),
                ],
                [
                    'attribute'=>'state',
                    'value'=>'stat.state',
                    'format'=>'raw',
                    'width'=>'200px',
                    'filter'=> Select2::widget([

                        'name' => 'ProjectsSearch[state]',
                        'options' => ['placeholder' => 'All ...'],
                        'model' => $searchModel,
                        'value' => $searchModel->stat->state,
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => \yii\helpers\Url::to(['ajax/state-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ]),
                ],
                [
                    'attribute'=>'city',
                    'value'=>'cit.city',
                    'format'=>'raw',
                    'width'=>'200px',
                    'filter'=> Select2::widget([

                        'name' => 'ProjectsSearch[city]',
                        'options' => ['placeholder' => 'All ...'],
                        'model' => $searchModel,
                        'value' => $searchModel->cit->city,
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => \yii\helpers\Url::to(['ajax/city-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ]),
                ],
                //'city',
                'address',
				
				 [
					'attribute'=>'project_show',
	
					'format'=>'raw',
					'value'=>function($model) {
						if (Yii::$app->user->can("projects/show-other")) {
	
	
							if ($model->project_show) {
								
								$model->project_show = explode(',', $model->project_show);
								$model->project_show=array_unique($model->project_show);
								$e = '';
	
								foreach ($model->project_show as $key => $data) {
									$user = \app\models\User::findOne($data);
	
									$e .= $user->first_name . ',<br>';
								}
	
								$e = substr_replace($e, "", -1);
	
								return '<a href="JavaScript:Void(0);"  onclick="assign(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">' . $e . '</button>';
	
							} else {
								return '<a href="JavaScript:Void(0);"  onclick="assign(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">Not Set</button>';
	
							}
						}else {
							if ($model->project_show) {
							   
								 $model->project_show = explode(',', $model->project_show);
								$model->project_show=array_unique($model->project_show);
								$e = '';
								foreach ($model->project_show as $key => $data) {
									$user = \app\models\User::findOne($data);
									$e .= $user->first_name . ',<br>';
								}
	
								$e = substr_replace($e, "", -1);
	
								return '<a href="JavaScript:Void(0);"   class="kv-editable-value kv-editable-link assign">' . $e . '</button>';
	
							} else {
								return '<a href="JavaScript:Void(0);"   class="kv-editable-value kv-editable-link assign">Not Set</button>';
							}
						}
	
					}
            	],
				 [
					'attribute'=>'project_assign',
					 'visible'=> \Yii::$app->user->can('projects/assign-other'),
					'format'=>'raw',
					'value'=>function($model) {
						if (Yii::$app->user->can("projects/assign-other")) {
	
	
							if ($model->project_assign) {
								
								$model->project_assign = explode(',', $model->project_assign);
								$model->project_assign=array_unique($model->project_assign);
								$e = '';
	
								foreach ($model->project_assign as $key => $data) {
									$user = \app\models\User::findOne($data);
	
									$e .= $user->first_name . ',<br>';
								}
	
								$e = substr_replace($e, "", -1);
	
								return '<a href="JavaScript:Void(0);"  onclick="assignproject(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">' . $e . '</button>';
	
							} else {
								return '<a href="JavaScript:Void(0);"  onclick="assignproject(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">Not Set</button>';
	
							}
						}else {
							if ($model->project_assign) {
							   
								 $model->project_assign = explode(',', $model->project_assign);
								$model->project_assign=array_unique($model->project_assign);
								$e = '';
								foreach ($model->project_assign as $key => $data) {
									$user = \app\models\User::findOne($data);
									$e .= $user->first_name . ',<br>';
								}
	
								$e = substr_replace($e, "", -1);
	
								return '<a href="JavaScript:Void(0);"   class="kv-editable-value kv-editable-link assign">' . $e . '</button>';
	
							} else {
								return '<a href="JavaScript:Void(0);"   class="kv-editable-value kv-editable-link assign">Not Set</button>';
							}
						}
	
					}
            	],
                /*[
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'width' => '80px',
                    'header'=>'Files',
                    //'enableRowClick'=>true,
                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'detail' => function ($model, $key, $index, $column) {
                        return Yii::$app->controller->renderPartial('file', ['model' => $model]);
                    },
                   // 'expandOneOnly' => true,
                    'mergeHeader'=>false,
                ],*/
                /*[
                    'attribute'=>'active',
                    'width' => '125px',
                    'value'=>function($model)
                    {
                        return statusLabel($model->active);
                    },
                    'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                    'filter' => [
                        0 => 'Inactive',
                        1 => 'Active',
                    ],
                    'filterWidgetOptions' => [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'All...'],
                    'format'=>'raw',
                ],*/
				[
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'active',
				'visible' => Yii::$app->user->can('projects/project-status-active-inactive'),
                'label'=>'Active',
                 'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(\app\models\ProjectStatus::find()->all(), 'id', 'name'),
                    'filterWidgetOptions' => [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'All...'],
                    'format'=>'raw',
					'editableOptions'=> function ($model, $key, $index, $widget) {
                    $appttypes = ArrayHelper::map(\app\models\ProjectStatus::find()->all(), 'id', 'name');
                    return [
                        'header' => 'Active',
                        'attribute' => 'active',
                        'asPopover' => false,
                        'inlineSettings' => [
                            'closeButton' => '<button class="btn btn-sm btn-danger kv-editable-close kv-editable-submit" title="Cancel Edit"><i class="fa fa-times-circle"></i></button>'
                        ],

                        'type' => 'primary',
                        //'size'=> 'lg',
                        'size' => 'md',
                        'options' => ['class'=>'form-control', 'placeholder'=>'Enter Status...'],
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'displayValueConfig' => $appttypes,
                        'data' => $appttypes,
                        'formOptions'=> ['action' => ['/ajax/update-project-status']] // point to new action
                    ];
                },
					
            ],
                [
                    'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                    'contentOptions' => ['class' => 'text-center'],
                    'attribute' =>  'created_on',
                    'value' => function($model){
                        return date("d/m/Y", strtotime($model->created_on));
                    },
                    'filterType'=>GridView::FILTER_DATE,
                    'filterWidgetOptions'=> [
                        'type' => DatePicker::TYPE_INPUT,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd/mm/yyyy',
                        ],
                    ],
                    //'filter'=>false

                ],
                [
                    'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                    'attribute' => 'created_by',
                    'contentOptions' => ['class' => 'text-center'],
                    'value'=>'user.username',
                    'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                    'filterWidgetOptions' => [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'All...'],
                    'format'=>'raw',

                ],
                //'description:ntext',
                //'created_on',
                //'created_by',
                //'updated_on',
                //'updated_by',
                [

                    //'attribute' =>  'created_on',
                    'header'=>'Last Update',
                    'headerOptions' => ['style' => 'width:15%','class' => 'text-center'],
                    'contentOptions' => ['class' => 'text-center'],
                    'format'=>'raw',
                    'visible'=> \Yii::$app->user->can('projects/last-update'),
                    'value' => function($model){
                        if($model->updated_by)
                        {
                            return date("d/m/Y h:i A", strtotime($model->updated_on)).'<br>'.\app\helpers\Helper::getUser($model->updated_by);

                        }else
                        {
                            return '';
                        }
                    },
                    //'filter'=>false

                ],

                [
                    'class' => '\kartik\grid\ActionColumn',
                    'template'=>'{view} {update} {attach} {delete}',
                    'buttons' => [

                        'view' => function ($url, $model)

                        {
                            if(\Yii::$app->user->can('projects/view')){
                                return '&nbsp;<a href="'.$url.'" data-pjax="0"><span class="fa fa-eye" ></span></a>';

                            }else
                            {
                                return '';
                            }
                        } ,
                        'update' => function ($url, $model)

                        {
                            if(\Yii::$app->user->can('projects/update')){
                                return '<a href="'.$url.'"><span class="fa fa-pencil-alt" ></span></a>';

                            }else
                            {
                                return '';
                            }
                        } ,
                        'delete' => function ($url, $model, $key) {
                            if (\Yii::$app->user->can('projects/delete')) {
                                return Html::a('<span class="fas fa-trash"></span>', $url,
                                    [
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-pjax' => '1',
                                        'data' => [
                                            'method' => 'post',
                                            'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                            'pjax' => 1,],
                                    ]
                                );
                            }
                        },
                        'attach' => function ($url, $model)

                        {
                            if(\Yii::$app->user->can('projects/attach-files')){
                                return '<a href="javascript:void(0)" onclick="attachFiles('.$model->id.',event);"><span class="fas fa-paperclip mr-2 "></span></a>';

                            }else
                            {
                                return '';
                            }
                        } ,
                    ],
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>



<script>
	function assign(event,id) {

        event.preventDefault();
        var url = '<?=Yii::$app->homeUrl?>ajax/assign-projects?id='+id+'';

        var dialog = bootbox.dialog({
            title: 'Show Other',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',

        });

        dialog.init(function(){
            var request = $.ajax({
                url: url,
                method: "GET",
            });

            request.done(function( msg ) {
                dialog.find('.bootbox-body').html(msg);
            });

            $(document).on("submit", "#form", function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                $(this).submit(function() {
                    return false;
                });



                $form = $(this); //wrap this in jQuery

                var url = $form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#form").serialize(),
                    // serializes the form's elements.
                    success: function(data)
                    {
                        if(data==true)
                        {
                            $.pjax.defaults.timeout = 5000;
                            $.pjax.reload({container:'#p0'});
                            bootbox.hideAll();
                        }


                    }
                });
                // avoid to execute the actual submit of the form.

            });

        });

    };
	
	function assignproject(event,id) {

        event.preventDefault();
        var url = '<?=Yii::$app->homeUrl?>ajax/assign-property-projects?id='+id+'';

        var dialog = bootbox.dialog({
            title: 'Assign Other',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',

        });

        dialog.init(function(){
            var request = $.ajax({
                url: url,
                method: "GET",
            });

            request.done(function( msg ) {
                dialog.find('.bootbox-body').html(msg);
            });

            $(document).on("submit", "#form", function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                $(this).submit(function() {
                    return false;
                });



                $form = $(this); //wrap this in jQuery

                var url = $form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#form").serialize(),
                    // serializes the form's elements.
                    success: function(data)
                    {
                        if(data==true)
                        {
                            $.pjax.defaults.timeout = 5000;
                            $.pjax.reload({container:'#p0'});
                            bootbox.hideAll();
                        }


                    }
                });
                // avoid to execute the actual submit of the form.

            });

        });

    };
	
   function attachFiles(id,event) {
       event.preventDefault();
       var url = '<?= Yii::$app->homeUrl?>ajax/attach-files?type=project&id='+id;
       var dialog = bootbox.dialog({
           title: 'Attach Files',
           message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
           size: 'large',
           onEscape: function() {
               $.pjax.reload({container: '#p0', async: false});
           }

       });

       dialog.init(function(){
           var request = $.ajax({
               url: url,
               method: "GET",
           });

           request.done(function( msg ) {
               dialog.find('.bootbox-body').html(msg);
           });



       });

   }
   function showactivity(id,event) {
    event.preventDefault();

    var dialog = bootbox.dialog({
        title:'<h4 style="color:#85C0E7"><b>Property Activities</b></h4>',
        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
        onEscape: function() {
            // you can do anything here you want when the user dismisses dialog
            $(".sp-palette-buttons-disabled").hide();
        }

    });

    dialog.init(function(){
        var request = $.ajax({
            url:'<?php echo Yii::$app->homeUrl;?>ajax/getpropertyactivities',
            method: "GET",
            data:{id:id}
        });

        request.done(function( msg ) {
            dialog.find('.bootbox-body').html(msg);
        });

        
    });
};
</script>
<style>
.select2-selection
{
	overflow:auto;
}
</style>