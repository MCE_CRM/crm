
<?php

use app\models\User;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\web\JsExpression;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LeadStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */




$this->title = 'Cities';
$this->params['breadcrumbs'][] = $this->title;



?>
<div class="city-index">
    <?php Pjax::begin(); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'toolbar' =>  [
            //'{export}',
            '{toggleData}',
        ],

        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
            'before' => Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New City'), [
                'create'
            ], ['data-pjax' => 0,'onclick'=>'addnew(event,this)',
                'class' => 'btn btn-default btn-sm add-new'
            ]),
            'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                    'index'
                ], [
                    'class' => 'btn btn-primary btn-sm'
                ]),


            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            // 'id',
            'city',
            //'country_id',
            [
                'attribute'=>'state_id',
                'value'=>'state.state',
                'format'=>'raw',
                'filter'=> Select2::widget([

                    'name' => 'CitySearch[state_id]',
                    'options' => ['placeholder' => 'All ...'],
                    'model' => $searchModel,
                    'value' => $searchModel->state->state,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['state-list']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],
                ]),
            ],
            [
                'attribute'=>'country_id',
                'value'=>'county.country',
                'format'=>'raw',
                'filter'=> Select2::widget([

                    'name' => 'CitySearch[country_id]',
                    'options' => ['placeholder' => 'All ...'],
                    'model' => $searchModel,
                   'value' => $searchModel->county->country,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['country-list']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],
                ]),
            ],

            // 'state_code',

            [
                'attribute'=>'active',
                'width' => '125px',
                'value'=>function($model)
                {
                    return statusLabel($model->active);
                },
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => [
                    0 => 'Inactive',
                    1 => 'Active',
                ],
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',
            ],
            [
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' =>  'created_on',
                'value' => function($model){
                    return date("d/m/Y", strtotime($model->created_on));
                },
                'filterType'=>GridView::FILTER_DATE,
                'filterWidgetOptions'=> [
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy',
                    ],
                ],
                //'filter'=>false

            ],
            [
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'attribute' => 'created_by',
                'contentOptions' => ['class' => 'text-center'],
                'value'=>'user.username',
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',

            ],
            //'created_by',

           // 'created_on',
            //'created_by',
            //'updated_on',
            //'updated_by',
            //'country_id',

            [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{update}  {defaultValue}',
                'buttons' => [
                    'update' => function ($url, $model)

                    {
                        return '<a href="#"><span class="fa fa-pencil-alt"  onclick="updateRecord('.$model->id.',\'city\',\'Update City\',event)"></span></a>';
                    } ,
                    'defaultValue' => function ($url, $model) {
                        if(\app\models\DefaultValueModule::checkDefaultValue('city',$model->id)){
                            return Html::a('<span class="fa fa-eraser"></span>', Yii::$app->urlManager->createUrl(['city/index','del_id' => $model->id]), [
                                'title' => Yii::t('app', 'Delete Default'),
                            ]);
                        }else{
                            return Html::a('<span class="fa fa-tag"></span>', Yii::$app->urlManager->createUrl(['city/index','id' => $model->id]), [
                                'title' => Yii::t('app', 'Make Default'),
                            ]);
                        }
                    }

                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>


</div>


