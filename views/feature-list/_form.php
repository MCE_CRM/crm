<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FeatureList */
/* @var $form yii\widgets\ActiveForm */

$type  = ['text'=>'text','textarea'=>'textarea','checkbox'=>'checkbox','dropdown'=>'dropdown'];
$required  = ['required'=>'required','not'=>'not required'];

?>

<div class="feature-list-form">

    <?php
    $form = ActiveForm::begin([
        'id' => 'form',
        'enableAjaxValidation' => true,
        'validationUrl' => ($model->isNewRecord) ?Yii::$app->homeUrl.'feature-list/validate':Yii::$app->homeUrl.'feature-list/validate?id='.$model->id.'',
        'errorCssClass' => 'has-danger',
    ]);
    ?>


    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'feature_form_id')->dropDownList(ArrayHelper::map(\app\models\FeatureForm::find()->all(), 'id', 'name'),
        ['prompt' => '']) ?>

    <?= $form->field($model, 'type')->dropDownList($type,
        ['prompt' => '']) ?>
    <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'required')->dropDownList($required,
        ['prompt' => '']) ?>




    <?php //echo $form->field($model, 'created_on')->textInput() ?>

    <?php //echo $form->field($model, 'created_by')->textInput() ?>

    <?php //echo $form->field($model, 'updated_on')->textInput() ?>

    <?php //echo $form->field($model, 'updated_by')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
