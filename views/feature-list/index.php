<?php

use app\models\User;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FeatureListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Feature Lists';
$this->params['breadcrumbs'][] = $this->title;


$create = '';
$ordering = '';
if(\Yii::$app->user->can('feature-list/create')){
    $create = Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New Feature'), [
        'create'
    ], ['data-pjax' => 0,'onclick'=>'addnew(event,this)',
        'class' => 'btn btn-default btn-sm add-new'
    ]);
}





?>

<div class="feature-list-index">

    <?php

    Pjax::begin(['timeout' => '30000']);

    ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'toolbar' =>  [
            //'{export}',
            '{toggleData}',
        ],
        'export' => [
            'fontAwesome' => true
        ],
        /*'exportConfig' => [
            GridView::CSV => ['label' => 'Save as CSV', 'icon' => 'file-excel'],
            GridView::EXCEL => ['label' => 'Save as EXCEL'],
            GridView::TEXT => ['label' => 'Save as TEXT'],
            GridView::PDF => ['label' => 'Save as PDF'],

        ],*/
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
            'before'=>$create.' '.$ordering.' '.Html::a('<i class="fa fa-sync"></i> '.Yii::t ( 'app', 'Reset List' ), ['index'], ['class' => 'btn btn-info btn-sm']).' ',
            
            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'name',
            [
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'attribute' => 'type',
                'contentOptions' => ['class' => 'text-center'],
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => ['text'=>'text','textarea'=>'textarea','checkbox'=>'checkbox','dropdown'=>'dropdown',],
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',

            ],
            [
                'attribute'=>'feature_form_id',
                'value'=>'form.name',
                
            ],
            'value',
            [
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'attribute' => 'required',
                'contentOptions' => ['class' => 'text-center'],
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => ["required"=>"required","not"=>"not"],
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',

            ],
            //'sort_order',
            [
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' =>  'created_on',
                'value' => function($model){
                    return date("d/m/Y", strtotime($model->created_on));
                },
                'filterType'=>GridView::FILTER_DATE,
                'filterWidgetOptions'=> [
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy',
                    ],
                ],
                //'filter'=>false

            ],
            [
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'attribute' => 'created_by',
                'contentOptions' => ['class' => 'text-center'],
                'value'=>'user.username',
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',

            ],
            [

                //'attribute' =>  'created_on',
                'header'=>'Last Update',
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'format'=>'raw',
                'visible'=> \Yii::$app->user->can('feature-list/last-update'),
                'value' => function($model){
                    if($model->updated_by)
                    {
                        return date("d/m/Y h:i A", strtotime($model->updated_on)).'<br>'.\app\helpers\Helper::getUser($model->updated_by);

                    }else
                    {
                        return '';
                    }
                },
                //'filter'=>false

            ],
            //'created_on',
            //'created_by',
            //'updated_on',
            //'updated_by',

            [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{update} {delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        if (\Yii::$app->user->can('feature-list/delete')) {
                            return Html::a('<span class="fas fa-trash"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-pjax' => '1',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                        'pjax' => 1,],
                                ]
                            );
                        }
                    },
                    'update' => function ($url, $model)

                    {
                        if(\Yii::$app->user->can('feature-list/update')){
                            return '<a href="#"><span class="fa fa-pencil-alt"  onclick="updateRecord('.$model->id.',\'feature-list\',\'Update Feature\',event)"></span></a>';

                        }else
                        {
                            return '';
                        }
                    } ,

                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
