<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PropertyPurpose */

$this->title = 'Update Property Purpose: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Property Purposes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="property-purpose-update">
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
