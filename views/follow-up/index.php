<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FollowUpSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Follow Ups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="follow-up-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Follow Up', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

  
</div>
