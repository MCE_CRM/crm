<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FollowUp */

$this->title = 'Create Follow Up';
$this->params['breadcrumbs'][] = ['label' => 'Follow Ups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="follow-up-create">
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
