<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FollowUp */

$this->title = 'Update Follow Up: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Follow Ups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="follow-up-update">
    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
