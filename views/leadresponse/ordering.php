<?php
/**
 * Created by PhpStorm.
 * User: Multiline
 * Date: 9/26/2018
 * Time: 2:13 PM
 */


use kartik\sortable\Sortable;
use kartik\sortinput\SortableInput;

$this->title = 'Lead Response Sorting';
$dta = array();
foreach ($data as $item)
{
    $resp['content'] = '<i class="fas fa-arrows-alt"></i> '.$item->response;
    $dta[$item->id] = $resp;

}

?>

<div class="content-boody">

    <div class="row">
        <div class="col">
            <section class="card">

                <div class="card-body">

                    <?php


                    echo SortableInput::widget([
                        'name'=> 'sort_list_1',
                        'items' => $dta,
                        'hideInput' => true,
                        'pluginEvents' => [
                            'change' => 'function() { ajaxUpdate(); }',
                        ]
                    ]);



                    ?>

                </div>

                <footer class="card-header text-center">

                    <a href="<?= Yii::$app->homeUrl?>leadresponse/index" type="button" class="mb-1 mt-1 mr-1 btn btn-primary">GO Back</a>

                </footer>

            </section>
        </div>
    </div>


    <script>

        function ajaxUpdate() {


            var url = '<?= Yii::$app->homeUrl?>leadresponse/ordering';

            var getData = $('#w0').val();

            console.log(getData);

            $.ajax({
                type: "POST",
                url: url,
                data: {
                    data: getData
                },
                // serializes the form's elements.
                success: function(data)
                {
                    if(data==true)
                    {
                        console.log("Updated");
                    }
                    else
                    {
                        //toastr.success('', 'Some Error Occur', {timeOut: 2000});
                    }


                }
            });


            console.log("Hello");

        }
    </script>
