<?php

use kartik\widgets\Select2;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LeadStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */




$this->title = 'Countries';
$this->params['breadcrumbs'][] = $this->title;

$create = '';
if(\Yii::$app->user->can('country/create')){
    $create = Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New Country'), [
        'create'
    ], ['data-pjax' => 0,'onclick'=>'addnew(event,this)',
        'class' => 'btn btn-default btn-sm add-new'
    ]);
}



?>
<div class="country-index">
    <?php


    Pjax::begin(['timeout' => '30000']);


    ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'toolbar' =>  [
            //'{export}',
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
            'before'=>$create.' '.$ordering.' '.Html::a('<i class="fa fa-sync"></i> '.Yii::t ( 'app', 'Reset List' ), ['index'], ['class' => 'btn btn-info btn-sm']).' ',

            'showFooter' => false,

        ],


        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'country',
            'country_code',
            [
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute'=>'active',
                'value'=>function($model)
                {
                    return statusLabel($model->active);
                },
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => [
                    0 => 'Inactive',
                    1 => 'Active',
                ],
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',
            ],
            //'sort_order',
            //'created_on',
            //'created_by',
            //'updated_on',
            //'updated_by',

            [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{update}  {defaultValue}',
                'buttons' => [
                    'update' => function ($url, $model)
                    {
                        return '<a href="#"><span class="fa fa-pencil-alt"  onclick="updateRecord('.$model->id.',\'country\',\'Update Country\',event)"></span></a>';
                    } ,
                    'defaultValue' => function ($url, $model) {
                        if(\app\models\DefaultValueModule::checkDefaultValue('country',$model->id)){
                            return Html::a('<span class="fa fa-eraser"></span>', Yii::$app->urlManager->createUrl(['country/index','del_id' => $model->id]), [
                                'title' => Yii::t('app', 'Delete Default'),
                            ]);
                        }else{
                            return Html::a('<span class="fa fa-tag"></span>', Yii::$app->urlManager->createUrl(['country/index','id' => $model->id]), [
                                'title' => Yii::t('app', 'Make Default'),
                            ]);
                        }
                    }

                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>


</div>
