<?php

use app\helpers\Helper;
use app\models\DefaultValueModule;
use dosamigos\fileupload\FileUploadUI;
use kartik\widgets\DepDrop;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Property */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .required{
        display: flex !important;
    }
    .control-label{
        font-size: 0.9rem !important;
        color: black;
    }
</style>

<?php
if($model->isNewRecord)
{
    if($default = DefaultValueModule::getDefaultValueId('property-purpose'));
    {

        $status = \app\models\PropertyPurpose::findOne($default);
        $model->purpose = $status->name;
    }

    if($default = DefaultValueModule::getDefaultValueId('property-category'));
    {

        $status = \app\models\PropertyCategory::findOne($default);
        $model->property_type = $status->name;
    }
	if($default = DefaultValueModule::getDefaultValueId('property-location-type'));
    {

        $status = \app\models\PropertyLocationType::findOne($default);
        $model->property_location_type = $status->name;
    }
}

$country = '';

Yii::$app->user->identity->id;

if($model->isNewRecord)
{
    if($default_country = DefaultValueModule::getDefaultValueId('country'))
    {
        $c= \app\models\Country::findOne($default_country);
        $country = $c->country;
        $model->country = $default_country;
    }
}
else
{
    $c= \app\models\Country::findOne($model->country);
    $country = $c->country;

    $s = \app\models\State::findOne($model->state);
    $state = $s->state;

    $ci = \app\models\City::findOne($model->city);
    $city = $ci->city;

}


?>

<section class="card">
    
    <div class="card-body">

        <div class="property-form">

            <?php

            $form = ActiveForm::begin([
                'id' => 'form2',
                'enableAjaxValidation' => true,
                'validationUrl' => Yii::$app->homeUrl.'property/validate',
                'fieldConfig' => [
                    'template' => "{label}<div class=\"col-lg-6\">{input}<label class=\"error\">{error}</label></div>",
                    'options' => ['class' => 'form-group row'],
                    'labelOptions' => ['class' => 'col-lg-3 control-label text-lg-right pt-2'],
                ],
                'options' => [
                    'class' => 'form-horizontal form-bordered'
                ],
                'errorCssClass' => 'has-danger',
            ]);
            ?>	
			<?=$form->field($model, 'project_id')->widget(Select2::classname(), [
					'data' => ArrayHelper::map(\app\models\Projects::find()->andWhere(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,project_assign)'))->addParams([':cat_to_find' => Yii::$app->user->id])->orWhere(['=','project_assign', ''])->all(),'id','name'),
					'theme' => Select2::THEME_BOOTSTRAP,
					'options' => ['class'=>'project-id','placeholder' => 'Select a Project ...'],
					'pluginOptions' => [
						'allowClear' => true
					],
				]);?>
		



            <?=
            $form->field($model, 'blocksector')->widget(DepDrop::classname(), [
                'data'=> [$model->blocksector =>$bl],
                'type'=>DepDrop::TYPE_SELECT2,
                'select2Options'=>['theme' => Select2::THEME_BOOTSTRAP,'pluginOptions'=>['allowClear'=>true]],

                'pluginOptions'=>[
                    'depends'=>['property-project_id'],
                    'initDepends' => ['property-project_id'],
                    'initialize'=>true,
                    'placeholder'=>'Select...',
                    'url'=>Url::to(['/ajax/get-country-sector'])
                ],
                'pluginEvents' => [
                    "change" => 'function(data) { 
                         
                            var data_id = $(this).find(\':selected\').text();
                           
                           var proj =  $(".project-id").find(\':selected\').text();
                           
                           if(data_id == "Select..."){
                           
                           }
                           else{
                                document.getElementById("property-property_title").value = proj+" "+data_id;
                           }
                            //alert(data_id );
                            
                         }',

                ],
            ]);

            ?>

            <?=
            $form->field($model, 'purpose')
                ->radioList(
                    Helper::getPropertyPurpose(),
                    [
                        'item' => function($index, $label, $name, $checked, $value) {

                            if($checked ==1)
                            {
                                $checked = 'checked';
                            }



                                $return = '<div class="radio-custom checkbox-inline radio-primary">';
                                $return .= '<input type="radio" name="' . $name . '" value="' . $label . '" '.$checked.'>';
                                $return .= '<label>' . ucwords($label) . '</label>';
                                $return .= '</div>';

                                return $return;



                        }
                    ]
                )
            ;
            ?>

            <?= $form->field($model, 'property_type')->dropDownList(Helper::getPropertyType(),
                ['prompt' => 'Select Type']) ?>

			<?= $form->field($model, 'property_location_type')->dropDownList(Helper::getPropertyLocationType(),
                ['prompt' => 'Select Location Type']) ?>
                
                <?= $form->field($model, 'member_ship_no')->textInput(['maxlength' => true])->label('Plot/Membership/House#') ?>
			

            <div class="row">

                <div class="col-md-2"></div>


                    <div class="col-md-4">
                        <?= $form->field($model, 'width')->textInput(['maxlength' => true]) ?>
                  
                    </div>
                     <div class="col-md-4">
                        <?= $form->field($model, 'length')->textInput(['maxlength' => true]) ?>

                    </div>

                <div class="col-md-2"></div>
            </div>

            <br>
                  
                    
                    <?= $form->field($model, 'land_area')->textInput( ['oninput'=>'land_area(event)','class'=>'form-control float']) ?>
                    <?= $form->field($model, 'land_area_unit')->dropDownList(Helper::getPropertyUnit(),
                        ['prompt' => 'Select Unit','oninput'=>'land_area_unit(event)']) ?>


                    <?php

                   echo  $form->field($model, 'price_per', [
                        'template' => "{label}\n<div class='col-md-6'>{input}</div>\n{hint}\n{error}",
                        'labelOptions' => [ 'class' => 'col-lg-3 control-label text-lg-right pt-2 price_per' ]
                    ])->textInput(['maxlength' => true,'disabled' => true,'oninput'=>'basePrice(event)','class'=>'form-control digit']);

                    echo $form->field($model, 'price')->textInput(['disabled' => true,'maxlength' => true,'oninput'=>'basePriceToPricePer()','class'=>'form-control digit']);

                    ?>
			<?= $form->field($model, 'demand_price')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'property_title')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'property_assign')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\User::find()->all(),'id','username'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['multiple' => true,'placeholder' => 'Select a Source ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
            <?= $form->field($model, 'description')->textarea() ?>

            <div class="row">
                <div class="col-md-2">

                </div>
                <div class="col-md-8">
                   <?=
					$form->field($model, 'country')->widget(Select2::classname(), [
                        'initValueText' => $country, // set the initial display text
                        'options' => ['placeholder' => 'Search for a Country ...'],
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => \yii\helpers\Url::to(['ajax/country-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ]);
					?>
                    <?php

                    echo $form->field($model, 'state')->widget(Select2::classname(), [
                        'initValueText' => $country, // set the initial display text
                        'options' => ['placeholder' => 'Search for a State ...'],
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => \yii\helpers\Url::to(['ajax/state-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ]);

                    ?>
                    <?php

                    echo
                    $form->field($model, 'city')->widget(Select2::classname(), [
                        'initValueText' => $country, // set the initial display text
                        'options' => ['placeholder' => 'Search for a City ...'],
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => \yii\helpers\Url::to(['ajax/city-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ]);

                    ?>
                    <?= $form->field($model, 'address')->textarea() ?>

                    

                </div>
                <div class="col-md-2">

                </div>

            </div>






    <footer class="card-header text-center">

        <?= Html::submitButton('Save', ['class' => 'mb-1 mt-1 mr-1 btn btn-primary']) ?>


    </footer>


        </div>
    </div>

    <?php ActiveForm::end(); ?>

</section>

<script>

    var grand_total = 0;
    var total_price = 0;

    function land_area(event) {

        if($('#property-land_area').val() !=='' && $('#property-land_area_unit').val() !=='' )
        {

            $('#property-price_per').prop("disabled", false);
            $('#property-price').prop("disabled", false);
            basePrice();
        }else
        {
            $('#property-price_per').prop("disabled", true);
            $('#property-price_per').val('');
            $('#property-price').prop("disabled", true);
            $('#property-price').val('');

        }


    }

    function land_area_unit(event) {

        if($('#property-land_area').val() !=='' && $('#property-land_area_unit').val() !=='' )
        {

            $('#property-price_per').prop("disabled", false);
            $('#property-price').prop("disabled", false);

        }else
        {
            $('#property-price_per').prop("disabled", true);
            $('#property-price_per').val('');

            $('#property-price').prop("disabled", true);
            $('#property-price').val('');

        }



        $('.price_per').text("Price Per "+ $("#property-land_area_unit option:selected").text());
    }


    function basePrice(event) {

        var land_area  = $('#property-land_area').val();
        var price_per =  $('#property-price_per').val();




        var total = land_area * price_per;


        total_price = total;



        $('#property-price').val(total);
        grandTotal();


    }



    function basePriceToPricePer(event) {

        var basePrice  = $('#property-price').val();


        var land_area = $('#property-land_area').val();

        var price_per = basePrice/ land_area ;


        price_per = Math.round(price_per);

        console.log(price_per);

        total_price = price_per;

        $('#property-price_per').val(parseInt(price_per));
        grandTotal();

    }


    $(".extra-charges-calculate").on("input", function(e) {

        grandTotal();

    });

    function grandTotal() {

        var base_total = $('#property-price').val();
        var other = 0;


        $(".extra-charges-calculate").each(function() {

            if($(this).val() !== '')
            {
                if ($(this).val().indexOf('%') > -1){

                    var val = parseInt($(this).val());

                    var findpercent = (base_total * val)/100;
                    other = Math.round(findpercent) + other;

                }else
                {
                    other = parseInt($(this).val()) + other;

                }
            }

        });


        var grand_to = parseInt(base_total) + parseInt(other);
        $('#grand_total').text("Total Price : "+ grand_to);

    }



    //called when key is pressed in textbox
    $(".digit").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message

            return false;
        }
    });


    $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {


        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 37 || event.keyCode == 39 ) {
            if($('.allownumericwithdecimal').val().indexOf('.') == -1)
                return true;
            else
                return false;
        }
        else if ( key < 48 || key > 57 ) {
            return false;
        }
        else return true;

    });


<?php

    if($model->id)
    {?>
       grandTotal();
    <?php }
?>




    $("#property-blocksector").change(function () {
		var project = $("#property-project_id option:selected" ).text();
		var block = $( "#property-blocksector option:selected" ).text();
		var location = $( "#property-property_location_type option:selected" ).text();
		var plot_no = $( "#property-member_ship_no" ).val();
		var width = $("#property-width").val();
		var lenght = $("#property-length").val();
		var price = $("#property-price").val();
		if(project =='Select a Project ...')
		{
			project ='';
		}
		if(block =='Select...')
		{
			block ='';
		}
		if(location =='Select Location Type')
		{
			location ='';
		}
		if(plot_no =='')
		{
			plot_no ='';
		}
		if(width =='')
		{
			width ='';
		}else
		{
			width = " Size:"+width
		}
		if(lenght =='')
		{
			lenght ='';
		}else
		{
			lenght =  "X"+lenght;
		}
		if(price =='')
		{
			price ='';
		}
		document.getElementById("property-property_title").value = project+" "+block+" "+location+" "+plot_no+width+lenght+" "+price;
	});
	
	
	$("#property-project_id").change(function () {
		setTimeout(function(){ }, 5000);
        var val = this.value;
        if(val=='')
        {
            $('.field-property-country').show();
            $('.field-subcat-id').show();
            $('.field-subcat2-id').show();
            $('.field-property-address').show();

        }else {

            $('.field-property-country').hide();
            $('.field-subcat-id').hide();
            $('.field-subcat2-id').hide();
            $('.field-property-address').hide();


        }
		////////////////////////ubaid///////////////
		
		var project = $("#property-project_id option:selected" ).text();
		var block = $( "#property-blocksector option:selected" ).text();
		var location = $( "#property-property_location_type option:selected" ).text();
		var plot_no = $( "#property-member_ship_no" ).val();
		var width = $("#property-width").val();
		var lenght = $("#property-length").val();
		var price = $("#property-price").val();
		if(project =='Select a Project ...')
		{
			project ='';
		}
		if(block =='Select...')
		{
			block ='';
		}
		if(location =='Select Location Type')
		{
			location ='';
		}
		if(plot_no =='')
		{
			plot_no ='';
		}
		if(width =='')
		{
			width ='';
		}else
		{
			width = " Size:"+width
		}
		if(lenght =='')
		{
			lenght ='';
		}else
		{
			lenght =  "X"+lenght;
		}
		if(price =='')
		{
			price ='';
		}
		document.getElementById("property-property_title").value = project+" "+block+" "+location+" "+plot_no+width+lenght+" "+price;
	

    });
	
    $('#property-country').parents('.bootbox').removeAttr('tabindex');
    $('#property-city').parents('.bootbox').removeAttr('tabindex');
    $('#property-state').parents('.bootbox').removeAttr('tabindex');
	
	
	$("#property-property_location_type").change(function () {
		var project = $("#property-project_id option:selected" ).text();
		var block = $( "#property-blocksector option:selected" ).text();
		var location = $( "#property-property_location_type option:selected" ).text();
		var plot_no = $( "#property-member_ship_no" ).val();
		var width = $("#property-width").val();
		var lenght = $("#property-length").val();
		var price = $("#property-price").val();
		if(project =='Select a Project ...')
		{
			project ='';
		}
		if(block =='Select...')
		{
			block ='';
		}
		if(location =='Select Location Type')
		{
			location ='';
		}
		if(plot_no =='')
		{
			plot_no ='';
		}
		if(width =='')
		{
			width ='';
		}else
		{
			width = " Size:"+width
		}
		if(lenght =='')
		{
			lenght ='';
		}else
		{
			lenght =  "X"+lenght;
		}
		if(price =='')
		{
			price ='';
		}
		document.getElementById("property-property_title").value = project+" "+block+" "+location+" "+plot_no+width+lenght+" "+price;
	});
	
	$("#property-member_ship_no").change(function () {
		var project = $("#property-project_id option:selected" ).text();
		var block = $( "#property-blocksector option:selected" ).text();
		var location = $( "#property-property_location_type option:selected" ).text();
		var plot_no = $( "#property-member_ship_no" ).val();
		var width = $("#property-width").val();
		var lenght = $("#property-length").val();
		var price = $("#property-price").val();
		if(project =='Select a Project ...')
		{
			project ='';
		}
		if(block =='Select...')
		{
			block ='';
		}
		if(location =='Select Location Type')
		{
			location ='';
		}
		if(plot_no =='')
		{
			plot_no ='';
		}
		if(width =='')
		{
			width ='';
		}else
		{
			width = " Size:"+width
		}
		if(lenght =='')
		{
			lenght ='';
		}else
		{
			lenght =  "X"+lenght;
		}
		if(price =='')
		{
			price ='';
		}
		document.getElementById("property-property_title").value = project+" "+block+" "+location+" "+plot_no+width+lenght+" "+price;
	});
	
	$("#property-width").change(function () {
		var project = $("#property-project_id option:selected" ).text();
		var block = $( "#property-blocksector option:selected" ).text();
		var location = $( "#property-property_location_type option:selected" ).text();
		var plot_no = $( "#property-member_ship_no" ).val();
		var width = $("#property-width").val();
		var lenght = $("#property-length").val();
		var price = $("#property-price").val();
		if(project =='Select a Project ...')
		{
			project ='';
		}
		if(block =='Select...')
		{
			block ='';
		}
		if(location =='Select Location Type')
		{
			location ='';
		}
		if(plot_no =='')
		{
			plot_no ='';
		}
		if(width =='')
		{
			width ='';
		}else
		{
			width = " Size:"+width
		}
		if(lenght =='')
		{
			lenght ='';
		}else
		{
			lenght =  "X"+lenght;
		}
		if(price =='')
		{
			price ='';
		}
		document.getElementById("property-property_title").value = project+" "+block+" "+location+" "+plot_no+width+lenght+" "+price;
	});
	
	$("#property-length").change(function () {
		var project = $("#property-project_id option:selected" ).text();
		var block = $( "#property-blocksector option:selected" ).text();
		var location = $( "#property-property_location_type option:selected" ).text();
		var plot_no = $( "#property-member_ship_no" ).val();
		var width = $("#property-width").val();
		var lenght = $("#property-length").val();
		var price = $("#property-price").val();
		if(project =='Select a Project ...')
		{
			project ='';
		}
		if(block =='Select...')
		{
			block ='';
		}
		if(location =='Select Location Type')
		{
			location ='';
		}
		if(plot_no =='')
		{
			plot_no ='';
		}
		if(width =='')
		{
			width ='';
		}else
		{
			width = " Size:"+width
		}
		if(lenght =='')
		{
			lenght ='';
		}else
		{
			lenght =  "X"+lenght;
		}
		if(price =='')
		{
			price ='';
		}
		document.getElementById("property-property_title").value = project+" "+block+" "+location+" "+plot_no+width+lenght+" "+price;
	});
	
	$("#property-price").change(function () {
		var project = $("#property-project_id option:selected" ).text();
		var block = $( "#property-blocksector option:selected" ).text();
		var location = $( "#property-property_location_type option:selected" ).text();
		var plot_no = $( "#property-member_ship_no" ).val();
		var width = $("#property-width").val();
		var lenght = $("#property-length").val();
		var price = $("#property-price").val();
		if(project =='Select a Project ...')
		{
			project ='';
		}
		if(block =='Select...')
		{
			block ='';
		}
		if(location =='Select Location Type')
		{
			location ='';
		}
		if(plot_no =='')
		{
			plot_no ='';
		}
		if(width =='')
		{
			width ='';
		}else
		{
			width = " Size:"+width
		}
		if(lenght =='')
		{
			lenght ='';
		}else
		{
			lenght =  "X"+lenght;
		}
		if(price =='')
		{
			price ='';
		}
		document.getElementById("property-property_title").value = project+" "+block+" "+location+" "+plot_no+width+lenght+" "+price;
	});
	
	$("#property-price_per").change(function () {
		var project = $("#property-project_id option:selected" ).text();
		var block = $( "#property-blocksector option:selected" ).text();
		var location = $( "#property-property_location_type option:selected" ).text();
		var plot_no = $( "#property-member_ship_no" ).val();
		var width = $("#property-width").val();
		var lenght = $("#property-length").val();
		var price = $("#property-price").val();
		if(project =='Select a Project ...')
		{
			project ='';
		}
		if(block =='Select...')
		{
			block ='';
		}
		if(location =='Select Location Type')
		{
			location ='';
		}
		if(plot_no =='')
		{
			plot_no ='';
		}
		if(width =='')
		{
			width ='';
		}else
		{
			width = " Size:"+width
		}
		if(lenght =='')
		{
			lenght ='';
		}else
		{
			lenght =  "X"+lenght;
		}
		if(price =='')
		{
			price ='';
		}
		document.getElementById("property-property_title").value = project+" "+block+" "+location+" "+plot_no+width+lenght+" "+price;
	});
	
	

</script>
