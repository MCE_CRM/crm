<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LeadType */

$this->title = 'Update Lead Type: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Lead Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lead-type-update">

   <!-- <h1><?/*= Html::encode($this->title) */?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
