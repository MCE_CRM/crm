<?php

use app\models\TaskStatus;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TasksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;


?>


<div class="tasks-index">

    <div class="row">
        <div class="col-md-3">
            <div class="list-group" id="list-tab" role="tablist">

                <a href="<?= Yii::$app->homeUrl?>tasks/create"  class="list-group-item d-flex justify-content-between align-items-center list-group-item-action">Add New Task
                </a>

<?php $my_id = Yii::$app->user->id;?>
                <a href="#leads-list" onclick="myTasks(<?= $my_id?>);" data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action" id="my_task" role="tab" aria-controls="my_task">My Tasks
                    <?php
                    $my_id = \app\models\Tasks::find()->where(['user_assigned_id'=>$my_id])->count();

                    ?>

                    <span class="badge badge-success badge-pill"><?= $my_id ?></span>
                </a>

                <a href="#leads-list" onclick="manageTasks();" data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action" id="manage-tasks" role="tab" aria-controls="manage-tasks">Manage Tasks
                    <?php
                    $my_tasks = \app\models\Tasks::find()->count();

                    ?>

                    <span class="badge badge-success badge-pill"><?= $my_tasks?></span>
                </a>


                <a href="#leads-list" data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action" id="delete_tasks role="tab" aria-controls="delete_tasks">Delete Leads
                    <span class="badge badge-success badge-pill">0</span>
                </a>

                <br>

                <?php
                $i=0;
                foreach ($task_status as $status)
                {
                    $stu = \app\models\Tasks::find()->where(['like', 'task_status', $status->status])->count();


                    if($i==0){


                        ?>
                        <a href="#leads-list" onclick="taskTab('<?= $status->status?>')" data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action active" id="<?= $status->status?> " role="tab" aria-controls="<?= $status->status?>"><?= $status->status?>
                            <span class="badge badge-success badge-pill"><?= $stu?></span>
                        </a>
                    <?php }else { ?>

                        <a href="#leads-list" onclick="taskTab('<?= $status->status?>')" data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action" id="<?= $status->status?> " role="tab" aria-controls="<?= $status->status?>"><?= $status->status?>
                            <span class="badge badge-success badge-pill"><?= $stu?></span>
                        </a>

                    <?php  } $i++; }

                ?>


            </div>

        </div>
        <div class="col-md-9">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane" id="bootstrap-list" role="tabpanel" aria-labelledby="bootstrap-list-item">Some content linked with 1. </div>
                <div class="tab-pane show active" id="leads-list"  role="tabpanel" aria-labelledby="jquery-list-item">

                    <?php Pjax::begin(['id' => 'taskGridview']) ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsiveWrap' => false,
                'toolbar' =>  [
                    '{export}',
                    '{toggleData}',
                ],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i><span id="card-title">'.Yii::t ( 'app', ' List' ).'</span> </h5>',
                    'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                            'index'
                        ], [
                            'class' => 'btn btn-primary btn-sm'
                        ]),


                    'showFooter' => false,

                ],

                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'task_id',
                        //'width' => '350px' ,
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $widget)
                        {
                            return '<a href="tasks/update?id='.$model->id.'">'.$model->task_id.'</a>';
                        }
                    ],
                    [
                        'attribute' => 'task_name',
                        'width' => '250px' ,
                        'format' => 'raw',

                    ],
                    [
                        'attribute' => 'expected_end_datetime',
                        'label'=> Yii::t('app','Due'),
                        'filterType' => GridView::FILTER_DATE,
                        'width' => '200px',
                        'filterWidgetOptions' => [
                            'language' => 'eg',
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd'
                            ]
                        ],
                        'value' => function ($model, $key, $index, $widget) {
                            if(isset($model->expected_end_datetime))
                                return date('F d, Y',($model->expected_end_datetime));
                        }
                    ],
                    [
                        'attribute' => 'task_status',
                        'filterType' => GridView::FILTER_SELECT2,
                        'format' => 'raw',
                        //'width' => '100px',
                        'filter' => ArrayHelper::map ( TaskStatus::find ()->where("active=1")->orderBy ('sort_order' )->asArray ()->all (), 'id', 'status' ),
                        'filterWidgetOptions' => [
                            'options' => [
                                'placeholder' => Yii::t('app', 'All...')
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ],
                    ],
                    [
                        'attribute' => 'task_priority',
                        'filterType' => GridView::FILTER_SELECT2,
                        'format' => 'raw',
                        //'width' => '100px',
                        'filter' => ArrayHelper::map ( TaskStatus::find ()->where("active=1")->orderBy ('sort_order' )->asArray ()->all (), 'id', 'status' ),
                        'filterWidgetOptions' => [
                            'options' => [
                                'placeholder' => Yii::t('app', 'All...')
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ],
                    ],
                    //'task_description:ntext',
                    //'task_status',
                    //'task_priority',
                    //'user_assigned_id',
                    [
                        'attribute' => 'task_progress',
                        //'width'=>'80px',
                        'label' => Yii::t('app','Progress'),
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $widget)
                        {
                            $per = $model->task_progress==''?0:$model->task_progress;
                            return '<small>Progress: '.$per.'%</small>
<div class="progress progress-mini">
<div class="progress-bar" style="width:'.$model->task_progress.'%;"></div>';
                            /*return "<div class='progress'>
<div class='progress-bar progress-bar-info progress-bar-striped' role='progressbar' aria-valuenow='" . $model->task_progress . "' aria-valuemin='0' aria-valuemax='100' style='width: " . $model->task_progress . "%'>" . $model->task_progress . "</div>
</div>";*/
                        }
                    ],
                    //'task_progress',
                    //'expected_start_datetime:datetime',
                    //'expected_end_datetime:datetime',
                    //'actual_start_datetime:datetime',
                    //'actual_end_datetime:datetime',
                    //'created_on',
                    //'created_by',
                    //'updated_on',
                    //'updated_by',

                    [
                        'class' => '\kartik\grid\ActionColumn',
                        'header'=>'Actions',
                        'template'=>'{update}',
                        'buttons' => [
                            'width' => '100px',
                            'update' => function ($url, $model)
                            {
                                return Html::a ( '<span class="fa fa-pencil-alt"></span>', Yii::$app->urlManager->createUrl ( [
                                    'tasks/update',
                                    'id' => $model->id
                                ] ), [
                                    'title' => Yii::t('app', 'Edit' )
                                ] );
                            },

                        ]

                    ]
                ],
            ]); ?>
            <?php Pjax::end(); ?>

                </div>
            </div>


        </div>


    </div>

</div>

<script>

    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+"/web/";

    var tit = 'List';

    function myTasks(id) {

        var base = baseUrl+'tasks?TasksSearch[user_assigned_id]='+id;
        base = encodeURI(base);


        $.pjax.defaults.timeout = 5000;
        $.pjax.reload({container:'#taskGridview', url: base});
        tit = 'My Tasks';

    }

    function taskTab(title) {

        var base = baseUrl+'tasks?TasksSearch[task_status]='+title;

        base = encodeURI(base);


        $.pjax.defaults.timeout = 5000;
        $.pjax.reload({container:'#taskGridview', url: base});
        tit = title;





    }


    function manageTasks() {
        var base = baseUrl+'tasks/index';

        base = encodeURI(base);


        $.pjax.defaults.timeout = 5000;
        $.pjax.reload({container:'#taskGridview', url: base});
        tit = 'Manage Tasks';

    }

    $(document).on('pjax:success', function() {
        window.history.pushState('page2', 'Title', baseUrl+'tasks/index');
        $('#card-title').text(" "+tit);
    });
</script>