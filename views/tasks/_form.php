<?php

use app\models\DefaultValueModule;
use app\models\TaskStatus;
use kartik\slider\Slider;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .control-label{
        font-size: 0.9rem !important;
        color: black;
    }

    .tooltip{
        opacity: 1;
    }
</style>

<div class="tasks-form">

    <?php

    $dFlag = false;
    $disabled_task_status_id=false;
    if(!isset($_REQUEST['id'])){


        if($default_task_priority = DefaultValueModule::getDefaultValueId('task-priority'))
        {
            $task_priority = \app\models\TaskPriority::findOne($default_task_priority);
            $model->task_priority = $task_priority->priority;
        }

        if($default_task_status = DefaultValueModule::getDefaultValueId('task_status'))
        {
            $task_status = \app\models\TaskStatus::findOne($default_task_status);
            $model->task_status = $task_status->status;
            $disabled_task_status_id=false;
        }

    }

    if ($model->expected_start_datetime != '')
    {
        //date_default_timezone_set(Yii::$app->params['TIME_ZONE']);
        $model->expected_start_datetime=date('Y-m-d H:i:s', $model->expected_start_datetime);
    }

    if ($model->expected_end_datetime != '')
    {
       // date_default_timezone_set(Yii::$app->params['TIME_ZONE']);
        $model->expected_end_datetime=date('Y-m-d H:i:s', $model->expected_end_datetime);
    }

    if ($model->actual_start_datetime != '')
    {
       // date_default_timezone_set(Yii::$app->params['TIME_ZONE']);
        $model->actual_start_datetime=date('Y-m-d H:i:s', $model->actual_start_datetime);
    }

    if ($model->actual_end_datetime != '')
    {
       // date_default_timezone_set(Yii::$app->params['TIME_ZONE']);
        $model->actual_end_datetime=date('Y-m-d H:i:s', $model->actual_end_datetime);
    }



    ?>

    <?php $form = ActiveForm::begin ( [
        'type' => ActiveForm::TYPE_VERTICAL,
        'errorCssClass' => 'has-danger',
        
    ] ); ?>


    <section class="card " id="card-1" data-portlet-item>
        <header class="card-header portlet-handler">


            <h4 class="card-title">Add New Task</h4>
        </header>
        <div class="card-body">
            <?php // $form->field($model, 'task_id')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'task_name')->textInput(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-lg-3">
                    <?php

                    // Normal select with ActiveForm & model
                    echo $form->field($model, 'user_assigned_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\User::find()->all(),'id','username'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['placeholder' => 'Select a User ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);




                    ?>
                </div>
                <div class="col-lg-3">
                    <?php

                    // Normal select with ActiveForm & model
                    echo $form->field($model, 'task_status')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\TaskStatus::find()->where(['active'=>1])->orderBy([
                            'sort_order' => SORT_ASC,
                        ])->all(),'status','status'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['placeholder' => 'Select a Status ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);




                    ?>
                </div>
                <div class="col-lg-3">
                    <?php

                    // Normal select with ActiveForm & model
                    echo $form->field($model, 'task_priority')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\TaskPriority::find()->where(['active'=>1])->orderBy([
                            'sort_order' => SORT_ASC,
                        ])->all(),'priority','priority'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['placeholder' => 'Select a Priority ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);




                    ?>

                </div>

                <div class="col-lg-3">
                    <?php

                    if(!isset($_REQUEST['id'])){
                        $model->task_progress=0;
                    }

                    echo $form->field($model, 'task_progress')->widget(Slider::classname(), [
                        'value'=>0,

                        'sliderColor' => Slider::TYPE_SUCCESS,
                        'handleColor' => Slider::TYPE_SUCCESS,
                        'pluginOptions' => [
                            'handle' => 'square',
                            'min' => 0,
                            'max' => 100,
                            'step' => 10,
                            'tooltip'=>'always',
                        ]
                    ]);



                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <?php

                        // usage without model
                        echo '<label class="control-label" for="expected_start_datetime">Expected Start Date</label>';
                        echo DatePicker::widget([
                            'model' => $model,
                            'attribute' => 'expected_start_datetime',
                            'options' => ['placeholder' => 'Select date ...'],
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true,
                            ],
                            'disabled' => $dFlag,
                        ]);

                    ?>

                </div>
                <div class="col-lg-3">
                    <?php

                    // usage without model
                    echo '<label class="control-label" for="expected_end_datetime">Expected End Date</label>';
                    echo DatePicker::widget([
                        'model' => $model,
                        'attribute' => 'expected_end_datetime',
                        'options' => ['placeholder' => 'Select date ...'],
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true,
                        ],
                        'disabled' => $dFlag,
                    ]);

                    ?>

                </div>
                <div class="col-lg-3">
                    <?php

                    // usage without model
                    echo '<label class="control-label" for="actual_start_datetime">Actual Start Date</label>';
                    echo DatePicker::widget([
                        'model' => $model,
                        'attribute' => 'actual_start_datetime',
                        'options' => ['placeholder' => 'Select date ...'],
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true,
                        ],
                        'disabled' => $dFlag,
                    ]);

                    ?>

                </div>

                <div class="col-lg-3">
                    <?php

                    // usage without model
                    echo '<label class="control-label" for="actual_end_datetime">Actual End Date</label>';
                    echo DatePicker::widget([
                        'model' => $model,
                        'attribute' => 'actual_end_datetime',
                        'options' => ['placeholder' => 'Select date ...'],
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true,
                        ],
                        'disabled' => $dFlag,
                    ]);

                    ?>

                </div>
            </div>

            <?= $form->field($model, 'task_description')->textarea(['rows' => 6]) ?>




            <?php // $form->field($model, 'created_on')->textInput() ?>

            <?php // $form->field($model, 'created_by')->textInput() ?>

            <?php // $form->field($model, 'updated_on')->textInput() ?>

            <?php // $form->field($model, 'updated_by')->textInput() ?>

        </div>
        <footer class="card-header text-center">



            <a href="<?= Yii::$app->homeUrl?>leads" class="mb-1 mt-1 mr-1 btn btn-danger">Cancel</a>

            <?= Html::submitButton('Save', ['class' => 'mb-1 mt-1 mr-1 btn btn-primary']) ?>

        </footer>


            <?php ActiveForm::end(); ?>



    </section>


</div>
