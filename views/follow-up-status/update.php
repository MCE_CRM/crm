<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FollowUpStatus */

$this->title = 'Update FollowUpStatus: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'FollowUpStatus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="FollowUpStatus-update">

    <!--<h1><?/*= Html::encode($this->title) */?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
