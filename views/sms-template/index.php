<?php

use app\models\User;
use kartik\date\DatePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SmsTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sms Templates';
$this->params['breadcrumbs'][] = $this->title;



$create = '';
$ordering = '';
if(\Yii::$app->user->can('sms-template/create')){
    $create = Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New Sms Template'), [
        'create'
    ], ['data-pjax' => 0,'onclick'=>'addnew(event,this)',
        'class' => 'btn btn-default btn-sm add-new'
    ]);
}



?>
<div class="sms-template-index">

    <div id="sms" class="tab-pane" style="margin-bottom:25px">

                <div class="card-body">
                    <div class="text-right">

                        <button type="button" onclick="smsSendShow(this)" class="mb-1 mt-1 mr-1 btn btn-default"><i class="fas fa-mobile"></i> Gridview</button>
                    </div>


                    <br>


                    <div class="row">

                        <div class="col-md-12">
                            <div class="row" id="sendSMSShow">
                                <div class="col-md-12"></div>
                                <div class="col-md-12">
                                     <?php Pjax::begin(['timeout' => '30000']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,

        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
            'before' => Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New SMS Template'), [
                'create'
            ], ['data-pjax' => 0,'onclick'=>'addnew(event,this)',
                'class' => 'btn btn-default btn-sm add-new'
            ]),
            'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                    'index'
                ], [
                    'class' => 'btn btn-primary btn-sm'
                ]),


            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'name',

            [
                'attribute' => 'message',
                'format' => 'raw',
                'width' => '500px'
            ],
            [
                'attribute'=>'active',
                'value'=>function($model)
                {
                    return statusLabel($model->active);
                },
                'format'=>'raw',
            ],
            [

                'attribute' =>  'created_on',
                'value' => function($model){
                    return date("d/m/Y", strtotime($model->created_on));
                },
                'filterType'=>GridView::FILTER_DATE,
                'filterWidgetOptions'=> [
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy',
                    ],
                ],
                //'filter'=>false

            ],
            [

                'attribute' => 'created_by',
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'value'=>function($model)
                {
                    return \app\helpers\Helper::getUser($model->created_by);
                },
                //'value'=>'user.username',
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',

            ],
            [

                //'attribute' =>  'created_on',
                'header'=>'Last Update',
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'format'=>'raw',
                'visible'=> \Yii::$app->user->can('sms-template/last-update'),
                'value' => function($model){
                    if($model->updated_by)
                    {
                        return date("d/m/Y", strtotime($model->updated_on)).'<br>'.\app\helpers\Helper::getUser($model->updated_by);

                    }else
                    {
                        return '';
                    }
                },
                //'filter'=>false

            ],
            //'created_by',
            //'updated_on',
            //'updated_by',

            [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{update}  {delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        if (\Yii::$app->user->can('sms-template/delete')) {
                            return Html::a('<span class="fas fa-trash"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-pjax' => '1',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                        'pjax' => 1,],
                                ]
                            );
                        }
                    },
                    'update' => function ($url, $model) {
                        if (\Yii::$app->user->can('sms-template/update')) {
                            return '<a href="#"><span class="fa fa-pencil-alt"  onclick="updateRecord(' . $model->id . ',\'sms-template\',\'Update Sms Template\',event)"></span></a>';

                        } else {
                            return '';
                        }
                    },

                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>


                                </div>
                            </div>
                     
                        </div>


                    </div>

                </div>

            </div>

  
</div>
<script type="text/javascript">
      
            function smsHistoryShow(event) {
                $('#smsHistory').show();
                $('#sendSMSShow').hide();
            }


              function changeFunc($i) {
                var id = $i;
                $.ajax({
                    type: "POST",
                    url: baseUrl+"/ajax/get-template?id="+id,
                    success: function(data)
                    {
                        $('#commentt').val(data);
                        var len = data.length;
                        $('#count').text(len);


                    }
                });
            }
          
        

        
          

</script>