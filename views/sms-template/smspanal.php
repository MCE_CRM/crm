<?php

use app\models\User;
use kartik\date\DatePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use app\models\Clients;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SmsTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sms History';
$this->params['breadcrumbs'][] = $this->title;



$create = '';
$ordering = '';
if(\Yii::$app->user->can('sms-template/create')){
    $create = Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New Sms Template'), [
        'create'
    ], ['data-pjax' => 0,'onclick'=>'addnew(event,this)',
        'class' => 'btn btn-default btn-sm add-new'
    ]);
}



?>
<div class="sms-template-index">

    <div id="sms" class="tab-pane" style="margin-bottom:25px">

                <div class="card-body">
                    <div class="text-right">

                         <button type="button" onclick="smsSendShow(this)" class="mb-1 mt-1 mr-1 btn btn-default"><i class="fas fa-mobile"></i> Sms History</button> 
                          <button type="button" onclick="smsHistoryShow(this)" class="mb-1 mt-1 mr-1 btn btn-default"><i class="fas fa-calendar-alt"></i> Send Sms </button>  
                    </div>


                    <br>


                    <div class="row">

                        <div class="col-md-12">
                             <div class="row" id="sendSMSShow">
                                <div class="col-md-12"></div> 
                                 <div class="col-md-12"> 
                                     <?php Pjax::begin(['timeout' => '30000']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'toolbar' =>  [
                        '{export}',
                        '{toggleData}',
                    ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
            'after' => Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                    'index'
                ], [
                    'class' => 'btn btn-primary btn-sm'
                ]),


            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'label'=>'Client Name',
                 'value'=>function($model)
                 {
                    $client=Clients::find()->where(['id'=>$model->client_id])->one();
                    return $client->name;
                 }
            ],
            'number',
            'message',
             [
                            'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                            'contentOptions' => ['class' => 'text-center'],
                            'filter'=>false,
                            'attribute' =>  'created_on',
                            'value' => function($model){
                                return date("d/m/Y h:i:s A", strtotime($model->created_on));
                            },
                            

            ],
            [
                'label'=>'Send By',
                'value'=>function($model)
                {
                    $user=User::find()->select('first_name,last_name')->where(['id'=>$model->created_by])->one();
                    return $user->first_name." ".$user->last_name;
                }
            ],
            //'updated_on',
            //'updated_by',
            //'lead_id',
           [
            'label'=>'Added By',
            'value'=>function($model)
            {
               $client=Clients::find()->select('created_by')->where(['id'=>$model->client_id])->one();
                    $created_by=$client->created_by;
                $user=User::find()->select('first_name,last_name')->where(['id'=>$created_by])->one();
                return $user->first_name." ".$user->last_name; 
            }
           ],
           [
            'label'=>'Show To',
            'format'=>'raw',
            'value'=>function($model)
            {
                $show_to=Clients::find()->where(['id'=>$model->client_id])->one();
                 $show_to = explode(',', $show_to->show_to);
                            $show_to=array_unique($show_to);
                            $e = '';
                            foreach ($show_to as $key => $data) {
                                $user = \app\models\User::findOne($data);

                                $e .= $user->username .',<br>';


                            }

                            $e = substr_replace($e, "", -1);

                            return $e;
                
                           
          }
           ],
            

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>


                                 </div>
                            </div> 
                         
                            <div class="files-index" id="smsHistory" style="display: none;">
                              <form method="post" action="<?php echo Yii::$app->homeUrl;?>ajax/sendclient">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label" for="template">Template:</label>
                                                    
                                                        <select id="inputState" onchange="changeFunc(value);" class="form-control">
                                                            <option selected>Select Template</option>
                                                            <?php
                                                             $getTemplateName = \app\models\SmsTemplate::find()->select('id,name')->where(['type'=>0])->all();
                                                            foreach ($getTemplateName as $template_name)
                                                            {?>
                                                                <option value="<?php echo $template_name->id?>"><?php echo $template_name->name?></option>
                                                            <?php } ?>
                                                           
                                                        </select>
                                                    


                                                </div>

                                             </div>
                                              <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label " for="template">Country:</label>
                                                    
                                                        <!-- <select id="Country" class="form-control">
                                                            <option selected value="selectcountry">Select Country</option>
                                                            <?php
                                                             $clientName = \app\models\Country::find()->all();
                                                            foreach ($clientName as $value)
                                                            {?>
                                                                <option value="<?php echo $value->id;?>"><?php echo $value->country;?></option>
                                                            <?php } ?>
                                                           
                                                        </select> -->
                                                       <?php echo Select2::widget([
                                                          'name' => 'country',
                                                          'id'=>'Country',
                                                          'data' => ArrayHelper::map(\app\models\Country::find()->where(['active'=>1])->all(),'id','country'),
                                                          'options' => [
                                                              'placeholder' => 'Select Country...',

                                                              
                                                          ],
                                                          'pluginOptions' => ['allowClear' => true],
                                                      ]);?>
                                                                                                       


                                                </div>
                                             </div>
                                             <div class="col-md-3">
                                                 <div class="form-group">
                                                    

                                                             <label class="control-label " for="template">City:</label>
                                                          <input id="something" name="city" list="somethingelse" class="form-control" placeholder="Search" disabled>
                                                                <datalist id="somethingelse">
                                                               
                                                                    
                                                                </datalist>
                                                 


                                                </div> 

                                             </div>

                                              <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label " for="template">Agents Wise Client</label>
                                                    
                                                        <select id="Client" name="client" class="form-control">
                                                            
                                                           
                                                            <?php
                                                             if(Yii::$app->user->can('Admin') || Yii::$app->user->can('SuperAdmin'))
                                                             {
                                                             $clientName = \app\models\User::find()->where([
                                                               'and',
                                                               ['!=','username','admin'],
                                                               ['!=','username','superadmin']
                                                             ])->all();
                                                             echo'<option selected value="">Select Agent</option>';
                                                             echo'<option value="all">All</option>';
                                                            foreach ($clientName as $value)
                                                            {?>
                                                                <option value="<?php echo $value->id;?>"><?php echo $value->username;?></option>
                                                            <?php } 

                                                            }
                                                            else
                                                            {
                                                                $clientName = \app\models\User::find()->where([
                                                               'and',
                                                               ['id'=>Yii::$app->user->id],
                                                               ['!=','username','admin'],
                                                               ['!=','username','superadmin']
                                                             ])->all();
                                                                 echo'<option selected value="">Select Agent</option>';
                                                                 foreach ($clientName as $value)
                                                            {?>
                                                                <option value="<?php echo $value->id;?>"><?php echo $value->username;?></option>
                                                            <?php } 

                                                            }

                                                            ?>
                                                           
                                                        </select>
                                                 


                                                </div>
                                             </div>
                                            
                                    </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="pwd">Message:</label>
                                            <div class="col-sm-12">
                                                <textarea class="form-control" rows="5" name="message" id="commentt" onkeyup="countChar(this)" required></textarea>
                                                <em>Character Count : <span id="count">0</span></em>
                                            </div>
                                        </div>

                                        <div class="form-group" id="mobile">
                                            <label class="control-label col-sm-2" for="number">Mobiles:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="number" name="number" placeholder="Multiple Number Separated By Comma" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" id="smsButton" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>
                                


                            </div>
                           </form>
                        </div>


                    </div>

                </div>

            </div>

  
</div>
<script type="text/javascript">
      function smsSendShow(event) {
                $('#sendSMSShow').show();
                $('#smsHistory').hide();

            }
            function smsHistoryShow(event) {
                $('#smsHistory').show();
                $('#sendSMSShow').hide();
            }


              function changeFunc($i) {
                var id = $i;
                $.ajax({
                    type: "POST",
                    url: baseUrl+"/ajax/get-template?id="+id,
                    success: function(data)
                    {
                        $('#commentt').val(data);
                        var len = data.length;
                        $('#count').text(len);


                    }
                });
            }
          
          $(document).ready(function(){
            
              $("#Client").change(function(){
                 var value=$(this).val();
                 if(value!=="")
                 {
                    $("#mobile").hide();
                    $("#number").prop('required',false);
                 }
                 else
                 {
                    $("#mobile").show();
                    $("#number").prop('required',true);
                 }
              });


             
            });
            
            
          $(document).ready(function(){
            $("#smsButton").click(function(){
               
              
              var agentid=$("#Client").val();
              var location=$("#something").val();
              var message=$("#commentt").val();
              var number=$("#number").val();
              /*if(message=="")
              {
                $("#commentt").css('border-color','red');
                return false;
              }*/


          /*  $.ajax({
                url:"<?php echo Yii::$app->homeUrl;?>ajax/sendclient",
                method:"post",
                data:{agentid:agentid,location:location,message:message,number:number},
                success:function(res)
                {
                   
                    window.location.reload();
                }

            });*/
          });
        });

            $("#Country").change(function(){
               var countryid=$(this).val();
               if(countryid=="selectcountry")
               {
               
                $("#something").prop('disabled',true);
               }
               else
               {
                
               $.ajax({
                   url:"<?php echo Yii::$app->homeUrl;?>ajax/getcity",
                   method:"post",
                   data:{countryid:countryid},
                   success:function(res)
                   {
                    $("#something").prop('disabled', false);
                    $("#somethingelse").find('option').remove();
                    $("#somethingelse").append(res);
                   }
               });
              }
            });
          
           function countChar(val) {
                var len = val.value.length;
                $('#count').text(len);
            }
          

</script>