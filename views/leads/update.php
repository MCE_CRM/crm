<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\leads */
/*echo"<pre>";
echo print_r($dataProvider->models);
echo"</pre>";exit;*/

$this->title = 'Update Leads';
$this->params['breadcrumbs'][] = ['label' => 'Leads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="leads-update">

<!--    <h1><?/*= Html::encode($this->title) */?></h1>
-->
    <?= $this->render('_update_form', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'searchActivityModel' => $searchActivityModel,
        'dataProviderActivity' => $dataProviderActivity,
    ]) ?>

</div>
