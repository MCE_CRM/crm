<?php

use kartik\editable\Editable;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use app\models\User;
use kartik\select2\Select2;
use app\models\Notes;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use app\models\Country;
/* @var $this yii\web\View */
/* @var $searchModel app\models\leadsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$Country=Country::find()->where(['!=','id',177])->all();

$this->title = 'All Leads';
$this->params['breadcrumbs'][] = $this->title;

if(isset($_GET["leadsSearch"]))
{
    $data=$_GET["leadsSearch"];
    if(!empty($data['created_on'])){
        $todayclass='active';
    }
    else{
        $allleadclass='active';
    }
}
else
{
    $allleadclass='active';
}
 
  
?>



<div class="leads-index">
      <div class="card-body">
        <div class="text-right" style="width:50% !important;float:right">
            
            
            <button type="button" onclick="gridShow(this)" class="mb-1 mt-1 mr-1 btn btn-default"><i class="fas fa-box"></i> Grid View</button>
            <button type="button" onclick="calenderShow(this)" class="mb-1 mt-1 mr-1 btn btn-default"><i class="fas fa-calendar-alt"></i> Follow Up Calender</button>
        </div>

        <div class="text-left">
        <div class="follow-up-search">
      
        <?php $form = ActiveForm::begin([
            'action' => ['index?type=all'],
            'method' => 'get',
        ]); ?>
     



        <div class="form-group">
             <?= Html::label( 'Page Size', 'pagesize', array( 'style' => 'margin-left:10px; margin-top:8px;' ) ) ?>

            <?= Html::dropDownList(
    'pagesize', 
    ( isset($_GET['pagesize']) ? $_GET['pagesize'] : 20 ),  // set the default value for the dropdown list
    // set the key and value for the drop down list
    array( 
        20=>20,
        50 => 50, 
        100 => 100,
        200=>200,
        1000=>'All'
    ),
    // add the HTML attritutes for the dropdown list
    // I add pagesize as an id of dropdown list. later on, I will add into the Gridview widget.
    // so when the form is submitted, I can get the $_POST value in InvoiceSearch model.
    array( 
        'id' => 'pagesize', 
        'style' => 'margin-left:5px; margin-top:8px;'
        )
    ) 
?>
          
           <select name="country">
               <option value="">Select Country</option>
              <?php foreach($Country as $country){?>
                <option value="<?php echo $country->id;?>"><?php echo $country->country;?></option>

               <?php } ?>
               <option value="177" <?php if($_GET['country']==177){echo "selected";}?>>Pakistan</option>
               <option value="771"  <?php if($_GET['country']==771){echo "selected";}?>>International</option>
           </select>
           
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary mysearchbtn']) ?>
            
           
        </div>

        <?php ActiveForm::end(); ?>

    </div>
    		
        <form class="form-horizontal" action="<?= Yii::$app->homeUrl?>ajax/upload-file" method="post" name="uploadCSV"   enctype="multipart/form-data">
                <select name="upload_type" required="required">
                   <option value="">Select Types</option>
                   <option value="1">CRM Software Leads sheet</option>
                   <option value="2">WinWin_Fb Leads Messages</option>
                   <option value="3">WinWin_Fb Leads Followup</option>
                </select>
	            <h2 for="myfile1">Select files : </h2>
                <input type = "file" name="file" id="file" accept=".csv" required="required"/>
                <input type = "submit" name="import" value="Import"/><br><br>
        </form>    
		</div>
<br>

    <div class="row" id="gridview-display">
        <div class="col-md-2">
            <div class="list-group" id="list-tab" role="tablist">

                <?php


                if(\Yii::$app->user->can('leads/create'))
                {?>
                    <a href="<?= Yii::$app->homeUrl?>leads/create"  class="list-group-item d-flex justify-content-between align-items-center list-group-item-action">Add Lead
                    </a>
                <?php  } ?>



                <a href="#leads-list" onclick="todayLeads('<?php echo Date("Y-m-d");?>');" data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action <?php echo $todayclass;?>" id="today_lead" role="tab" aria-controls="today_lead">Today Leads
                    <?php  $today = $date = date('Y-m-d');
                    $today = \app\models\Leads::find()->where(['like', 'created_on', $today])->count();

                    ?>

                    <span class="badge badge-success badge-pill"><?= $today?></span>
                </a>

                <?php

                if(\Yii::$app->user->can('AllLeadsView'))
                { ?>

                    <a href="#leads-list" data-toggle="list" onclick="leadTab('')" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action <?php echo $allleadclass;?>" id="my_lead" role="tab" aria-controls="delete_lead">All Leads
                        <?php
                        $my_lead = \app\models\Leads::find()->where(['active'=>1])->count();

                        ?>

                        <span class="badge badge-success badge-pill"><?= $my_lead?></span>
                    </a>

                <?php } ?>

                <br>

                <?php
                $i=0;
                foreach ($leadStaus as $status)
                {
                    $stu = \app\models\Leads::find()->where(['like', 'lead_status', $status->status])->andWhere(['active'=>1])->count();


                    if($i==0){


                        ?>
                        <a href="#leads-list" onclick="leadTab('<?= $status->status?>')" data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action" id="<?= $status->status?> " role="tab" aria-controls="<?= $status->status?>"><?= $status->status?>
                            <span class="badge badge-success badge-pill"><?= $stu?></span>
                        </a>
                    <?php }else { ?>

                        <a href="#leads-list" onclick="leadTab('<?= $status->status?>')" data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action" id="<?= $status->status?> " role="tab" aria-controls="<?= $status->status?>"><?= $status->status?>
                            <span class="badge badge-success badge-pill"><?= $stu?></span>
                        </a>

                    <?php  } $i++; }

                ?>


            </div>

        </div>
        <div class="col-md-10" style="background-color: white;padding: 0px">

            <?php
     if(Yii::$app->user->can('allleads/download'))
     {
        $show='';
     }
     else
     {
        $show=false;
     }

    ?>

            <?php Pjax::begin(['id' => 'leadGridview','timeout' => '30000']) ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsiveWrap' => false,
                'tableOptions' => [
                    'class' => 'table table-condensed table-sm',
                ],

                'export'=>$show,
                'toolbar' =>  [
                    '{export}',
                    '{toggleData}',
                ],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i><span id="card-title">'.Yii::t ( 'app', ' List' ).'</span> </h5>',
                    'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                            'index'
                        ], [
                            'class' => 'btn btn-primary btn-sm'
                        ]),


                    'showFooter' => false,

                ],

                'columns' => [

                                   /* [

                                    'class' => 'yii\grid\CheckboxColumn',

                                    'checkboxOptions' =>function($model, $key, $index, $column){
                                        return ['value' => $model->id];
                                       }, 

                                    ],*/

                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'contact_name',
                    'contact_mobile',
                   /* [
                        'attribute'=>'contact_city',
                        'value'=> function($model)
                        {
                            $city=  \app\models\City::findOne($model->contact_city);
                            return $city->city;
                        }
                    ],*/
                    [
                        'header'=>'Contact_City',
                        'headerOptions' => ['style' => 'width:20%','class' => 'text-center'],
                        'attribute'=>'city',
                        'options' =>['style' => 'width: 1800px;'],
                        'value'=> function($model)
                        {
                            $city=  \app\models\City::findOne($model->contact_city);
                            return $city->city;
                        },
                
                        'filter'=> Select2::widget([

                            'name' => 'leadsSearch[contact_city]',
                            'options' => ['placeholder' => 'All ...'],
                            'model' => $searchModel,
                            'value' => $searchModel->cit->city,
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                ],
                                'ajax' => [
                                    'url' => \yii\helpers\Url::to(['ajax/city-list']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                            ],
                        ]),
                    ],
                    [
                        'attribute'=>'country',
                        'value'=> function($model)
                        {
                            $country=  \app\models\Country::findOne($model->contact_country);
                            return $country->country;
                        },

                        'filter'=> Select2::widget([

                            'name' => 'leadsSearch[contact_country]',
                            'options' => ['placeholder' => 'All ...'],
                            'model' => $searchModel,
                            'value' => $searchModel->country->country,
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                ],
                                'ajax' => [
                                    'url' => \yii\helpers\Url::to(['ajax/country-list']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                            ],
                        ]),
                    ],
                    /*[
                        'attribute'=>'lead_title',
                        'label'=>'Name',
                    ],*/
                    [
                        'attribute'=>'lead_type',
                        'label'=>'Purpose',
                    ],
                    [
                        'attribute'=>'lead_source',
                        'label'=>'Source',
                    ],
                    //'lead_description',
					
                     [
					'attribute'=>'lead_assign',
					'format'=>'raw',
					'value'=>function($model) {
						if (Yii::$app->user->can("leads/assign-other")) {
	
	
							if ($model->lead_assign) {
								$lead_assign = $model->lead_assign;
								$model->lead_assign = explode(',', $model->lead_assign);
								if(!empty($model->lead_assign[0]))
								{
									$model->lead_assign=array_unique($model->lead_assign);
									$e = '';
		
									foreach ($model->lead_assign as $key => $data) {
										$user = \app\models\User::findOne($data);
		
										$e .= $user->first_name . ',<br>';
									}
	
									$e = substr_replace($e, "", -1);
									return '<a href="JavaScript:Void(0);"  onclick="assign(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">' . $e . '</a>';
								}else
								{
									foreach ($lead_assign as $data) {
										$user = \app\models\User::findOne($data);
		
										$e .= $user->first_name . ',<br>';
									}
									return '<a href="JavaScript:Void(0);"  onclick="assign(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">' . $e . '</a>';
								}
	
							} else {
								return '<a href="JavaScript:Void(0);"  onclick="assign(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">Not Set</a>';
							}
						}else {
							if ($model->lead_assign) {
									
									$model->lead_assign = explode(',', $model->lead_assign);
									$model->lead_assign=array_unique($model->lead_assign);
									$e = '';
		
									foreach ($model->lead_assign as $key => $data) {
										$user = \app\models\User::findOne($data);
		
										$e .= $user->first_name . ',<br>';
									}
		
									//$e = substr_replace($e, "", -1);
									
									return '<a href="JavaScript:Void(0);"  onclick="assignsingle(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">' . $e . '</a>';
		
								} else {
									return '<a href="JavaScript:Void(0);"  onclick="assignsingle(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">Not Set</a>';
								}
						}
	
					}
            	],

                       [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'lead_status',
                'label'=>'Status',
                 'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(\app\models\LeadStatus::find()->all(), 'status', 'status'),
                    'filterWidgetOptions' => [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'All...'],
                    'format'=>'raw',
                //'options' => ['style' => 'width: 8%'],

                'editableOptions'=> function ($model, $key, $index, $widget) {
                    $appttypes = ArrayHelper::map(\app\models\LeadStatus::find()->all(), 'status', 'status');
                    return [
                        'header' => 'Status',
                        'attribute' => 'status',
                        'asPopover' => false,
                        'inlineSettings' => [
                            'closeButton' => '<button class="btn btn-sm btn-danger kv-editable-close kv-editable-submit" title="Cancel Edit"><i class="fa fa-times-circle"></i></button>'
                        ],

                        'type' => 'primary',
                        //'size'=> 'lg',
                        'size' => 'md',
                        'options' => ['class'=>'form-control', 'placeholder'=>'Enter person name...'],
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'displayValueConfig' => $appttypes,
                        'data' => $appttypes,
                        'formOptions'=> ['action' => ['/ajax/update-lead-status']] // point to new action
                    ];
                },

            ],



                    


                     [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'lead_response',
                'label'=>'Lead Response',
                 'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(\app\models\Leadresponse::find()->where(['active'=>1])->all(), 'response', 'response'),
                    'filterWidgetOptions' => [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'All...'],
                    'format'=>'raw',
                //'options' => ['style' => 'width: 8%'],

                'editableOptions'=> function ($model, $key, $index, $widget) {
                    $appttypes = ArrayHelper::map(\app\models\Leadresponse::find()->where(['active'=>1])->all(), 'response', 'response');
                    return [
                        'header' => 'Response',
                        'attribute' => 'lead_response',
                        'asPopover' => false,
                        'inlineSettings' => [
                            'closeButton' => '<button class="btn btn-sm btn-danger kv-editable-close kv-editable-submit" title="Cancel Edit"><i class="fa fa-times-circle"></i></button>'
                        ],

                        'type' => 'primary',
                        //'size'=> 'lg',
                        'size' => 'md',
                        'options' => ['class'=>'form-control', 'placeholder'=>'Enter person name...'],
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'displayValueConfig' => $appttypes,
                        'data' => $appttypes,
                        'formOptions'=> ['action' => ['/ajax/update-lead-response']] // point to new action
                    ];
                },

            ],


                   /* [
                        'attribute'=>'Property status',
                        'value'=>function($model)
                        {
                            if( !empty($model->property_id))
                            {
                                $status= \app\models\Property::find()->select('status')->where(['id'=>$model->property_id])->one();
                                return $status->status;
                            }
                            else
                            {
                                return"";
                            }
                        }
                    ],*/
                    
                    //'contact_email:email',
                    //'contact_country',
                    //'contact_state',
                    //'contact_city',
                    //'contact_address',
                    //'contact_address2',

                    [
                        'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                        'contentOptions' => ['class' => 'text-center'],
                        'filter'=>false,
                        'attribute' =>  'created_on',
                        'value' => function($model){
                            return date("d/m/Y", strtotime($model->created_on));
                        },
                        'filterType'=>GridView::FILTER_DATE,
                        'filterWidgetOptions'=> [
                            'type' => DatePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'dd/mm/yyyy',
                            ],
                        ],
                        //'filter'=>false

                    ],
                    [
                        'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                        'attribute' => 'created_by',
                        'contentOptions' => ['class' => 'text-center'],
                        'value'=>function($model)
                        {
                           $user=  \app\models\User::findOne($model->created_by);
                            return $user->first_name.' '.$user->last_name;
                        },
                        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                        'filterWidgetOptions' => [
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ],
                        'filterInputOptions' => ['placeholder' => 'All...'],
                        'format'=>'raw',

                    ],
                    [
                        'label'=>'Call Status',
                        'value'=>'sms_status'
                    ],
                    /*'created_by',*/
                    //'lead_status',

                    //'updated_on',
                    //'updated_by',
                   /* [
                        'label'=>'Last Note',
                        'value'=>function($model)
                        {
                           $lastnote=Notes::find()->where(['lead_id'=>$model->id])->orderBy('id DESC')->one();
                           $lastnotcommint= $lastnote->comment;
                           if(!empty($lastnotcommint))
                           {
                             
                            return $lastnotcommint;
                           }
                           else
                           {
                            return"";
                           }
                        }
                    ],*/

                    [
                        'class' => '\kartik\grid\ActionColumn',
                        'header'=>'Actions',
                        'template'=>'{view} {update} {delete} {obaid}',
                        'buttons' => [
                            'delete' => function ($url, $model, $key) {
                                if (\Yii::$app->user->can('leads/delete')) {
                                    return Html::a('<span class="fas fa-trash"></span>', $url,
                                        [
                                            'title' => Yii::t('app', 'Delete'),
                                            'data-pjax' => '1',
                                            'data' => [
                                                'method' => 'post',
                                                'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                                'pjax' => 1,],
                                        ]
                                    );
                                }
                            },
                            'update' => function ($url, $model)

                            {
                                if(\Yii::$app->user->can('leads/update')){
                                    return '<a href="'.$url.'"><span class="fa fa-pencil-alt"></span></a>';

                                }else
                                {
                                    return '';
                                }
                            } ,

                               'obaid' => function ($url, $model)

                            {
                                     if(Yii::$app->user->can('multipleleadassign')){
                                    return '<input type="checkbox" id='.$model->id.' onclick="myFunction('.$model->id.')" value='.$model->id.'>';
                                       }
                                       else
                                       {
                                        return'';
                                       }

                               
                            } ,

                        ],

                    ]
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>

      <div class="row" id="calender-display" style="display: none;">
            <div class="col-md-1"></div>
            <div class="col-md-10">

                <div id="calendar"></div>

            </div>
            <div class="col-md-1"></div>


        </div>





</div>

<script>
    var array=new Array();
     function myFunction(id)
     {
  
   /* $("input[type='checkbox']")click(function(e){*/
        if ($("#"+id).is(':checked')){
           array.push(id);
          

        } else {
           var index=array.indexOf(id);
           if (index > -1) {
    		   array.splice(index, 1);
			}

        }
    }

    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+"/web/";
       
    var lastUrl = baseUrl+'leads/index';

    var tit = 'List';




    function leadTab(title) {

        var base = baseUrl+'leads?type=all&leadsSearch[lead_status]='+title;
  
        base = encodeURI(base);
            
        lastUrl = base;
         

        $.pjax.defaults.timeout = 5000;
        $.pjax.reload({container:'#leadGridview', url: base});
        if(title=='')
        {
            tit = "List";
        }else {

            tit = title;
        }






    }

    function todayLeads(date) {

        var base = baseUrl+'leads?type=all&leadsSearch[created_on]='+date;

        base = encodeURI(base);

        lastUrl = base;


        $.pjax.defaults.timeout = 5000;
        $.pjax.reload({container:'#leadGridview', url: base});
        if(title=='')
        {
            tit = "List";
        }else {

            tit = title;
        }






    }

    $(document).on('pjax:success', function() {
        window.history.pushState('page2', 'Title', baseUrl+'leads/index?type=all');
        $('#card-title').text(" "+tit);
    });



    function assign(event,id) {

        event.preventDefault();
        var url = '<?=Yii::$app->homeUrl?>ajax/assign-lead?id='+id+'&array='+array+'';
       

       

        var dialog = bootbox.dialog({
            title: 'Assign Lead',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',

        });

        dialog.init(function(){
            var request = $.ajax({
                url: url,
                method: "GET",
            });

            request.done(function( msg ) {
                dialog.find('.bootbox-body').html(msg);
            });

            $(document).on("submit", "#form", function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                $(this).submit(function() {
                    return false;
                });



                $form = $(this); //wrap this in jQuery

                var url = $form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#form").serialize(),

                    // serializes the form's elements.
                    success: function(data)
                    {
                        if(data==true)
                        {
                            $.pjax.defaults.timeout = 5000;
                            $.pjax.reload({container:'#leadGridview'});
                            array=[];
                            bootbox.hideAll();
                        }


                    }
                });
                // avoid to execute the actual submit of the form.

            });

        });

    };
	
	function assignsingle(event,id) {
        event.preventDefault();
        var url = '<?=Yii::$app->homeUrl?>ajax/assign-lead-single?id='+id+'&array='+array+'';
       	var dialog = bootbox.dialog({
            title: 'Assign Lead',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',

        });

        dialog.init(function(){
            var request = $.ajax({
                url: url,
                method: "GET",
            });

            request.done(function( msg ) {
				dialog.find('.bootbox-body').html(msg);
            });

            $(document).on("submit", "#form", function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                $(this).submit(function() {
                    return false;
                });



                $form = $(this); //wrap this in jQuery

                var url = $form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#form").serialize(),

                    // serializes the form's elements.
                    success: function(data)
                    {
                        if(data==true)
                        {
                            $.pjax.defaults.timeout = 5000;
                            $.pjax.reload({container:'#leadGridview'});
                            array=[];
                            bootbox.hideAll();
                        }


                    }
                });
                // avoid to execute the actual submit of the form.

            });

        });

    };

</script>
<script type="text/javascript">
 	function gridShow(event) {
        $('#gridview-display').show();
        $('#summary-display').hide();
        $('#calender-display').hide();
    }
	function calenderShow(event) {
        $('#gridview-display').hide();
        $('#summary-display').hide();
        $('#calender-display').show();
        showcalendar();
    }
	$(function(){

        <?php foreach ($meter as $met) { ?>

        $('#meterDark<?=$met?>').liquidMeter({

            shape: 'circle',
            color: $('#meterDark<?=$met?>').attr("color"),
            background: '#272A31',
            stroke: '#33363F',
            fontSize: '24px',
            fontWeight: '600',
            textColor: '#FFFFFF',
            liquidOpacity: 0.9,
            liquidPalette: ['#0088CC'],
            speed: 3000,
            animate: !$.browser.mobile
        });



        <?php } ?>
	});

    /*
     Name:          Pages / Calendar - Examples
     Written by:    Okler Themes - (http://www.okler.net)
     Theme Version:     2.1.1
     */
    function showcalendar(){
    (function($) {

        'use strict';

        var initCalendarDragNDrop = function() {
            $('#external-events div.external-event').each(function() {

                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };

                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });
        };

        var initCalendar = function() {
            var $calendar = $('#calendar');
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            $calendar.fullCalendar({
                header: {
                    left: 'title',
                    right: 'prev,today,next,basicDay,basicWeek,month'
                },
                timeFormat: 'h:mm',

                themeButtonIcons: {
                    prev: 'fas fa-caret-left',
                    next: 'fas fa-caret-right',
                },

                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar !!!
                drop: function(date, allDay) { // this function is called when something is dropped
                    var $externalEvent = $(this);
                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $externalEvent.data('eventObject');

                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;
                    copiedEventObject.className = $externalEvent.attr('data-event-class');

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                    // is the "remove after drop" checkbox checked?
                    if ($('#RemoveAfterDrop').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }

                },
                events:'<?=Yii::$app->homeUrl?>ajax/allevents',
                eventRender: function(event, element) {
                    $(element).tooltip({title: event.description});
                }


            });

            // FIX INPUTS TO BOOTSTRAP VERSIONS
            var $calendarButtons = $calendar.find('.fc-header-right > span');
            $calendarButtons
                .filter('.fc-button-prev, .fc-button-today, .fc-button-next')
                .wrapAll('<div class="btn-group mt-sm mr-md mb-sm ml-sm"></div>')
                .parent()
                .after('<br class="hidden"/>');

            $calendarButtons
                .not('.fc-button-prev, .fc-button-today, .fc-button-next')
                .wrapAll('<div class="btn-group mb-sm mt-sm"></div>');

            $calendarButtons
                .attr({ 'class': 'btn btn-sm btn-default' });
        };

        $(function() {
            initCalendar();
            initCalendarDragNDrop();
        });

    }).apply(this, [jQuery]);
}
</script>
<style>
.select2-selection
{
	overflow:auto;
}
</style>