<?php

use app\models\DefaultValueModule;
use app\models\User;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\leads */
/* @var $form yii\widgets\ActiveForm */

?>
<style>

    .control-label{
        font-size: 0.9rem !important;
        color: black;
    }
    .form-control{
        padding:0.275rem 0.75rem;
    }

    .row{
        margin-bottom : -12px;
    }

    .coll{
        margin-bottom : -12px;
        margin-left: 0px;
        padding-right:0px;
        padding-left: 0px;
    }

    .table > thead > tr > td.info, .table > tbody > tr > td.info, .table > tfoot > tr > td.info, .table > thead > tr > th.info, .table > tbody > tr > th.info, .table > tfoot > tr > th.info, .table > thead > tr.info > td, .table > tbody > tr.info > td, .table > tfoot > tr.info > td, .table > thead > tr.info > th, .table > tbody > tr.info > th, .table > tfoot > tr.info > th{
        color:#000 !important;
        background-color: white !important;
    }
    .simple-compose-box .compose-box-footer .compose-btn{
        padding: 5px !important;
    }
</style>

<?php


$c= \app\models\Country::findOne($model->contact_country);
$country = $c->country;

$s = \app\models\State::findOne($model->contact_state);
$state = $s->state;



$ci = \app\models\City::findOne($model->contact_city);
$city = $ci->city;




?>



<section class="card">

    <div class="card-body">

        <?php

            $form = \yii\widgets\ActiveForm::begin([
                'id' => 'form',
                'enableAjaxValidation' => true,
                'validationUrl' => Yii::$app->homeUrl.'leads/validate?id='.$model->id.'',
                'fieldConfig' => [
                    'template' => "{label}{input}<label class=\"error\">{error}</label>",
                    //'options' => ['class' => 'form-group'],
                    'labelOptions' => ['class' => 'control-label'],
                ],
                'options' => [
                    'class' => 'form-horizontal'
                ],
                'errorCssClass' => 'has-danger',
            ]);




        ?>


        <br>

        <div class="tabs">
            <ul class="nav nav-tabs">
                <li class="nav-item active">
                    <a class="nav-link" href="#info" data-toggle="tab">Info</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#projects" data-toggle="tab">Projects</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contact" data-toggle="tab">Contact Details</a>
                </li>
               <!--  <li class="nav-item">
                    <a class="nav-link" href="#attachment" data-toggle="tab">Attachments</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#notes" data-toggle="tab">Notes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#activities" data-toggle="tab">Activities</a>
                </li> -->
            </ul>
            <div class="tab-content">
                <div id="info" class="tab-pane active show">



                    <div class="row">
                        <div class="col-lg-8 ">
                            <?= $form->field($model, 'lead_description')->textarea(['rows' => '8']) ?>
                        </div>
                        <div class="col-lg-4 ">

                            <div class="coll col-lg-12 ">
                                <?php

                                // Normal select with ActiveForm & model
                                echo $form->field($model, 'lead_status')->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(\app\models\LeadStatus::find()->where(['active'=>1])->orderBy([
                                        'sort_order' => SORT_ASC,
                                    ])->all(),'status','status'),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => ['placeholder' => 'Select a Status ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);




                                ?>


                            </div>

                            <div class="coll col-lg-12">
                                <?php

                                // Normal select with ActiveForm & model
                           

                                echo $form->field($model, 'lead_type')->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(\app\models\LeadType::find()->where(['active'=>1])->orderBy([
                                        'sort_order' => SORT_ASC,
                                    ])->all(),'type_name','type_name'),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => ['placeholder' => 'Select a Type ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);




                                ?>

                            </div>

                            <div class="coll col-lg-12">
                                 <?php

                                // Normal select with ActiveForm & model
                                echo $form->field($model, 'lead_assign')->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(\app\models\User::find()->all(),'id','username'),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => ['placeholder' => ''],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);




                                ?> 

                                 <?php

                                    /*echo $form->field($model, 'lead_assign')->widget(Select2::classname(), [
                                        'data' =>  app\helpers\Helper::assignLead(),
                                        'theme' => Select2::THEME_BOOTSTRAP,
                                        /*'options' => ['multiple' =>true],*/
                                       /* 'pluginOptions' => [
                                            'allowClear' => true,
                                        ],
                                    ])->label(false);*/

                            

                            

                                  ?>

                            </div>
                            <div class="coll col-lg-12">
                                <?php

                                // Normal select with ActiveForm & model
                                echo $form->field($model, 'lead_source')->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(\app\models\LeadSource::find()->where(['active'=>1])->orderBy([
                                        'sort_order' => SORT_ASC,
                                    ])->all(),'name','name'),
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'options' => ['placeholder' => 'Select a Source ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);




                                ?>
                            </div>
                            <div class="coll col-lg-12">
                                 <?php

                        // Normal select with ActiveForm & model
                        echo $form->field($model, 'lead_response')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Leadresponse::find()->where(['active'=>1])->all(),'response','response'),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => ['placeholder' => 'Select a Response ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);




                        ?>
                            </div>

                            <div class="coll col-lg-12">
                                <?= $form->field($model, 'sms_status')->dropDownList(['incoming'=>'Incoming Call','outgoing'=>'Outgoing Call']) ?>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="projects" class="tab-pane">
                    <div class="row">
                        <div class="col-lg-4 ">
                            <?php


                            // Normal select with ActiveForm & model
                            echo $form->field($model, 'project_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(\app\models\Projects::find()->where(['active'=>1])->all(),'id','name'),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => ['placeholder' => 'Select a Project ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);





                            ?>

                        </div>

                        <div class="col-lg-4 ">

                            <?php


                            echo $form->field($model, 'block_sector')->widget(DepDrop::classname(), [
                                'options'=> ['id'=>'block-sector1'],
                                'type'=>DepDrop::TYPE_SELECT2,
                                'select2Options'=>['theme' => Select2::THEME_BOOTSTRAP,'pluginOptions'=>['allowClear'=>true]],
                                'pluginOptions'=> [
                                    'depends'=>['leads-project_id'],
                                    'initDepends' => ['leads-project_id'],
                                    'initialize'=>true,
                                    'placeholder'=>'Select...',
                                    'url'=>Url::to(['/ajax/get-block-sector'])
                                ]
                            ]);


                            ?>
                        </div>
                        <div class="col-lg-4 ">
                            <!-- <?= $form->field($model, 'property_id')->textInput() ?>  -->

                            <?=  $form->field($model, 'property_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(\app\models\Property::find()->where(['like', 'status', 'Available'])->all(),'id','property_title'),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => ['placeholder' => 'Select a Property ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);?>


                        </div>
                        <div class="col-lg-4 ">
                            <?php

                            // usage without model
                           /* echo '<label class="control-label" for="leads-follow_up">Follow Up</label>';
                            echo DatePicker::widget([
                                'name' => 'check_issue_date',
                                'value' => date('d-M-Y', strtotime('+2 days')),
                                'options' => ['placeholder' => 'Select issue date ...'],
                                'pluginOptions' => [
                                    'format' => 'dd-M-yyyy',
                                    'todayHighlight' => true
                                ]
                            ]);
*/

                            ?>

                        </div>
                    </div>
                </div>
                <div id="contact" class="tab-pane">

                    <div class="card-body">



                        <div class="row">
                            <div class="col-lg-4 ">
                                <?= $form->field($model, 'contact_name')->textInput(['maxlength' => true]) ?>

                            </div>
                            <div class="col-lg-4 ">
                                <?= $form->field($model, 'contact_mobile')->textInput(['maxlength' => true]) ?>


                            </div>
                            <div class="col-lg-4 ">
                                <?= $form->field($model, 'contact_phone')->textInput(['maxlength' => true]) ?>


                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 ">
                                <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-lg-4 ">
                                <?=

                                $form->field($model, 'contact_country')->widget(Select2::classname(), [
                                    'initValueText' => $country, // set the initial display text
                                    'options' => ['placeholder' => 'Search for a Country ...'],
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'minimumInputLength' => 1,
                                        'language' => [
                                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                        ],
                                        'ajax' => [
                                            'url' => \yii\helpers\Url::to(['ajax/country-list']),
                                            'dataType' => 'json',
                                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                        ],
                                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                                    ],
                                ]);



                                ?>



                            </div>
                            <div class="col-lg-4 ">
                                <?php

                                echo $form->field($model, 'contact_state')->widget(DepDrop::classname(), [
                                    'options'=>['id'=>'subcat-id'],
                                    'type'=>DepDrop::TYPE_SELECT2,
                                    'data'=> [$model->contact_state =>$state],
                                    'select2Options'=>['theme' => Select2::THEME_BOOTSTRAP,'pluginOptions'=>['allowClear'=>true]],

                                    'pluginOptions'=>[
                                        'depends'=>['leads-contact_country'],
                                        'initDepends' => ['leads-contact_country'],
                                        'initialize'=>true,
                                        'placeholder'=>'Select...',
                                        'url'=>Url::to(['/ajax/get-country-state'])
                                    ]
                                ]);

                                ?>


                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 ">

                                <?php

                                echo $form->field($model, 'contact_city')->widget(DepDrop::classname(), [
                                    'options'=>['id'=>'subcat2-id'],
                                    'type'=>DepDrop::TYPE_SELECT2,
                                    'select2Options'=>['theme' => Select2::THEME_BOOTSTRAP,'pluginOptions'=>['allowClear'=>true]],
                                    'data'=> [$model->contact_city =>$city],

                                    'pluginOptions'=>[
                                        'depends'=>['subcat-id'],

                                        'placeholder'=>'Select...',
                                        'url'=>Url::to(['/ajax/get-state-city'])
                                    ]
                                ]);

                                ?>




                            </div>
                            <div class="col-lg-4 ">
                                <?= $form->field($model, 'contact_address')->textarea(['rows' => '3']) ?>

                            </div>
                            <div class="col-lg-4 ">
                                <?php  $form->field($model, 'contact_address2')->textarea(['rows' => '3']) ?>


                            </div>
                        </div>

                    </div>

                </div>
                <div id="attachment" class="tab-pane">


                    <div class="row">

                        <div class="col-md-12">
                            <div class="files-index">
                                <?php Pjax::begin(['id' => 'filesPjax']); ?>

                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


                                <?= GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    //'filterModel' => $searchModel,
                                    'responsiveWrap' => false,
                                    //'filter'=>false,
                                    'toolbar' =>  [
                                        //'{export}',
                                        //'{toggleData}',
                                    ],

                                    'panel' => [
                                        'type' => GridView::TYPE_PRIMARY,
                                        'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
                                        'showFooter' => false,

                                    ],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        // 'id',
                                        'file_name',
                                        'custom_name',
                                        'type',
                                        //'size',
                                        [
                                            'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                                            'contentOptions' => ['class' => 'text-center'],
                                            'attribute' =>  'size',
                                            'value' => function($model){
                                                return \app\helpers\Helper::getSize($model->size);
                                            },
                                            //'filter'=>false

                                        ],

                                        //'project_id',
                                        //'inventory_id',
                                        [
                                        'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                                        'contentOptions' => ['class' => 'text-center'],
                                        'attribute' =>  'created_on',
                                        'value' => function($model){
                                            return date("d/m/Y", strtotime($model->created_on));
                                        },
                                        //'filter'=>false

                                    ],
                                        [
                                            'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                                            'attribute' => 'created_by',
                                            'contentOptions' => ['class' => 'text-center'],
                                            'value'=>'user.username',
                                            'format'=>'raw',

                                        ],
                                        //'updated_on',
                                        //'updated_by',
                                        //'custom_name',
                                        //'type',
                                        //'size',

                                        [
                                            'class' => '\kartik\grid\ActionColumn',
                                            'template'=>'{play}{update}{download}{delete}',
                                            'buttons' => [
                                                'play' => function ($url, $model)

                                                {
                                                    return '<a href="'.\app\helpers\Helper::getBaseUrl().'files/lead'.$model->lead_id.'/'.$model->file_name.'" class="html5lightbox"><span class="fas fa-play"></span></a>'.'&nbsp;';
                                                } ,
                                                'update' => function ($url, $model)

                                                {
                                                    return '<a href="javascript:void(0)"><span class="fa fa-pencil-alt"  onclick="editLeadFile('.$model->id.',\''.$model->custom_name.'\')"></span></a>'.'&nbsp;';
                                                } ,
                                                'download' => function ($url, $model)

                                                {
                                                    return '<a href="'.\app\helpers\Helper::getBaseUrl().'files/lead'.$model->lead_id.'/'.$model->file_name.'"  data-pjax =0 download><span class="fas fa-download" ></span></a>'.'&nbsp;';
                                                } ,
                                                'delete' => function ($url, $model) {
                                                    return '<a href="javascript:void(0)"><span class="far fa-trash-alt"  onclick="delLeadFile('.$model->id.')"></span></a>'.'&nbsp;';

                                                }

                                            ],
                                        ],
                                    ],
                                ]); ?>
                                <?php Pjax::end(); ?>
                            </div>
                        </div>


                    </div>

                </div>
                <div id="notes" class="tab-pane">

                    <div id="LoadingOverlayApi" data-loading-overlay>



                    <div class="timeline timeline-simple mt-3 mb-3">
                        <div class="tm-body">
                            <div class="tm-title">
                                <h5 class="m-0 pt-2 pb-2 text-uppercase">Notes</h5>
                            </div>
                            <ol class="tm-items">
                                
                            </ol>
                        </div>
                    </div>

                    </div>

                </div>
                <div id="activities" class="tab-pane">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="files-index">

                                <?php Pjax::begin(['id' => 'activitiesPjax']);
                                ?>
                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


                                <?= GridView::widget([
                                    'dataProvider' => $dataProviderActivity,
                                    //'filterModel' => $searchModel,
                                    'responsiveWrap' => false,
                                    //'filter'=>false,
                                    'toolbar' =>  [
                                        //'{export}',
                                        //'{toggleData}',
                                    ],

                                    'panel' => [
                                        'type' => GridView::TYPE_PRIMARY,
                                        'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
                                        'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                                                'index'
                                            ], [
                                                'class' => 'btn btn-primary btn-sm'
                                            ]),


                                        'showFooter' => false,

                                    ],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        // 'id',
                                        'activity',
                                        [
                                            'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                                            'contentOptions' => ['class' => 'text-center'],
                                            'attribute' =>  'created_on',
                                            'value' => function($model){
                                                return date("d/m/Y", strtotime($model->created_on));
                                            },
                                            //'filter'=>false

                                        ],
                                        [
                                            'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                                            'attribute' => 'created_by',
                                            'contentOptions' => ['class' => 'text-center'],
                                            'value'=>'user.username',
                                            'format'=>'raw',

                                        ],
                                        //'updated_on',
                                        //'updated_by',
                                        //'custom_name',
                                        //'type',
                                        //'size',

                                    ],
                                ]); ?>
                                <?php Pjax::end(); ?>
                            </div>
                        </div>


                    </div>
                </div>
                


            </div>

            <?= Html::submitButton('Update', ['class' => 'btn btn-primary btn-sm ']) ?>

          <!--  <a href="javascript:void(0)" class="btn btn-primary btn-sm notes" ><i class="fas fa-comment"></i> New Note</a>
            <a href="javascript:void(0)" class="btn btn-primary btn-sm attach-files"><i class="fas fa-save"></i> New Attachment</a>-->

            <?php ActiveForm::end(); ?> 
        </div>




    </div>
   <!-- <footer class="card-header text-center">



        <a href="<?/*= Yii::$app->homeUrl*/?>leads" class="mb-1 mt-1 mr-1 btn btn-danger">Cancel</a>

        <?/*= Html::submitButton('Save', ['class' => 'mb-1 mt-1 mr-1 btn btn-primary']) */?>
        <?/*= Html::submitButton('Save', ['class' => 'mb-1 mt-1 mr-1 btn btn-primary']) */?>

    </footer>-->



</section>
<script>



    $( ".attach-files" ).on( "click", function() {

        event.preventDefault();
        var url = '<?= Yii::$app->homeUrl?>ajax/attach-files?id=<?= $model->id?>';
        var dialog = bootbox.dialog({
            title: 'Attach Files',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
            size: 'large',
            onEscape: function() {
                $.pjax.reload({container: '#filesPjax', async: false});
            }

        });

        dialog.init(function(){
            var request = $.ajax({
                url: url,
                method: "GET",
            });

            request.done(function( msg ) {
                dialog.find('.bootbox-body').html(msg);
            });



        });


    });

    $( ".notes" ).on( "click", function() {


        var html  = '<section class="simple-compose-box mb-3">'+
            '<form method="get" id="note_form" action="<?= Yii::$app->homeUrl?>ajax/submit-note?id=<?= $model->id?>">'+
            '<textarea name="message-text" data-plugin-textarea-autosize placeholder="Whats on your mind?" rows="3"></textarea>'+
            '<div class="compose-box-footer">'+
            '<ul class="compose-btn">'+
            '<li>'+
            '<button type="submit" class="btn btn-primary btn-xs">Post</button>'+
            '</li>'+
            '</ul>'+
            '</div>'+
            '</form>'+
            '</section>';



        var dialog = bootbox.dialog({
            title: 'Add New Notes',
            message: html,
        });
        dialog.init(function(){

            $(document).on("submit", "#note_form", function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                $form = $(this); //wrap this in jQuery

                var url = '<?= Yii::$app->homeUrl?>ajax/submit-note?id=<?= $model->id?>';

                $.ajax({
                type: "GET",
                    url: url,
                    data: $("#note_form").serialize(),
                    // serializes the form's elements.
                    success: function(data)
                {

                    if(data==true)
                    {
                        reloadComment();
                        bootbox.hideAll();
                    }
                    else
                    {
                        //toastr.success('', 'Some Error Occur', {timeOut: 2000});
                    }


                }
            });
            // avoid to execute the actual submit of the form.
            });

        });



    });

    
    function updateComment(id,event) {


        var text = $('#comment'+id+'').text();
        
        event.preventDefault();
        var html  = '<section class="simple-compose-box mb-3">'+
            '<form method="get" id="update-note_form" action="<?= Yii::$app->homeUrl?>ajax/update-note?id='+id+'">'+
            '<textarea name="message-text" data-plugin-textarea-autosize  placeholder="Whats on your mind?" rows="3">'+text+'</textarea>'+
            '<div class="compose-box-footer">'+
            '<ul class="compose-btn">'+
            '<li>'+
            '<button type="submit" class="btn btn-primary btn-xs">Post</button>'+
            '</li>'+
            '</ul>'+
            '</div>'+
            '</form>'+
            '</section>';



        var dialog = bootbox.dialog({
            title: 'Update Note',
            message: html,
        });
        dialog.init(function(){

            $(document).on("submit", "#update-note_form", function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                $form = $(this); //wrap this in jQuery

                var url = '<?= Yii::$app->homeUrl?>ajax/update-note?id='+id+'';

                $.ajax({
                    type: "GET",
                    url: url,
                    data: $("#update-note_form").serialize(),
                    // serializes the form's elements.
                    success: function(data)
                    {

                        if(data==true)
                        {
                            reloadComment();
                            bootbox.hideAll();
                        }
                        else
                        {
                            //toastr.success('', 'Some Error Occur', {timeOut: 2000});
                        }


                    }
                });
                // avoid to execute the actual submit of the form.
            });

        });



    }

    function reloadComment() {

        var $el = $('#LoadingOverlayApi');
        $el.trigger('loading-overlay:show');

        var url = '<?= Yii::$app->homeUrl?>ajax/get-note?id=<?= $model->id?>';

        $.ajax({
            url: url,
            // serializes the form's elements.
            success: function(data)
            {
                $('.tm-items').html('');
                $('.tm-items').html(data);
                //$el.trigger('loading-overlay:hide');
            }
        });



        $.pjax.reload({container: '#activitiesPjax', async: false});

    }

    function deleteComment(id,event) {

        var url = '<?= Yii::$app->homeUrl?>ajax/delete-note?id='+id+'';


        bootbox.confirm({
            message: "Are You Sure Delete This?",
            buttons: {
                confirm: {
                    label: 'OK',
                    className: 'btn btn-primary'
                },
                cancel: {
                    label: 'Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if(result==true)
                {
                    $.ajax({
                        url: url,
                        success: function(result){

                            if(result==true)
                            {
                                reloadComment();
                            }

                        }});


                }
            }
        });





    }
    
    
    function editLeadFile(id,custom) {

        bootbox.prompt("Update", function(result){

            console.log(result);

            if(result === null)
            {
                console.log(result);

            }else {

                $.ajax({
                    url: baseUrl+"files/update",
                    type: "POST",
                    data: { name: result,id:id},
                    success: function(result){

                        if(result==true)
                        {
                            bootbox.hideAll();
                            $.pjax.reload({container: '#filesPjax', async: false});
                        }

                    }});
            }

        });

        $('.bootbox-input').val(custom);
        
        
    }

    function delLeadFile(id) {

        bootbox.confirm({
            message: "Are You Sure Delete This?",
            buttons: {
                confirm: {
                    label: 'OK',
                    className: 'btn btn-primary'
                },
                cancel: {
                    label: 'Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if(result==true)
                {
                    $.ajax({
                        url: "<?= Yii::$app->homeUrl?>files/delete",
                        type: "POST",
                        data: {id:id},
                        success: function(result){

                            if(result==true)
                            {
                                bootbox.hideAll();
                                $.pjax.reload({container: '#filesPjax', async: false});
                            }

                        }});


                }
            }
        });


    }




    $( document ).ready(function() {
        reloadComment();
    });


</script>