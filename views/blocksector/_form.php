<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Blocksector */
/* @var $form yii\widgets\ActiveForm */
$url = \yii\helpers\Url::to(['state-list']);
$url1 = \yii\helpers\Url::to(['city-list']);
?>

<div class="blocksector-form">

    <?php
    $form = ActiveForm::begin([
        'id' => 'form',
        'enableAjaxValidation' => true,
        'validationUrl' => ($model->isNewRecord) ?Yii::$app->homeUrl.'blocksector/validate':Yii::$app->homeUrl.'blocksector/validate?id='.$model->id.'',
        'errorCssClass' => 'has-danger',
    ]);
    ?>

    <?= $form->field($model, 'blocksector')->textInput(['maxlength' => true]) ?>

     <?php

     echo $form->field($model, 'project_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\Projects::find()->where(['active'=>1])->all(),'id','name'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['placeholder' => 'Select a Project ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
   /* $form->field($model, 'country_id')->widget(Select2::classname(), [
        //'initValueText' => $cityDesc, // set the initial display text
        'options' => ['placeholder' => 'Search for a Country ...'],
        'theme' => Select2::THEME_BOOTSTRAP,
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 1,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            ],
            'ajax' => [
                'url' => \yii\helpers\Url::to(['country-list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(blocksector) { return blocksector.text; }'),
            'templateSelection' => new JsExpression('function (blocksector) { return blocksector.text; }'),
        ],
    ]);*/



    ?>

         <?php

    /*$form->field($model, 'state_id')->widget(Select2::classname(), [
        //'initValueText' => $cityDesc, // set the initial display text
        'options' => ['placeholder' => 'Search for a State ...'],
        'theme' => Select2::THEME_BOOTSTRAP,
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 1,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            ],
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ]);*/



    ?>

     <?php

   /* $form->field($model, 'city_id')->widget(Select2::classname(), [
        //'initValueText' => $cityDesc, // set the initial display text
        'options' => ['placeholder' => 'Search for a State ...'],
        'theme' => Select2::THEME_BOOTSTRAP,
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 1,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            ],
            'ajax' => [
                'url' => $url1,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ]);
*/


    ?>

    <?php //$form->field($model, 'city_id')->textInput(['maxlength' => true]) ?>
     
  
    <?php //$form->field($model, 'state_id')->textInput(['maxlength' => true]) ?>
    
    
    <?= $form->field($model, 'active')->dropDownList(['1'=>'Active','0'=>'InActive', ]) ?>
     <?php //$form->field($model, 'country_id')->textInput(['maxlength' => true]) ?> 

    <?php //$form->field($model, 'active')->textInput() ?>

  

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $('#blocksector-country_id').parents('.bootbox').removeAttr('tabindex');
</script>
