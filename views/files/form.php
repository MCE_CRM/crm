<?php
/** @var \dosamigos\fileupload\FileUploadUI $this */
use yii\helpers\Html;

$context = $this->context;
?>
    <!-- The file upload form used as target for the file upload widget -->
<?= Html::beginTag('div', $context->options); ?>
    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
    <div class="row fileupload-buttonbar">
        <div class="col-lg-12">
            <!-- The fileinput-button span is used to style the file input field as button -->
            <span class="btn btn-default fileinput-button">
                <i class="fa fa-plus"></i>
                <span><?= Yii::t('fileupload', 'Add files') ?>...</span>

                <?= $context->model instanceof \yii\base\Model && $context->attribute !== null
                    ? Html::activeFileInput($context->model, $context->attribute, $context->fieldOptions)
                    : Html::fileInput($context->name, $context->value, $context->fieldOptions);?>

            </span>
            <button class="mb-1 mt-1 mr-1 btn btn-primary start hidden">
                <i class="fa fa-upload"></i>
                <span><?= Yii::t('fileupload', 'Start upload') ?></span>
            </button>
            <button class="btn btn-warning cancel hidden">
                <i class="fa fa-ban-circle"></i>
                <span><?= Yii::t('fileupload', 'Cancel upload') ?></span>
            </button>
            <!--<button class="btn btn-danger delete">
                <i class="fa fa-trash"></i>
                <span><?= Yii::t('fileupload', 'Delete') ?></span>
            </button>-->
            <!--  <input type="checkbox" class="toggle"> -->
             <!-- The global file processing state -->
            <!--  <span class="fileupload-process"></span> -->
        </div>
        <!-- The global progress state -->
        <div class="col-lg-12 fileupload-progress fade">
            <!-- The global progress bar -->
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
            </div>
            <!-- The extended global progress state -->
            <div class="progress-extended">&nbsp;</div>
        </div>
    </div>

    <!-- The table listing the files available for upload/download -->
    <table role="presentation" class="table table-responsive-lg table-bordered table-striped table-sm mb-0">
        <thead class="thead hidden">
        <tr>
            <th class="text-center">File</th>
            <th class="text-center">File Name</th>
            <th class="text-center">Custom Name</th>
            <th class="text-center">Size</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody class="files">

        </tbody></table>
<?= Html::endTag('div');?>
