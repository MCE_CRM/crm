<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FeatureForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feature-form-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form',
        'enableAjaxValidation' => true,
        'validationUrl' => ($model->isNewRecord) ?Yii::$app->homeUrl.'feature-form/validate':Yii::$app->homeUrl.'feature-form/validate?id='.$model->id.'',
        'errorCssClass' => 'has-danger',
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'show_on')->dropDownList([ 'project' => 'Project', 'property' => 'Property', '' => '', ], ['prompt' => '']) ?>


    <?= $form->field($model, 'active')->dropDownList(['1'=>'Active','0'=>'InActive', ]) ?>

    <?php // $form->field($model, 'sort_order')->textInput() ?>


    <?php //echo $form->field($model, 'created_on')->textInput() ?>

    <?php //echo $form->field($model, 'created_by')->textInput() ?>

    <?php //echo $form->field($model, 'updated_on')->textInput() ?>

    <?php //echo $form->field($model, 'updated_by')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
