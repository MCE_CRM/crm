<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FeatureForm */

$this->title = 'Create Feature Form';
$this->params['breadcrumbs'][] = ['label' => 'Feature Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feature-form-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
