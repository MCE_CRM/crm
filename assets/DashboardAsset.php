<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light',
        'porto/vendor/bootstrap/css/bootstrap.css',
        'porto/vendor/animate/animate.css',
        'porto/vendor/font-awesome/css/fontawesome-all.min.css',
        'porto/vendor/magnific-popup/magnific-popup.css',
        'porto/vendor/jquery-ui/jquery-ui.css',
        'porto/vendor/jquery-ui/jquery-ui.theme.css',
        'porto/vendor/morris/morris.css',
        'porto/css/theme.css',
        'porto/css/skins/default.css',
        'porto/css/custom.css',
        'porto/vendor/modernizr/modernizr.js',
        'amcharts/export.css',
        'css/custom.css'

    ];
    public $js = [

        //'porto/vendor/jquery/jquery.js',
        'porto/vendor/jquery-browser-mobile/jquery.browser.mobile.js',
        'porto/vendor/popper/umd/popper.min.js',
        'porto/vendor/bootstrap/js/bootstrap.js',
        'porto/vendor/common/common.js',
        'js/chart.min.js',
        'js/chart.bundle.min.js',
        'amcharts/amchart.js',
        'amcharts/serial.js',
        'amcharts/plugins/export/export.min.js',
        'amcharts/themes/light.js',
        'amcharts/plugins/dataloader/dataloader.min.js',



    ];
    public $depends = [



    ];
}
