<?php

namespace app\controllers;

use app\helpers\Sender;
use app\modules\rbac\models\AuthAssignment;
use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='user/validate')
                        {
                            return true;
                        }
                        else if (\Yii::$app->user->can($route)) {
                            return true;
                        }


                    }
                ],
            ],
        ];

        return $behaviors;
    }
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAlluser()
    {
       $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('alluser', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]); 
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post()))
        {


            if($id = $model->signup())
            {
                $authModel = new AuthAssignment();
                $authModel->item_name = $model->role;
                $authModel->user_id = "$id";
                if($authModel->save())
                {
                    //Sender::sendAccountEmail($model->email);
                    return true;
                }
                else
                {
                    return false;
                }

            }


            //return $this->redirect(['view', 'id' => $model->id]);
        }
    
        return $this->renderAjax('create', [
            'model' => $model,
        ]);
     
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(isset($_GET['change-password']))
        {
            $model->scenario = 'changepassword';
        }else
        {
            $model->scenario = 'update';
        }
        $model->updated_by = Yii::$app->user->id;
        $model->updated_on = date("Y-m-d H:i:s");

        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {

            if(isset($_GET['change-password'])) {

                $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
            }

            if($model->save())
            {
                return true;
            }
            else
            {

                return false;
            }



        }

        if(isset($_GET['change-password'])){
            return $this->renderAjax('changepassword', [
                'model' => $model,
            ]);
        }else
            
        {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
        
    }



    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }



    public function actionValidate()
    {

        if($_GET['id'])
        {
            if(isset($_GET['change-password']))
            {
                $model = $this->findModel($_GET['id']);
                $model->scenario = 'changepassword';
            }else
            {
                $model = $this->findModel($_GET['id']);
                $model->scenario = 'update';
            }
            
        }
        else
        {
            $model = new User();
            $model->scenario = 'create';
        }

        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    public function actionProfile()
    {

        $model = $this->findModel(Yii::$app->user->id);
        $changepassword = $this->findModel(Yii::$app->user->id);

        $model->scenario = 'profile';

        $changepassword->scenario = 'profile_changepassword';
        
        $model->updated_by = Yii::$app->user->id;
        $model->updated_on = date("Y-m-d H:i:s");


        $changepassword->updated_by = Yii::$app->user->id;
        $changepassword->updated_on = date("Y-m-d H:i:s");

        $request = \Yii::$app->getRequest();

        if ($request->isPost && $model->load($request->post())) {

            if(isset($_POST['User']['username']))
            {

                $directory = Yii::getAlias('@app/files') . DIRECTORY_SEPARATOR . 'profile_image' . DIRECTORY_SEPARATOR;
                if (!is_dir($directory)) {
                    FileHelper::createDirectory($directory);
                }else {

                    if ($files = UploadedFile::getInstance($model, 'file')) {

                        $model->image = $model->first_name . '00' . $model->id .'.'.$files->extension;

                        $filePath = $directory . $model->image;

                        if ($files->saveAs($filePath)) {

                            echo "Good";
                        }else
                        {
                            echo "Eoor";
                            exit;
                        }

                     }


                    if($model->save())
                    {
                        return $this->redirect(['user/profile']);
                    }else
                    {
                        return $this->render('profile', [
                            'model' => $model,
                            'changepassword' => $changepassword,
                        ]);
                    }

              }



                }

            }else
        {
            return $this->render('profile', [
                'model' => $model,
                'changepassword'=>$changepassword,
            ]);
        }



        if ($request->isPost && $changepassword->load($request->post())) {

            if(isset($_POST['User']['password']))
            {
                if (Yii::$app->getSecurity()->validatePassword($changepassword->old_password, $changepassword->password_hash)) {

                    $changepassword->password_hash = Yii::$app->security->generatePasswordHash($changepassword->password);
                    if ($changepassword->save()) {
                        Yii::$app->session->setFlash('success', "Password Change Successfully");

                        return $this->redirect(['user/profile']);

                    }
                } else {
                    $changepassword->addError('old_password', 'Old Password is Incorrect');
                    return $this->render('profile', [
                        'model' => $model,
                        'changepassword' => $changepassword,
                    ]);
                }

            }


        }else
        {
            return $this->render('profile', [
                'model' => $model,
                'changepassword'=>$changepassword,
            ]);
        }



    }



    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
