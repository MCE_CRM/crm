<?php

namespace app\controllers;

use app\models\Clients;
use app\models\EmailForm;
use app\models\Files;
use app\models\Leads;
use app\models\LeadStatus;
use app\models\Projects;
use app\models\Property;
use app\models\Settings;
use app\models\Tasks;
use app\models\TaskStatus; 
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
   
     public function behaviors()
    {

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['login','lock-screen'],
                    'allow' => true,
                ],
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='site/logout' || $route=='site/validate-complain')
                        {
                            return true;
                        }
                       else if (\Yii::$app->user->can($route)) {
                            return true;
                        }else{
                           return true;
                       }

                    }
                ],
            ],
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'logout' => ['GET'],
            ]
        ];


        return $behaviors;
    }


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $this->layout = 'dashboard';
        

        if(\Yii::$app->user->can('adminDashboard'))
        {

            //count projects from projects table===============
            $projects = Projects::find()->count();

            $todayProperties = Property::find()->count();


            $totalLeads= Leads::find()->where(['active'=>1])->count();

          

            //=================================================

            /*count all pending tasks*/
            $pendingtask= Tasks::find()->where(['!=','task_status','completed'])->count();

            //===========================================
            /*Total tasks */
            $totaltasks=Tasks::find()->count();


            /*Today Leads*/
            $todayleads= Leads::find()->where(['LIKE','created_on',date("Y-m-d")])->count();

            /*ALL MONTHLY WISE LEADS*/
            $monthlyleads=Leads::find()->where(['LIKE','created_on',date("Y-m")])->count();

            /*All time leads*/
            $alltimeleads=Leads::find()->count();


            $lead_status = LeadStatus::find()->where(['active'=>1])->orderBy([
                'sort_order' => SORT_ASC,
            ])->all();

            $task_status=TaskStatus::find()->where(['active'=>1])->orderBy(['sort_order'=>SORT_ASC,
            ])->all();


            return $this->render('adminDashboard',
                ['total_projects'=>$projects,
                    'pending_tasks'=>$pendingtask,
                    'todayProperties'=>$todayProperties,
                    'todayleads'=>$todayleads,
                    'totalLeads'=>$totalLeads,
                    'totaltasks'=>$totaltasks,
                    'monthlyleads'=>$monthlyleads,
                    'alltimeleads'=>$alltimeleads,
                    'leadStaus' =>$lead_status,
                    'taskstatus'=>$task_status
                ]);
 
        }elseif(\Yii::$app->user->can('reports/user-dashboard')){

                $projects = Projects::find()->count();

            $todayProperties = Property::find()->count();


            $totalLeads= Leads::find()->where([
                'and', 
                ['active'=>1],
                ['lead_assign'=>Yii::$app->user->id]
            ])->count();

          

            //=================================================

            /*count all pending tasks*/
            $pendingtask= Tasks::find()->where([
                'and',
                ['!=','task_status','completed'],
                ['user_assigned_id'=>Yii::$app->user->id]
            ])->count();

            //===========================================
            /*Total tasks */
            $totaltasks=Tasks::find()->where(['user_assigned_id'=>Yii::$app->user->id])->count();


            /*Today Leads*/
            $todayleads= Leads::find()->where([
                'and',
                ['LIKE','created_on',date("Y-m-d")],
                ['active'=>1],
                ['lead_assign'=>Yii::$app->user->id]
            ])->count();

            /*ALL MONTHLY WISE LEADS*/
            $monthlyleads=Leads::find()->where(['LIKE','created_on',date("Y-m")])->count();

            /*All time leads*/
            $alltimeleads=Leads::find()->count();


            $lead_status = LeadStatus::find()->where(['active'=>1])->orderBy([
                'sort_order' => SORT_ASC,
            ])->all();

            $task_status=TaskStatus::find()->where(['active'=>1])->orderBy(['sort_order'=>SORT_ASC,
            ])->all();
            return $this->render('userDashboard',
                 ['total_projects'=>$projects,
                    'pending_tasks'=>$pendingtask,
                    'todayProperties'=>$todayProperties,
                    'todayleads'=>$todayleads,
                    'totalLeads'=>$totalLeads,
                    'totaltasks'=>$totaltasks,
                    'monthlyleads'=>$monthlyleads,
                    'alltimeleads'=>$alltimeleads,
                    'leadStaus' =>$lead_status,
                    'taskstatus'=>$task_status
                ]);

        }


        elseif(\Yii::$app->user->can('teamDashboard')){

                $projects = Projects::find()->count();

            $todayProperties = Property::find()->count();

            $team_lead=Yii::$app->user->identity->lead_assign;
            $result =  array_map('intval', explode(',', $team_lead));

            $totalLeads= Leads::find()->where([
                'and',
                ['active'=>1],
                ['lead_assign'=>$result]
                
            ])->count();

          

            //=================================================

            /*count all pending tasks*/
            $pendingtask= Tasks::find()->where([
                'and',
                ['!=','task_status','completed'],
                ['user_assigned_id'=>$result]
            ])->count();

            //===========================================
            /*Total tasks */
            $totaltasks=Tasks::find()->where(['user_assigned_id'=>Yii::$app->user->id])->count();


            /*Today Leads*/
            $todayleads= Leads::find()->where([
                'and',
                ['LIKE','created_on',date("Y-m-d")],
                ['lead_assign'=>$result]
            ])->count();

            /*ALL MONTHLY WISE LEADS*/
            $monthlyleads=Leads::find()->where(['LIKE','created_on',date("Y-m")])->count();

            /*All time leads*/
            $alltimeleads=Leads::find()->count();

 
            $lead_status = LeadStatus::find()->where(['active'=>1])->orderBy([
                'sort_order' => SORT_ASC,
            ])->all();

            $task_status=TaskStatus::find()->where(['active'=>1])->orderBy(['sort_order'=>SORT_ASC,
            ])->all();
            return $this->render('teamDashboard',
                 ['total_projects'=>$projects,
                    'pending_tasks'=>$pendingtask,
                    'todayProperties'=>$todayProperties,
                    'todayleads'=>$todayleads,
                    'totalLeads'=>$totalLeads,
                    'totaltasks'=>$totaltasks,
                    'monthlyleads'=>$monthlyleads,
                    'alltimeleads'=>$alltimeleads,
                    'leadStaus' =>$lead_status,
                    'taskstatus'=>$task_status
                ]);

        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['site/index']);
        }

        //$model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


    // action examples

    public function actionFileUpload()
    {
       $model = new Files();

        $model->file = $_FILES['file'];

        $model->custom_name = $_POST['custom_name'][0];

        $directory = Yii::getAlias('@app/files') . DIRECTORY_SEPARATOR . Yii::$app->user->id . DIRECTORY_SEPARATOR;
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }



        if ($files = UploadedFile::getInstance($model,'file')) {
            $model->file_name = $files->baseName.'.'.$files->extension;
            $model->type = $files->type;



            $model->size = "".$files->size;
            //$fileName = $model->file->type;
            $filePath = $directory . $model->file_name ;

            //echo $model->file['type'];

            if ($files->saveAs($filePath)) {
                $model->created_by = Yii::$app->user->id;
                $model->created_on = date("Y-m-d H:i:s");
                $model->save();
                $path = '../../files/'. Yii::$app->user->id.'/'.$model->file_name ;
                $a = $model->type;

                if (strpos($a, 'images') !== false) {
                   $thumb = '../../files/'. Yii::$app->user->id.'/'.$model->file_name ;
                }
                else
                {
                    $thumb = Yii::$app->homeUrl.'images/file.png';
                }

                return Json::encode([


                    'files' => [
                        [
                            'id'=>$model->id,
                            'name' => $model->file_name,
                            'size' => (float)$model->size,
                            'custom_name'=>$model->custom_name,
                            'url' => $path,
                            'thumbnailUrl' => $thumb,
                            'deleteUrl' => Yii::$app->homeUrl.'site/file-remove?name=' . $model->id,
                            'deleteType' => 'GET',
                        ],
                    ],
                ]);
            }
        }
        else
        {
            echo "Failed";
        }

        return '';
    }


    public function actionFileUploadProperty()
    {
        $model = new Files();

        $model->file = $_FILES['file'];
        $model->property_id = $_GET['id'];

        $model->custom_name = $_POST['custom_name'][0];



        $directory = Yii::getAlias('@app/files') . DIRECTORY_SEPARATOR . 'property'.$model->property_id . DIRECTORY_SEPARATOR;
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }



        if ($files = UploadedFile::getInstance($model,'file')) {
            $model->file_name = $files->baseName.'.'.$files->extension;
            $model->type = $files->type;



            $model->size = "".$files->size;
            //$fileName = $model->file->type;
            $filePath = $directory . $model->file_name ;

            //echo $model->file['type'];

            if ($files->saveAs($filePath)) {
                $model->created_by = Yii::$app->user->id;
                $model->created_on = date("Y-m-d H:i:s");
                $model->save();
                $path = '../../files/'. 'property'.$model->property_id .'/'.$model->file_name ;
                $a = $model->type;

                if (strpos($a, 'images') !== false) {
                    $thumb = '../../files/'. 'project'.$model->property_id .'/'.$model->file_name ;
                }
                else
                {
                    $thumb = Yii::$app->homeUrl.'images/file.png';
                }

                return Json::encode([


                    'files' => [
                        [
                            'id'=>$model->id,
                            'name' => $model->file_name,
                            'size' => (float)$model->size,
                            'custom_name'=>$model->custom_name,
                            'url' => $path,
                            'thumbnailUrl' => $thumb,
                            'deleteUrl' => Yii::$app->homeUrl.'site/file-remove?name=' . $model->id,
                            'deleteType' => 'GET',
                        ],
                    ],
                ]);
            }
        }
        else
        {
            echo "Failed";
        }

        return '';
    }
    
    
    
    
    
    public function actionFileUploadLead()
    {
        $model = new Files();

        $model->file = $_FILES['file'];
        $model->lead_id = $_GET['id'];

        $model->custom_name = $_POST['custom_name'][0];

        $directory = Yii::getAlias('@app/files') . DIRECTORY_SEPARATOR . 'lead'.$model->lead_id . DIRECTORY_SEPARATOR;
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }



        if ($files = UploadedFile::getInstance($model,'file')) {
            $model->file_name = $files->baseName.'.'.$files->extension;
            $model->type = $files->type;



            $model->size = "".$files->size;
            //$fileName = $model->file->type;
            $filePath = $directory . $model->file_name ;

            //echo $model->file['type'];

            if ($files->saveAs($filePath)) {
                $model->created_by = Yii::$app->user->id;
                $model->created_on = date("Y-m-d H:i:s");
                $model->save();
                $path = '../../files/'. 'lead'.$model->lead_id .'/'.$model->file_name ;
                $a = $model->type;

                if (strpos($a, 'images') !== false) {
                    $thumb = '../../files/'. 'lead'.$model->lead_id .'/'.$model->file_name ;
                }
                else
                {
                    $thumb = Yii::$app->homeUrl.'images/file.png';
                }

                return Json::encode([


                    'files' => [
                        [
                            'id'=>$model->id,
                            'name' => $model->file_name,
                            'size' => (float)$model->size,
                            'custom_name'=>$model->custom_name,
                            'url' => $path,
                            'thumbnailUrl' => $thumb,
                            'deleteUrl' => Yii::$app->homeUrl.'site/file-remove?name=' . $model->id,
                            'deleteType' => 'GET',
                        ],
                    ],
                ]);
            }
        }
        else
        {
            echo "Failed";
        }

        return '';
    }


    public function actionFileUploadTask()
    {
        $model = new Files();

        $model->file = $_FILES['file'];
        $model->task_id = $_GET['id'];

        $model->custom_name = $_POST['custom_name'][0];

        $directory = Yii::getAlias('@app/files') . DIRECTORY_SEPARATOR . 'task'.$model->task_id . DIRECTORY_SEPARATOR;
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }



        if ($files = UploadedFile::getInstance($model,'file')) {
            $model->file_name = $files->baseName.'.'.$files->extension;
            $model->type = $files->type;



            $model->size = "".$files->size;
            //$fileName = $model->file->type;
            $filePath = $directory . $model->file_name ;

            //echo $model->file['type'];

            if ($files->saveAs($filePath)) {
                $model->created_by = Yii::$app->user->id;
                $model->created_on = date("Y-m-d H:i:s");
                $model->save();
                $path = '../../files/'. 'task'.$model->task_id .'/'.$model->file_name ;
                $a = $model->type;

                if (strpos($a, 'images') !== false) {
                    $thumb = '../../files/'. 'task'.$model->task_id .'/'.$model->file_name ;
                }
                else
                {
                    $thumb = Yii::$app->homeUrl.'images/file.png';
                }

                return Json::encode([


                    'files' => [
                        [
                            'id'=>$model->id,
                            'name' => $model->file_name,
                            'size' => (float)$model->size,
                            'custom_name'=>$model->custom_name,
                            'url' => $path,
                            'thumbnailUrl' => $thumb,
                            'deleteUrl' => Yii::$app->homeUrl.'site/file-remove?name=' . $model->id,
                            'deleteType' => 'GET',
                        ],
                    ],
                ]);
            }
        }
        else
        {
            echo "Failed";
        }

        return '';
    }


    public function actionFileUploadProject()
    {
        $model = new Files();

        $model->file = $_FILES['file'];
        $model->project_id = $_GET['id'];

        $model->custom_name = $_POST['custom_name'][0];



        $directory = Yii::getAlias('@app/files') . DIRECTORY_SEPARATOR . 'project'.$model->project_id . DIRECTORY_SEPARATOR;
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }



        if ($files = UploadedFile::getInstance($model,'file')) {
            $model->file_name = $files->baseName.'.'.$files->extension;
            $model->type = $files->type;



            $model->size = "".$files->size;
            //$fileName = $model->file->type;
            $filePath = $directory . $model->file_name ;

            //echo $model->file['type'];

            if ($files->saveAs($filePath)) {
                $model->created_by = Yii::$app->user->id;
                $model->created_on = date("Y-m-d H:i:s");
                $model->save();
                $path = '../../files/'. 'project'.$model->property_id .'/'.$model->file_name ;
                $a = $model->type;

                if (strpos($a, 'images') !== false) {
                    $thumb = '../../files/'. 'project'.$model->property_id .'/'.$model->file_name ;
                }
                else
                {
                    $thumb = Yii::$app->homeUrl.'images/file.png';
                }

                return Json::encode([


                    'files' => [
                        [
                            'id'=>$model->id,
                            'name' => $model->file_name,
                            'size' => (float)$model->size,
                            'custom_name'=>$model->custom_name,
                            'url' => $path,
                            'thumbnailUrl' => $thumb,
                            'deleteUrl' => Yii::$app->homeUrl.'site/file-remove?name=' . $model->id,
                            'deleteType' => 'GET',
                        ],
                    ],
                ]);
            }
        }
        else
        {
            echo "Failed";
        }

        return '';
    }
    

    public function actionFileRemove()
    {
        $id = $_GET['name'];
        $model = Files::findOne($id);
        $name= $model->file_name;
        $model->delete();
        $directory = Yii::getAlias('@app/files') . DIRECTORY_SEPARATOR . Yii::$app->user->id . DIRECTORY_SEPARATOR;

        if (is_file($directory . DIRECTORY_SEPARATOR . $name)) {
            unlink($directory . DIRECTORY_SEPARATOR . $name);
        }

        $files = FileHelper::findFiles($directory);
        $output = [];
        foreach ($files as $file) {
            $fileName = basename($file);
            $path = '../../files/'. Yii::$app->user->id.'/'.$fileName ;
            $output['files'][] = [
                'name' => $fileName,
                'size' => filesize($file),
                'url' => $path,
                'thumbnailUrl' => $path,
                'deleteUrl' => 'site/file-remove?name=' . $fileName,
                'deleteType' => 'POST',
            ];
        }
        return Json::encode($output);

    }
    
    
    
    public function actionSettings()
    {

        $items = Settings::find()->asArray()->all();
        foreach ($items as $item)
        {
            if ($item['config_item_name'])
            {
                Yii::$app->params[$item['config_item_name']] = $item['config_item_value'];
                Yii::$app->params[$item['config_item_name']."_description"] = $item['config_item_description'];
            }
        }
        
        return $this->render('settings');
    }


    public function actionSettingsUpdate()
    {
        if(isset($_POST['project_category']))
        {
            $project_category = implode (",", $_POST['project_category']);
            $model = Settings::findByName('PROJECT_CATEGORY');
            $model->config_item_value = $project_category;
            $model->save();

        }
        if(isset($_POST['inventory_purpose']))
        {
            $inventory_purpose = implode (",", $_POST['inventory_purpose']);
            $model = Settings::findByName('PURPOSE');
            $model->config_item_value = $inventory_purpose;
            $model->save();

        }

        if(isset($_POST['property_heading_category']))
        {
            $property_heading_category = implode (",", $_POST['property_heading_category']);
            $model = Settings::findByName('PROPERTY_HEADING_CATEGORY');
            $model->config_item_value = $property_heading_category;
            $model->save();

        }


        if(isset($_POST['property_category']) && $_POST['property_type'])
        {

            $type = $_POST['property_type'];
            $name = $_POST['property_category'];
            $response = array();
            foreach ($type as $index => $code)
            {

                $cat['code'] = $code;
                $cat['name'] = $name[$index];

                $response['property_category'][] = $cat;
            }

            $property_data =  json_encode($response);
            $model = Settings::findByName('PROPERTY_CATEGORY');
            if($model != null)
            {
                $model->config_item_value = $property_data;
                $model->save();
            }
        }

        if(isset($_POST['property_unit']) && $_POST['property_unit_name']) {

            $type = $_POST['property_unit'];
            $name = $_POST['property_unit_name'];
            $response = array();
            foreach ($type as $index => $code) {

                $cat['unit'] = $code;
                $cat['name'] = $name[$index];

                $response['property_land_area_unit'][] = $cat;
            }

            $property_data = json_encode($response);
            $model = Settings::findByName('PROPERTY_LAND_AREA_UNIT');
            if ($model != null) {
                $model->config_item_value = $property_data;
                $model->save();
            }


        }


        return $this->redirect(['settings']);
    }
    
    
    public function actionSendEmail()
    {
        $model = new EmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->renderAjax('sendEmail', [
            'model' => $model,
        ]);
    }


    public function sendSms()
    {
        return $this->render('sendSms');
    }

    public function actionNewProperty()
    {
        return $this->render('new-property');
    }



    public function actionSwitchIdentity()
    {
        $id = $_GET['id'];
        $initialId = Yii::$app->user->getId(); //here is the current ID, so you can go back after that.
        if ($id == $initialId) {
            //Same user!
        } else {
            $user = User::findOne($id);
            Yii::$app->user->switchIdentity($user);
            Yii::$app->session->set('user.idbeforeswitch',$initialId); //Save in the session the id of your admin user.
            return $this->goHome();
        }

    }




  public function actionLockScreen()
  {


          $this->layout = 'login';
          // save current username
          $username = Yii::$app->user->identity->username;
          $image = Yii::$app->user->identity->image;
          $first_name = Yii::$app->user->identity->first_name;
          $last_name = Yii::$app->user->identity->last_name;
          $email = Yii::$app->user->identity->email;

          // force logout
          Yii::$app->user->logout();

          // render form lockscreen
          $model = new LoginForm();
          $model->username = $username;
          $model->image = $image;

      
          //set default value
          return $this->render('lock-screen', [
              'model' => $model,
              'first_name'=>$first_name,
              'last_name'=>$last_name,
              'email'=>$email
          ]);


  }

   



}
