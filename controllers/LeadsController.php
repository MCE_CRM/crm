<?php

namespace app\controllers;

use app\helpers\Helper;
use app\models\Activities;
use app\models\ActivitiesSearch;
use app\models\FilesSearch;
use app\models\FollowUpSearch;
use app\models\LeadStatus;
use app\models\Projects;
use app\models\SmsHistory;
use app\models\SmsHistorySearch;
use app\models\SmsTemplate;
use phpDocumentor\Reflection\DocBlock\Tags\Property;
use Yii;
use app\models\Leads;
use app\models\leadsSearch;
use yii\debug\models\search\User;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\Clients;
use app\models\FollowUp;
use yii\swiftmailer\Mailer;
use app\models\EmailTemplate;
use app\models\EmailHistory;

use DateTime;





/**
 * LeadsController implements the CRUD actions for leads model.
 */
class LeadsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='leads/validate')
                        {
                            return true;
                        }
                        else if (\Yii::$app->user->can($route)) {
                            return true;
                        }


                    }
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all leads models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new leadsSearch();
        $lead_status = LeadStatus::find()->where(['active'=>1])->orderBy([
            'sort_order' => SORT_ASC,
        ])->all();


        if(isset($_GET['type']) && $_GET['type']=='all')
        {   
             
            if(\Yii::$app->user->can('AllLeadsView')) {
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                return $this->render('all-leads', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'leadStaus' => $lead_status,
                ]);
            }else
            {
                throw new ForbiddenHttpException('Not Allowed');

            }
        }
        elseif(isset($_GET['type']) && $_GET['type']=='teamlead')
        {   
            
           
       
            if(\Yii::$app->user->can('teamlead')) {
                 $dataProvider = $searchModel->searchTeamLead(Yii::$app->request->queryParams);
            return $this->render('team-lead', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'leadStaus'=>$lead_status,
            ]);
            }
            else
            {
                throw new ForbiddenHttpException('Not Allowed');

            }
        }
		else if(isset($_GET['type']) && $_GET['type']=='delete')
        {

            if(\Yii::$app->user->can('DeleteLeadsView'))
            {
                $dataProvider = $searchModel->searchDelete(Yii::$app->request->queryParams);
                return $this->render('delete-leads', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'leadStaus'=>$lead_status,
                ]);
            }else
            {
                throw new ForbiddenHttpException('Not Allowed');
            }


        }
		else if(isset($_GET['type']) && $_GET['type']=='upload')
        {

            //if(\Yii::$app->user->can('DeleteLeadsView'))
            {
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                return $this->render('all-leads-upload', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'leadStaus'=>$lead_status,
                ]);
            }/*else
            {
                throw new ForbiddenHttpException('Not Allowed');
            }*/


        }
		else if(isset($_GET['type']) && $_GET['type']=='myregister')
        {

            /*if(\Yii::$app->user->can('DeleteLeadsView'))
            {*/
            $dataProvider = $searchModel->searchRegister(Yii::$app->request->queryParams);
            return $this->render('my-register', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'leadStaus'=>$lead_status,
            ]);
            /*}else
            {
                throw new ForbiddenHttpException('Not Allowed');
            }*/

        }
        else
        {
            $dataProvider = $searchModel->searchMyAssigned(Yii::$app->request->queryParams);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'leadStaus'=>$lead_status,
            ]);
        }


    }

    /**
     * Displays a single leads model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
	

    public function MoveLead()
    {
        $sendtoclient=Helper:: moveLead();
    }
	
    public function actionView($id)
    {
        $searchModel = new FilesSearch();
        $dataProvider = $searchModel->searchByLead(Yii::$app->request->queryParams,$id);
        $searchModel2 = new FollowUpSearch();
        $dataProvider2 = $searchModel2->searchByLead(Yii::$app->request->queryParams,$id);

        $searchModel3 = new SmsHistorySearch();
        $dataProvider3 = $searchModel3->searchByLead(Yii::$app->request->queryParams,$id);

        $searchActivityModel = new ActivitiesSearch();
        $dataProviderActivity = $searchActivityModel->searchByLead(Yii::$app->request->queryParams,$id);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider'=>$dataProvider,
            'dataProvider2'=>$dataProvider2,
            'dataProvider3'=>$dataProvider3,
            'dataProviderActivity'=>$dataProviderActivity,
        ]);
    }


     public function actionTeamlead($id)
    {
        
           if(\Yii::$app->user->can('AllLeadsView')){ 
                            

                        $team_lead=Yii::$app->user->identity->lead_assign;
                        $split = preg_split('/(?:"[^"]*"|)\K\s*(,\s*|$)/', $team_lead);
                        $result = array_filter($split);

                        foreach ($result as $val){


                        $my_lead = \app\models\Leads::find()->select('id')->where('lead_assign='.$val)->andwhere(['active'=>1])->all();

                        
                         foreach ($my_lead as $value){

                        $id = $value['id'];
                       
                       }

                    }
                        }

    
        return $this->render('team-lead', [
           
            'leadid' => $id,
             
        ]);
    }

    /**
     * Creates a new leads model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    { 
        

        $model = new Leads();
        $model->created_by = Yii::$app->user->id;
        $model->created_on = date("Y-m-d H:i:s");
        $model->active = 1;
        $addleadsclient=$_POST['addleadclient'];


        if ($model->load(Yii::$app->request->post()) &&  $model->save()) {

			//////////folow////////////////
			if(!empty($_POST['Leads']['follow_date_time']) && !empty($_POST['Leads']['note']))
			{
				$christmas =$_POST['Leads']['follow_date_time'];
				$follow_date_time = DateTime::createFromFormat('d/m/Y H:i',$christmas)->format('Y-m-d H:i:s');
				$followup = new FollowUp();
				$followup->follow_date_time = $follow_date_time;
				$followup->lead_id = $model->id;
				$followup->note = $_POST['Leads']['note'];
				$followup->created_on = $follow_date_time;
				$followup->created_by = Yii::$app->user->id;
				$followup->status = 'Need Action';
				$followup->save();
				if($followup->save())
				{
			  		$christmas =$_POST['Leads']['follow_date_time'];
					$follow_date_time = DateTime::createFromFormat('d/m/Y H:i',$christmas)->format('Y-m-d H:i:s');
					$activities = new Activities();
					$username=\app\models\User::find()->select('first_name')->where(['id'=>Yii::$app->user->id])->one();
					$activities->activity=$username->first_name.' created the follow-up '.$_POST['Leads']['note'].' '.$follow_date_time.'';
					$activities->created_on=$follow_date_time;
					$activities->created_by=Yii::$app->user->id;
					$activities->lead_id=$model->id;
					$activities->save();
				}
			}
			//////////////////////////////////
            $activity = new Activities();
            $activity->activity = Yii::$app->user->identity->username.' Created The Lead';
            $activity->lead_id = $model->id;
            $activity->created_on = date("Y-m-d H:i:s");
            $activity->created_by = Yii::$app->user->id;
            $activity->save();
            $clients = new Clients();
            $clients->name=$_POST['Leads']['contact_name'];
            $clients->mobile=$_POST['Leads']['contact_mobile'];
            $clients->email=$_POST['Leads']['contact_email'];
            $clients->country=$_POST['Leads']['contact_country'];
            $clients->state=$_POST['Leads']['contact_state'];
            $clients->city=$_POST['Leads']['contact_city'];
            $clients->address=$_POST['Leads']['contact_address'];
            $clients->description=$_POST['Leads']['contact_description'];
            $clients->created_by = Yii::$app->user->id;
            $clients->created_on = date("Y-m-d H:i:s");
            $clients->LeadType="Leads Client";

            $record=Clients::find()->select('mobile')->where(['mobile'=>$_POST['Leads']['contact_mobile']])->count();
            if($record>0){
                $record=Clients::find()->where(['mobile'=>$_POST['Leads']['contact_mobile']])->one();
                if($addleadsclient==1)
                {
                    $record->LeadType="Leads Client";
                }
                $record->show_to.=",".$model->lead_assign."";
                $record->save();
            }
            else{
                $clients->show_to=$model->lead_assign;
                $clients->save();
            }



            $id=$model->project_id;
            $smsstatus=$model->sms_status;
            $mobileno=$model->contact_mobile;

//send sms to client with some inforamtion==================================================================== 
//send auto sms to client with template====================================================================================
            if($id=="" && $smsstatus=="incoming")
            {
                $result=SmsTemplate::find()->where([
                    'and',
                    ['=','sms_status',$smsstatus],
                    ['projectname'=>""]
                ])->one();

            }
            elseif(!empty($id) && $smsstatus=="outgoing")
            {
                $result=SmsTemplate::find()->where([
                    'and',
                    ['=','sms_status',$smsstatus],
                    ['projectname'=>$id]
                ])->one();
            }
            elseif($id=="" && $smsstatus=="outgoing")
            {
                $result=SmsTemplate::find()->where([
                    'and',
                    ['=','sms_status',$smsstatus],
                    ['projectname'=>""]
                ])->one();
            }
            elseif(!empty($id) && $smsstatus=="incoming")
            {
                $result=SmsTemplate::find()->where([
                    'and',
                    ['=','sms_status',$smsstatus],
                    ['projectname'=>$id]
                ])->one();
            }
			$message=$result->message;
            $sendSms = Helper::sendSMS($mobileno,$message);

             if($sendSms)
            {
                $smsHistory = new SmsHistory();
                $smsHistory->number = $mobileno;
                $smsHistory->lead_id = $model->id;
                $smsHistory->message =  $message;
                $smsHistory->created_by = Yii::$app->user->id;
                $smsHistory->created_on = date("Y-m-d H:i:s");
                $smsHistory->save();

            }



            //another for sms for client at the time of registration 
            //If Assign To Agent AUTO

        if($model->lead_assign)
        {
                     
                $model->assign_by=Yii::$app->user->id;
                $sms = SmsTemplate::find()->where(['name'=>'Lead Assign SMS'])->one();
                $emailresult=EmailTemplate::find()->where(['template_name'=>'Lead Assign Email'])->one();
                $user = \app\models\User::findOne($model->lead_assign);
                $user2 = \app\models\User::findOne($model->assign_by);
                if($model->project_id)
                {
                    $project = Projects::findOne($model->project_id);
                    $sms->message = str_replace("#Project Name",$project->name,$sms->message);
                    $emailresult->template_body = str_replace("#Project Name",$project->name,$emailresult->template_body);
                    $emailresult->template_subject=str_replace("#Project_Name",$project->name,$emailresult->template_subject);

                }else
                {
                    $sms->message = str_replace("#Project Name",'',$sms->message);
                    $emailresult->template_body = str_replace("#Project Name",'',$emailresult->template_body);
                    $emailresult->template_subject=str_replace("#Project_Name",'',$emailresult->template_subject);
                }

                if($model->property_id)
                {
                    $property = \app\models\Property::findOne($model->property_id);
                    $sms->message = str_replace("#Property name",$property->property_title,$sms->message);
                    $emailresult->template_body = str_replace("#Property name",$property->property_title,$emailresult->template_body);
                }
                else
                {
                    $sms->message = str_replace("#Property name",'',$sms->message);
                    $emailresult->template_body = str_replace("#Property name",'',$emailresult->template_body);
                }
                
    //send sms to client if agent assign=======================================
            $clientmessage="Respected ".$model->contact_name." 
 Your Query Id ".$model->id." for Purpose ".$model->lead_type." has been registered and assigned to our representative
 Mr/Ms. ".$user->first_name." ".$user->last_name." ".$user->phone_no."";
            $sendtoclient=Helper::sendSMS($mobileno,$clientmessage);

            if($sendtoclient)
            {
                $smsHistory = new SmsHistory();
                $smsHistory->number =$mobileno;
                $smsHistory->lead_id = $model->id;
                $smsHistory->message =  $clientmessage;
                $smsHistory->created_by = Yii::$app->user->id;
                $smsHistory->created_on = date("Y-m-d H:i:s");
                $smsHistory->save();

            }


          //===============================================================
				
                     //sending email template to agent           
				$emailresult->template_body = str_replace("#Agent_name",$user->first_name,$emailresult->template_body);
				$emailresult->template_body = str_replace("#ID",$model->id,$emailresult->template_body);
			   $emailresult->template_body = str_replace("#Assigned_User_Name",$user2->first_name,$emailresult->template_body);
			   $emailresult->template_body = str_replace("#Client Name",$model->contact_name,$emailresult->template_body);
			   $emailresult->template_body = str_replace("#Cell Number",$model->contact_mobile,$emailresult->template_body);
				$emailresult->template_body = str_replace("#Email",$model->contact_email,$emailresult->template_body);
				$emailresult->template_body= str_replace("#Purpose",$model->lead_type,$emailresult->template_body);
				$emailresult->template_body= str_replace("#Lead_Status",$model->lead_status,$emailresult->template_body);
				$emailresult->template_body= str_replace("#Lead_Source",$model->lead_source,$emailresult->template_body);
				$emailresult->template_body= str_replace("#Lead_assigned_Date",Date('Y-m-d'),$emailresult->template_body);
				$emailresult->template_body= str_replace("#Lead_ID",$model->id,$emailresult->template_body);
				$email=$user->email;
				
				$emailresult->template_subject = str_replace("#Lead_ID",$model->id,$emailresult->template_subject);
				$emailresult->template_subject = str_replace("#Agent_name",$user2->first_name,$emailresult->template_subject);
                //echo 'temp'.$emailresult->template_body.'        ';
				
				$emailsend=Helper::sendEmail($email,$emailresult->template_body,$emailresult->template_subject);
             //exit;
                if($emailsend)
                {
                    
                    $emailHistory = new EmailHistory();
                    $emailHistory->email = $email;
                    $emailHistory->lead_id = $model->id;
                    $emailHistory->message = $emailresult->template_body;
                    $emailHistory->created_by = Yii::$app->user->id;
                    $emailHistory->created_on = date("Y-m-d H:i:s");
                    $emailHistory->save();
                    $sendActivity = Helper::setCommit($user2->first_name." Has send Email to the ".$user->first_name,'',$model->id);

                }

                 $emailsendtoclient=Helper::sendEmail($model->contact_email,$clientmessage,"YOUR LEAD ID ".$model->id." Has Registered");
             
                if($emailsendtoclient)
                {
                    
                    $emailHistory = new EmailHistory();
                    $emailHistory->email = $model->contact_email;
                    $emailHistory->lead_id = $model->id;
                    $emailHistory->message = $emailresult->template_body;
                    $emailHistory->created_by = Yii::$app->user->id;
                    $emailHistory->created_on = date("Y-m-d H:i:s");
                    $emailHistory->save();
                    $sendActivity = Helper::setCommit($user2->first_name." Has send Email to the ".$model->contact_name.'',$model->id);

                }
               
                 $emailsend=Helper::sendEmail($email,$emailresult->template_body,$emailresult->template_subject);
                if($emailsend)
                {
                    
                    $emailHistory = new EmailHistory();
                    $emailHistory->email = $email;
                    $emailHistory->lead_id = $model->id;
                    $emailHistory->message = $emailresult->template_body;
                    $emailHistory->created_by = Yii::$app->user->id;
                    $emailHistory->created_on = date("Y-m-d H:i:s");
                    $emailHistory->save();
                    $sendActivity = Helper::setCommit($user2->first_name." Has send Email to the ".$user->first_name,'',$model->id);

                }

				//=============end of email template===========================================================

				//=============send sms to agent===============================================================
            	$sms->message = str_replace("#Agent_name",$user->first_name,$sms->message);
            	$sms->message = str_replace("#ID",$model->id,$sms->message);
            	$sms->message = str_replace("#Assigned_User_Name",$user2->first_name,$sms->message);
        	    $sms->message = str_replace("#Client Name",$model->contact_name,$sms->message);
    	        $sms->message = str_replace("#Cell Number",$model->contact_mobile,$sms->message);
	            $sms->message = str_replace("#Purpose",$model->lead_type,$sms->message);
                $sms->message.="182.191.120.180:85/crm/web/leads/view?id=".$model->id;
                $sendSms = Helper::sendSMS($user->phone_no, $sms->message);
                if($sendSms)
                {
                    
                    $smsHistory = new SmsHistory();
                    $smsHistory->number = $user->phone_no;
                    $smsHistory->lead_id = $model->id;
                    $smsHistory->message =  $sms->message;
                    $smsHistory->created_by = Yii::$app->user->id;
                    $smsHistory->created_on = date("Y-m-d H:i:s");
                    $smsHistory->save();

                    $sendActivity = Helper::setCommit($user2->first_name." ".$user2->last_name." Has send sms To ".$user->first_name,'',$model->id);


                }
    
                  $sendActivity = Helper::setCommit($user2->first_name." ".$user2->last_name." Has Assigned The Lead To ".$user->first_name,'',$model->id);
//====================end of sms ==========================================================================================
		}
        else
        {

   
              $clientmessage="Respected ".$model->contact_name." 
Your Query Id ".$model->id." for Purpose ".$model->lead_type." has been registered";
             $sendtoclient=Helper::sendSMS($mobileno,$clientmessage);
             if($sendtoclient)
             {
                $smsHistory = new SmsHistory();
                $smsHistory->number =$mobileno;
                $smsHistory->lead_id = $model->id;
                $smsHistory->message =  $clientmessage;
                $smsHistory->created_by = Yii::$app->user->id;
                $smsHistory->created_on = date("Y-m-d H:i:s");
                $smsHistory->save();
             }

              $emailsendtoclient=Helper::sendEmail($model->contact_email,$clientmessage,"YOUR LEAD ID ".$model->id." Has Registered");
             
                if($emailsendtoclient)
                {
                    
                    $emailHistory = new EmailHistory();
                    $emailHistory->email = $model->contact_email;
                    $emailHistory->lead_id = $model->id;
                    $emailHistory->message = $emailresult->template_body;
                    $emailHistory->created_by = Yii::$app->user->id;
                    $emailHistory->created_on = date("Y-m-d H:i:s");
                    $emailHistory->save();
                    $sendActivity = Helper::setCommit($user2->first_name." Has send Email to the ".$model->contact_name.'',$model->id);

                }
            }
            return $this->redirect(['index?type=myregister']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing leads model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionUpdate($id)
    {
        
        $model = $this->findModel($id);
        $searchModel = new FilesSearch();
        $dataProvider = $searchModel->searchByLead(Yii::$app->request->queryParams,$id);
        $searchActivityModel = new ActivitiesSearch();
        $dataProviderActivity = $searchActivityModel->searchByLead(Yii::$app->request->queryParams,$id);
        /*$model->lead_assign = explode(',', $model->lead_assign);*/

        if ($model->load(Yii::$app->request->post())) {
            /*$model->lead_assign = implode(',', $model->lead_assign);*/


            if($model->save())
            {
            $user=\app\models\User::find()->where(['id'=>Yii::$app->user->id])->one();
            $sendActivity = Helper::setCommit($user->first_name." ".$user->last_name." Has updated The Lead ","",$id);
                return $this->redirect(['update', 'id' => $model->id]);
            }else
            {
                echo "<pre>";
                print_r($model);
                echo "</pre>";
            }
        }

        return $this->render('update', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchActivityModel' => $searchActivityModel,
            'dataProviderActivity' => $dataProviderActivity,
            'model' => $model,
        ]);
    }




    /**
     * Deletes an existing leads model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);
        $model->active =0;
        $model->updated_by = Yii::$app->user->id;
        $model->updated_on = date("Y-m-d H:i:s");
        $model->save();
        $user=\app\models\User::find()->where(['id'=>Yii::$app->user->id])->one();
        $sendActivity = Helper::setCommit($user->first_name." ".$user->last_name." Has deleted The Lead ","",$id);

        return $this->redirect(['index']);
    }


    public function actionValidate()
    {
        if($_GET['id'])
        {
            $model = $this->findModel($_GET['id']);
            // $model->scenario = 'update';
        }
        else
        {
            $model = new Leads();
            // $model->scenario = 'create';
        }
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }



    /**
     * Finds the leads model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return leads the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Leads::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }





}
