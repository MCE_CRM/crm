<?php

namespace app\controllers;

use app\models\ExtraCharges;
use app\models\FeatureList;
use app\models\Files;
use Yii;
use app\models\Leads;
use app\models\Property;
use app\models\PropertySearch;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\Activities;
use app\models\User;

/**
 * PropertyController implements the CRUD actions for Property model.
 */
class PropertyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='property/validate' ||  $route=='property/create-popup' )
                        {
                            return true;
                        }
                        else if (\Yii::$app->user->can($route)) {
                            return true;
                        }


                    }
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Property models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $searchModel = new PropertySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Property model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Property model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Property();

        if ($model->load(Yii::$app->request->post())) {
            $dimension=$model->width."*".$model->length;
            
            $model->dimension=$dimension;
              
            if(!$model->project_id)
            {
                $model->country = $_POST['Property']['country'];
                $model->state = $_POST['Property']['state'];
                $model->city= $_POST['Property']['city'];
                $model->address= $_POST['Property']['address'];
            }
			$model->property_assign = implode(',', $_POST['Property']['property_assign']);//$_POST['Property']['property_assign'];
            $model->total_price = 0;
            $model->created_by = Yii::$app->user->id;
            $model->created_on = date("Y-m-d H:i:s");

            $dyn = implode(', ', $_POST['dynamic_form']);

            $model->additional_form = $dyn;

            
            if($model->save())
            {       $userdata=User::find()->select('first_name,last_name')->where(['id'=>Yii::$app->user->id])->one();
                    $activity = new Activities();
                    $activity->activity = $userdata->first_name.' '.$userdata->last_name.' Created The Property';
                    $activity->created_on = date("Y-m-d H:i:s");
                    $activity->created_by = Yii::$app->user->id;
                    $activity->property_id = $model->id;
                    $activity->save();

                foreach ($_POST['dynamic_form'] as $foo) {

                    foreach ($_POST['feature'][$foo] as $key => $data) {
                        if (!empty($data)) {

                            $feature = FeatureList::find()->where(['name' => $key])->andWhere(['feature_form_id'=>$foo])->one();
                            if (in_array($feature->feature_form_id, $_POST['dynamic_form'])) {
                                $extra = new ExtraCharges();
                                $extra->name = $key;

                                $extra->value = $data;
                                $extra->type = 1;
                                $extra->property_id = $model->id;
                                $extra->feature_form_id = $feature->feature_form_id;

                                $feature = FeatureList::find()->where(['name' => $key])->andWhere(['feature_form_id'=>$foo])->one();

                                $extra->feature_type = $feature->type;

                                $extra->save();

                            }

                        }

                    }

                }



                $total = 0;

                foreach ($_POST['extra_charge'] as $key=>$data)
                {
                    if(!empty($data))
                    {
                        $extra = new ExtraCharges();
                        if (strpos($data, '%') !== false) {
                            $extra->value = $data;

                                $extra->charges = str_replace("%","",$data);;
                                $cal = ($model->price * $extra->charges)/100;
                                $cal = round($cal);
                                $extra->charges = $cal;

                        }else
                        {
                            $extra->charges = $data;
                        }


                        $extra->name = $key;

                        $extra->type = 2;

                        $total = $total + $extra->charges;

                        $extra->property_id = $model->id;
                        $extra->save();

                    }

                }


                $model->extra_charges = $total;


                $total = $total + (int)$model->price;

                $model->total_price = $total;
                $model->status = "Available";

                $model1=new Leads();
                if (Yii::$app->user->can('CSR')) {
                    $model1->lead_assign='';
                }
                else
                {
                $model1->lead_assign=Yii::$app->user->id;
                }
                $model1->save();

                $model->save();

                return $this->redirect('index');
                
            }else
            {
                echo "<pre>";
                print_r($model);
                echo "</pre>";
                exit;
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }





    public function actionCreatePopup()
    {

        $model = new Property();

        if ($model->load(Yii::$app->request->post())) {
            $dimension=$model->width."*".$model->length;

            $model->dimension=$dimension;

            if(!$model->project_id)
            {
                $model->country = $_POST['Property']['country'];
                $model->state = $_POST['Property']['state'];
                $model->city= $_POST['Property']['city'];
                $model->address= $_POST['Property']['address'];
            }
            $model->total_price = 0;
            $model->created_by = Yii::$app->user->id;
            $model->created_on = date("Y-m-d H:i:s");
			$model->property_assign =  implode(',', $_POST['Property']['property_assign']);


            if($model->save())
            {       $userdata=User::find()->select('first_name,last_name')->where(['id'=>Yii::$app->user->id])->one();
                $activity = new Activities();
                $activity->activity = $userdata->first_name.' '.$userdata->last_name.' Created The Property';
                $activity->created_on = date("Y-m-d H:i:s");
                $activity->created_by = Yii::$app->user->id;
                $activity->property_id = $model->id;
                $activity->save();


                $total = 0;

                $model->extra_charges = 0;


                $total = $total + (int)$model->price;

                $model->total_price = $total;
                $model->status = "Available";

                $model1=new Leads();
                if (Yii::$app->user->can('CSR')) {
                    $model1->lead_assign='';
                }
                else
                {
                    $model1->lead_assign=Yii::$app->user->id;
                }
                $model1->save();

                $model->save();

                $resp['name'] = $model->property_title;
                $resp['id'] = $model->id;
                $resp['status'] = 'true';


                $response = Yii::$app->response;
                $response->format = \yii\web\Response::FORMAT_JSON;
                $response->data = $resp;

                return $response;


            }else
            {

                $resp['status'] = 'false';


                $response = Yii::$app->response;
                $response->format = \yii\web\Response::FORMAT_JSON;
                $response->data = $resp;

                return $response;

            }
        }else {
            return $this->renderAjax('open-property', [
                'model' => $model,
            ]);

        }
    }

    /**
     * Updates an existing Property model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $dimension=$model->width."*".$model->length;
            
            $model->dimension=$dimension;

            $dyn = implode(', ', $_POST['dynamic_form']);

            $model->additional_form = $dyn;

            $model->total_price = 0;
            $model->updated_by = Yii::$app->user->id;
            $model->updated_on = date("Y-m-d H:i:s");
			$model->property_assign = implode(',', $_POST['Property']['property_assign']);


            if($model->save())
            {
                     $userdata=User::find()->select('first_name,last_name')->where(['id'=>Yii::$app->user->id])->one();
                    $activity = new Activities();
                    $activity->activity = $userdata->first_name.' '.$userdata->last_name.' Updated The Property';
                    $activity->created_on = date("Y-m-d H:i:s");
                    $activity->created_by = Yii::$app->user->id;
                    $activity->property_id = $model->id;
                    $activity->save();

                    foreach ($_POST['dynamic_form'] as $foo){

                        foreach ($_POST['feature'][$foo] as $key=>$data)
                        {

                            $find = \app\models\ExtraCharges::find()->where(['property_id'=>$model->id])->andWhere(['type'=>'1'])->andWhere(['name'=>$key])->andWhere(['feature_form_id'=>$foo])->one();

                            if($find)
                            {
                                if(in_array($find->feature_form_id, $_POST['dynamic_form'])) {

                                    if (empty($data)) {
                                        $find->delete();
                                    } else {
                                        $find->name = $key;

                                        $find->value = $data;
                                        $find->type = 1;

                                        $feature = FeatureList::find()->where(['name' => $key])->one();

                                        $find->feature_type = $feature->type;
                                        $find->save();
                                    }
                                }
                            }else
                            {
                                if(!empty($data))
                                {
                                    $feature = FeatureList::find()->where(['name'=>$key])->andWhere(['feature_form_id'=>$foo])->one();
                                    if(in_array($feature->feature_form_id, $_POST['dynamic_form']))
                                    {
                                        $extra = new ExtraCharges();
                                        $extra->name = $key;

                                        $extra->value = $data;
                                        $extra->type = 1;
                                        $extra->property_id = $model->id;
                                        $extra->feature_form_id = $feature->feature_form_id;

                                        $feature = FeatureList::find()->where(['name'=>$key])->andWhere(['feature_form_id'=>$foo])->one();

                                        $extra->feature_type =$feature->type;

                                        $extra->save();

                                    }

                                }
                            }



                        }

                    }





                $total = 0;

                foreach ($_POST['extra_charge'] as $key=>$data)
                {
                    $find = \app\models\ExtraCharges::find()->where(['property_id'=>$model->id])->andWhere(['type'=>'2'])->andWhere(['name'=>$key])->one();
                    if($find)
                    {
                        if(empty($data))
                        {
                            $find->delete();
                        }else
                        {
                            if (strpos($data, '%') !== false) {
                                $find->value = $data;

                                $find->charges = str_replace("%","",$data);;
                                $cal = ($model->price * $find->charges)/100;
                                $cal = round($cal);
                                $find->charges = $cal;

                            }else
                            {
                                $find->charges = $data;
                            }


                            $find->name = $key;

                            $find->type = 2;

                            $total = $total + $find->charges;

                            $find->property_id = $model->id;
                            $find->save();
                        }
                    }else {

                        if (!empty($data)) {
                            $extra = new ExtraCharges();
                            if (strpos($data, '%') !== false) {
                                $extra->value = $data;

                                $extra->charges = str_replace("%", "", $data);;
                                $cal = ($model->price * $extra->charges) / 100;
                                $cal = round($cal);
                                $extra->charges = $cal;

                            } else {
                                $extra->charges = $data;
                            }


                            $extra->name = $key;

                            $extra->type = 2;

                            $total = $total + $extra->charges;

                            $extra->property_id = $model->id;
                            $extra->save();


                        }
                    }

                }


                $model->extra_charges = $total;


                $total = $total + (int)$model->price;

                $model->total_price = $total;
                //$model->status = "Available";

                $model->save();

                return $this->redirect('index');

            }else
            {
                echo "<pre>";
                print_r($model);
                echo "</pre>";
                exit;
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Property model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $extraCharges = ExtraCharges::find()->where(['property_id'])->all();
        foreach ($extraCharges as $extra)
        {
            $find = ExtraCharges::findOne($extra->id);
            $find->delete();
        }
        $model->delete();
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the Property model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Property the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Property::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionValidate()
    {
        if($_GET['id'])
        {
            $model = $this->findModel($_GET['id']);
            // $model->scenario = 'update';
        }
        else
        {
            $model = new Property();
            $model->total_price = 0;
            $model->created_by = Yii::$app->user->id;
            $model->created_on = date("Y-m-d H:i:s");
            // $model->scenario = 'create';
        }
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }


}
