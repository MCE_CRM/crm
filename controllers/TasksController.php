<?php

namespace app\controllers;

use app\models\ActivitiesSearch;
use app\models\FilesSearch;
use app\models\TaskStatus;
use Yii;
use app\models\Tasks;
use app\models\TasksSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \DateTime;
use \DateTimeZone;

/**
 * TasksController implements the CRUD actions for Tasks model.
 */
class TasksController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tasks models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $searchModel = new TasksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $task_status = TaskStatus::find()->orderBy([
            'sort_order' => SORT_ASC,
        ])->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'task_status'=>$task_status,
        ]);
    }

    /**
     * Displays a single Tasks model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tasks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tasks();
        $model->created_by = Yii::$app->user->id;
        $model->created_on = date("Y-m-d H:i:s");

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $stringId = 'TASK'.str_pad($model->id, 9, "0", STR_PAD_LEFT);
            $model->task_id=$stringId;

            if(isset($model->expected_start_datetime) && !empty($model->expected_start_datetime))
            {
                $expected_start_datetime = new DateTime($_REQUEST['Tasks']['expected_start_datetime']);
                $model->expected_start_datetime = $expected_start_datetime->getTimestamp();
            }

            if(isset($model->expected_end_datetime) && !empty($model->expected_end_datetime))
            {
                $expected_end_datetime = new DateTime($_REQUEST['Tasks']['expected_end_datetime']);
                $model->expected_end_datetime = $expected_end_datetime->getTimestamp();
            }

            if(isset($model->actual_start_datetime) && !empty($model->actual_start_datetime))
            {
                $actual_start_datetime = new DateTime($_REQUEST['Tasks']['actual_start_datetime']);
                $model->actual_start_datetime = $actual_start_datetime->getTimestamp();
            }

            if(isset($model->actual_end_datetime) && !empty($model->actual_end_datetime))
            {
                $actual_end_datetime = new DateTime($_REQUEST['Tasks']['actual_end_datetime']);
                $model->actual_end_datetime = $actual_end_datetime->getTimestamp();
            }

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            $model->save();


            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tasks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $searchModel = new FilesSearch();
        $dataProvider = $searchModel->searchByTask(Yii::$app->request->queryParams,$id);
        $searchActivityModel = new ActivitiesSearch();
        $dataProviderActivity = $searchActivityModel->searchByTask(Yii::$app->request->queryParams,$id);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchActivityModel' => $searchActivityModel,
            'dataProviderActivity' => $dataProviderActivity,
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tasks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tasks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tasks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tasks::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
