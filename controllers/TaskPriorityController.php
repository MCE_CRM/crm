<?php

namespace app\controllers;

use Yii;
use app\models\TaskPriority;
use app\models\TaskPrioritySearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * TaskPriorityController implements the CRUD actions for TaskPriority model.
 */
class TaskPriorityController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='task-priority/validate')
                        {
                            return true;
                        }
                        else if (\Yii::$app->user->can($route)) {
                            return true;
                        }


                    }
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all TaskPriority models.
     * @return mixed
     */
    public function actionIndex()
    {
        \app\models\DefaultValueModule::upsertDefault('task-priority');

        if(isset($_GET['id']) || $_GET['del_id'])
        {
            if(isset($_GET['id']))
            {
                $id = $_GET['id'];
            }else
            {
                $id = $_GET['del_id'];
            }
            $updateModel = TaskPriority::findOne($id);
            $updateModel->updated_by = Yii::$app->user->id;
            $updateModel->updated_on = date("Y-m-d H:i:s");
            $updateModel->save();
        }



        $searchModel = new TaskPrioritySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);




        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionOrdering()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $myString = $_POST['data'];
            $myArray = explode(',', $myString);
            $i = 1;
            foreach ($myArray as $key=>$value)
            {
                $model = TaskPriority::findOne($value);
                $model->sort_order = $i;
                $model->save();
                $i++;
            }
            return true;
        }else {

            $query = \app\models\TaskPriority::find()->select(['id','priority'])->orderBy('sort_order')->all();
            return $this->render('ordering',
                [
                    'data'=>$query
                ]);

        }
    }

    /**
     * Displays a single TaskPriority model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TaskPriority model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaskPriority();
        $model->created_by = Yii::$app->user->id;
        $model->created_on = date("Y-m-d H:i:s");


        //Get Max Value

        $getMax = TaskPriority::find()->select('sort_order')->orderBy(['sort_order' => SORT_DESC])->one();

        $model->sort_order = $getMax->sort_order + 1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
return true;
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TaskPriority model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->updated_by = Yii::$app->user->id;
        $model->updated_on = date("Y-m-d H:i:s");

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return true;
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TaskPriority model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TaskPriority model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaskPriority the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaskPriority::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionValidate()
    {

        if($_GET['id'])
        {
            $model = $this->findModel($_GET['id']);
            //$model->scenario = 'update';
        }
        else
        {
            $model = new TaskPriority();
            //$model->scenario = 'create';
        }

        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }
}
